/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

var RNFS = require('react-native-fs');
var TimerMixin = require ('react-timer-mixin');
/* var Image = require('react-native-image-progress');*/
/* var ProgressBar = require('react-native-progress/Bar');*/
import  config from './config'
import styles_web from './styles/style_web'
import Language from './Language'
var Utils = require('./utils')


import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  Image,
  View,
  TouchableOpacity,
  ToastAndroid,
  WebView,
  IntentAndroid,
  Dimensions,
  Navigator,
  Linking,
  Platform,
} from 'react-native';

var UMeng = require('./UMeng');

var DEFAULT_URL = config.server_base+config.ad_uri;
var deviceWidth = Dimensions.get('window').width;
var deviceHeight = Dimensions.get('window').height;

var WebPage = React.createClass({
	
	mixins:[TimerMixin],
  timer_delay:null,
  action:'',

  getInitialState:function (){
    return {
      status:'No Page Loaded',
      loaded:false,
      url:'',
      number:1,
      next:'',
      action:'',
    };
  },
  fetchData:function (){

    this.getAdAction();

    // if (Platform.OS === 'android') {
    //   this.getAdAction();
    // } else {
    //   fetch (DEFAULT_URL)
    //     .then ((response) => response.json())
    //     .then((responseData) => {
    //       this.setState({
    //         url: responseData.data.url,
    //         number:responseData.code,
    //         next:'跳过',
    //         action:responseData.data.action,
    //       });
    //     })
    //     .catch((error) => {
    //     });
    // }
    },

  render: function() {
    return (
      <Navigator
        renderScene = {this.renderScene}
        navigator={this.props.navigator}/>
    );
  },
  renderScene(route,navigator){

    var ad = this.state.url;
    if (Platform.OS === 'android') {
      ad= 'file://' + RNFS.PicturesDirectoryPath + '/rctad.png';
    } else {
      ad = 'file://' + RNFS.CachesDirectoryPath + '/rctad.png';
      // if (this.state.number === 201){

      //     return (

      //         <View style = {styles_web.container}>
      //             <TouchableOpacity
      //               onPress = {this.gotoAction}>
      //                  <Image style={[styles_web.webview,{resizeMode: 'stretch'}]} source={{uri:ad}}/>
      //             </TouchableOpacity>
      //             <View
      //               style = {styles_web.welcome}>
      //               <TouchableOpacity
      //               onPress = {this.renderPush}>
      //                 <Text allowFontScaling={false} style = {{color:'#323C4B',fontSize:18,padding:3,}}>{this.state.next}</Text>
      //             </TouchableOpacity>
      //             </View>

      //           </View>
      //     );
      // }
    }
    // if (this.state.number === 201){

          return (

            	<View style = {styles_web.container}>
                  <TouchableOpacity
                    onPress = {()=>{this.gotoAction();UMeng.onEvent('web_01');}}>
                       <Image style={[styles_web.webview,{resizeMode: 'stretch'}]} source={{uri:ad}}/>
                  </TouchableOpacity>
                  <View
                    style = {styles_web.welcome}>
                    <TouchableOpacity
                    onPress = {()=>{this.renderPush();UMeng.onEvent('web_02');}}>
                      <Text allowFontScaling={false} style = {{color:'#323C4B',fontSize:18,padding:3,}}>{this.state.next}</Text>
                  </TouchableOpacity>
                  </View>

            		</View>
          );
    // }
  },

  componentDidMount() {
    // console.log('componentDidMount');
    var navigator = this.props.navigator;
      this.timer_delay = this.setTimeout(() => {
       navigator.replace({
         id: 'TabbarPage',
       });
      }, 5000);
    
    this.fetchData();
  },

  componentWillUnmount(){

	  this.timer_delay && clearTimeout(this.timer_delay);

  },
  

renderPush:function(){
	this.props.navigator.replace({
		id: 'TabbarPage',
	});
	this.timer_delay && clearTimeout(this.timer_delay);
},

gotoAction:function(){
    // console.log('------------------ _skipBrowser' + this.state.action + '--a');
    if (this.state.action === '') {
        // console.log('------------------ _skipBrowser' + this.state.action + '--a`');
      return;
    }
  // console.log('------------------ _skipBrowser' + this.state.action + '--a``');
  // this.props.navigator.replace({
  //   id: 'WebViewPage',
  //   params:{
  //     title:'',
  //     url: this.state.action,
  //     hasGoBack: true,
  //     webviewStyle:styles_web.webview_protocol,
  //     backAddress: 'TabbarPage',
  //     hasProgressBar: false,
  //   }
  // });
  // this.timer_delay && clearTimeout(this.timer_delay);

  if (Platform.OS === 'android') {
    if (this.state.action != null || this.state.action != ''){
        Linking.openURL(this.state.action);
    }

  } else {
    if (this.state.action != null || this.state.action != ''){
      Linking.openURL(this.state.action);

    }
  }

  this.props.navigator.replace({
    id: 'TabbarPage',
  });
  this.timer_delay && clearTimeout(this.timer_delay);
},

getAdAction:function() {
  // console.log('get action');
  if (Platform.OS === 'android') {
    Utils.GetAdAction(
      (action) => {
        this.setState({
              // url: responseData.data.url,
              // number:responseData.code,
              next:'跳过',
              action:action,
            });
        // console.log('action--------------------:' + action);
      }
    );
  } else {
    Utils.GetAdAction((error, events) =>{
        if (error) {
          // console.error(error);/
        } else {
          this.setState({
                // url: responseData.data.url,
                // number:responseData.code,
                next:'跳过',
                action:events[0],
              });
          // console.log('actionIOS--------------------:');
          // console.log(events[0]);
        }
      }
    );
  }
},

});


module.exports = WebPage;
