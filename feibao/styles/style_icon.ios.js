 'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
} from 'react-native';
var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;
var viewLength=deviceHeight*0.35;

// var marginTop1 = viewLength * 0.62;
var fontSize = 70;
var marginTop2 = 10;
var marginTop3 = 0;
var height1 = viewLength/4;
var fontSize1 = 13;
var width = 55;
var height = 30;
var marginTop4 = 20;

  if (viewLength > 221) {
    // marginTop1 = viewLength * 0.60;
    fontSize = 70;
    marginTop2 = 10;
    marginTop3 = 0;
    height1 = viewLength / 4;
    fontSize1 = 9;
    width = 37;
    height = 20;
    marginTop4 = 20;
  } else {
    // marginTop1 = viewLength * 0.62;
    fontSize = 50;
    marginTop2 = 8;
    marginTop3 = 0;//viewLength * 0.08;
    height1 = viewLength / 5;
    fontSize1 = 7;
    width = 37;
    height = 20;
    marginTop4 = 8;
  }

var styles_icon = StyleSheet.create({


  changefilterView:{
    // marginLeft:viewLength/2-27.5,
    // marginTop:marginTop1,//140,
    // marginRight:0,
    width:0,
    height:0,
    // position:'absolute',
    // justifyContent:'center',
    // alignItems: 'center',
  },
  changefilterView2:{
    // marginLeft:0,//viewLength/2-27.5,
    marginTop:height * 20 / 40,//140,
    // marginRight:0,
    top: 0,
    left: 0,
    width:width,//40,//viewLength / 11 * 75 / 40,
    // height:20,
    position:'absolute',
    justifyContent:'center',
    alignItems: 'center',
  },
  changefilterView1:{
    // marginLeft:viewLength/2-27.5,
    // marginTop:165,
    // marginRight:0,
    width:0,//55,
    height:0,//25,
    // position:'absolute',
    // justifyContent:'center',
    // alignItems: 'center',
  },
	imageOff:{
      height:4,
      width:38.5,
      margin:viewLength/15,
    },
    image:{
      alignItems: 'center',
      justifyContent: 'center',
      height:viewLength,
      width:viewLength,
    },
    fontSizeBig:{
      fontSize:fontSize,
    },
    fontSizeSmall:{
      fontSize:13,
    },
    position:{
      position: 'absolute', 
      top: 0, 
      left: 0,
    },
    position1:{
      position: 'absolute', 
      top: 0, 
      left: 0,
    },
    filterImage:{
      height:20,
      width:37,
      marginTop:21,
    },
    filterImage1:{
      height:height,
      width:width,//40,
    },
    background:{
      backgroundColor:'#00000000',
	    height:viewLength,
	    width:viewLength,
      alignItems: 'center',
      justifyContent: 'center',
    },
    font:{
      color: '#E6E6E6',
      textAlign:'center',
    },
    image1:{
      alignItems: 'center',
      justifyContent: 'center',
      height:328,
      width:328,
    },
    view:{
      alignItems:'center',
      justifyContent:'center',
      height:height1,
      marginTop:10,
    },

    adjust1: {
      height:12,
      width:25,
    },
    adjustMarginTop:{
      marginTop: marginTop2,
    },
    adjust2: {
      marginTop:marginTop3,
    },

    adjustFont: {
      fontSize:fontSize1,
    },
    adjust3: {
      marginTop: marginTop4,
    }
  
});
module.exports = styles_icon;

