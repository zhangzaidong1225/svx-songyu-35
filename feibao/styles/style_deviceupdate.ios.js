'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  PixelRatio,
} from 'react-native';

var mwidth =Dimensions.get ('window').width;
var mheight = Dimensions.get('window').height;

var style_deviceupdate = StyleSheet.create({
	container:{
		flex:1,
		backgroundColor:'white',
	},
	view1:{
		marginTop:20,
		backgroundColor:'white',
		//height:100,
		justifyContent:'center',
		alignItems:'center',
		marginBottom:10,
	},
	line:{
		backgroundColor:'#C8C8C8',
		height:1/PixelRatio.get(),
		marginLeft:20,
		marginRight:20
	},
	line1:{
		backgroundColor:'#C8C8C8',
	    height:1 / PixelRatio.get(),
	    width:mwidth-40,
	},
	listview:{
		//backgroundColor:'white',
		//height:mheight * 2/10,
		height:145,
	},
	infoview:{
		height:mheight * 2/10,
		alignItems: 'center',
	},
	infoscroll:{
		height:mheight * 2/10,
		width:mwidth,
		// alignItems: 'center',
	},
	infoview1:{
		alignItems: 'center',
		//marginTop:10,
		//marginBottom:10,
	},

	button:{
		height:40,
		marginLeft:20,
		position:'absolute',
        marginRight:20,
        marginTop:30,
        marginBottom:10,
        justifyContent:'center',
        alignItems: 'center',
	},
	buttonview1:{
		justifyContent:'center',
		alignItems: 'center',
      	 width:mwidth-40,
      	 height:40,
      	 borderColor:'#2AB9F1',
       	borderWidth: 2 / PixelRatio.get(),
      	backgroundColor:'#2AB9F1',
        borderRadius:20,
        marginTop:10,
        marginBottom:10,
	},
	buttonview2:{
		width:mwidth-80,
       backgroundColor:'#2AB9F1',
	},

});

module.exports = style_deviceupdate;