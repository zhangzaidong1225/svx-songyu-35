'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
} from 'react-native';
var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;
var viewLength=123/180*deviceWidth;


var styles_icon = StyleSheet.create({
  changefilterView:{
    marginLeft:83,
    marginTop:138,
    marginRight:0,
    width:55,
    height:25,
    position:'absolute',
    justifyContent:'center',
    alignItems: 'center',
  },
  changefilterView2:{
    // marginLeft:viewLength/2-27.5,
    // marginTop:marginTop1,//140,
    // marginRight:0,
    width:0,
    height:0,
    // position:'absolute',
    // justifyContent:'center',
    // alignItems: 'center',
  },
  changefilterView1:{
    marginLeft:83,
    marginTop:138,
    marginRight:0,
    width:55,
    height:25,
    position:'absolute',
    justifyContent:'center',
    alignItems: 'center',
  },
	imageOff:{
      height:21,
      width:46,
      marginTop:10,
    },
    image:{
      alignItems: 'center',
      justifyContent: 'center',
      height:viewLength,
      width:viewLength,
    },
    fontSizeBig:{
      fontSize:55,
    },
    fontSizeSmall:{
      fontSize:13,
    },
    position:{
      position: 'absolute', 
      top: 0, 
      left: 0,
    },
    position1:{

    },
    filterImage1:{
      height:18/180*deviceWidth,
      width:16/180*deviceWidth,
    },
    filterImage:{
      height:18/180*deviceWidth,
      width:16/180*deviceWidth,
    },
    background:{
      backgroundColor:'#00000000',
	    height:viewLength,
	    width:viewLength,
      alignItems: 'center',
      justifyContent: 'center',
    },
    font:{
      color: '#E6E6E6',
      textAlign:'center',
    },
    image1:{
      alignItems: 'center',
      justifyContent: 'center',
      height:328,
      width:328,
    },
    view:{
      alignItems:'center',
      justifyContent:'center',
      height:viewLength/4,
    },
    adjust1: {
      height:12,
      width:25,
    },
    adjustMarginTop:{
      marginTop: 10,
    },
    adjust2: {
      marginTop:0,
    },

    adjust3: {
      marginTop: 20,
    }

});
module.exports = styles_icon;

