'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  PixelRatio,
} from 'react-native';

var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;

var style_about = StyleSheet.create({

	container:{
		height: deviceHeight,
		width: deviceWidth,
		backgroundColor:'rgb(239,239,239)', // R:239,G:239,B:239
	},

	appIcon: {
		height: deviceHeight * 0.1,
		width: deviceHeight * 0.1,
		marginLeft: (deviceWidth - deviceHeight * 0.1) / 2,
		marginTop: deviceHeight * 0.05,
		borderRadius: deviceHeight * 0.1 / 4,
	},
	appName: {
		fontSize: 14,
		color:'rgb(87, 139, 201)',
		fontFamily: 'Hiragino Sans GB',
		width: deviceWidth,
		textAlign: 'center',
		marginTop: 8,
	},
	appVersion: {
		fontSize: 10,
		color:'rgb(87, 177, 221)',
		fontFamily: 'Hiragino Sans GB',
		width: deviceWidth,
		textAlign: 'center',
		marginTop: 3,
	},

	goodsContainer: {
		height: deviceHeight * 0.08,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
		backgroundColor: 'white',
	},

	goodsName: {
		fontSize: 14,
		color: 'rgb(50, 50, 50)',
		fontFamily: 'Hiragino Sans GB',
		marginLeft:20,
		textAlign: 'center',
	},

	xinfengName: {
		fontSize: 12,
		color: 'rgb(87, 177, 221)',
		fontFamily: 'Hiragino Sans GB',
		marginRight:20,
		textAlign: 'center',
	},

	nextImage: {
		width:7.5,
		height:13,
		marginRight:20,
	},

	splitLines: {
		height: 1/PixelRatio.get(),
		backgroundColor: '#969696',
		width: deviceWidth,
	},

	phoneContainer: {
		position:'absolute',
		top: deviceHeight * 0.80,
		width: deviceWidth,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
	},

	phoneName: {
		fontSize: 10,
		color: 'rgb(127, 127, 127)',
		fontFamily: 'Hiragino Sans GB',
		textAlign: 'center',
	},

	phone: {
		fontSize: 10,
		color: 'rgb(87, 177, 221)',
		fontFamily: 'Hiragino Sans GB',
		textAlign: 'center',
	},

	webContainer: {
		position:'absolute',
		top: deviceHeight * 0.82,
		width: deviceWidth,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
	},
	lianluoCPIcon: {
		position:'absolute',
		top: deviceHeight * 0.88,
		height: deviceWidth * 0.05,
		width: deviceWidth * 0.05 * 138 / 30,
		marginLeft: (deviceWidth - deviceWidth * 0.2) / 2,
	},

	powerContainer: {
		position:'absolute',
		top: deviceHeight * 0.88 + deviceWidth * 0.05,
		width: deviceWidth,
		textAlign: 'center',
		fontSize: 10,
		color:'rgb(186, 188, 189)',
		fontFamily: 'Hiragino Sans GB',
	},

});

module.exports = style_about;