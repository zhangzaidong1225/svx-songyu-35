'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  PixelRatio,
} from 'react-native';
var mwidth=Dimensions.get('window').width;
var mheight=Dimensions.get('window').height;   

var style_setting = StyleSheet.create({
  head:{
    backgroundColor:'#DCDCDC',
    height:mheight/10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  touch_view:{
    height:mheight/10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image:{
    height:27.5,
    width:27.5,
    margin: 30,
  },
  imageNext:{
    height:13,
    width:7.5,
  },
  font2:{
    fontSize:13,
    color:'#969696',
  },
  font1:{
    fontSize:15,
    color:'#323232',
  },
  font1_11:{
    fontSize:13,
    color:'#323232',
  },
  font1_1:{
    fontSize:13,
    color:'rgb(23, 201, 180)',
  },
  font3:{
    fontSize:17,
    color:'rgb(82, 82, 82)',
  },
  font:{
    fontSize:12,
    color:'#969696',
    margin: 10,
  },
  line:{
    //flex:1,
    //borderColor:'#C8C8C8',
    //borderWidth: 1 / PixelRatio.get(),
        //flex:1,
    borderColor:'#C8C8C8',
    borderWidth: 1 / PixelRatio.get(),
    height:1 / PixelRatio.get(),
    width:mwidth-60,
  },
  line2:{
    borderColor:'#C8C8C8',
    borderWidth: 1 / PixelRatio.get(),
    height:1 / PixelRatio.get(),
    width:mwidth-40,
  },
  contentContainer: {
      paddingVertical: 20,
  },
  direction:{
    flex:1,
    flexDirection: 'row',
    justifyContent:'flex-end',
    alignItems: 'center',
  },
  view:{
    flexDirection: 'row',
    alignItems: 'center',
    height:60,
  },
  view1:{
    flex:1,
    height:mheight-mheight/10,
    backgroundColor:'#FFFFFF',
  },
  view2:{
    marginRight:30,
    marginLeft:30,
  },
  view3:{
    flexDirection: 'row',
    justifyContent: 'center',
    margin:15,alignItems: 'center',
  },
  view4:{
    marginLeft:10,
  },
  view5:{
    flexDirection: 'row',
    marginTop:8,
  },
  view6:{
    width:10,
    alignItems: 'center',
  },
  view7:{
    height:15,
    borderColor:'#646464',
    borderWidth: 1 / PixelRatio.get(),
  },
  view8:{
    flex:1,
    height:mheight-mheight/8,
    backgroundColor:'#FFFFFF',
  },
  text:{
    fontSize:10,
    color:'#646464',
  },
  text1:{
    flex:1,
    textAlign: 'center',
  },
  text2:{
    fontSize:13,
    color:'#969696',
  },
  comment:{
    height:56.5,
    width:56.5,
  },
  item:{
    paddingRight:20,
    paddingLeft:20,
    height:mheight-mheight/8.5,
    backgroundColor:'#FFFFFF',
    //backgroundColor: '#F5FCFF',
  },
  item1:{
    flex:1,
    paddingRight:20,
    paddingLeft:20,
    backgroundColor:'#FFFFFF',
   // backgroundColor: '#F5FCFF',
  },
  listView: {
    //marginTop: 20,
    //backgroundColor: '#F5FCFF',   
  },
  second:{
    flex:1,
  },
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor:'#FFFFFF',
    //backgroundColor: '#F5FCFF',
  },

});
module.exports = style_setting;

