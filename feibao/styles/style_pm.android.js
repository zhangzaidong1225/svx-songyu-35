'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,

} from 'react-native';

var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;

var styles_pm = StyleSheet.create({
  container:{
    backgroundColor:'#6EDC82',
    marginLeft:10,
    marginRight:10,
    marginTop:10,
    height:60,
    width:deviceWidth-20,
    flexDirection:'row'
  },
  text:{
    flex:2,
    flexDirection:'column',
    justifyContent:'center',
  },
  text_whether:{
    fontSize:14,
    color:'#E6E6E6',
  },
  text_location:{
    fontSize:10,
    color:'#E6E6E6',
  },
  right:{
    flex:1,
    flexDirection:'row',
    justifyContent:'center'
  },
  pm:{
    alignItems:'center',
    marginRight:10,
    marginTop:15,
  },
  text_pm:{
    fontSize:10,
    color:'#E6E6E6',
  },
  number:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
  },
  text_number:{
    fontSize:35,
    color:'#E6E6E6',
  },
});

module.exports = styles_pm;
