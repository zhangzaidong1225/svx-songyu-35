'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,

} from 'react-native';

var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;

var styles_actionbar = StyleSheet.create({
  container_actionbar:{
    alignItems:'center',
    justifyContent:'center',
  //backgroundColor:'green',
  },
  left:{
    height:70,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    width:deviceWidth,
    //backgroundColor:'red',
  },
  right:{
    //backgroundColor:'green',
    height:70,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    width:100,
    position:'absolute',
    left:deviceWidth*0.7,
  },
  img_update:{
    resizeMode: 'stretch',
    width:17,
    height:17,
    marginLeft:20,
  },
  device_list:{
    flex:1,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    //backgroundColor:'green',
  },
  add:{
    fontSize:17,
    color:'white',
    marginRight:5,
  },

  text:{
    fontSize:17,
    color:'#E6E6E6',
    textAlign:'center',
  },
  view1:{
    flex:1,
    height:deviceHeight*0.9-156-deviceHeight*3/4,
    alignItems:'center',
    justifyContent:'center',
    //backgroundColor:'blue'
   //  justifyContent:'center',
   // // height:deviceHeight*0.38,
   //  height:deviceHeight*0.9-200-deviceHeight*3/32,
  },
  view2:{
    marginBottom:deviceHeight*0.06,
    //height:86+deviceHeight*3/4,
    //height:190,
    // flex:2,
    // top:10,
    // justifyContent:'center',
    // marginTop:20,
  },
  img:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'flex-end',
    marginRight:10,
  },
  img_share:{
    marginLeft:deviceWidth*0.05,
    width:17,
    height:17,
    marginRight:20,
  },
  img_set:{
    marginLeft:10,
    width:17,
    height:17,
    marginRight:20,
  },
});
module.exports = styles_actionbar;

var styles_circle = StyleSheet.create({
  text:{
    fontSize:15,
    color:'#E6E6E6',
    textAlign:'center',
    marginTop:20,
  },
  text_frame:{
    alignItems:'center',
    justifyContent:'center',
    marginTop:25,
  },
  circle_frame:{
    width:250,
    alignItems:'center',
    justifyContent:'center',
  },
  icon_frame:{
    position:'absolute',
    height:70,
    width:70,
    top:0,
    left:200,
  },
  icon:{
    height:35,
    width:35,
    position:'absolute',
  },
});
module.exports = styles_actionbar;
