'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  PixelRatio,
} from 'react-native';

var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;

var styles_feedback = StyleSheet.create({
	view_all:{
		flex:1,
		backgroundColor:'#FFFFFF',	
	},
	container:{
		backgroundColor:'#DCDCDC',
        height:deviceHeight/10,
        flexDirection: 'row',
	},
	header:{
		backgroundColor:'#DCDCDC',
        height:deviceHeight/10,
        flexDirection: 'row',
	},
	first:{
		marginTop:50,
        flexDirection:'row',
		height:40,
		borderWidth:1/PixelRatio.get(),
	},
	selectList:{
		width:70,
		height:90,
        top:90,
        position:'absolute',
        alignItems:'center',
        justifyContent:'center',
        marginLeft:20,
        backgroundColor:'#ffffffff',        
	},
	mailView:{
		height:45,
		alignItems:'center',
		justifyContent:'center',
		flex:1,
		left:90,
		right:20,
        borderWidth:1/PixelRatio.get(),
        borderLeftWidth:0,
        borderColor:'gray',
        top:90,
        position:'absolute',

	},
	mail:{
		paddingLeft:5,
		height:20,
		fontSize:13,
	},
	
	detailView:{
		flex:3,
		width:deviceWidth-40,
		position:'absolute',
		borderWidth:1/PixelRatio.get(),
		top:150,
		marginRight:20,
		marginLeft:20,
		borderColor:'gray',
	},
	detail:{
		height:140,
		borderColor:'gray',
        paddingTop:20,
        paddingLeft:20,
        fontSize:13,

	},
	commitView:{
		height:45,
		position:'absolute',
		marginLeft:20,
		marginRight:20,
        bottom:10,
        justifyContent:'center',
        alignItems: 'center',
	},
	borderView:{
		justifyContent:'center',
		alignItems: 'center',
         position:'absolute',
		 width:deviceWidth-42,
		 height:46,
         borderColor:'rgb(23, 201, 180)',
		 borderWidth: 2 / PixelRatio.get(),
		 borderRadius:25,
         bottom:10,
	},
	commitText:{
		textAlign:'center',
		fontSize:12,
		color:'rgb(23, 201, 180)',
	},
	commitText1:{
		textAlign:'center',
		fontSize:15,
		color:'rgb(242, 38, 19)',
	},	
	textBorder:{
		width:220,
		height:30,
		justifyContent:'center',
  	    alignItems: 'center',
	},
	borderView1:{
		height:40,
		width:deviceWidth-80,
		position:'absolute',
		top:295,
		marginLeft:40,
        flexDirection:'row',
        alignItems:'center',
	},
	image1:{
		height:10,
		width:10,
		marginLeft:20
	},
	text1:{
		fontSize:14,
		marginLeft:10,
	},
	view2:{
		height:185,
		width:deviceWidth-60,
		marginRight:30,
		marginLeft:30,
		marginTop:40,
		borderWidth: 1 / PixelRatio.get(),
		borderRadius:5,
	},
	view3:{
		height:45,
		width:deviceWidth-60,
		flexDirection: 'row',
		borderWidth: 1 / PixelRatio.get(),
		borderLeftWidth:0,
		borderRightWidth:0,
		borderTopWidth:0,
	},
	view4:{
		height:45,
		flex:1,
		justifyContent:'center',
		alignItems: 'center',
	},
	view5:{
		height:19,
		width:16.5,
	},
	view6:{
		height:45,
		justifyContent:'center',
		alignItems: 'center',
	},
	view7:{
		height:40,
		width:0.5,

	},
	view8:{
		height:45,
		flex:5,
		justifyContent:'center',
		alignItems: 'center',
	},
	view1_1:{
		height:140,
		width:deviceWidth-60,
		borderWidth: 1 / PixelRatio.get(),
		borderLeftWidth:0,
		borderRightWidth:0,
		borderBottomWidth:0,
	},
	view1_2:{
		height:140,
		justifyContent:'center',
		alignItems: 'center',
	},

	
	

});

module.exports = styles_feedback;
