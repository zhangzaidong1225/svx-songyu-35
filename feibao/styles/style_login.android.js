'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  PixelRatio,
} from 'react-native';

var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;
var mwidth =Dimensions.get ('window').width;
var mheight = Dimensions.get('window').height-60;
var style_login = StyleSheet.create({
	scrolls:{
		height:deviceHeight,
		width:deviceWidth,
		backgroundColor:'white',
	},
	scroll:{
		height:deviceHeight-260,
		width:deviceWidth,
		//backgroundColor:'white',
		backgroundColor:'red',
	},
	scroll1:{
		height:deviceHeight,
		width:deviceWidth,
	},
	container:{
		backgroundColor:'white',
	},
	title:{
		backgroundColor:'#DCDCDC',
		height:deviceHeight/10,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent:'center'
	},
	headView:{
		height:deviceHeight * 2/10,
		backgroundColor:'#FFFFFF',
		alignItems:'center',
		justifyContent:'center',

	},
	borderView:{
		height:deviceHeight * 1/4,
		backgroundColor:'white',
	},
	borderCenter:{
		marginLeft:30,
		marginRight:30,
		flexDirection:'column',
	},
	borderView1:{
		borderWidth:1/PixelRatio.get(),
		borderColor:'black',
		borderRadius:5,
        width:deviceWidth - 60,
        height:100,
        flexDirection:'column',
	},
	borderView2:{
		flexDirection:'row',
		borderBottomWidth:1/PixelRatio.get(),
		borderColor:'#C8C8C8',
	},
	imageView1:{
		height:50,
		width:45,
		borderColor:'#C8C8C8',
        borderTopLeftRadius:5,
        backgroundColor:'#00000000',
        justifyContent:'center',
        alignItems:'center'
	},
	image1:{
		width:16.5,
		height:19,
		resizeMode:'stretch'
	},
	lineImageView:{
		height:50,
		width:1,
		justifyContent:'center',
		alignItems: 'center',
	},
	line:{
		resizeMode:'stretch',
		height:50,
		width:0.5,
	},

	imageView2:{
		height:50,
		width:45,
		borderColor:'#C8C8C8',
        borderTopLeftRadius:5,
        backgroundColor:'#00000000',
        justifyContent:'center',
        alignItems:'center'
	},
	lockImage:{
		width:16.5,
		height:19,
		resizeMode:'stretch'
	},
	eyeView:{
		height:50,
		width:45,
		borderColor:'#C8C8C8',
        justifyContent:'center',
        alignItems:'center'
	},
	eyeImage:{
		width:19,
		height:10.5,
		resizeMode:'stretch'
	},

	register:{
		height:20,
		width:deviceWidth-60,
		marginTop:10,
		flexDirection:'row',
	},
	registerView:{
		position:'absolute',
		left:10,
	},
	registerText:{
		fontSize:12,
		color:'rgb(23, 201, 180)',
	},
	passwordView:{
		position:'absolute',
		right:10,
	},
	passwordText:{
		fontSize:12,
		color:'#656565',
	},

	view3:{
		height:deviceHeight * 3 /10,
		marginTop:20,
        //backgroundColor:'blue',
		flexDirection:'column',
		backgroundColor:'white',
		alignItems:'center'
	},
	lineView:{
		width:deviceWidth-60,
		height:1/PixelRatio.get(),
		backgroundColor:'#CECECE'	
	},
	listview1:{
		height:60,
		width:deviceWidth-60,
		flexDirection:'row',
		justifyContent:'center',
		alignItems:'center'
	},
	listText1:{
		fontSize:14,
		color:'#B7B7B7'
	},
	nextView:{
		flex:1,
		flexDirection: 'row',
		justifyContent:'flex-end',
		alignItems: 'center',
	},
	nextImage:{
		width:7.5,
		height:13,
		resizeMode:'stretch'
	},

	button:{
		height:40,
		backgroundColor:'white',
		marginBottom:10,
		marginTop:10,
	},

	buttonView:{
		height:40,
		marginLeft:20,
		marginRight:20,
		marginTop:10,
		marginBottom:10,
		justifyContent:'center',
		alignItems: 'center',	
	},
	viewBottom:{
		height:160,
		// marginLeft:20,
		// marginRight:20,
        //justifyContent:'center',
        alignItems: 'center',
        //marginBottom:60,
        backgroundColor:'white',
        //backgroundColor:'blue',
	},
	buttonView2:{
		justifyContent:'center',
		alignItems: 'center',
		width:deviceWidth-40,
		height:40,
		//position:'absolute',
		marginTop:30,
	    borderWidth: 2 / PixelRatio.get(),
	    borderRadius:20,
	    borderColor:'rgb(23, 201, 180)',	
	},

	//登录成功
	view1:{
		flex:1,
		height:mheight-mheight/10,
		backgroundColor:'#FFFFFF',
		marginTop:15
	},
	view1Margin:{
		marginRight:30,
		marginLeft:30,
	},
	view1_2:{
		justifyContent: 'center',
		margin:15,
		alignItems: 'center',
	},

	view1_image:{
		resizeMode: 'stretch',
		height:56.5,
		width:56.5,
	},
	view1_text:{
		marginTop:20,
		alignItems: 'center',
		justifyContent:'center',
	},
	view1_text1:{
		marginTop:8,
		alignItems: 'center',
		justifyContent:'center',
	},
	view1_text2:{
		fontSize:14,
		color:'#646464',
	},


	viewBottom1:{
		justifyContent:'center',
		alignItems: 'center',
		width:mwidth-40,
		height:40,
		marginTop:30,
        borderWidth: 2 / PixelRatio.get(),
        borderRadius:20,
        borderColor:'rgb(242, 38, 19)',	
	},
	viewBottom1_1:{
		justifyContent:'center',
		alignItems: 'center',
		width:mwidth-40,
		height:40,
        borderWidth: 2 / PixelRatio.get(),
        borderRadius:20,
        borderColor:'rgb(23, 201, 180)',	
	},
});


module.exports = style_login;
