'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  PixelRatio,
} from 'react-native';

var mwidth =Dimensions.get ('window').width;
var mheight = Dimensions.get('window').height;


var styles_hometitle = StyleSheet.create({

	hometitle : {
		height:mheight / 10,
		alignItems: 'center',
		justifyContent: 'space-around',//'center',
		width : mwidth,

	},

	firstPageText : {
		fontSize:17,
    	color:'#323232',
	},
	xinfengtitle : {
		height : mheight / 10,
		flexDirection:'row',
		alignItems:'center',
	    justifyContent:'space-around',//'center',
	    width:mwidth,
	},
	touch:{
		height: mheight / 10, 
		width : 17 + 20 * 2, 
		justifyContent:'space-around',//'center',
		alignItems: 'center',
	},
	image : {
		width:17,
		height:17,
		resizeMode:'stretch',
	},

	add:{
	    fontSize:17,
	    color:'white',
	    // marginRight:5,
	 },
	text:{
	    fontSize:17,
	    color:'#E6E6E6',
	    textAlign:'center',
	},
	view1:{
		height:70,
	},
	view2:{
		position:'absolute',
		left:20,
	},
	view3:{
		height: 70, 
		width: 100, 
		justifyContent:'center',
	},
	img1:{
		width:17,
		height:17,
		resizeMode:'stretch',
	},
	view4:{
		//height: 100, 
		height: 70, 
		justifyContent:'center',
	},
	view5:{
		//height: 100,
		height: 70,  
		justifyContent:'center',
	},
	view6:{
		width:17,
		height:17,
	},
	view7:{
		//height: 100,
		height: 70,   
		justifyContent:'center',
	},
	view8:{
		//height: 100,
		height: 70,  
		justifyContent:'center',
	},
});
module.exports = styles_hometitle;