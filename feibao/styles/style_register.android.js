'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  PixelRatio,
} from 'react-native';

var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;

var style_register = StyleSheet.create({
	sel2_view1:{
		height:40,
		width:deviceWidth-80,
		position:'absolute',
		top:240,
		marginLeft:30,
        flexDirection:'row',
        alignItems:'center',
	},
	borderCenter:{
		marginLeft:30,
		marginRight:30,
		flexDirection:'column',
	},
	sel2_view2:{
		height:40,
		backgroundColor:'#ff000000',
		width:40,
		left:0,
		position:'absolute',
	},
	sel2_view3:{
		flexDirection:'row',
		alignItems:'center',
		height:40,
	},

	sel2_view3_img:{
		height:10,
		width:10,
		resizeMode:'stretch',
	},
	sel2_view3_text:{
		fontSize:13,
		marginLeft:10,
	},
	sel2_view3_text1:{
		fontSize:13,
		marginLeft:10,
	},
	sel2_view3_text2:{
		fontSize:13,
		marginLeft:10,
		color:'rgb(23, 201, 180)',
	},
	sel2_view3_text3:{
		fontSize:13,
		marginLeft:10,
	},

	//大布局
	container:{
		backgroundColor:'#FFFFFF',
		flex:1,
	},

	borderview1:{
		height:135,
		width:deviceWidth-60,
		marginRight:30,
		marginLeft:30,
		marginTop:40,
		borderWidth: 1 / PixelRatio.get(),
		borderRadius:5,

	},
	privilege_view:{
		height:deviceHeight-deviceHeight/10-40,
		width:deviceWidth,
		justifyContent:'center',
		alignItems: 'center',
	},
	borderview2:{
		height:45,
		width:deviceWidth-60,
		flexDirection: 'row',
		borderWidth: 1 / PixelRatio.get(),
		borderLeftWidth:0,
		borderRightWidth:0,
		borderTopWidth:0,
	},
	borderview3:{
		height:45,
		width:45,
		justifyContent:'center',
		alignItems: 'center',
	},
	borderview3_img:{
		resizeMode:'stretch',
		height:19,
		width:16.5,
	},
	borderview4:{
		height:45,
		width:1,
		justifyContent:'center',
		alignItems: 'center',
	},
	borderview4_img:{
		height:40,
		width:0.5,
	},
	borderview5:{
		height:45,
		flex:1,
		paddingLeft:5,
		// justifyContent:'center',
		// alignItems: 'center',
	},
	borderview6:{
		height:45,
		width:75,
		justifyContent:'center',
		alignItems: 'center',
	},
	borderview6_1:{
		height:26,
		width:65,
		backgroundColor:'rgb(23, 201, 180)',
		justifyContent:'center',
		alignItems: 'center',
		borderWidth: 2 / PixelRatio.get(),
		borderRadius:5,
		borderColor:'rgb(23, 201, 180)'
	},
	borderview6_text:{
		fontSize:12,
		textAlign:'center',
		color:'white',
	},

	view2:{
		height:45,
		width:deviceWidth-60,
		flexDirection: 'row',
		borderWidth: 1 / PixelRatio.get(),
		borderLeftWidth:0,
		borderRightWidth:0,
		borderTopWidth:0,
	},
	view2_1:{
		height:45,
		width:45,
		justifyContent:'center',
		alignItems: 'center',
	},
	view2_1_img:{
		resizeMode:'stretch',
		height:11.5,
		width:18.5,
	},

	view2_2:{
		height:45,
		width:1,
		justifyContent:'center',
		alignItems: 'center',
	},
	view2_2_img:{
		resizeMode:'stretch',
		height:40,
		width:0.5,
	},
	view2_3:{
		height:45,
		flex:1,
		paddingLeft:5,
		// justifyContent:'center',
		// alignItems: 'center',
	},
	view2_4_img:{
		resizeMode:'stretch',
		height:19,
		width:19,
		// justifyContent:'center',
		// alignItems: 'center',
	},
	view3:{
		height:45,
		width:deviceWidth-60,
		flexDirection: 'row',
		borderWidth: 1 / PixelRatio.get(),
		borderLeftWidth:0,
		borderRightWidth:0,
		borderTopWidth:0,
		borderBottomWidth:0,

	},
	view3_1:{
		height:45,
		width:45,
		justifyContent:'center',
		alignItems: 'center',

	},
	view3_1_img:{
		resizeMode:'stretch',
		height:20,
		width:14,
	},
	view3_2:{
		height:45,
		width:1,
		justifyContent:'center',
		alignItems: 'center',
	},

	view3_2_img:{
		resizeMode:'stretch',
		height:40,
		width:0.5,
	},
	view3_3:{
		height:45,
		flex:1,
		paddingLeft:5,
		color:'black',
		// alignItems: 'center',
		// justifyContent:'center',
	},
	view3_4:{
		height:45,
		width:45,
		justifyContent:'center',
		alignItems: 'center',
	},
	view3_4_img:{
		resizeMode:'stretch',
		height:10.5,
		width:19,
	},

	view4:{
		justifyContent:'center',
		alignItems: 'center',
		width:deviceWidth-40,
		height:40,
       borderWidth: 2 / PixelRatio.get(),
       borderRadius:20,
       borderColor:'rgb(23, 201, 180)',
	},



});

module.exports = style_register;