'use strict'

import React, { Component } from 'react';
import {
  StyleSheet,
  Dimensions,
  PixelRatio,
} from 'react-native';
var mwidth =Dimensions.get ('window').width;
var mheight = Dimensions.get('window').height;
var styles_filter = StyleSheet.create({

  // view:{
  //   width:mwidth,
  //   height:mheight,
  //   backgroundColor:'#11aacc',
  // },
 text1:{
  textAlign:'center',
  fontSize:15,
  color:'#2DBAF1',
  //color:'rgb(23, 201, 180)',
 },


  // line:{
  //   flex:1,
  //   marginLeft:20,
  //   marginRight:20,
  //   borderColor:'C8C8C8',
  //   borderWidth: 1 / PixelRatio.get(),
  // },
  // view1:{
  //   justifyContent:'center',
  //   alignItems: 'center',
  //   width:200,
  //   height:200,
  //   marginTop:10,
  // },
  // view2:{
  //   justifyContent:'center',
  //   alignItems: 'center',
  //   height:40,
  // },
  uview4:{
    height:40,
    marginLeft:20,
    marginRight:20,
    marginTop:30,
    marginBottom:30,
  },
  listView: {
   
  },
  uview5:{
    justifyContent:'center',
    alignItems: 'center',
    width:120,
    height:40,
    borderColor:'#2DBAF1',
    //borderColor:'rgb(23, 201, 180)',
    borderWidth: 2 / PixelRatio.get(),
    borderRadius:25,
  },
  uview6:{
    height:40,
    // margin:30,
    marginLeft:20,
    marginRight:20,
    marginTop:30,
    marginBottom:30,
  },
  uview7:{
    width:mwidth-50,
    height:45,
    justifyContent:'center',
    alignItems: 'center',  
  },
  // view3:{
  //   height:mheight*0.45,
  //   width:mwidth,
  //   flexDirection: 'row',
  //   alignItems: 'center',
  // },
  // view4:{
  //   height:mheight*0.4,
  //   width:mwidth*0.6,
  //   justifyContent:'center',
  //   alignItems: 'flex-start',
  //   paddingLeft: mwidth*0.1,
  // },
  // view5:{
  //   height:mheight*0.4,
  //   width:mwidth*0.4,
  //   justifyContent:'center',
  // },
  // view6:{
  //   borderColor:'C8C8C8',
  //   // borderWidth: 1 / PixelRatio.get(),
  //   marginTop:20,
  //   marginLeft:40,
  //   marginRight:40,
  // },
  // textview:{
  //   height:20,
  //   flexDirection: 'row',
  //   alignItems: 'center',
  // },
  bottomView:{
    height:120,
    width:mwidth,
    flexDirection: 'row',
    justifyContent:'center',
    position:'absolute',
    bottom:20,
  },
  textSelectedDevice:{
    fontSize:16,
    left:mwidth*0.05,
    color:'#3BB4B4'
  },
  textUnselectedDevice:{
    fontSize:14,
    left:mwidth*0.05,
    color:'#969696',
  },
  textSelectedMark:{
    // alignSelf:'flex-end',

    right:mwidth*0.05,
    marginVertical:3,
    fontSize:12,
    color:'#969696',
  },

  // view10:{
  //   height:mheight*0.45,
  //   width:mwidth,
  //   alignItems:'center',
  // },
  /////////////
  textBrandName:{
    textAlign:'left',
    fontSize:13,
    color:'#323232',
  },
  textFilterStateName:{
    textAlign:'center',
    fontSize:15,
    color:'#506E82',
  },
  textFilterState:{
    textAlign:'left',
    fontSize:16,
  },
  textTotalTimeName:{
    marginTop:5,
    height:20,
    textAlign:'center',
    fontSize:12,
    color:'#969696',
  },
  textCheckDevice:{
    textAlign:'center',
    fontSize:12,
    color:'#3BB4B4',
  },

  viewFilter: {
    height:Dimensions.get('window').height*0.45,//mheight*0.45,
    width:mwidth,
    flexDirection: 'row',
    alignItems: 'center',
  },
  viewFilterText: {
    height:Dimensions.get('window').height*0.35,//mheight*0.35,
    width:mwidth*0.6,
    justifyContent:'center',
    alignItems: 'flex-start',
    paddingLeft: mwidth*0.1,
    paddingTop:mwidth*0.1,
  },
  viewTextInfo: {
    flexDirection:'row',
    paddingTop:20,
  },
  viewColLine: {
    width:6/PixelRatio.get(),
    height: 40,
    backgroundColor:'#506E82',
  },
  viewColInfo: {
    height:40,
    paddingLeft:10,
  },
  viewFilterState: {
    height:20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  viewCheck: {
    height:Dimensions.get('window').height*0.15,//mheight*0.15,
    width:mwidth*0.5,
    alignItems:'center',
    paddingTop:mwidth*0.1
  },
  imageCheckDevice:{
    width:Dimensions.get('window').height*0.08,//mheight*0.08,
    height:Dimensions.get('window').height*0.08,//mheight*0.08,
  },
  viewFilterGui: {
    height:Dimensions.get('window').height*0.35,//mheight*0.35,
    width:mwidth*0.45, 
    justifyContent:'center',
  },
  viewGuiBack: {
    marginLeft:10,
    marginTop:14,
    marginRight:10,
    width:105,
    height:136,
    position:'absolute',
    justifyContent:'center',
    alignItems: 'center',
  },
  viewGuiStatus: {
    marginLeft:10,
    marginTop:14,
    marginRight:10,
    width:105,
    position:'absolute',
    justifyContent:'center',
    alignItems: 'center',
  },
  imageGui:{
    width:125,
    height:178,
  },

  //ListItem
  viewItem: {
    marginLeft:20,
    marginRight:20,
  },
  viewItemLine: {
    backgroundColor:'#969696',
    height:1/PixelRatio.get(),
    marginLeft:10,
    marginRight:10,
  },
  viewItemRow: {
    flexDirection: 'row',
    justifyContent: 'space-between', 
    marginLeft:0,
    marginRight:0,
  },
  viewRowHS: {
    height:40,
  },
  viewRowUHS: {
    height:26,
  },
  viewRowMS:{
    margin:10,
  },
  viewRowUPS: {
    margin:6,
  },
  viewEndLine: {
    backgroundColor:'#969696',
    height:1/PixelRatio.get(),
    marginLeft:30,
    marginRight:30,
  },
  viewFilters: {
    flex:1,
    backgroundColor:'#ffffff',
  },
  viewGuiShow: {
    marginLeft: 23,
    marginTop:46,

    width:74,
    height:74,
    position:'absolute',
    justifyContent:'center',
    alignItems: 'center',
  },
  viewGuiHide: {
    marginLeft:10,
    marginTop:14,
    marginRight:10,
    width:0,
    height:0,
    position:'absolute',
    justifyContent:'center',
    alignItems: 'center',
  },

});
module.exports = styles_filter;