'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  PixelRatio,
} from 'react-native';

var mwidth =Dimensions.get ('window').width;
var mheight = Dimensions.get('window').height;
var styles_filter = StyleSheet.create({
 text1:{
  fontSize:15,
  color:'#2DBAF1',
 },
  uview4:{
    height:mheight * 68 / 1280,
    width:mwidth * 260 / 720,
  },
  listView: {
    marginTop:mheight * 60 / 1280,
  },
  uview5:{
    width:mwidth * 260 / 720,
    height:mheight * 68 / 1280,
    borderColor:'#2DBAF1',
    borderWidth: 2/PixelRatio.get(),
    borderRadius:15,
  },
  uview6:{
    height:mheight * 68 / 1280,
    width:mwidth * 260 / 720,
  },
  uview7:{
    width:mwidth * 260 / 720,
    height:mheight * 68 / 1280,
    justifyContent:'center',
    alignItems: 'center',
  },
  bottomView:{
    height:mheight * 68 / 1280,
    width:mwidth - 70,
    marginRight:35,
    marginLeft:35,
    position:'absolute',
    top: mheight * 0.85,
    flexDirection: 'row',
    justifyContent:'space-between',
  },
  textSelectedDevice:{
    fontSize:17,
    left:mwidth*0.05,
    color:'#3BB4B4'
  },
  textUnselectedDevice:{
    fontSize:15,
    left:mwidth*0.05,
    color:'#969696',
  },
  textSelectedMark:{
    right:mwidth*0.05,
    marginVertical:3,
    fontSize:13,
    color:'#969696',
  },
  textBrandName:{
    textAlign:'left',
    fontSize:15,
    color:'#323232',
  },
  textFilterStateName:{
    textAlign:'center',
    fontSize:12,
    color:'#506E82',
  },
  textFilterState:{
    textAlign:'left',
    fontSize:13,
  },
  textTotalTimeName:{
    textAlign:'center',
    fontSize:10,
    color:'#969696',
  },
  textCheckDevice:{
    textAlign:'center',
    fontSize:11,
    color:'#3BB4B4',
  },
 
  viewFilter: {
    width:mwidth,
    flexDirection: 'row',
    alignItems: 'center',
  }, 
  viewFilterText: {
    justifyContent:'center',
    alignItems: 'flex-start',
    paddingLeft: mwidth * 60 / 720,
    paddingTop:mheight * 180 / 1280,
  },
  viewTextInfo: {
    flexDirection:'row',
    paddingTop:mheight * 36 / 1280,
  },
  viewColLine: {
    width: 6/PixelRatio.get(),
    height: 30,
    backgroundColor:'#506E82',
  },
  viewColInfo: {
    paddingLeft:mwidth * 20 / 720,
  },
  viewFilterState: {
    height:18,
    flexDirection: 'row',
  },
  viewCheck: {
    alignItems:'center',
    paddingTop:mheight * 105 / 1280,
    paddingLeft:mwidth * 140 / 720,
  },
  imageCheckDevice:{
    width:mheight * 85 / 1280,
    height:mheight * 85 / 1280,
  },
  viewFilterGui: {
    position: 'absolute',
    right: mwidth * 66 / 720,
    justifyContent:'center',
    top : mheight * 120 / 1280,
  },
  viewGuiBack: {
    right: mwidth * 21 / 720,
    top: mwidth * 21 / 720,
    width:mwidth * 169 / 720,
    height:mwidth / 720 * (268 - 21 - 41),
    position:'absolute',
    justifyContent:'center',
    alignItems: 'center',
  },
  viewGuiStatus: {
    right:mwidth * 21 / 720,
    width:mwidth * 169 / 720,
    top: mwidth * 21 / 720,
    position:'absolute',
    justifyContent:'center',
    alignItems: 'center',
  },
  imageGui:{
    width:mwidth * 211 / 720,
    height:mwidth * 268  / 720,
    backgroundColor:'#00000000'
  },
  viewItem: {
    marginLeft:10,
    marginRight:10,
  },
  viewItemLine: {
    backgroundColor:'#969696',
    height:1/PixelRatio.get(),
    marginLeft:10,
    marginRight:10,
  },
  viewItemRow: {
    flexDirection: 'row',
    justifyContent: 'space-between', 
    alignItems: 'center',
    marginLeft:0,
    marginRight:0,
  },
  viewRowHS: {
    height: mheight * 105 / 1280,
  },
  viewRowUHS: {
    height:mheight * 71 / 1280,
  },
  viewEndLine: {
    backgroundColor:'#969696',
    height:1/PixelRatio.get(),
    marginLeft:20,
    marginRight:20,
  },
  viewFilters: {
    flex:1,
    backgroundColor:'#ffffff',
  },
  viewGuiShow: {
    right: mwidth * 30 / 720,
    top: mheight * 59 / 1280,
    width: mwidth * 151 / 720,
    height: mwidth * 150 / 720,
    position:'absolute',
    justifyContent:'center',
    alignItems: 'center',
  },
  viewGuiHide: {
    marginLeft:10,
    marginTop:14,
    marginRight:10,
    width:0,
    height:0,
    position:'absolute',
    justifyContent:'center',
    alignItems: 'center',
  },

});
module.exports = styles_filter;