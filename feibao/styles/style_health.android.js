'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  PixelRatio,
} from 'react-native';

var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;


var styles_health = StyleSheet.create({
	scrollView:{
		height:deviceHeight-100,
		width:deviceWidth,	
	},
	container:{
		backgroundColor:'#DCDCDC',
        height:deviceHeight/10,
        flexDirection: 'row',
        alignItems: 'center',
	},
	header:{
		backgroundColor:'#DCDCDC',
        height:deviceHeight/10,
        flexDirection: 'row',
        alignItems: 'center',
	},
	first:{
		marginTop:50,
        flexDirection:'row',
		height:40,
		borderWidth:1/PixelRatio.get(),
	},
	selectList:{
		width:50,
		height:45,
        top:110,
        position:'absolute',
        alignItems:'center',
        justifyContent:'center',
        marginLeft:40,
        borderColor:'#CCCCCC',
        borderWidth:1/PixelRatio.get(),
        borderRightWidth:0,
        borderTopWidth:0,
        borderLeftWidth:0,
	},
	mailView:{
		height:45,
		alignItems:'center',
		justifyContent:'center',
		flex:1,
		left:90,
		right:40,
        borderWidth:1/PixelRatio.get(),
        //borderLeftWidth:0,
        borderRightWidth:0,
        borderTopWidth:0,
        borderColor:'#CCCCCC',
        top:110,
        position:'absolute',
	},
	mail:{
		paddingLeft:70,
	},
	
	detailView:{
		flex:3,
		position:'absolute',
		borderWidth:0,
		top:155,
		marginRight:40,
		marginLeft:40,
	},
	detail:{
		height:140,
		borderColor:'#CCCCCC',
        borderWidth:1,
        paddingTop:10,
        paddingLeft:10,
	},
	commitView:{
		height:40,
		position:'absolute',
		marginLeft:20,
		marginRight:20,
        bottom:10,
        justifyContent:'center',
        alignItems: 'center',
	},
	borderView:{
		justifyContent:'center',
		alignItems: 'center',
         position:'absolute',
		 width:deviceWidth-40,
		 height:40,
         borderColor:'rgb(23, 201, 180)',
		 borderWidth: 2 / PixelRatio.get(),
		 borderRadius:20,
         bottom:10,
	},
	commitText:{
		textAlign:'center',
		fontSize:18,
		color:'#323232',
	},
	textBorder:{
		width:300,
		backgroundColor:'#FFFFFF',
	},
});
module.exports = styles_health;
