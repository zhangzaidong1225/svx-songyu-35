'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  PixelRatio,
} from 'react-native';
var mwidth=Dimensions.get('window').width;
var mheight=Dimensions.get('window').height;   

var style_update = StyleSheet.create({
  uhead:{
    flex:1,
    backgroundColor:'#DCDCDC',
    height:mheight/10,
    width:mwidth,
    flexDirection: 'row',
  },
  uview:{
    flexDirection: 'row',
    justifyContent:'flex-end',
    alignItems: 'center',
  },
  uview1:{
    flex:1,
    height:mheight*0.9/3*2,
    width:mwidth,alignItems: 'center',
  },
  uview2:{
    flex:1,
    justifyContent:'center',
    alignItems: 'center',
  },
  uview3:{
   flex:1,
   height:mheight*0.9/3,
   width:mwidth,
  },
  uview4:{
    height:45,
    position:'absolute',
    marginLeft:25,
    marginRight:25,
    bottom:60,
    justifyContent:'center',
    alignItems: 'center',
  },
  uview5:{
    justifyContent:'center',
    alignItems: 'center',
    position:'absolute',
    width:mwidth-51,
    height:46,
    borderColor:'rgb(23, 201, 180)',
    borderWidth: 2 / PixelRatio.get(),
    borderRadius:25,
    bottom:10,
  },
  uview6:{
    flex:1,
    width:mwidth,
  },
  uview7:{
  	width:mwidth-80,
  	height:40,
  	justifyContent:'center',
  	alignItems: 'center',  
  },
  ubackgroundColor:{
	 backgroundColor:'#FFFFFF',
  },
  uline1:{
    borderColor:'#C8C8C8',
    borderWidth: 1 / PixelRatio.get(),
    margin:20,
  },
  utext:{
    color:'#323C4B',
    fontSize:18,
    padding:3,
  },

});
module.exports = style_update;

