'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  PixelRatio,
} from 'react-native';

var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;

var styles_modal1 = StyleSheet.create({
    modal_container:{
      backgroundColor:'white',
      alignItems: 'center',
      height: 300,
      marginTop:-50,
    },
    modal_view:{
      backgroundColor:'white',
      height:300,
      width:deviceWidth-40,
      position:'absolute',
      right:20,
      left:20
    },
    modal_head:{
      //backgroundColor:'#AA33CC',
      flexDirection:'row',
      backgroundColor:'white',
      position:'absolute',
      width:deviceWidth-40,
      height:40,
      alignItems:'center',
      justifyContent:'center'
    },
    modal_headtext:{
      color: "black",
      fontSize: 15,
      textAlign:'center',
    },
    modal_line:{
      position:'absolute',
      backgroundColor:'#D9D9D9',
      marginTop:45,
      height:1,
      width:deviceWidth-40
    },
    modal_borderview:{
      position:'absolute',
      marginBottom:10,
      marginTop:250,
      height:40,
      justifyContent:'center',
      alignItems:'center',
    },
    modal_borderview2:{
      justifyContent:'center',
      alignItems: 'center',
      position:'absolute',
        width:deviceWidth-40,
        height:40,
        borderColor:'rgb(23, 201, 180)',
       // borderColor:'#33cc66',
        borderWidth: 2 / PixelRatio.get(),
        borderRadius:20,bottom:0,
    },
    modal_borderview3:{
      width:deviceWidth-40,
      backgroundColor:'transparent'
    },
    modal_listview:{
      height:180,
      marginTop:46,
      marginBottom:55,
      backgroundColor:'white'
    },

    modal2_container:{
    backgroundColor:'white',
    alignItems:'center',
    height:150,
    width:deviceWidth-20
    },
    modal2_view:{
      backgroundColor:'white',
      height:150,
      width:deviceWidth-20,
      position:'absolute',
    },
    modal2_view1:{
      backgroundColor:'white',
      position:'absolute',
      width:deviceWidth - 20,
      height:60,
      alignItems:'center',
      justifyContent:'center'
    },
    modal2_text:{
      color: "black",
      fontSize: 18,
      textAlign:'center',
    },

    modal2_view2:{
      flexDirection:'row',
      marginLeft:10,
      marginRight:10,
      position:'absolute',
      marginBottom:10,
      marginTop:70,
      height:40,
      justifyContent:'center',
      alignItems:'center',
    },

    modal2_close:{
      justifyContent:'center',
      alignItems: 'center',
      position:'absolute',
      width:100,
      height:40,
      borderColor:'#2AB9F1',
      borderWidth: 2 / PixelRatio.get(),
      borderRadius:20,
      bottom:0,
    },
    modal2_closetext:{
      width:50,
      backgroundColor:'transparent'
    },
    
    modal2_touch:{
      width:100, 
      backgroundColor:'transparent',
      alignItems:'center',
      justifyContent:'center'
    },

    modal2_wait:{
      left:110,
      justifyContent:'center',
      alignItems: 'center',
      position:'absolute',
        width:100,
        height:40,
        borderColor:'#2AB9F1',
        borderWidth: 2 / PixelRatio.get(),
        borderRadius:20,
        bottom:0,
    },
    modal2_cancel:{
      left:220,
      justifyContent:'center',
      alignItems: 'center',
      position:'absolute',
        width:100,
        height:40,
        borderColor:'#2AB9F1',
        borderWidth: 2 / PixelRatio.get(),
        borderRadius:20,bottom:0,
    },
});

module.exports = styles_modal1;
