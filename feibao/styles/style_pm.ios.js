'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,

} from 'react-native';

var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;

var styles_pm = StyleSheet.create({
  container:{
    //backgroundColor:'red',
    backgroundColor:'#6EDC82',
    marginTop:10,
    marginRight:10,
    marginLeft:10,
    height:deviceHeight * 120 / 1280,//60,
    flexDirection:'row'
  },
  text:{
    flex:2,
    flexDirection:'column',
    justifyContent:'center',
  },
  text_whether:{
    fontSize:15,
    color:'#E6E6E6',

  },
  text_location:{
    fontSize:13,
    color:'#E6E6E6',
  },
  right:{
    flex:1,
    flexDirection:'row',
    justifyContent:'center'
  },
  pm:{
    alignItems:'center',
    marginRight:20,
    marginTop:15,
  },
  text_pm:{
    fontSize:13,
    color:'#E6E6E6',
    marginRight:10,
  },
  number:{
    // flex:1,
    alignItems:'center',
    justifyContent:'center',
    marginRight:30,
  },
  text_number:{
    fontSize:37,
    color:'#E6E6E6',
  },
});

module.exports = styles_pm;
