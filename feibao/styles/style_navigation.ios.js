'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  PixelRatio,
} from 'react-native';

var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;

var styles_navigation = StyleSheet.create({

	container:{
		backgroundColor:'white',
		alignItems:'center',
		height:150,
		width:deviceWidth-40,
	},
	view1:{
		backgroundColor:'white',
		height:150,
		width:deviceWidth-40,
		position:'absolute',
	},
	view2:{
		backgroundColor:'white',
		position:'absolute',
		width:deviceWidth - 40
		,height:60,
		marginTop:20,
		alignItems:'center',
		justifyContent:'center',
	},
	text1:{
		color: "rgb(144,144,144)",
		fontSize: 18,
		textAlign:'center',
	},
	text2:{
		color: "rgb(144,144,144)",
		fontSize: 18,
		textAlign:'center',
	},
	view3:{
		backgroundColor:'white',
		alignItems:'center',
		justifyContent:'center',
		width:deviceWidth-40,
		height:40,
		marginTop:100,
	},
	view4:{
		justifyContent:'center',
		alignItems: 'center',
		width:100, 
		height:40,
        borderColor:'rgb(23, 201, 180)',
        borderWidth: 5 / PixelRatio.get(),
        borderRadius:20,
	},
	view5:{
		width:50,
		backgroundColor:'transparent'
	},
	text3:{
		color:'rgb(23, 201, 180)',
		textAlign:'center',
		fontSize:16,
	},
});	

module.exports = styles_navigation;

