'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,

} from 'react-native';

var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;

var styles_actionbar = StyleSheet.create({
  container_actionbar:{
    alignItems:'center',
    justifyContent:'center',
  },
  left:{
    height:70,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    //backgroundColor:'green',
  },
  right:{
    height:70,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    width:100,
    position:'absolute',
    left:deviceWidth*0.7,
    //backgroundColor:'red',
  },
  img_update:{
    marginLeft:20,
    resizeMode: 'stretch',
    width:17,
    height:17,
    marginRight:10,
  },
  device_list:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    //backgroundColor:'blue',
  },
  add:{
    fontSize:17,
    color:'white',
    marginRight:5,
  },
  text:{
    fontSize:17,

    textAlign:'center',
  },
  view1:{
    //height:deviceHeight-315,
    flex:1,
    alignItems:'center',
    justifyContent:'center',
  },
  view2:{
    height:190,
    //position:'absolute',
   // bottom:48,
    //justifyContent:'center',
    //backgroundColor:'red',
  },
  img:{
    alignItems:'flex-end',
    //backgroundColor:'blue',
  },
  img_share:{
    marginLeft:deviceWidth*0.05,
    width:17,
    height:17,
    marginRight:20,
  },
  img_set:{
    marginLeft:10,
    width:17,
    height:17,
    marginRight:20,
  },
});
module.exports = styles_actionbar;

var styles_circle = StyleSheet.create({
  text:{
    fontSize:15,
    color:'#E6E6E6',
    textAlign:'center',
    marginTop:20,
  },
  text_frame:{
    alignItems:'center',
    justifyContent:'center',
    marginTop:25,
  },
  circle_frame:{
    width:250,
    alignItems:'center',
    justifyContent:'center',
  },
  icon_frame:{
    position:'absolute',
    height:70,
    width:70,
    top:0,
    left:200,
  },
  icon:{
    height:35,
    width:35,
    position:'absolute',
  },
});
module.exports = styles_actionbar;
