/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

import  styles from './styles/style_setting';
//var AsyncStorage = require('./AsyncStorage');

import Title from './title'
import Language from './Language'

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  Dimensions,
  PixelRatio,
  TouchableOpacity,
  ListView,
  ToastAndroid,
   AsyncStorage,
} from 'react-native';

var city_data = require('./city_data');
var async = require('async');
 
var mwidth=Dimensions.get('window').width;
var mheight=Dimensions.get('window').height;


var mcity;
var mcityid;
var mprovinceid;

var UMeng = require('./UMeng');
var City= React.createClass({  
    getInitialState() {
        return {
           dataSource: new ListView.DataSource({  
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      loaded: false,
            value:mheight,
            m:mwidth,
            provinceid:null,
            province:null,
        }
    },

    componentDidMount: function() {
      this.setState({
            provinceid: this.props.provinceid,
            province :this.props.province,
        });

      // ToastAndroid.show(this.props.provinceid+this.props.province,ToastAndroid.SHORT);   
         this.fetchData();
    },

    getLocation:function(cb){

        async.parallel([
          function(callback){
            AsyncStorage.getItem('city',(error,result)=>{
            if(error){}else{
                callback(null, result);
               }
            });
            
          },
          function(callback){
          AsyncStorage.getItem('cityid',(error,result)=>{
            if(error){}else{
              callback(null, result);
             }
            });
          },
          function(callback){
          AsyncStorage.getItem('provinceid',(error,result)=>{
            if(error){}else{
              callback(null, result);
             }
            });
          },
          function(callback){
          AsyncStorage.getItem('provinceid',(error,result)=>{
            if(error){}else{
              callback(null, result);
             }
            });
          },
        ],
        function(err, results){
          mcity=results[0];
          mcityid=results[1];
          mprovinceid=results[2];
          cb();
        });
    },

  
    
fetchData: function() {
    this.getLocation(()=>{
      var responseData = city_data.get_city_list('CH',this.props.provinceid);
        var index=-1;
        for(var i=0;i<responseData.length;i++){
          if(responseData[i].id===mcityid){
            index=i;
            break;
          }
        }
        if(index!=-1){
          responseData.splice(index,1);
        }
        this.setState({
          dataSource: this.state.dataSource.cloneWithRows(responseData),
          loaded: true,
        });
    });
  },


 rederview:function(){
    if(mcity===null||mprovinceid!=this.props.provinceid){
      var navigator = this.props.navigator;
    return (
      <View>
             
        <Title title = {Language.locationtitle} hasGoBack = {true}  navigator = {this.props.navigator}/>
        <ScrollView style={styles.item} >   
    <View  >
        <View style={styles.view} >
            <Text allowFontScaling={false} style={styles.font2}>{Language.allareas}</Text>  
       </View>
       <View style={styles.line2}/>

        <ListView
        dataSource={this.state.dataSource}
        renderRow={this.renderMovie}
        style={styles.listView}
        removeClippedSubviews={false}
      />     
      </View>   
    </ScrollView>
    </View>
    );}else{
var navigator = this.props.navigator;
    return (
      <View>
          
        <Title title = {Language.locationtitle} hasGoBack = {true}  navigator = {this.props.navigator}/>  

         <ScrollView  style={styles.item}  >   
    <View >
        <View style={styles.view} >
            <Text allowFontScaling={false} style={styles.font2}>{Language.allareas}</Text>  
       </View>
       <View style={styles.line2}/>

        <TouchableOpacity style={styles.view} onPress={()=>this.gotoArea2()}>
            <Text allowFontScaling={false} style={styles.font1}>{mcity}</Text>  
            <View style={styles.direction}>
            <Text allowFontScaling={false} style={styles.font2}>{Language.selected}</Text>
            </View>
       </TouchableOpacity>

       <View style={styles.line2}/>

        <ListView
        dataSource={this.state.dataSource}
        renderRow={this.renderMovie}
        style={styles.listView}
        removeClippedSubviews={false}
      />     
      </View>   
    </ScrollView>
    </View>
    );}
 },
 render: function(){
     return this.rederview();
  },
  renderMovie: function(city) {
    return (
     <View >
      <TouchableOpacity style={styles.view} onPress={()=>this.gotoArea(city)}>
            <Text allowFontScaling={false} style={styles.font1}>{city.name}</Text>                      
       </TouchableOpacity>   
       <View style={styles.line2}/>
      </View>
    );
  },

  gotoArea:function(city){
      UMeng.onEvent('city_03');
      this.props.navigator.push({
      id: 'Area',
      params:{
          cityid:city.id,
          city:city.name,
          province:this.state.province,
          provinceid:this.state.provinceid, 
          start_page:this.props.start_page,
        }
    });
  },

   gotoArea2:function(){
   
    UMeng.onEvent('city_03');
    this.props.navigator.push({
      id: 'Area',
      params:{
          cityid:mcityid,
          city:mcity,
          province:this.state.province,
          provinceid:this.state.provinceid, 
          start_page:this.props.start_page,
        }
    });
  },

});

module.exports = City;
