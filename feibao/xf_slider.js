'use strict'

import  styles_pm from './styles/style_pm'
var TimerMixin = require ('react-timer-mixin');
var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter'); /*用于界面组件之间传递消息*/
import tools from './tools'
import styles_actionbar  from './styles/style_actionbar'
var Slider = require ('./Slider');
var Whether_PM = require ('./whether_pm');
var UMeng = require('./UMeng');
import Language from './Language'

import React, { Component,PropTypes } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    
    TouchableOpacity,
    NetInfo,
} from 'react-native';


var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;


var XF_Slider = React.createClass({
		mixins:[TimerMixin],

    _handle:null,

  PropTypes:{
      whether:PropTypes.string,
      location:PropTypes.string,
      number:PropTypes.number,
  },
  getInitialState:function(){
    return {
    }
  },
 // style = {{position:'absolute',bottom:-(deviceHeight* 0.35)}}
  render:function (){
      return (
        <View >
          <View
            style = {styles_actionbar.view2}>
            <Slider />
            <TouchableOpacity onPress={()=>{this.gotoProvice();UMeng.onEvent('home_07');}}>
                <Whether_PM />
            </TouchableOpacity>
          </View>
        </View>
      );
  },
  gotoProvice:function(){
     NetInfo.isConnected.fetch().done(
        (isConnected) => { 
          if(isConnected){
              this.props.navigator.push({
              id: 'Provice',
              params:{
                start_page:1
              }
              });
          }else{
            tools.alertShow(Language.connectnetwork);
          }
        });
   },  
});



module.exports = XF_Slider;
