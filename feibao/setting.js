/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

// var React = require('react-native');
import React, { Component,PropTypes} from 'react';
import  styles from './styles/style_setting';
import tools from './tools'
import Language from './Language'
import  config from './config'


var share = require('./share');
var async = require('async');
var AsyncStorage_And = require('./AsyncStorage');
var UMeng = require('./UMeng');
var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter');
import Title from './title'
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  Alert,
  Dimensions,
  PixelRatio,
  TouchableOpacity,
  TouchableWithoutFeedback,
   AsyncStorage,
  NativeModules,
  NetInfo,
  ToastAndroid,
  DeviceEventEmitter,
  Platform,
  Linking,
} from 'react-native';
var hasFilter=true;
var mwidth=Dimensions.get('window').width;
var mheight=Dimensions.get('window').height; 
var BlueToothUtil= require('./BlueToothUtil');
var alertMessage = '查询前请您先登录帐号';
var setting= React.createClass({
    getInitialState() {
        return {
            value:mheight,
            m:mwidth,
            location:null,
            deviceName:'我的智能随身净化器',
            filterLeft:70,
            isConnect:'已连接',
            integral:125,
            number:1,
            isConnected: null,
            key:null,
            typeisf:false,
        }
    },

    componentWillMount:function(){
      // AsyncStorage.getItem('key',(error,result) => {
      //     if (error){}else {
      //     this.setState({
      //       key:result,
      //     });
      //   }
      // });
    },
  componentWillUnmount:function(){
  },
  componentDidMount:function(){
    this.set_area();
  },

    componentWillReceiveProps:function(){
      this.set_area();
    },

  set_area: function(){
    async.parallel([
          function(callback){
            AsyncStorage.getItem('province',(error,result)=>{
                callback(null, result);
            });
          },
          function(callback){
            AsyncStorage.getItem('city',(error,result)=>{
                callback(null, result);
            });
          },
          function(callback){
          AsyncStorage.getItem('area',(error,result)=>{
              callback(null, result);
            });
          },
        ],
        (err, results)=>{
          if(results[2] == null){
            this.setState({
              location:Language.automatic,
            }); 
          }else{
            var tmp = '';
            for (var i = 0; i < 3; i++) {
              if (results[i] == '直辖市' || results[i] == '特别行政区') continue;
              if (i>0 && results[i] == results[i-1]) continue;
              tmp += results[i];
              tmp += ' ';
            }
            this.setState({
              location:tmp,
            }); 
          }
        });
  },
//<Filter navigator = {this.props.navigator} typeisf = {this.props.typeisf}/>
//<Filter navigator = {this.props.navigator}/>
  render: function(){
    var s,s1;
    s=[styles.imageNext,{resizeMode: 'stretch'}];
    s1=styles.text;
    
      if(this.props.typeisf==1){
        hasFilter = false;
      }else{
         hasFilter = true;
      }
      console.log('========hasFilter===='+hasFilter+this.props.typeisf);
      var navigator = this.props.navigator;

       if (config.global_language =='en'){
if (Platform.OS === 'android') {
        return (
      <View>

        <Title title = {Language.more} hasGoBack = {true}  navigator = {this.props.navigator}/>
      <View style={styles.view1}>
      <ScrollView contentContainerStyle={styles.contentContainer} showsVerticalScrollIndicator ={false}>
        <View style={styles.view2}>

          <TouchableOpacity style={styles.view} onPress={()=>{this.gotoDeviceStatus();UMeng.onEvent('setting_02');}}>
            <Text allowFontScaling={false} style={styles.font1}>{Language.mydevice}</Text>
            <View style={styles.direction}>
            <Image style={s} source={require('image!ic_next')}/>
            </View>
          </TouchableOpacity>
          <View style={styles.line}/>

          <TouchableOpacity style={styles.view} onPress={()=>{this.gotoProvice();UMeng.onEvent('setting_04');}} >          
            <Text allowFontScaling={false} style={styles.font1} >{Language.location}</Text>
            <View style={styles.direction}>
            <Text allowFontScaling={false} style={[styles.font]}>{this.state.location}</Text>
            <Image style={s} source={require('image!ic_next')}/>
            </View>
          </TouchableOpacity>
          <View style={styles.line}/>
          
          <TouchableOpacity
            onPress = {() => {this.gotoUpdate();UMeng.onEvent('setting_05');}}>
          <View style={styles.view}>
            <Text allowFontScaling={false} style={styles.font1}>{Language.softwareupdate}</Text>
            <View style={styles.direction}>
            <Image style={s} source={require('image!ic_next')}/>
            </View>
          </View>
          </TouchableOpacity> 
          <View style={styles.line}/>
          <TouchableOpacity
            onPress = {()=>{this.gotoDeviceUpdate();UMeng.onEvent('setting_06');}}>
          <View style={styles.view}>
            <Text allowFontScaling={false} style={styles.font1}>{Language.firmwareupdate}</Text>
            <View style={styles.direction}>
            <Image style={s} source={require('image!ic_next')}/>
            </View>
          </View>
          </TouchableOpacity> 
            <View style={styles.line}/>
            <TouchableOpacity style={styles.view} onPress={()=>{this.gotoSafe_code();UMeng.onEvent('setting_07');}}>
              <Text allowFontScaling={false} style={styles.font1}>{Language.devicesecuritycheck}</Text>
              <View style={styles.direction}>
              <Image style={s} source={require('image!ic_next')}/>
              </View>
            </TouchableOpacity>
          <View style={styles.line}/>
           <TouchableOpacity
            onPress = {()=>{this.gotoAbout();UMeng.onEvent('setting_08');}}>
            <View style={styles.view}>
              <Text allowFontScaling={false} style={styles.font1} >{Language.about}</Text>
              <View style={styles.direction}>
                  <Image style={s} source={require('image!ic_next')}/>
              </View>
            </View>
          </TouchableOpacity>     
        <View style={styles.line}/>
                    
        </View>
        </ScrollView>
      </View>
    </View>
    );

  }else{
      return (
      <View>
        <Title title = {Language.more} hasGoBack = {true}  navigator = {this.props.navigator}/>
      <View style={styles.view1}>
      <ScrollView contentContainerStyle={styles.contentContainer} showsVerticalScrollIndicator ={false}>
        <View style={styles.view2}>

          <TouchableOpacity style={styles.view} onPress={()=>{this.gotoDeviceStatus();UMeng.onEvent('setting_10');}}>
            <Text allowFontScaling={false} style={styles.font1}>{Language.mydevice}</Text>
            <View style={styles.direction}>
            <Image style={s} source={require('image!ic_next')}/>
            </View>
          </TouchableOpacity>
          <View style={styles.line}/>

             

          <TouchableOpacity
            onPress = {()=>{this.gotoDeviceUpdate();UMeng.onEvent('setting_13');}}>
          <View style={styles.view}>
            <Text allowFontScaling={false} style={styles.font1}>{Language.firmwareupdate}</Text>
            <View style={styles.direction}>
            <Image style={s} source={require('image!ic_next')}/>
            </View>
          </View>
          </TouchableOpacity> 

           <TouchableOpacity
            onPress = {()=>{this.gotoAbout();UMeng.onEvent('setting_15');}}>
            <View style={styles.view}>
              <Text allowFontScaling={false} style={styles.font1}>{Language.about}</Text>
              <View style={styles.direction}>
                  <Image style={s} source={require('image!ic_next')}/>
              </View>
            </View>
          </TouchableOpacity>                        
        </View>
        </ScrollView>
      </View>
    </View>
    );

  }

       } else {

if (Platform.OS === 'android') {
        return (
      <View>

        <Title title = {Language.more} hasGoBack = {true}  navigator = {this.props.navigator}/>
      <View style={styles.view1}>
      <ScrollView contentContainerStyle={styles.contentContainer} showsVerticalScrollIndicator ={false}>
        <View style={styles.view2}>

          <TouchableOpacity style={styles.view} onPress={()=>{this.gotoDeviceStatus();UMeng.onEvent('setting_02');}}>
            <Text allowFontScaling={false} style={styles.font1}>{Language.mydevice}</Text>
            <View style={styles.direction}>
            <Image style={s} source={require('image!ic_next')}/>
            </View>
          </TouchableOpacity>
          <View style={styles.line}/>


          

          <TouchableOpacity style={styles.view} onPress={()=>{this.gotoProvice();UMeng.onEvent('setting_04');}} >          
            <Text allowFontScaling={false} style={styles.font1} >{Language.location}</Text>
            <View style={styles.direction}>
            <Text allowFontScaling={false} style={[styles.font]}>{this.state.location}</Text>
            <Image style={s} source={require('image!ic_next')}/>
            </View>
          </TouchableOpacity>
          <View style={styles.line}/>
          <TouchableOpacity
            onPress = {() => {this.gotoUpdate();UMeng.onEvent('setting_05');}}>
          <View style={styles.view}>
            <Text allowFontScaling={false} style={styles.font1}>{Language.softwareupdate}</Text>
            <View style={styles.direction}>
            <Image style={s} source={require('image!ic_next')}/>
            </View>
          </View>
          </TouchableOpacity> 
          <View style={styles.line}/>
          <TouchableOpacity
            onPress = {()=>{this.gotoDeviceUpdate();UMeng.onEvent('setting_06');}}>
          <View style={styles.view}>
            <Text allowFontScaling={false} style={styles.font1}>{Language.firmwareupdate}</Text>
            <View style={styles.direction}>
            <Image style={s} source={require('image!ic_next')}/>
            </View>
          </View>
          </TouchableOpacity> 
            <View style={styles.line}/>
            <TouchableOpacity style={styles.view} onPress={()=>{this.gotoSafe_code();UMeng.onEvent('setting_07');}}>
              <Text allowFontScaling={false} style={styles.font1}>{Language.devicesecuritycheck}</Text>
              <View style={styles.direction}>
              <Image style={s} source={require('image!ic_next')}/>
              </View>
            </TouchableOpacity>
          <View style={styles.line}/>
           <TouchableOpacity
            onPress = {()=>{this.gotoAbout();UMeng.onEvent('setting_08');}}>
            <View style={styles.view}>
              <Text allowFontScaling={false} style={styles.font1} >{Language.about}</Text>
              <View style={styles.direction}>
                  <Image style={s} source={require('image!ic_next')}/>
              </View>
            </View>
          </TouchableOpacity>     
        <View style={styles.line}/>
                    
        </View>
        </ScrollView>
      </View>
    </View>
    );

  }else{
      return (
      <View>
        <Title title = {Language.more} hasGoBack = {true}  navigator = {this.props.navigator}/>
      <View style={styles.view1}>
      <ScrollView contentContainerStyle={styles.contentContainer} showsVerticalScrollIndicator ={false}>
        <View style={styles.view2}>

          <TouchableOpacity style={styles.view} onPress={()=>{this.gotoDeviceStatus();UMeng.onEvent('setting_10');}}>
            <Text allowFontScaling={false} style={styles.font1}>{Language.mydevice}</Text>
            <View style={styles.direction}>
            <Image style={s} source={require('image!ic_next')}/>
            </View>
          </TouchableOpacity>
          <View style={styles.line}/>

             

          <TouchableOpacity style={styles.view} onPress={()=>{this.gotoProvice();UMeng.onEvent('setting_12');}} >          
            <Text allowFontScaling={false} style={styles.font1} >{Language.location}</Text>
            <View style={styles.direction}>
            <Text allowFontScaling={false} style={[styles.font]}>{this.state.location}</Text>
            <Image style={s} source={require('image!ic_next')}/>
            </View>
          </TouchableOpacity>
          <View style={styles.line}/>
          <TouchableOpacity
            onPress = {()=>{this.gotoDeviceUpdate();UMeng.onEvent('setting_13');}}>
          <View style={styles.view}>
            <Text allowFontScaling={false} style={styles.font1}>{Language.firmwareupdate}</Text>
            <View style={styles.direction}>
            <Image style={s} source={require('image!ic_next')}/>
            </View>
          </View>
          </TouchableOpacity> 
          <View style={styles.line}/>
            <TouchableOpacity style={styles.view} onPress={()=>{this.gotoSafe_code();UMeng.onEvent('setting_14');}}>
              <Text allowFontScaling={false} style={styles.font1}>{Language.devicesecuritycheck}</Text>
              <View style={styles.direction}>
              <Image style={s} source={require('image!ic_next')}/>
              </View>
          </TouchableOpacity>
          <View style={styles.line}/>
           <TouchableOpacity
            onPress = {()=>{this.gotoAbout();UMeng.onEvent('setting_15');}}>
            <View style={styles.view}>
              <Text allowFontScaling={false} style={styles.font1}>{Language.about}</Text>
              <View style={styles.direction}>
                  <Image style={s} source={require('image!ic_next')}/>
              </View>
            </View>
          </TouchableOpacity>                        
        </View>
        </ScrollView>
      </View>
    </View>
    );

  }
       }
     
  

  },

  gotoProvice:function(){
     NetInfo.isConnected.fetch().done(
        (isConnected) => { 
          if(isConnected){
              this.props.navigator.push({
              id: 'Provice',
              params:{
                start_page:0
              }
              // id: 'SetPage',
              });
          }else{
            tools.alertShow(Language.connectnetwork);
            // ToastAndroid.show(Language.connectnetwork,ToastAndroid.SHORT);  
          }
        });
  },
  gotoAbout:function(){
    this.props.navigator.push({
      id: 'About',//'FeedPage',
    });
  },
  gotoLogin:function(){
    this.props.navigator.push({
      id:'Login',
      params:{
       codepage:1,
      }
    });
  },
  gotoUpdate:function(){
	NetInfo.isConnected.fetch().done(
        (isConnected) => { 
          if(isConnected){
			this.props.navigator.push({
				id: 'Update',
			});
          }else{
            tools.alertShow(Language.connectnetwork);
            // ToastAndroid.show(Language.connectnetwork,ToastAndroid.SHORT);  
          }
        });
  },
  gotoDeviceUpdate:function(){
      NetInfo.isConnected.fetch().done(
        (isConnected) => { 
          if(isConnected){
      this.props.navigator.push({
        id: 'DeviceUpdate',
      });
          }else{
            tools.alertShow(Language.connectnetwork);
            // ToastAndroid.show(Language.connectnetwork,ToastAndroid.SHORT);  
          }
        });
  },

  gotoSafe_code:function(){
    AsyncStorage_And.getItem('key',(error,result) => {
        if (error){}else {
        this.setState({
          key:result,
        });
        console.log('Safe_code--'+ this.state.key + result);
        
          if(this.state.key==null){
            Alert.alert(
              Language.prompt+'',
              Language.securityalert +'',
              [
                {text: Language.ok+'', onPress: () => {this.gotoLogin();UMeng.onEvent('setting_16');}},
                {text: Language.cancel+'', onPress: () => {UMeng.onEvent('setting_17');return true;}},
              ]
            )
          }else{
            this.gotoLogin();
            // this.props.navigator.push({
            //   id: 'Safe_code',
            // });
          }

      }
    });

  },
  gotoDeviceStatus:function(){
      // if(this.state.connect==='已连接'){
      //      this.props.navigator.jumpTo(this.props.navigator.getCurrentRoutes()[2]);
      // }else{
      this.props.navigator.push({
      id: 'DeviceStatus',
      });
      // }
   
    },

});
  var Filter= React.createClass({
    PropTypes:{
        type:PropTypes.number,
        status:PropTypes.string,
        number:PropTypes.number,
    },
    render(){
      if(hasFilter){
          return (
            <View>
                <TouchableOpacity style={styles.view} onPress={()=>{this.gotoFilterPage();UMeng.onEvent('setting_03');}}>
                  <Text allowFontScaling={false} style={styles.font1}>滤芯</Text>
                  <View style={styles.direction}>
                  <Image style={[styles.imageNext,{resizeMode: 'stretch'}]} source={require('image!ic_next')}/>
                  </View>
                </TouchableOpacity>
                <View style={styles.line}/> 
            </View>
          );
      }else{
          return (
          <View/>
          );
      }
    },
    gotoFilterPage:function() {
      if(this.props.typeisf==0){
        this.props.navigator.push({
          id: 'FilterXinfengPage',
        });

      }else{
        this.props.navigator.push({
          id: 'FilterVehidePage',
        });
      }

  },
  });
module.exports = setting;