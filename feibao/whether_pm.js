'use strict'

import  styles_pm from './styles/style_pm'
var TimerMixin = require ('react-timer-mixin');
var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter'); /*用于界面组件之间传递消息*/

import React, { Component, PropTypes } from 'react';
import Language from './Language'
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    NetInfo,
} from 'react-native';

var NetWork=require('./NetWork');


var sub_chanage_local = null;


var weather='室外严重污染';
var location='北京市朝阳区';
var pm25='305';

var Whether_PM = React.createClass({
		mixins:[TimerMixin],

    _handle:null,

  PropTypes:{
      whether:PropTypes.string,
      location:PropTypes.string,
      number:PropTypes.number,
  },
  getInitialState:function(){
    return {
      v_pm25:'--',
      v_weather:'--',
      v_location:'--',
      v_load:false,
      v_cityid:null,
    }
  },


   _handleConnectionInfoChange: function(connectionInfo) {

    console.log(connectionInfo+'是否联网');
    this.getData();
    
  },

  componentDidMount: function() {

    //检测网络变化
    NetInfo.addEventListener(
        'change',
        this._handleConnectionInfoChange
    );
    NetInfo.isConnected.fetch().done((isConnected) => {
      console.log(isConnected+'联网状态');
      console.log('First, is ' + (isConnected ? 'online' : 'offline'));
      if (isConnected){
        console.log(isConnected+'联网成功');

      }

    });


    this._handle = this.setInterval(()=>{
      this.setState({
          v_load:false,
        });
      this.getData();
    }, 1000*60*60);
  },

  componentWillMount: function(){
    this.getData();
    sub_chanage_local = RCTDeviceEventEmitter.addListener('msg_chanage_local',(val)=>{
      this.getData();
    });
  },
  componentWillReceiveProps: function(){
    //this.getData();
  },
  componentWillUnmount: function(){
	  this.clearInterval(this._handle);
    sub_chanage_local.remove();

   NetInfo.removeEventListener(
        'change',
        this._handleConnectionInfoChange
    );
  },
  getData:function(){

    NetWork.fetch_all_data((city_name, text, temperature, max_min, wind_direction, wind_scale, pm25)=>{
        var tmp1 = text+'  '+wind_direction + wind_scale +'级'+'  '+ temperature+'℃' +'  ';

        var tmp = city_name.split(',');
          var tmp_str = '';
          for (var i = tmp.length - 1; i >= 0; i--) {
            if (tmp[i] == '中国') continue;
            if (tmp[i] == '直辖市') continue;
            if (i>0 && tmp[i] == tmp[i-1]) continue;
            tmp_str += tmp[i];
            tmp_str += ' ';
          }
          RCTDeviceEventEmitter.emit('PM25',pm25);
          RCTDeviceEventEmitter.emit('weather',text);
          this.setState({
            v_weather:tmp1,
            v_pm25:pm25,
            v_location:tmp_str,
          });   
    });
},
  render:function (){
      return (
          <View
          style = {styles_pm.container}>

          <View
            style = {styles_pm.text}>
            <View style = {{marginLeft:20,}}>
                <Text allowFontScaling={false}
                  style = {styles_pm.text_whether}>
                  {this.state.v_weather}
                </Text>
              <Text allowFontScaling={false}
                style = {styles_pm.text_location}>
                {this.state.v_location}
              </Text>
            </View>
          </View>
       
          <View
            style = {styles_pm.right}>
              <View
                style = {styles_pm.pm}>
                  <Text allowFontScaling={false}
                    style = {styles_pm.text_pm}>
                  PM2.5
                  </Text>
              </View>
              <View
                  style = {styles_pm.number}>
                <Text allowFontScaling={false}
                  style = {styles_pm.text_number}>
                    {this.state.v_pm25}
                </Text>
              </View>
            </View>
          </View>
      );
    }
});



module.exports = Whether_PM;
