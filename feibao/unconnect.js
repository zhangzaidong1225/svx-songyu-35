'use strict'

var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter');
import styles from './styles/style_setting'
import styles_feedback from './styles/style_feedback'
var UMeng = require('./UMeng');
import Title from './title'
import React, { Component } from 'react';
import Language from './Language'
import TimerMap from './TimerMap'

import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  PixelRatio,
  TouchableOpacity,
  ToastAndroid,
  Navigator,
  ScrollView,
  ListView,
  TextInput,
  NativeModules,
  NativeAppEventEmitter,
  // DeviceEventEmitter,
  Platform,
} from 'react-native';


var deviceWidth=Dimensions.get('window').width;
var deviceHeight=Dimensions.get('window').height;

var BlueToothUtil= require('./BlueToothUtil');

var sub_ble_state_ios = null;

// var sub_ble_state_android = null;

var Unconnect = React.createClass({

  getInitialState(){

    return {
      isbond:Language.cancelthepairing,
      device_name:'',
      content:'',
    };
  },
  openModal: function() {

    this.refs.modal.open();


  },

  ble_state_event_update:function(data){
    for (var i = 0; i < data.dev_list.length; i++) {
            if(data.dev_list[i].addr == this.props.deviceid){
              console.log(data.dev_list[i].name);
              this.setState({device_name:data.dev_list[i].name});
            }
    };
  },

  componentDidMount:function (){

    //ToastAndroid.show(this.props.device.id+'',ToastAndroid.SHORT);

    this.setState({device_name:this.props.device});
    // sub_ble_state_ios = NativeAppEventEmitter.addListener(
    //      'ble_state',
    //      (data) => {
    //         this.ble_state_event_update(data);
    //      }
    //  );
     // sub_ble_state_android = DeviceEventEmitter.addListener(
     //     'ble_state',
     //     (data) => {
     //        this.ble_state_event_update(data);
     //     }
     // );
     sub_ble_state_ios = NativeAppEventEmitter.addListener(
         'ble_state',
         (data) => {
            this.ble_state_event_update(data);
         }
     );

     BlueToothUtil.sendMydevice();
  },

  componentWillUnmount:function(){
      sub_ble_state_ios.remove();
      // sub_ble_state_android.remove();
  },



  render:function (){
    return (
      <View style = {{flex:1,backgroundColor:'#FFFFFF'}}>
      
        <Title title = {Language.unconnecteddevice} hasGoBack = {true}  navigator = {this.props.navigator}/>

        <View style = {{alignItems:'center',justifyContent:'center',height:180,}}>
            <Text allowFontScaling={false} style = {{fontSize:25,textAlign:'center',color:'#808080',}}>{this.state.device_name}</Text>
        </View>


        <View style = {{height:40,marginTop:40,
          marginRight:100,marginLeft:100,justifyContent:'center',alignItems: 'center',}}>

                <View style={{justifyContent:'center',alignItems: 'center',position:'absolute',
             width:deviceWidth-200,height:40,borderColor:'rgb(23, 201, 180)',borderWidth: 2 / PixelRatio.get(),
             borderRadius:20,bottom:10,}}>
                  <TouchableOpacity
                      onPress = {()=> {this.cancelBond(this.props.deviceid);UMeng.onEvent('unconnect_02');}}>
                     <View style = {{width:100,backgroundColor:'transparent'}}>
                      <Text allowFontScaling={false} style={{textAlign:'center',fontSize:18,color:'#6FC8B2',}}>{Language.cancelthepairing}</Text>
                     </View>
                  </TouchableOpacity>
                </View>
              </View>

      </View>

    );
  },
  

  cancelBond:function(address){
    if (TimerMap.has(address)){
      TimerMap.delete(address);
    }

      BlueToothUtil.cancelBond(address);

       this.props.navigator.pop();
    
  },

});



var styles_modal1 = StyleSheet.create({
    modal2_container:{
    backgroundColor:'white',
    alignItems:'center',
    height:180,
    width:deviceWidth-20
    },
    modal2_view:{
      backgroundColor:'white',
      height:180,
      width:deviceWidth-20,
      position:'absolute',
    },
    modal2_view1:{
      backgroundColor:'white',
      position:'absolute',
      width:deviceWidth - 20,
      height:60,
      alignItems:'center',
      justifyContent:'center'
    },
    modal2_text:{
      color: 'black',
      fontSize: 18,
      textAlign:'center',
    },

    modal2_view2:{
      flexDirection:'row',
      marginLeft:10,
      marginRight:10,
      position:'absolute',
      marginBottom:10,
      marginTop:120,
      height:40,
      justifyContent:'center',
      alignItems:'center',
    },

    modal2_close:{
      justifyContent:'center',
      alignItems: 'center',
      position:'absolute',
        width:100,
        height:40,
        borderColor:'rgb(23, 201, 180)',
        borderWidth: 2 / PixelRatio.get(),
        borderRadius:20,bottom:0,
    },
    modal2_closetext:{
      width:50,
      backgroundColor:'transparent'
    },

    modal2_wait:{
      left:30,
      justifyContent:'center',
      alignItems: 'center',
      position:'absolute',
        width:100,
        height:40,
        borderColor:'rgb(23, 201, 180)',
        borderWidth: 2 / PixelRatio.get(),
        borderRadius:20,
        bottom:0,
    },
    text_input:{
        marginTop:60,
        marginRight:40,
        marginLeft:40,
        justifyContent:'center',
        alignItems: 'center',
        height:40,
        borderWidth: 2 / PixelRatio.get(),
    },
    modal2_cancel:{
      left:190,
      justifyContent:'center',
      alignItems: 'center',
      position:'absolute',
        width:100,
        height:40,
        borderColor:'rgb(23, 201, 180)',
        borderWidth: 2 / PixelRatio.get(),
        borderRadius:20,bottom:0,
    },
});

module.exports = Unconnect;
