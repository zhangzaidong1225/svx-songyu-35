//
//  MXUpdateHelper.m
//  Knowme
//
//  Created by shenzw on 8/4/16.
//  Copyright © 2016 Facebook. All rights reserved.
//

#import "MXUpdateHelper.h"

@implementation MXUpdateHelper
+(void)checkUpdate:(FinishBlock)finish{
  //获取当前已存储js版本
  NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
  NSString * hotUpdateVersion = [userDefaultes objectForKey:@"hotUpdateVersion"];
  NSLog(@"当前版本:%@",hotUpdateVersion);
  if (hotUpdateVersion == nil) {
    hotUpdateVersion = @"1.2.0";
  }
  NSString *url = [@"https://mops-xinfeng-dev.lianluo.com/iface/system/hot_update?platform=1&version=" stringByAppendingString:hotUpdateVersion];
  //TODO 测试服务器 和 正式服务器
  NSString * isTestXinfeng = [userDefaultes stringForKey:@"isTestXinfeng"];
  if (isTestXinfeng == nil || [isTestXinfeng isEqualToString:@("忻风")] || [isTestXinfeng isEqualToString:@("XinFeng")]) {
    
//    url = [@"https://mops.lianluo.com/iface/system/hot_update?platform=1&version=" stringByAppendingString:hotUpdateVersion];
    return;
  } else {
    url = [@"https://mops-xinfeng-dev.lianluo.com/iface/system/hot_update?platform=1&version=" stringByAppendingString:hotUpdateVersion];
  }

  
  
  NSMutableURLRequest *newRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
  [newRequest setHTTPMethod:@"GET"];
  [NSURLConnection sendAsynchronousRequest:newRequest queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse * response, NSData * data, NSError * connectionError) {
    if(connectionError == nil){
      //请求自己服务器的API，判断当前的JS版本是否最新
      /*
       {
       "version":"1.0.5",
       "fileurl":"http://localhost:8888/RNBundle_1.0.1.zip",
       "message":"有新版本，请更新到我们最新的版本",
       "force:"NO"
       }
       */
      NSError *error;
      NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
      if(!error){
        int code = [jsonDic[@"code"] intValue];
        NSLog(@"返回码为：%d", code);
        NSString *newVersion = jsonDic[@"data"][@"ver"];
        if(code == 100 && ![hotUpdateVersion isEqualToString:newVersion]){
          NSLog(@"发现新版本:%@",newVersion);
          finish(1,jsonDic[@"data"]);
        }else{
          NSLog(@"未发现新版本");
          finish(0,nil);
        }
      }else{
        NSLog(@"json解析失败");
        finish(0,nil);
      }
    }
  }];
}
@end
