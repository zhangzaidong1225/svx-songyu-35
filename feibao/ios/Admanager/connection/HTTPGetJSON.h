//
//  HTTPGetJSON.h
//  feibao
//
//  Created by Louis Liu on 16/4/27.
//  Copyright © 2016年 Lianluo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HTTPGetJSON : NSObject

//@property (strong, nonatomic) NSDictionary * data;
-(NSDictionary *) getJsonFromUrl:(NSString *) urlString;

@end
