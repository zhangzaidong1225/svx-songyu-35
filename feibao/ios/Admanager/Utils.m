//
//  Utils.m
//  feibao
//
//  Created by Louis Liu on 16/4/28.
//  Copyright © 2016年 Lianluo. All rights reserved.
//

#import "Utils.h"
#import "AdManager.h"
#import <CommonCrypto/CommonDigest.h>

@implementation Utils

RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(get_ad_action:(RCTResponseSenderBlock)callback) {
  NSMutableDictionary * data = [[AdManager sharedInstance] data];
  NSLog(@"get_ad_action:%@", [[data objectForKey:@"data"] objectForKey:@"action"]);
  NSString * action = [[data objectForKey:@"data"] objectForKey:@"action"];
  
  NSArray *events =  [NSArray arrayWithObjects:action, nil];
  callback(@[[NSNull null], events]);
}

RCT_EXPORT_METHOD(set_test_mark_str:(NSString*)mark) {
  NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
  [userDefaults setObject:mark forKey:@"isTestXinfeng"];
}

- (NSString *) get_test_mark_str {
  NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
  return [userDefaultes stringForKey:@"isTestXinfeng"];
}

RCT_EXPORT_METHOD(is_english_language:(RCTResponseSenderBlock)callback){

  NSString * language = [Utils get_language];
  callback(@[[NSNull null], language]);

}

+ (NSString *) get_language {
  NSArray * allLanguages = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
  NSString * language = [allLanguages objectAtIndex:0];
  NSLog(@"language: %@",language);
  if ([language isEqualToString:@"zh-Hans-CN"] || [language isEqualToString:@"zh-Hans"]) {
    NSLog(@"language zh");
    return @"zh";

  }
  else if ([language isEqualToString:@"en-CN"] || [language isEqualToString:@"en"]){
    NSLog(@"language en");
    return @"en";
  }
  return @"zh";
}

+ (NSString *) get_file_md5:(NSString*)path {
  NSFileHandle *handle = [NSFileHandle fileHandleForReadingAtPath:path];
  if( handle== nil )
  {
    return @"";
  }
  
  CC_MD5_CTX md5;
  
  CC_MD5_Init(&md5);
  
  BOOL done = NO;
  while(!done)
  {
    NSData* fileData = [handle readDataOfLength: 256 ];
    CC_MD5_Update(&md5, [fileData bytes], [fileData length]);
    if( [fileData length] == 0 ) done = YES;
  }
  unsigned char digest[CC_MD5_DIGEST_LENGTH];
  CC_MD5_Final(digest, &md5);
  NSString* s = [NSString stringWithFormat: @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
                 digest[0], digest[1],
                 digest[2], digest[3],
                 digest[4], digest[5],
                 digest[6], digest[7],
                 digest[8], digest[9],
                 digest[10], digest[11],
                 digest[12], digest[13],
                 digest[14], digest[15]];
  
  NSLog(@"--------%@", s);
  return s;
}

RCT_EXPORT_METHOD(get_select_item_addr:(RCTResponseSenderBlock)callback) {
  NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
  callback(@[[NSNull null], [userDefaultes stringForKey:@"select_item_addr"]]);
  
}

RCT_EXPORT_METHOD(set_select_item_addr:(NSString*)addr) {
  NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
  [userDefaults setObject:addr forKey:@"select_item_addr"];
}
@end
