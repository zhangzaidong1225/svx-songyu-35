//
//  Utils.h
//  feibao
//
//  Created by Louis Liu on 16/4/28.
//  Copyright © 2016年 Lianluo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RCTBridgeModule.h"

@interface Utils : NSObject <RCTBridgeModule>

- (NSString *) get_test_mark_str;
+ (NSString *) get_file_md5:(NSString*)path;
+ (NSString *) get_language;
@end
