//
//  FileUtil.h
//  feibao
//
//  Created by Louis Liu on 16/8/24.
//  Copyright © 2016年 Lianluo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileUtil : NSObject
- (void)purgeDocumentsDirectory;
@end
