/**
 * Copyright (c) 2015-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

#import "AppDelegate.h"

#import "RCTRootView.h"

#import <AdSupport/AdSupport.h>

//＝＝＝＝＝＝＝＝＝＝ShareSDK头文件＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
#import <ShareSDK/ShareSDK.h>
#import <ShareSDKConnector/ShareSDKConnector.h>
#import "Countly.h"
//以下是ShareSDK必须添加的依赖库：
//1、libicucore.dylib
//2、libz.dylib
//3、libstdc++.dylib
//4、JavaScriptCore.framework

//＝＝＝＝＝＝＝＝＝＝以下是各个平台SDK的头文件，根据需要继承的平台添加＝＝＝
//腾讯开放平台（对应QQ和QQ空间）SDK头文件
//#import <TencentOpenAPI/TencentOAuth.h>
//#import <TencentOpenAPI/QQApiInterface.h>
//以下是腾讯SDK的依赖库：
//libsqlite3.dylib

//微信SDK头文件
#import "WXApi.h"
//以下是微信SDK的依赖库：
//libsqlite3.dylib

//新浪微博SDK头文件
#import "WeiboSDK.h"
//新浪微博SDK需要在项目Build Settings中的Other Linker Flags添加"-ObjC"
//以下是新浪微博SDK的依赖库：
//ImageIO.framework
//libsqlite3.dylib
//AdSupport.framework

//人人SDK头文件
//#import <RennSDK/RennSDK.h>

//GooglePlus SDK头文件
//#import <GooglePlus/GooglePlus.h>
//GooglePlus SDK需要在项目Build Settings中的Other Linker Flags添加"-ObjC"
//以下是GooglePlus SDK的依赖库
//1、CoreMotion.framework
//2、CoreLocation.framework
//3、MediaPlayer.framework
//4、AssetsLibrary.framework
//5、AddressBook.framework

//Kakao SDK头文件
//#import <KakaoOpenSDK/KakaoOpenSDK.h>

//支付宝SDK
//#import "APOpenAPI.h"

#import <MOBFoundation/MOBFoundation.h>
#import "BluetoothManager.h"
#import "AdManager.h"
#import "UMMobClick/MobClick.h"
#import "DeviceCountShare.h"
#import "FileUtil.h"

//热更新
#import "MXBundleHelper.h"
#import "MXUpdateHelper.h"
#import "MXFileHelper.h"
#import "SSZipArchive.h"
#import "Utils.h"

@interface AppDelegate()<UIAlertViewDelegate>
@property (nonatomic,strong) RCTBridge *bridge;
@property (nonatomic,strong) NSDictionary *versionDic;
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  NSLog(@"%@",[[UIDevice currentDevice] systemVersion]);
  //save log to document
//  UIDevice *device = [UIDevice currentDevice];
//  if (![[device model]isEqualToString:@"iPad Simulator"]) {
//    // 开始保存日志文件
//    [self redirectNSLogToDocumentFolder];
//  }
  
  //拷贝固件升级文件
  [[[FileUtil alloc] init] purgeDocumentsDirectory];
  
  //UMeng
  [MobClick setLogEnabled:YES];
  UMConfigInstance.appKey = @"574f99e6e0f55ae606003364";
  UMConfigInstance.channelId = @"App Store";
  [MobClick startWithConfigure:UMConfigInstance];

  //Countly
  CountlyConfig* config = CountlyConfig.new;
  config.appKey = @"d4ea5db8a8cd37c301910c63cb969019ddb9d36b";
  config.host = @"https://mops-countly.lianluo.com";
  config.features = @[CLYCrashReporting];
  [Countly.sharedInstance startWithConfig:config];
  
  NSURL *jsCodeLocation;
  
  //统计分享 Demo
//  DeviceCountShare * deviceCountShare = [DeviceCountShare sharedInstance];
//  [deviceCountShare createShareImage:@("你今天打败了 75％ 的好友") name:@("Louis") number:@("1") grade:@("99")];
  [self log_current_time_with:@("adManager start: ")];
  AdManager * adManager = [AdManager sharedInstance];
  [adManager get_ad_data];
  [self log_current_time_with:@("adManager end: ")];
  


  //[ble scanForPeripherals:true];

  /**
   * Loading JavaScript code - uncomment the one you want.
   *
   * OPTION 1
   * Load from development server. Start the server from the repository root:
   *
   * $ npm start
   *
   * To run on device, change `localhost` to the IP address of your computer
   * (you can get this by typing `ifconfig` into the terminal and selecting the
   * `inet` value under `en0:`) and make sure your computer and iOS device are
   * on the same Wi-Fi network.
   */
  

  RCTRootView *rootView;
  #if defined (__i386__) || defined (__x86_64__)
      
      
      
      //模拟器下执行
        jsCodeLocation = [NSURL URLWithString:@"http://127.0.0.1:8081/index.ios.bundle?platform=ios&dev=true"];
        [self log_current_time_with:@("rootView start: ")];
        rootView = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation
                                                            moduleName:@"feibao"
                                                     initialProperties:nil
                                                         launchOptions:launchOptions];

  #else
  
      //真机下执行
      #ifndef __OPTIMIZE__
  
        //这里执行的是debug模式下
          jsCodeLocation = [NSURL URLWithString:@"http://192.168.0.65:8081/index.ios.bundle?platform=ios&dev=true"];
//          jsCodeLocation = [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];

          [self log_current_time_with:@("rootView start: ")];
          rootView = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation
                                                              moduleName:@"feibao"
                                                       initialProperties:nil
                                                           launchOptions:launchOptions];

      #else
  
        //这里执行的是release模式下
        jsCodeLocation = [MXBundleHelper getBundlePath];
  
        _bridge = [[RCTBridge alloc] initWithBundleURL:jsCodeLocation
                                  moduleProvider:nil
                                   launchOptions:launchOptions];
        rootView = [[RCTRootView alloc] initWithBridge:_bridge moduleName:@"feibao" initialProperties:nil];
      #endif
    
      

  #endif
  //[NSThread sleepForTimeInterval:3];




  /**
   * OPTION 2
   * Load from pre-bundled file on disk. The static bundle is automatically
   * generated by "Bundle React Native code and images" build step.
   */
//  jsCodeLocation = [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
// jsCodeLocation = [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];

//  [self log_current_time_with:@("rootView start: ")];
//  RCTRootView *rootView = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation
//                                                      moduleName:@"feibao"
//                                               initialProperties:nil
//                                                   launchOptions:launchOptions];
  
  [self log_current_time_with:@("sleepForTimeInterval start: ")];
  [NSThread sleepForTimeInterval:2];
  [self log_current_time_with:@("sleepForTimeInterval end: ")];

  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  UIViewController *rootViewController = [UIViewController new];
  rootViewController.view = rootView;
  self.window.rootViewController = rootViewController;
  [self.window makeKeyAndVisible];
  
  [self reg_share];
  [self log_current_time_with:@("rootView end: ")];
  
  #if defined (__i386__) || defined (__x86_64__)
  
  
  
  //模拟器下执行

  
  #else
  
  //真机下执行
    #ifndef __OPTIMIZE__
  
    //这里执行的是debug模式下

    #else
    
    //这里执行的是release模式下
      __weak AppDelegate *weakself = self;
      //更新检测
      [MXUpdateHelper checkUpdate:^(NSInteger status, id data) {
        if(status == 1){
          weakself.versionDic = data;
          
          //更新
          [[MXFileHelper shared] downloadFileWithURLString:_versionDic[@"url"] finish:^(NSInteger status, id data) {
            if(status == 1){
              NSLog(@"下载完成");
              NSString *filePath = (NSString *)data;

              // MD5校验
              NSString * file_md5 = [Utils get_file_md5:filePath];
              if ([file_md5 isEqualToString:_versionDic[@"md5"]]) {
                  [[NSUserDefaults standardUserDefaults] setObject:_versionDic[@"ver"] forKey:@"hotUpdateVersion"];

              } else {
                NSLog(@"md5:%@不匹配",file_md5);
//                [[NSFileManager defaultManager] removeItemAtURL:[NSURL URLWithString:filePath] error:nil];
              }
              
            }
          }];

          
//          /*
//           这里具体关乎用户体验的方式就多种多样了，比如自动立即更新，弹框立即更新，自动下载下次打开再更新等。
//           */
//          UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"有新版本更新"/*data[@"message"]*/ delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"现在更新", nil];
//          [alert show];
//          //进行下载，并更新
//          //下载完，覆盖JS和assets，并reload界面
//          //      [weakself.bridge reload];
        }
      }];

    #endif
  
  
  
  #endif

  

  return YES;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
  if(buttonIndex == 1){
    //更新
    [[MXFileHelper shared] downloadFileWithURLString:_versionDic[@"url"] finish:^(NSInteger status, id data) {
      if(status == 1){
        NSLog(@"下载完成");
        NSError *error;
        NSString *filePath = (NSString *)data;
        NSString *desPath = [NSString stringWithFormat:@"%@",NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0]];
        // TODO MD5校验
        NSString * file_md5 = [Utils get_file_md5:filePath];
        if ([file_md5 isEqualToString:_versionDic[@"md5"]]) {
          [SSZipArchive unzipFileAtPath:filePath toDestination:desPath overwrite:YES password:nil error:&error];
          if(!error){
            NSLog(@"解压成功");
            [_bridge reload];
          }else{
            NSLog(@"解压失败");
          }
        } else {
          NSLog(@"md5:%@不匹配",file_md5);
        }

      }
    }];
  }
}

- (void)reg_share
{
  [ShareSDK registerApp:@"f0038fc8c42e"
   
        activePlatforms:@[
                          @(SSDKPlatformTypeSinaWeibo),
                          //@(SSDKPlatformTypeMail),
                          //@(SSDKPlatformTypeSMS),
                          //@(SSDKPlatformTypeCopy),
//                          @(SSDKPlatformTypeWechat),
//                          @(SSDKPlatformSubTypeWechatFav),
                          @(SSDKPlatformSubTypeWechatSession),
                          @(SSDKPlatformSubTypeWechatTimeline),
                          //@(SSDKPlatformTypeQQ),
                          //@(SSDKPlatformTypeRenren),
                          //@(SSDKPlatformTypeGooglePlus)
                          ]
               onImport:^(SSDKPlatformType platformType)
   {
     switch (platformType)
     {
       case SSDKPlatformTypeWechat:
       [ShareSDKConnector connectWeChat:[WXApi class]];
        
       break;
       case SSDKPlatformSubTypeWechatSession:
         [ShareSDKConnector connectWeChat:[WXApi class]];
         break;
       case SSDKPlatformSubTypeWechatTimeline:
         [ShareSDKConnector connectWeChat:[WXApi class]];
         break;
//       case SSDKPlatformTypeQQ:
//       [ShareSDKConnector connectQQ:[QQApiInterface class] tencentOAuthClass:[TencentOAuth class]];
//       break;
       case SSDKPlatformTypeSinaWeibo:
       [ShareSDKConnector connectWeibo:[WeiboSDK class]];
       break;
       case SSDKPlatformTypeRenren:
       //[ShareSDKConnector connectRenren:[RennClient class]];
       break;
       default:
       break;
     }
   }
        onConfiguration:^(SSDKPlatformType platformType, NSMutableDictionary *appInfo)
   {
     
     switch (platformType)
     {
       case SSDKPlatformTypeSinaWeibo:
       //设置新浪微博应用信息,其中authType设置为使用SSO＋Web形式授权
       [appInfo SSDKSetupSinaWeiboByAppKey:@"4002540601"
                                 appSecret:@"a2d20ada15e1a3c44a59bdd24a5594e2"
                               redirectUri:@"http://www.sharesdk.cn"
                                  authType:SSDKAuthTypeBoth];
       break;
       case SSDKPlatformTypeWechat:
       [appInfo SSDKSetupWeChatByAppId:@"wx6f9068726bed828d"
                             appSecret:@"bf0538e89301a098ca2949a5b745fc05"];
       break;
       case SSDKPlatformSubTypeWechatTimeline:
         [appInfo SSDKSetupWeChatByAppId:@"wx6f9068726bed828d"
                               appSecret:@"bf0538e89301a098ca2949a5b745fc05"];
         break;
       case SSDKPlatformSubTypeWechatSession:
         [appInfo SSDKSetupWeChatByAppId:@"wx6f9068726bed828d"
                               appSecret:@"bf0538e89301a098ca2949a5b745fc05"];
         break;
//       case SSDKPlatformTypeQQ:
//       [appInfo SSDKSetupQQByAppId:@"100371282"
//                            appKey:@"aed9b0303e3ed1e27bae87c33761161d"
//                          authType:SSDKAuthTypeBoth];
//       break;

       default:
       break;
     }
   }];
}

-(void)log_current_time_with:(NSString *) mark{
  NSString* date;
  NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
  [formatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
  date = [formatter stringFromDate:[NSDate date]];
  NSLog(@"%@", [mark stringByAppendingString:date]);
}

// save log to document
- (void)redirectNSLogToDocumentFolder{
  NSString * DOCUMENT_PATH = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
  
  NSString *fileName =[NSString stringWithFormat:@"%@.log",[NSDate date]];
  NSString *logFilePath = [DOCUMENT_PATH stringByAppendingPathComponent:fileName];
  freopen([logFilePath cStringUsingEncoding:NSASCIIStringEncoding],"a+",stderr);
}

@end
