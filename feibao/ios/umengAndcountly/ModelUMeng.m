//
//  ModelShare.m
//  feibao
//
//  Created by Wiel on 15/12/19.
//  Copyright © 2015年 Facebook. All rights reserved.
//

#import "ModelUMeng.h"

#import "UMMobClick/MobClick.h"

#import "Countly.h"

@implementation UMeng

RCT_EXPORT_MODULE();

//退出事件
RCT_EXPORT_METHOD(onProfileSignOff)
{
  NSLog(@"t退出");
//  [MobClick event:@"signIn"];
  [MobClick profileSignOff];
}

//登陆事件
RCT_EXPORT_METHOD(onProfileSignIn:(NSString *)userId)
{
  NSLog(@"t登陆  %@",userId);
  [MobClick profileSignInWithPUID:userId];
  
  Countly.user.name = userId;
  [Countly.user recordUserDetails];
}
//打开页面
RCT_EXPORT_METHOD(onPageStart:(NSString *)pageName)
{
    [MobClick beginLogPageView:pageName];
  [Countly.sharedInstance startEvent:pageName];
  NSLog(@"t打开页面 %@",pageName);
}

//退出页面
RCT_EXPORT_METHOD(onPageEnd:(NSString *)pageName)
{
    [MobClick endLogPageView:pageName];
  [Countly.sharedInstance endEvent:pageName segmentation:@{} count:1 sum:0];
  NSLog(@"t关闭页面");
}
//点击事件
RCT_EXPORT_METHOD(onEvent:(NSString *)eventId)
{
  NSLog(@"onEvent  %@",eventId);
  [MobClick event:eventId];
  [Countly.sharedInstance recordEvent:eventId];
}
////点击事件
RCT_EXPORT_METHOD(onEventWithValue:(NSString *)eventId key:(NSString *)key value:(NSString *)value)
{
  NSLog(@"onEventWithValue %@   key %@   value %@",eventId,key,value);
  NSDictionary *dict = @{key : value};
  [MobClick event:eventId attributes:(NSDictionary *)dict];
  [Countly.sharedInstance recordEvent:eventId segmentation:dict];
  
}

//TODO 计算事件
@end