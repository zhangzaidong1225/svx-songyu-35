/*
 * Copyright (c) 2015, Nordic Semiconductor
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "RCTBridgeModule.h"
#import "RCTBridge.h"
#import "RCTEventDispatcher.h"
#import "DFUController.h"
//#import "DFUOperations.h"
//#import "DFUHelper.h"
//#import "DFUUpdate.h"
//#import "feibao-Swift.h"

@protocol BluetoothManagerDelegate

-(void)didDeviceConnected:(NSString *)peripheralName;
-(void)didDeviceDisconnected;
-(void)didDiscoverUARTService:(CBService *)uartService;
-(void)didDiscoverRXCharacteristic:(CBCharacteristic *)rxCharacteristic;
-(void)didDiscoverTXCharacteristic:(CBCharacteristic *)txCharacteristic;
-(void)didReceiveTXNotification:(NSData *)data;
-(void)didError:(NSString *)errorMessage;


@end

@interface BluetoothManager : NSObject <CBCentralManagerDelegate, CBPeripheralDelegate,  RCTBridgeModule, DFUControllerDelegate>

//Singleton Design pattern
+ (id)sharedInstance;


@property (strong, nonatomic) CBCentralManager *centralManager;
@property (strong, nonatomic) CBPeripheral *bluetoothPeripheral;
@property (strong, nonatomic) CBUUID *filterUUID;
@property (strong, nonatomic) NSMutableDictionary *peripherals;

//@property (strong, nonatomic) DFUUpdate *up;


@property (strong, atomic) NSString * update_path_uri;

@property (strong, nonatomic) NSTimer * update_timeout_timer;

@property (nonatomic) int mPercentage;
@property (nonatomic) int m_update_state;
@property (strong, nonatomic) NSString * m_update_key;

@property (nonatomic) BOOL mBleAction;

@property (nonatomic) BOOL isScan;




//Delegate properties for UARTViewController
@property (nonatomic, assign)id<BluetoothManagerDelegate> uartDelegate;

-(void)connectDevice:(CBPeripheral *)peripheral;
-(void)disconnectDevice:(CBPeripheral *)peripheral;
-(void)writeRXValue:(NSString *)value;
-(int)scanForPeripherals:(BOOL)enable;



@property (nonatomic, strong) CBUUID *UART_Service_UUID;
@property (nonatomic, strong) CBUUID *UART_RX_Characteristic_UUID;
@property (nonatomic, strong) CBUUID *UART_TX_Characteristic_UUID;



/*
 update start---
 */
@property (strong, nonatomic) CBCentralManager *update_centralManager;
@property (strong, nonatomic) CBPeripheral *selectedPeripheral;



@property BOOL isTransferring;
@property BOOL isTransfered;
@property BOOL isTransferCancelled;
@property BOOL isConnected;
@property BOOL isErrorKnown;

/*
 update end---
 */

@end
