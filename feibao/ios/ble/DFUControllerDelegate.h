//
//  DFUControllerDelegate.h
//  feibao
//
//  Created by Louis Liu on 16/12/7.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DFUControllerDelegate <NSObject>

@required
- (void) didProgressNumber:(int)progress;
- (void) didDFUFail;
- (void) didDFUSuccess;
@optional

@end
//func didProgressNumber(_ progress : Int)
//func didDFUFail()
//func didDFUSuccess()
