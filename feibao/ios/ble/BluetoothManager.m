/*
 * Copyright (c) 2015, Nordic Semiconductor
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#import "BluetoothManager.h"
#import "ScannedPeripheral.h"
#import "Constants.h"
#import "CharacteristicReader.h"
#import <CommonCrypto/CommonDigest.h>
#import "UMengDeviceManager.h"
#import "DFUController.h"
#import "Utils.h"

@implementation BluetoothManager

@synthesize bridge = _bridge;



RCT_EXPORT_MODULE();





+ (id)sharedInstance
{
  __strong static BluetoothManager *sharedInstance = nil;
  if (sharedInstance == nil) {
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
      sharedInstance = [self alloc];
    });
  }
    return sharedInstance;
}

-(id)init
{
    if (self = [super init]) {
        self.isScan = false;
        self.mBleAction = NO;
        self.UART_Service_UUID = [CBUUID UUIDWithString:uartServiceUUIDString];
        self.UART_TX_Characteristic_UUID = [CBUUID UUIDWithString:uartTXCharacteristicUUIDString];
        self.UART_RX_Characteristic_UUID = [CBUUID UUIDWithString:uartRXCharacteristicUUIDString];
        dispatch_queue_t centralQueue = dispatch_queue_create("no.nordicsemi.ios.nrftoolbox", DISPATCH_QUEUE_SERIAL);
        self.centralManager = [[CBCentralManager alloc]initWithDelegate:self queue:centralQueue];
        self.update_centralManager = [[CBCentralManager alloc] init];
        self.peripherals = [NSMutableDictionary dictionary];
      

        [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(auto_connect:) userInfo:nil repeats:YES];
    }

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    self.update_path_uri = [NSString stringWithFormat:@"%@/%@", [paths objectAtIndex:0], @"Version_1.zip" ];
  
  
  /*
    NSFileManager* fm=[NSFileManager defaultManager];
    NSArray *files = [fm subpathsAtPath:self.update_path_uri ];
    NSLog(@"%@", files);
   */
  
  

    return self;
}


-(void)setUARTDelegate:(id<BluetoothManagerDelegate>)uartDelegate
{
    self.uartDelegate = uartDelegate;
}

-(void)setLogDelegate:(id<BluetoothManagerDelegate>)logDelegate
{
    self.logDelegate = logDelegate;
}

/*-(void)setBluetoothCentralManager:(CBCentralManager *)manager
{
    if (manager) {
        self.centralManager = manager;
        self.centralManager.delegate = self;
    }
}
*/
-(void)connectDevice:(CBPeripheral *)peripheral
{
    if (peripheral) {
        //self.bluetoothPeripheral = peripheral;
        peripheral.delegate = self;
      [self.centralManager connectPeripheral:peripheral options:nil];
      NSLog(@"set---- connect_timeout");
      dispatch_async(dispatch_get_main_queue(), ^{
      [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(connect_timeout:) userInfo:peripheral repeats:NO];
      });
    }
}

-(void)disconnectDevice:(CBPeripheral *)peripheral
{
    if (peripheral) {
        [self.centralManager cancelPeripheralConnection:peripheral];
    }
}

-(void)writeRXValue:(NSString *)value per:(ScannedPeripheral *)per
{
    if (per.uartRXCharacteristic) {
        NSLog(@"writing command: %@ to UART peripheral: %@",value,per.peripheral.name);
        [per.peripheral writeValue:[value dataUsingEncoding:NSUTF8StringEncoding] forCharacteristic:per.uartRXCharacteristic type:CBCharacteristicWriteWithoutResponse];
    }
}

-(void)writeRX:(NSData *)value per:(ScannedPeripheral *)per
{
    [per.peripheral writeValue:value forCharacteristic:per.uartRXCharacteristic type:CBCharacteristicWriteWithoutResponse];
}

#pragma mark - CentralManager delegates
- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    NSLog(@"centralManagerDidUpdateState -- %@", central );
  
  if(central.state == CBCentralManagerStatePoweredOn)
  {
//    NSLog(@("CBCentralManagerStatePoweredOn"));
    [self read_matchd_data];
    self.mBleAction = YES;
  }
  
  if (central.state == CBCentralManagerStatePoweredOff)
  {
//    NSLog(@("CBCentralManagerStatePoweredOff"));

    self.mBleAction = NO;
    [self send_to_rndata_none];
  }
  
}

-(void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{

    ScannedPeripheral * per = [self.peripherals objectForKey:peripheral.identifier.UUIDString];
    NSLog(@"自动重连上%@", per.name);
    if(per != nil)
    {
        [peripheral discoverServices:nil];

    }
  
}

-(void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
  
    NSLog(@"didDisconnectPeripheral");
    [self.uartDelegate didDeviceDisconnected];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CBPeripheralDisconnectNotification" object:self];
    //self.bluetoothPeripheral = nil;
  
  ScannedPeripheral * per = [self.peripherals objectForKey:peripheral.identifier.UUIDString];
  if(per != nil)
  {
    [per setIsConnected:0];
    [self save_matchd_data];
    [self send_to_rndata];
  }
}

-(void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
  
  ScannedPeripheral * per = [self.peripherals objectForKey:peripheral.identifier.UUIDString];
  if(per != nil)
  {
    [per setIsConnected:0];
    [self save_matchd_data];
    [self send_to_rndata];
  }
    NSLog(@"didFailToConnectPeripheral");
    [self.uartDelegate didDeviceDisconnected];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CBPeripheralDisconnectNotification" object:self];
    //self.bluetoothPeripheral = nil;
}

#pragma mark Peripheral delegate methods

-(void) peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    NSLog(@"didDiscoverServices");
    if (!error) {
        NSLog(@"services discovered %lu",(unsigned long)[peripheral.services count] );
        for (CBService *uartService in peripheral.services) {
            NSLog(@"service discovered: %@",uartService.UUID);
            if ([uartService.UUID isEqual:self.UART_Service_UUID])
            {
                NSLog(@"UART service found");
                [self.uartDelegate didDiscoverUARTService:uartService];
                [peripheral discoverCharacteristics:nil forService:uartService];
            }
        }
    } else {
      NSLog(@"error in discovering services on device: %@",self.bluetoothPeripheral.name);
      ScannedPeripheral * per = [self.peripherals objectForKey:peripheral.identifier.UUIDString];
      if(per != nil)
      {
        [per setIsConnected:0];
        [self save_matchd_data];
        [self send_to_rndata];
      }
      [self.uartDelegate didDeviceDisconnected];
      [[NSNotificationCenter defaultCenter] postNotificationName:@"CBPeripheralDisconnectNotification" object:self];
      //self.bluetoothPeripheral = nil;
    }
}

-(void) peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    ScannedPeripheral * per = [self.peripherals valueForKey:service.peripheral.identifier.UUIDString];
    if (!error) {

      bool b_r_device = false;
      bool b_w_device = false;
      if (per != nil)
      {
        if ([service.UUID isEqual:self.UART_Service_UUID]) {
          for (CBCharacteristic *characteristic in service.characteristics)
          {
            if ([characteristic.UUID isEqual:self.UART_TX_Characteristic_UUID]) {
              NSLog(@"UART TX characteritsic is found");
              b_w_device = true;
              [self.uartDelegate didDiscoverTXCharacteristic:characteristic];
              [per.peripheral setNotifyValue:YES forCharacteristic:characteristic ];
            }
            else if ([characteristic.UUID isEqual:self.UART_RX_Characteristic_UUID]) {
              NSLog(@"UART RX characteristic is found");
              //[self.uartDelegate didDiscoverRXCharacteristic:characteristic];
              b_r_device = true;
              per.uartRXCharacteristic = characteristic;
            }
          }
        }
        
        if (b_w_device && b_r_device) {
          NSLog(@"---------------------error in discovering characteristic on device: ....");

          per.peripheral = peripheral;
          [per setIsConnected:1];
          [per setIsMatchd:true];
          [per setAutoConnect:true];
          [self save_matchd_data];
          NSLog(@"send_to_rndata_none:centralManagerdidDiscoverCharacteristicsForService" );
          //添加设备，自动设置最后添加的设备为当前选中设备
          NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
          NSLog(@"%@+%@", peripheral.identifier.UUIDString, [userDefaults stringForKey:@"select_item_addr"]);
          [userDefaults setObject:peripheral.identifier.UUIDString forKey:@"select_item_addr"];
          [self.bridge.eventDispatcher sendAppEventWithName:@"add_to_current_device" body:NULL];

          [self send_to_rndata];
          
          [self get_name_from_fireware:service.peripheral.identifier.UUIDString];
          
        }

      }
      
    } else {
        NSLog(@"error in discovering characteristic on device: ....");
        //[self disconnectDevice:per.peripheral];
    }
}

-(void) peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if (!error) {

        if (characteristic.value.length >= 3) {
          NSData *data = characteristic.value;
          uint8_t *array = (uint8_t*) data.bytes;
          uint8_t *array1 = array;
          uint8_t vValue = 0;
          

          /*
           校验数据 前面所有字节数据相加 和 最后个字节相等
           */
          
          for (int i = 0; i < characteristic.value.length-1; i++)
          {
            uint8_t tmp = [CharacteristicReader readUInt8Value:&array];
            vValue = vValue + tmp;
          }
          uint8_t vRValue = [CharacteristicReader readUInt8Value:&array];
          uint8_t phead = [CharacteristicReader readUInt8Value:&array1];
          if(vValue == vRValue && phead == 0xaa)
          {
            uint8_t *array2 = array1;
            [self parse_data:&array1 uuid:characteristic.service.peripheral.identifier.UUIDString];
            
            [[UMengDeviceManager sharedInstance] parseData:&array2 uuid:characteristic.service.peripheral.identifier.UUIDString];
          }
          
          
        }
    }
    else {
        NSLog(@"error in update UART value");
    }
}


/*
 搜索触发
 */
-(int) scanForPeripherals:(BOOL)enable
{

    if (self.centralManager.state != CBCentralManagerStatePoweredOn)
    {
      return -1;
    }
    
    // Scanner uses other queue to send events. We must edit UI in the main queue
    dispatch_async(dispatch_get_main_queue(), ^{
      
        if (enable)
        {
          
          NSLog(@"scanForPeripherals");
            NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], CBCentralManagerScanOptionAllowDuplicatesKey, nil];
          

          [self.centralManager scanForPeripheralsWithServices:nil options:options];
          self.isScan = true;
          [self send_to_rndata];

          [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(timerActionStop:) userInfo:nil repeats:NO];
        }
        else
        {

            
            [self.centralManager stopScan];
            self.isScan = false;
            [self send_to_rndata];
        }
    });
    return 0;
}

/*
 搜索call back
 */
- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
  
  
    dispatch_async(dispatch_get_main_queue(), ^{
      
      // Add the sensor to the list and reload deta set
      
      ScannedPeripheral * per = [self.peripherals objectForKey:peripheral.identifier.UUIDString];
      
      NSArray * uuids = [advertisementData valueForKey:@"kCBAdvDataServiceUUIDs"];
      
      if (uuids == nil) {
        return;
      }
      
      NSArray *keys = [advertisementData allKeys];
      NSData *dataAmb, *dataObj;
      for (int i = 0; i < [keys count]; ++i) {
        id key = [keys objectAtIndex: i];
        NSString *keyName = (NSString *) key;
        NSObject *value = [advertisementData objectForKey: key];
        if ([value isKindOfClass: [NSArray class]]) {
          printf("   key: %s\n", [keyName cStringUsingEncoding: NSUTF8StringEncoding]);
          NSArray *values = (NSArray *) value;
          for (int j = 0; j < [values count]; ++j) {
            if ([[values objectAtIndex: j] isKindOfClass: [CBUUID class]]) {
              CBUUID *uuid = [values objectAtIndex: j];
              NSData *data = uuid.data;
              if (j == 0) {
                dataObj = uuid.data;
              } else {
                dataAmb = uuid.data;
              }
              printf("      uuid(%d):", j);
              for (int j = 0; j < data.length; ++j)
                printf(" %02X", ((UInt8 *) data.bytes)[j]);
              printf("\n");
            } else {
              const char *valueString = [[value description] cStringUsingEncoding: NSUTF8StringEncoding];
              printf("      value(%d): %s\n", j, valueString);
            }
          }
        } else {
          const char *valueString = [[value description] cStringUsingEncoding: NSUTF8StringEncoding];
          printf("   key: %s, value: %s\n", [keyName cStringUsingEncoding: NSUTF8StringEncoding], valueString);
        }
      }
      
      CBUUID * uuid = nil;
      if(uuids.count > 0)
      {
        uuid = uuids[0];
      }
      
      int type = 0;

      if ([[uuid UUIDString] isEqualToString:@"7676"]) {
//        NSLog(@"now the uuid is 7676. very good.");
        type = 0;
      } else {
        if ([[uuid UUIDString] isEqualToString:@"7777"]) {
//          NSLog(@"now the uuid is 7777. very good.");
          type = 1;
        } else {
          if ([[uuid UUIDString] isEqualToString:@"7878"]) {
//            NSLog(@"now the uuid is 7878. very good.");
            type = 2;
          } else {
            return;
          }
        }
      }
      
      NSObject * values = [advertisementData valueForKey:@"kCBAdvDataManufacturerData"];
      if(values != nil) {
        const char *valuesString = [[values description] cStringUsingEncoding: NSUTF8StringEncoding];
        char value = valuesString[6];
        NSLog(@"value = %c", value);

        if(value == '1') {
          NSLog(@"now the value is 1. very good.");
          return;
        }
      }
      
      NSString * name = [advertisementData valueForKey:@"kCBAdvDataLocalName"];
      
      if(per == nil)
      {
        ScannedPeripheral* sensor = [ScannedPeripheral initWithPeripheral:peripheral rssi:RSSI.intValue isPeripheralConnected:NO isPeripheraMatchd:NO PeripheraName:name Type:type];
        [self.peripherals setObject:sensor forKey:peripheral.identifier.UUIDString];
        [self send_to_rndata];
        [self save_matchd_data];
      }
      else
      {
        [per setRSSI:RSSI.intValue];
        [per setName:name];
      }
    });
  
}


//- (void)getConnectedPeripherals {
//
//        NSArray *connectedPeripherals = [self.centralManager retrieveConnectedPeripheralsWithServices:nil];
//        NSLog(@"Connected Peripherals without filter: %lu",(unsigned long)connectedPeripherals.count);
//        for (CBPeripheral *connectedPeripheral in connectedPeripherals) {
//            NSLog(@"Connected Peripheral: %@",connectedPeripheral.name);
//            //[self addConnectedPeripheral:connectedPeripheral];
//        }
//}


-(void) send_to_rndata_none
{
  NSArray * dev_list = @[];
  NSDictionary * ret = @{@"scan_state":@(NO),@"dev_list":dev_list};
  
  [self.bridge.eventDispatcher sendAppEventWithName:@"ble_state" body:ret];
  
  
}


-(void) send_to_rndata
{
  if (!self.mBleAction) {
      NSMutableArray * dev_list1 = [[NSMutableArray alloc] init];

    
//    NSDictionary * item = @{
//                            @"isconnectd": @(1),
//                            @"ismatchd": @(YES),
//                            @"autoConnect": @(YES),
//                            @"addr": @"0000000-01",
//                            @"name": @"0000000-01",
//                            @"speed": @(60),
//                            @"battery_power": @(80),
//                            @"use_time": @(1000),
//                            @"version": @(50),
//                            @"percentage":@(0),
//                            @"update_state":@(0),
//                            @"version_type":@(0),
//                            @"ischarged":@(1),
//                            @"pm25":@(105),
//                            @"wind_time":@(1000),
//                            @"filter_in":@(1),
//                            @"use_time_this":@(1000000),
//                            @"battery_over":@(1),
//                            @"type":@(0),
//                            };
//    [dev_list1 addObject:item];
//    
//    
//    NSDictionary * item1 = @{
//                             @"isconnectd": @(1),
//                             @"ismatchd": @(YES),
//                             @"autoConnect": @(YES),
//                             @"addr": @"0000000-02",
//                             @"name": @"0000000-02",
//                             @"speed": @(60),
//                             @"battery_power": @(80),
//                             @"use_time": @(1000000),
//                             @"version": @(6),
//                             @"percentage":@(0),
//                             @"update_state":@(0),
//                             @"version_type":@(0),
//                             @"ischarged":@(1),
//                             @"Pm25":@(105),
//                             @"filter_in":@(1),
//                             @"use_time_this":@(1000000),
//                             @"wind_time":@(1000),
//                             @"battery_over":@(0),
//                             @"type":@(2),
//                             @"wind01_speed" :@(50),
//                             @"open_cap" : @(1),
//                             @"vehide_model" : @(0),
//                             };
//    [dev_list1 addObject:item1];
    
    NSDictionary * ret = @{@"scan_state":@(NO),@"dev_list":dev_list1,@"ble_state":@(self.mBleAction)};
    [self.bridge.eventDispatcher sendAppEventWithName:@"ble_state" body:ret];
    return;
  }

  
  NSLog(@"%@", self.peripherals);
  if(self.peripherals == nil) return;
  NSMutableArray * dev_list = [[NSMutableArray alloc] init];
  for (NSString * key in self.peripherals)
  {
    
    
    ScannedPeripheral *per = [self.peripherals valueForKey:key];
    NSString * name = @"未命名";
    if (per.name != nil) {
      name=per.name;
    }
    NSLog(@"上传上去的名字：%@", name);
    int percentage = 0;
    int update_state = 0;
    if ([key isEqual:self.m_update_key]) {
      percentage = self.mPercentage;
      update_state = self.m_update_state;
    }
    NSDictionary * item = @{
                            @"isconnectd": @(per.isConnected),
                            @"ismatchd": @(per.isMatchd),
                            @"autoConnect": @(per.autoConnect),
                            @"addr": key,
                            @"name": name,
                            @"speed": @(per.speed),
                            @"battery_power": @(per.battery_power),
                            @"use_time": @(per.use_time),
                            @"version": @(per.version),
                            @"percentage":@(percentage),
                            @"update_state":@(update_state),
                            @"version_type":@(per.version_type),
                            @"ischarged":@(per.battery_charge),
                            @"Pm25":@(per.pm25),
                            @"wind_time":@(per.fan_use_time_this),
                            @"use_time_this":@(per.filter_use_time_this),
                            @"filter_in":@(per.filter_in),
                            @"battery_over":@(per.battery_over),
                            @"filter_number":@(per.filter_number),
                            @"filter_use_time_this":@(per.filter_time_use_this),
                            @"filters_maxfan_usetime":@(per.filters_maxfan_usetime),
                            @"type":@(per.type),
                            @"Pm25Flag":@(per.pm25_flag),
                            @"Pm25PickTime":@(per.pm25_picktime),
                            @"space":@(per.space),
                            @"model" : @(per.model),
                            @"SystemTime" : @(per.systemTime),
                            @"filter01_in" : @(per.filter01_in),
                            @"filter01_use_time" : @(per.filter01_use_time),
                            @"filter01_use_time_this" : @(per.filter01_use_time_this),
                            @"filter01_use_time_total" : @(per.filter01_use_time_total),
                            @"wind01_speed" :@(per.wind01_speed),
                            @"filter02_in" : @(per.filter02_in),
                            @"filter02_use_time" : @(per.filter02_use_time),
                            @"filter02_use_time_this" : @(per.filter02_use_time_this),
                            @"filter02_use_time_total" : @(per.filter02_use_time_total),
                            @"wind02_speed" : @(per.wind02_speed),
                            @"filter_use_time_vehide" : @(per.filter_use_time_vehide),
                            @"filter_num_vehide" : @(per.filter_num_vehide),
                            @"vehide_week" : @(per.vehide_week),
                            @"vehide_hour" : @(per.vehide_hour),
                            @"vehide_minute" : @(per.vehide_minute),
                            @"vehide_time_state" : @(per.vehide_time_state),
                            @"vehide_model" : @(per.vehide_model),
                            @"timeFlag01" : @(1),
                            @"timeFlag02" : @(2),
                            @"timeFlag03" : @(3),
                            @"vehide_week02" : @(per.vehide_week02),
                            @"vehide_hour02" : @(per.vehide_hour02),
                            @"vehide_minute02" : @(per.vehide_minute02),
                            @"vehide_time_state02" : @(per.vehide_time_state02),
                            @"vehide_week03" : @(per.vehide_week03),
                            @"vehide_hour03" : @(per.vehide_hour03),
                            @"vehide_minute03" : @(per.vehide_minute03),
                            @"vehide_time_state03" : @(per.vehide_time_state03),
                            @"vehide_is_smart" : @(per.vehide_is_smart),
                            @"open_cap" : @(per.open_cap)
                            };
    
    [dev_list addObject:item];
    
  }
  
  
  
  NSDictionary * ret = @{@"scan_state":@(self.isScan),@"dev_list":dev_list,@"ble_state":@(self.mBleAction)};
  
  NSLog(@"%@", ret);
  
  [self.bridge.eventDispatcher sendAppEventWithName:@"ble_state" body:ret];
  
  
}


- (void)timerActionStop:(NSTimer*)timer
{
  [self scanForPeripherals:false];
  //[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(timerAction:) userInfo:nil repeats:NO];
  //NSLog(@"Hi, Timer Action for instance %@", self);
}

/*
 保存序列化数据
 */
-(void) save_matchd_data
{
  NSMutableArray * dev_list = [[NSMutableArray alloc] init];
  for (NSString * key in self.peripherals)
  {
    
    
    ScannedPeripheral *per = [self.peripherals objectForKey:key];
    NSString * name = @"未命名";
//    if (per.peripheral.name != nil) {
//      name=per.peripheral.name;
//    }
    if (per.name != nil) {
      name=per.name;
    }
    NSDictionary * item = @{
                            @"isconnectd": @(per.isConnected),
                            @"ismatchd": @(per.isMatchd),
                            @"autoConnect": @(per.autoConnect),
                            @"addr": key,
                            @"name": name,
//                            @"speed": @(per.speed),
//                            @"battery_power": @(per.battery_power),
//                            @"use_time": @(per.use_time),
                            @"dev_type":@(per.type),
                            };
    NSLog(@"save type:%d",per.type);
    [dev_list addObject:item];
    
  }
  
  NSData *archiveCarPriceData = [NSKeyedArchiver archivedDataWithRootObject:dev_list];
  [[NSUserDefaults standardUserDefaults] setObject:archiveCarPriceData forKey:@"Peripherals"];
  
}

/*
 读取序列化数据
 */
-(void) read_matchd_data
{
  NSData *myEncodedObject = [[NSUserDefaults standardUserDefaults] objectForKey:@"Peripherals"];
  NSArray * dev_list = nil;
  if(myEncodedObject == nil){
    dev_list = @[];
  }else{
    dev_list= [NSKeyedUnarchiver unarchiveObjectWithData: myEncodedObject];
  }

  if(self.peripherals == nil)
  {
    self.peripherals = [NSMutableDictionary dictionary];
  }
  for (NSDictionary * item in dev_list)
  {
    
    NSArray * pers;
    if ([[NSProcessInfo processInfo] isOperatingSystemAtLeastVersion:(NSOperatingSystemVersion){.majorVersion = 10, .minorVersion = 0, .patchVersion = 0}]) {
      pers = [self.centralManager retrievePeripheralsWithIdentifiers:@[[[NSUUID alloc]initWithUUIDString:item[@"addr"]]]];
    } else {
      pers = [self.centralManager retrievePeripheralsWithIdentifiers:@[[CBUUID UUIDWithString:item[@"addr"]]]];
    }
//    NSArray * pers = [self.centralManager retrievePeripheralsWithIdentifiers:@[[CBUUID UUIDWithString:item[@"addr"]]]];
//      NSArray * pers = [self.centralManager retrievePeripheralsWithIdentifiers:@[[[NSUUID alloc]initWithUUIDString:item[@"addr"]]]];

    if([pers count] == 0) continue;
    NSLog(@"读取的序列化数据%@", pers);
    NSNumber * dev_type = item[@"dev_type"];
    
    NSLog(@"read type:%d", [dev_type intValue]);
    ScannedPeripheral* sensor = [ScannedPeripheral initWithPeripheral:pers[0] rssi:0 isPeripheralConnected:NO isPeripheraMatchd:NO PeripheraName:item[@"name"] Type:[dev_type intValue]];
    NSNumber * num_matchd = item[@"ismatchd"];
    NSNumber * num_autoconnect = item[@"autoConnect"];
    [sensor setAutoConnect:num_autoconnect.boolValue];
    [sensor setIsMatchd:num_matchd.boolValue];
    
    [self.peripherals setObject:sensor forKey:item[@"addr"]];
    
  }
  [self send_to_rndata];
  
}


/*
 解析数据
 */
-(void) parse_data:(uint8_t **) val uuid:(NSString *) uuid
{
  ScannedPeripheral * per = [self.peripherals valueForKey:uuid];
  if (per == nil) return;
  
  bool b_chanage = false;
  
  //[per setVersion:3];

  uint8_t phead1 = [CharacteristicReader readUInt8Value:val];
  
  //应答定时开机
  if (phead1 == 0x0c) {
    NSLog(@"string = 0x0c");

  }
  
  if (phead1 == 0x15) {
    NSLog(@"string = 0x15");
    uint8_t length = [CharacteristicReader readUInt8Value:val];
//    uint8_t name = [CharacteristicReader readUInt8Value:val];
    NSData * data = [NSData dataWithBytes:(const void *)(*val) length:(length)];
//    NSData *adata = [[NSData alloc] initWithBytes:*val length:1];
//    
    NSString * name = [[NSString alloc] initWithData:data  encoding:NSUTF8StringEncoding];
    
    NSLog(@"string = %i, %@, %@",length, data, name);
    
    if (!([name isEqualToString:per.name])) {
      [per setName: name];
      b_chanage = true;
    }
    
    if(b_chanage) {
      [self send_to_rndata];
      [self save_matchd_data];
    }
    
  }
  
  if (phead1 == 0x16) {
    NSLog(@"string = 0x16");
    //TODO parse pm2.5
    
  }
  
  if (phead1 == 0x54)
  {
    uint16_t v1 = [CharacteristicReader readUInt16Value:val];
    uint16_t v1s = ((v1 & 0x00FF) << 8) | ((v1 & 0xFF00) >> 8);
    if (v1s != per.version) {
      [per setVersion:v1s];
      b_chanage = true;
    }
    if (b_chanage) {
      [self send_to_rndata];
    }
  }
  // 关机命令
  if (phead1 == 0x01)
  {
    [per setAutoConnect:NO];
    [per setIsConnected:0];
    [self send_to_rndata];
    [NSThread sleepForTimeInterval:1];
    [per setAutoConnect:YES];
  }
  // 蓝牙按钮关闭
  if (phead1 == 0x03)
  {
    [per setAutoConnect:NO];
    [self send_to_rndata];
    
  }
  
  // 手机手动断开连接
  if (phead1 == 0x07) {

    NSLog(@"string = 0x07");
  }
  
  // 应答SN号
  if (phead1 == 0x5c) {
    NSLog(@"string = 0x5c");
    //    uint8_t name = [CharacteristicReader readUInt8Value:val];
    NSData * data = [NSData dataWithBytes:(const void *)(*val) length:(15)];
    //    NSData *adata = [[NSData alloc] initWithBytes:*val length:1];
    NSString * sn = [[NSString alloc] initWithData:data  encoding:NSUTF8StringEncoding];
    NSLog(@"string = %@, %@", data, sn);
//    if((*val)[0] == 0 && (*val)[7] == 0 && (*val)[14] == 0) {
//      NSLog(@"弹出来框");
//      
//      dispatch_async(dispatch_get_main_queue(), ^(void){
//        
//        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"设置成功" message:sn delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
//        [alertview show];
//      });
//    
//    }
  }
  
  if (phead1 == 0x50)
  {
    uint8_t phead2 = [CharacteristicReader readUInt8Value:val];
    switch (phead2) {
      
      case 0x0A : {
        
        // 第一个时钟－－开关标志 0-关 1-开
        uint8_t v1 = [CharacteristicReader readSInt8Value:val];
        if (v1 != per.vehide_time_state)
        {
          [per setVehide_time_state:v1];
          b_chanage = true;
        }
        
        //第一个时钟－－分钟
        uint8_t v2 = [CharacteristicReader readSInt8Value:val];
        if (v2 != per.vehide_minute)
        {
          [per setVehide_minute:v2];
          b_chanage = true;
        }
        //第一个时钟－－小时
        uint8_t v3 = [CharacteristicReader readSInt8Value:val];
        if (v3 != per.vehide_hour)
        {
          [per setVehide_hour:v3];
          b_chanage = true;
        }
        //第一个时钟－－星期
        uint8_t v4 = [CharacteristicReader readSInt8Value:val];
        if (v4 != per.vehide_week)
        {
          [per setVehide_week:v4];
          b_chanage = true;
        }
        
        // 第二个时钟－－开关标志 0-关 1-开
        uint8_t v5 = [CharacteristicReader readSInt8Value:val];
        if (v5 != per.vehide_time_state02)
        {
          [per setVehide_time_state02:v5];
          b_chanage = true;
        }
        
        //第二个时钟－－分钟
        uint8_t v6 = [CharacteristicReader readSInt8Value:val];
        if (v6 != per.vehide_minute02)
        {
          [per setVehide_minute02:v6];
          b_chanage = true;
        }
        //第二个时钟－－小时
        uint8_t v7 = [CharacteristicReader readSInt8Value:val];
        if (v7 != per.vehide_hour02)
        {
          [per setVehide_hour02:v7];
          b_chanage = true;
        }
        //第二个时钟－－星期
        uint8_t v8 = [CharacteristicReader readSInt8Value:val];
        if (v8 != per.vehide_week02)
        {
          [per setVehide_week02:v8];
          b_chanage = true;
        }
        
        // 第三个时钟－－开关标志 0-关 1-开
        uint8_t v9 = [CharacteristicReader readSInt8Value:val];
        if (v9 != per.vehide_time_state03)
        {
          [per setVehide_time_state03:v9];
          b_chanage = true;
        }
        
        //第三个时钟－－分钟
        uint8_t v10 = [CharacteristicReader readSInt8Value:val];
        if (v10 != per.vehide_minute03)
        {
          [per setVehide_minute03:v10];
          b_chanage = true;
        }
        //第三个时钟－－小时
        uint8_t v11 = [CharacteristicReader readSInt8Value:val];
        if (v11 != per.vehide_hour03)
        {
          [per setVehide_hour03:v11];
          b_chanage = true;
        }
        //第三个时钟－－星期
        uint8_t v12 = [CharacteristicReader readSInt8Value:val];
        if (v12 != per.vehide_week03)
        {
          [per setVehide_week03:v12];
          b_chanage = true;
        }

        
        if (b_chanage) {
          NSLog(@"1s一次:0x0A");
          [self send_to_rndata];
        }
        break;
      }
      
      case 0x09 : {
        /*设备使用虑棉总时长*/
        uint32_t v1 = [CharacteristicReader readSInt32Value:val];
        uint32_t v1s = ((v1 & 0x000000FF) << 24) | ((v1 & 0x0000FF00) << 8) | ((v1 & 0x00FF0000) >> 8) | ((v1 & 0xFF000000) >> 24);
        if (v1s - per.filter_use_time_vehide > 60) {
          [per setFilter_use_time_vehide:v1s];
          b_chanage = true;
        }

        /*虑棉1净化总时长*/
        uint32_t v2 = [CharacteristicReader readSInt32Value:val];
        uint32_t v2s = ((v2 & 0x000000FF) << 24) | ((v2 & 0x0000FF00) << 8) | ((v2 & 0x00FF0000) >> 8) | ((v2 & 0xFF000000) >> 24);
        if (v2s - per.filter01_use_time_total > 60) {
          [per setFilter01_use_time_total:v2s];
          b_chanage = true;
        }
        /*虑棉2净化总时长*/
        uint32_t v3 = [CharacteristicReader readSInt32Value:val];
        uint32_t v3s = ((v3 & 0x000000FF) << 24) | ((v3 & 0x0000FF00) << 8) | ((v3 & 0x00FF0000) >> 8) | ((v3 & 0xFF000000) >> 24);
        if (v3s - per.filter02_use_time_total > 60) {
          [per setFilter02_use_time_total:v3s];
          b_chanage = true;
        }
        /*滤棉使用个数*/
        uint16_t v4 = [CharacteristicReader readSInt16Value:val];
        uint16_t v4s = ((v4 & 0x00FF) << 8) | ((v4 & 0xFF00) >> 8);
        if (v4s != per.filter_num_vehide) {
          [per setFilter_num_vehide:v4s];
          b_chanage = true;
        }
        /*风扇1等级*/
        uint8_t v5 = [CharacteristicReader readSInt8Value:val];
        if (v5 != per.wind01_speed)
        {
          [per setWind01_speed:v5];
          b_chanage = true;
        }
        /*风扇2等级*/
        uint8_t v6 = [CharacteristicReader readSInt8Value:val];
        if (v6 != per.wind02_speed)
        {
          [per setWind02_speed:v6];
          b_chanage = true;
        }
        
        if (b_chanage) {
          NSLog(@"1s一次:0x09");
          [self send_to_rndata];
        }
        break;
      }
        
      case 0x08 : {
        /*虑棉1在位*/
        uint8_t v1 = [CharacteristicReader readSInt8Value:val];
        if (v1 != per.filter01_in)
        {
          [per setFilter01_in:v1];
          b_chanage = true;
        }

        /*虑棉2在位*/
        uint8_t v2 = [CharacteristicReader readSInt8Value:val];
        if (v2 != per.filter02_in)
        {
          [per setFilter02_in:v2];
          b_chanage = true;
        }
        
        /*虑棉1使用时长*/
        uint32_t v3 = [CharacteristicReader readSInt32Value:val];
        uint32_t v3s = ((v3 & 0x000000FF) << 24) | ((v3 & 0x0000FF00) << 8) | ((v3 & 0x00FF0000) >> 8) | ((v3 & 0xFF000000) >> 24);
        if (v3s - per.filter01_use_time > 60) {
          [per setFilter01_use_time:v3s];
          b_chanage = true;
        }

        /*虑棉2使用时长*/
        uint32_t v4 = [CharacteristicReader readSInt32Value:val];
        uint32_t v4s = ((v4 & 0x000000FF) << 24) | ((v4 & 0x0000FF00) << 8) | ((v4 & 0x00FF0000) >> 8) | ((v4 & 0xFF000000) >> 24);
        if (v4s - per.filter02_use_time > 60) {
          [per setFilter02_use_time:v4s];
          b_chanage = true;
        }

        /*本次滤棉1时长*/
        uint16_t v5 = [CharacteristicReader readSInt16Value:val];
        uint16_t v5s = ((v5 & 0x00FF) << 8) | ((v5 & 0xFF00) >> 8);
        int temp_n = v5s - per.filter01_use_time_this;
        if (temp_n > 60 || temp_n < 0) {
          [per setFilter01_use_time_this:v5s];
          b_chanage = true;
        }
        /*本次滤棉2时长*/
        uint16_t v6 = [CharacteristicReader readSInt16Value:val];
        uint16_t v6s = ((v6 & 0x00FF) << 8) | ((v6 & 0xFF00) >> 8);
        int temp_n1 = v6s - per.filter02_use_time_this;
        if (temp_n1 > 60 || temp_n1 < 0) {
          [per setFilter02_use_time_this:v6s];
          b_chanage = true;
        }
        
        if (b_chanage) {
          NSLog(@"1s一次:0x08");
          [self send_to_rndata];
        }
        break;
      }
        
      case 0x07: {
        /*采集间隔时间*/
        uint16_t v1 = [CharacteristicReader readSInt16Value:val];
        uint16_t v1s = ((v1 & 0x00FF) << 8) | ((v1 & 0xFF00) >> 8);
        if (v1s != per.space) {
          [per setSpace:v1s];
          b_chanage = true;
        }
        
        /*设备采集模式*/
        uint8_t v2 = [CharacteristicReader readSInt8Value:val];
        //单品
        if(per.type == 1) {
          if (v2 != per.model)
          {
            [per setModel:v2];
            b_chanage = true;
          }
        } else {
          //车载
          if (per.type == 2) {
            if (v2 != per.vehide_is_smart) {
              [per setVehide_is_smart:v2];
              b_chanage = true;
            }
          }
        }
        
        
        /*分钟*/
        /*uint8_t v3 = */[CharacteristicReader readSInt8Value:val];
//        if (v3 != per.vehide_minute && per.type == 1) {
//          [per setVehide_minute:v3];
//          b_chanage = true;
//        }
        
        /*小时*/
       /* uint8_t v4 = */[CharacteristicReader readSInt8Value:val];
//        if (v4 != per.vehide_hour && per.type == 1) {
//          [per setVehide_hour:v3];
//          b_chanage = true;
//        }
        
        /*星期*/
        /*uint8_t v5 = */[CharacteristicReader readSInt8Value:val];
        /*有没有单品*/
        uint8_t v6 = [CharacteristicReader readSInt8Value:val];
        if (v6 != per.vehide_model) {
          [per setVehide_model:v6];
          b_chanage = true;
        }
        
        uint8_t v7 = [CharacteristicReader readSInt8Value:val];
        
        if (v7 != per.open_cap) {
          [per setOpen_cap:v7];
          b_chanage = true;
        }

        if (b_chanage) {
          NSLog(@"1s一次:0x07");
          [self send_to_rndata];
        }
        
        break;
      }
      case 0x06:
      {
        /*Pm2.5*/
        uint16_t v1 = [CharacteristicReader readSInt16Value:val];
        uint16_t v1s = ((v1 & 0x00FF) << 8) | ((v1 & 0xFF00) >> 8);
        if (v1s != per.pm25) {
          [per setPm25:v1s];
          b_chanage = true;
        }
        
        /*温度*/
        /*uint8_t v2 = */[CharacteristicReader readSInt8Value:val];
        /*湿度*/
        /*uint8_t v3 = */[CharacteristicReader readSInt8Value:val];
        /*时间戳*/
        uint32_t v4 = [CharacteristicReader readSInt32Value:val];
        uint32_t v4s = ((v4 & 0x000000FF) << 24) | ((v4 & 0x0000FF00) << 8) | ((v4 & 0x00FF0000) >> 8) | ((v4 & 0xFF000000) >> 24);
        NSLog(@"picktime:%d", v4s);
        int temp_n = v4s - per.pm25_picktime;
        if (temp_n > 60 || b_chanage) {
          [per setPm25_picktime:v4s];
          b_chanage = true;
        }
        
        /*采集标志位*/
        uint8_t v5 = [CharacteristicReader readSInt8Value:val];
        if (v5 != per.pm25_flag)
        {
          [per setPm25_flag:v5];
          b_chanage = true;
        }
        
        /*采集系统时间戳*/
        uint32_t v6 = [CharacteristicReader readSInt32Value:val];
        uint32_t v6s = ((v6 & 0x000000FF) << 24) | ((v6 & 0x0000FF00) << 8) | ((v6 & 0x00FF0000) >> 8) | ((v6 & 0xFF000000) >> 24);
        NSLog(@"System Clock:%d", v6s);
        int temp_n1 = v6s - per.systemTime;
        if (temp_n1 > 60 || b_chanage) {
          [per setSystemTime:v6s];
          b_chanage = true;
        }
        
        
        if (b_chanage) {
          NSLog(@"1s一次:0x06");
          [self send_to_rndata];
        }

        break;
      }
        
      case 0x05:
      {
        /*总时长*/
       /* uint32_t v1 = */[CharacteristicReader readUInt32Value:val];
//        uint32_t v1s = ((v1 & 0x000000FF) << 24) | ((v1 & 0x0000FF00) << 8) | ((v1 & 0x00FF0000) >> 8) | ((v1 & 0xFF000000) >> 24);
        /*开机时长*/
        /*uint32_t v2 = */[CharacteristicReader readSInt32Value:val];
//        uint32_t v2s = ((v2 & 0x000000FF) << 24) | ((v2 & 0x0000FF00) << 8) | ((v2 & 0x00FF0000) >> 8) | ((v2 & 0xFF000000) >> 24);
        /*本次开机风扇使用时长*/
        uint16_t v3 = [CharacteristicReader readSInt16Value:val];
        uint16_t v3s = ((v3 & 0x00FF) << 8) | ((v3 & 0xFF00) >> 8);
        int temp_n = v3s - per.fan_use_time_this;
        int temp_p = per.fan_use_time_this - v3s;
        if(temp_n > 10 || temp_p > 0)
        {
          [per setFan_use_time_this:v3s];
          b_chanage = true;
        }
        
        
        //NSLog(@"工作状态 v1:%X, v2:%X, v3:%X, v4:%X", v1, v2, v3, v4);
        //NSLog(@"工作状态 v1:%X, v2:%X, v3:%X, v4:%X", v1s, v2s, v3s, v4);
        if (b_chanage) {
          NSLog(@"1s一次:0x05");
          [self send_to_rndata];
        }
        
        break;
      }
      case 0x04:
      {
        /*电量*/
        uint8_t v1 = [CharacteristicReader readUInt8Value:val];
        if (v1 != per.battery_power) {
          [per setBattery_power:v1];
          b_chanage = true;
        }
        
        //NSLog(@"电量 v1:%d", v1);
        //电池电压
        /*uint8_t v2 = */[CharacteristicReader readUInt8Value:val];
        //电池在位
        /*uint8_t v3 = */[CharacteristicReader readUInt8Value:val];
        //电池交电
        uint8_t v4 = [CharacteristicReader readUInt8Value:val];
        if (v4 != per.battery_charge) {
          [per setBattery_charge:v4];
          b_chanage = true;
        }
        //交流在位
        /*uint8_t v5 = */[CharacteristicReader readUInt8Value:val];
        //充电芯片温度
        /*uint16_t v6 = */[CharacteristicReader readUInt16Value:val];
        //电池充电实时电流
        /*uint16_t v7 = */[CharacteristicReader readUInt16Value:val];
        //电池放电实时电流
        /*uint16_t v8 = */[CharacteristicReader readUInt16Value:val];
        //充电状态
        /*uint8_t v9 = */[CharacteristicReader readUInt8Value:val];
        //充电设置点流
        /*uint16_t v10 = */[CharacteristicReader readUInt16Value:val];
        //充电电压过压
        uint8_t v11 = [CharacteristicReader readUInt8Value:val];
        if (v11 != per.battery_over) {
        
          if((v11 & 0x01) == 0x01) {
//            NSLog(@("battery over 01."));
            [per setBattery_over:1];
            b_chanage = true;
          } else if((v11 & 0x02) == 0x02) {
//            NSLog(@("battery over 02."));
            [per setBattery_over:1];
            b_chanage = true;
          }else if((v11 & 0x04) == 0x04) {
//            NSLog(@("battery over 04."));
            [per setBattery_over:1];
            b_chanage = true;
          }else if((v11 & 0x08) == 0x08) {
//            NSLog(@("battery over 08."));
            [per setBattery_over:1];
            b_chanage = true;
          }else if((v11 & 0x10) == 0x10) {
//            NSLog(@("battery over 10."));
            [per setBattery_over:1];
            b_chanage = true;
          }else {
//            NSLog(@("battery over 11."));
            [per setBattery_over:0];
            b_chanage = true;
          }
        }
        
        if (b_chanage) {
          NSLog(@"1s一次:0x04");
          [self send_to_rndata];
        }
        break;
      }
      case 0x03:
      {
        //异常关机次数
        /*uint32_t v1 = */[CharacteristicReader readUInt32Value:val];
        //设备使用虑棉总时长

        uint32_t v2 = [CharacteristicReader readSInt32Value:val];
        uint32_t v2s = ((v2 & 0x000000FF) << 24) | ((v2 & 0x0000FF00) << 8) | ((v2 & 0x00FF0000) >> 8) | ((v2 & 0xFF000000) >> 24);
        if (v2s - per.filter_time_use_this > 59) {
          [per setFilter_time_use_this:v2s];
          b_chanage = true;
        }
        
        //设备滤棉折合全风速使用时长
        uint32_t v3 = [CharacteristicReader readSInt32Value:val];
        uint32_t v3s = ((v3 & 0x000000FF) << 24) | ((v3 & 0x0000FF00) << 8) | ((v3 & 0x00FF0000) >> 8) | ((v3 & 0xFF000000) >> 24);
        if (v3s - per.filters_maxfan_usetime > 59) {
          [per setFilters_maxfan_usetime:v3s];
          b_chanage = true;
        }
        
        if (b_chanage) {
          NSLog(@"1s一次:0x03");
          [self send_to_rndata];
        }
        break;
      }
      case 0x02:
      {
//        /*Pm1.0标准浓度*/
//        uint16_t v1 = [CharacteristicReader readSInt16Value:val];
//        /*Pm2.5标准浓度*/
//        uint16_t v2 = [CharacteristicReader readSInt16Value:val];
//        uint16_t v2s = ((v2 & 0x00FF) << 8) | ((v2 & 0xFF00) >> 8);
//        if (v2s != per.pm25) {
//          [per setPm25:v2s];
//          b_chanage = true;
//        }
//        
//        
//        if (b_chanage) {
//          [self send_to_rndata];
//        }
        break;
      }
      case 0x01:
      {
        /*温湿度*/;
        /*uint16_t v1 = */[CharacteristicReader readSInt16Value:val];
        /*风扇等级*/
        uint8_t v2 = [CharacteristicReader readSInt8Value:val];
        if (v2 != per.speed) {
          [per setSpeed:v2];
          b_chanage = true;
        }
        /*是否待机*/
        /*uint8_t v3 = */[CharacteristicReader readSInt8Value:val];
        /*本次滤棉使用时长*/
        uint16_t v4 = [CharacteristicReader readSInt16Value:val];
        uint16_t v4s = ((v4 & 0x00FF) << 8) | ((v4 & 0xFF00) >> 8);
        int temp_n = v4s - per.filter_use_time_this;
        int temp_p = per.filter_use_time_this - v4s;
        if(temp_n > 10 || temp_p > 0)
        {
          [per setFilter_use_time_this:v4s];
          b_chanage = true;
        }
        /*滤棉使用个数*/
        uint16_t v5 = [CharacteristicReader readSInt16Value:val];
        uint16_t v5s = ((v5 & 0x00FF) << 8) | ((v5 & 0xFF00) >> 8);
        if (v5s != per.filter_number) {
          [per setFilter_number:v5s];
          b_chanage = true;
        }
        /*滤棉使用时长（s）*/
        uint32_t v6 = [CharacteristicReader readSInt32Value:val];
        uint32_t v6s = ((v6 & 0x000000FF) << 24) | ((v6 & 0x0000FF00) << 8) | ((v6 & 0x00FF0000) >> 8) | ((v6 &0xFF000000) >> 24);

        int temp_n1 = v6s - per.use_time;
        int temp_p1 = per.use_time - v6s;
        if (temp_n1 > 60 || temp_p1 > 0) {
          NSLog(@"1s一次:0x01:use_time");
          NSLog(@"v6s=%d, per.use_time = %d, temp_n = %d, temp_p = %d",v6s, per.use_time, temp_n, temp_p);
          [per setUse_time:v6s];
          b_chanage = true;
        }
        /*滤棉在位*/
        uint8_t v7 = [CharacteristicReader readSInt8Value:val];
        if (v7 != per.filter_in)
        {
          [per setFilter_in:v7];
          b_chanage = true;
        }
        
        
        if (b_chanage) {
          NSLog(@"1s一次:0x01");
          [self send_to_rndata];
        }
        break;
      }
      default:
        NSLog(@"未找到 %d", phead2);
        break;
    }
  }
  
}


/*
 自动连接
 */
- (void)auto_connect:(NSTimer*)timer
{
  if (!self.mBleAction)
  {
    return;
  }
  for (NSString * key in self.peripherals)
  {
    ScannedPeripheral *per = [self.peripherals valueForKey:key];
    if (per.isConnected == 0 && per.autoConnect) {
        NSLog(@"auto connect -- %@", key);
      [self connectDevice:per.peripheral];
      [per setIsConnected:3];
    }
  }
}

- (void)connect_timeout:(NSTimer*)timer
{
  NSLog(@"connect_timeout");
  CBPeripheral * per = timer.userInfo;
  ScannedPeripheral *sper = [self.peripherals valueForKey:per.identifier.UUIDString];
  if (sper.isConnected == 2 || sper.isConnected == 3) {
      NSLog(@"connect timeout cancelPeripheralConnection");
      [self.centralManager cancelPeripheralConnection:per];
  }

}





/*
 获取名称
 */

- (void) get_name_from_fireware:(NSString *)key {
  ScannedPeripheral * scannedperipheral = [self.peripherals objectForKey:key];
  NSLog(@"string = get_name");
  if (scannedperipheral == nil)
  {
    return;
  }
  if (scannedperipheral.isConnected != 1)
  {
    return;
  }
  
  Byte buf[3] = {0};
  buf[0] = 0xaa;
  buf[1] = 0x14;
  buf[2] = buf[0] + buf[1];

  NSData *adata = [[NSData alloc] initWithBytes:buf length:3];
  NSLog(@"string = %@", adata);
  [self writeRX:adata per:scannedperipheral];
}



/*
 固件升级
 */
RCT_EXPORT_METHOD(rn_deviceUpdate:(NSString *)key name:(NSString *)name path:(NSString *)path)
{
  
  NSLog(@"devicUpdate:%@", path);
  
  ScannedPeripheral * scannedperipheral = [self.peripherals objectForKey:key];
  
  if (self.m_update_state != 0)
  {
    return;
  }
  
  if (scannedperipheral == nil)
  {
    return;
  }
  if (scannedperipheral.peripheral == nil)
  {
    return;
  }
  if (scannedperipheral.isConnected != 1)
  {
    return;
  }
  [scannedperipheral setAutoConnect:false];
  
  
  [self disconnectDevice:scannedperipheral.peripheral];
  
//  if ([[NSProcessInfo processInfo] isOperatingSystemAtLeastVersion:(NSOperatingSystemVersion){.majorVersion = 10, .minorVersion = 0, .patchVersion = 0}]){
  
    for (int i = 0; i < 10; i++)
    {
      if (scannedperipheral.isConnected)
      {
        [NSThread sleepForTimeInterval:1];
      }
      else
      {
        break;
      }
    }
//    [NSThread sleepForTimeInterval:5];
    // TODO error caught and delegate define and implement
    //TODO some mark need restore
    DFUController * dfuController = [DFUController sharedInstance];
    [dfuController secureDFUMode:true];
    dfuController.delegate = self;
  
    NSArray * pers;
    if ([[NSProcessInfo processInfo] isOperatingSystemAtLeastVersion:(NSOperatingSystemVersion){.majorVersion = 10, .minorVersion = 0, .patchVersion = 0}]){
      pers = [self.update_centralManager retrievePeripheralsWithIdentifiers:@[[[NSUUID alloc] initWithUUIDString:key]]];
      dfuController.centralManager = self.update_centralManager;
//      [dfuController setCentralManager:self.update_centralManager];
    } else{
      if ([[NSProcessInfo processInfo] isOperatingSystemAtLeastVersion:(NSOperatingSystemVersion){.majorVersion = 9, .minorVersion = 0, .patchVersion = 0}]) {
        pers = [self.update_centralManager retrievePeripheralsWithIdentifiers:@[[CBUUID UUIDWithString:key]]];
        dfuController.centralManager = self.update_centralManager;
//        [dfuController setCentralManager:self.update_centralManager];

//        pers = [self.centralManager retrievePeripheralsWithIdentifiers:@[[[NSUUID alloc] initWithUUIDString:key]]];
        
      } else {
        pers = [self.centralManager retrievePeripheralsWithIdentifiers:@[[CBUUID UUIDWithString:key]]];
        dfuController.centralManager = self.centralManager;
//        [dfuController setCentralManager:self.centralManager];
      }
    }
  
    
  
    
    if([pers count] == 0) {
      [self didDFUFail];
      return;
    }
  
    dfuController.selectedFileURL = [NSURL URLWithString:path];
//    [dfuController setSelectedFileURL:[NSURL URLWithString:path]];
  
    self.m_update_key = key;
    self.selectedPeripheral = pers[0];
    self.m_update_state = 1;

//
  
  dfuController.dfuPeripheral = self.selectedPeripheral;
//    [dfuController setTargetPeripheral:self.selectedPeripheral];
    [dfuController startDFUProcess];
    
}



/*
 开始搜索
 */
RCT_EXPORT_METHOD(rn_scanner)
{
  if (!self.mBleAction)
  {
    dispatch_async(dispatch_get_main_queue(), ^(void){
      UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"提示"
                                                    message:@"请开启系统蓝牙"
                                                   delegate:nil
                                          cancelButtonTitle:@"确定"
                                          otherButtonTitles:nil];
      [alert show];
    });
    return;
  }
  
  if (self.peripherals == nil) {
    return;
  }
  NSMutableArray *tmp_arr = [[NSMutableArray alloc] init];
  
  for (NSString * key in self.peripherals)
  {
    ScannedPeripheral *per = [self.peripherals objectForKey:key];
    if (!per.isMatchd) {
      [tmp_arr addObject:key];
        NSLog(@"%@", key);
    }
  }
  for (NSString * key in tmp_arr) {
    [self.peripherals removeObjectForKey:key];
  }
  [self scanForPeripherals:true];
}


/*
 连接设备
 */
RCT_EXPORT_METHOD(rn_connectDevice:(NSString *)key)
{
  
  ScannedPeripheral * scannedperipheral = [self.peripherals objectForKey:key];
  if (scannedperipheral == nil)
  {
    return;
  }
  if (scannedperipheral.peripheral == nil)
  {
    return;
  }
  if(scannedperipheral.isConnected == 0)
  {
    [self connectDevice:scannedperipheral.peripheral];
    [scannedperipheral setIsConnected:2];
  }
  if (scannedperipheral.isConnected == 3) {
    [scannedperipheral setIsConnected:2];
  }
  
  [self send_to_rndata];
  
}

/*
 停止搜索
 */
RCT_EXPORT_METHOD(rn_stopScan)
{
    [self scanForPeripherals:false];
}

/*
 获取数据
 */
RCT_EXPORT_METHOD(rn_getConnectState)
{
  NSLog(@"-------------------rn_getConnectState--------------------------");
    [self send_to_rndata];
}

/*
 取消连接
 */
RCT_EXPORT_METHOD(rn_stopConnect:(NSString *)key)
{
  ScannedPeripheral * scannedperipheral = [self.peripherals objectForKey:key];
  if (scannedperipheral == nil)
  {
    return;
  }
  if (scannedperipheral.peripheral == nil)
  {
    return;
  }
  if (scannedperipheral.isConnected != 1)
  {
    return;
  }
  [scannedperipheral setAutoConnect:false];
  NSLog(@"setAutoConnect false");
  [self save_matchd_data];
  
  [self disconnectDevice:scannedperipheral.peripheral];
}



/*
 取消配对
 */
RCT_EXPORT_METHOD(rn_cancelBond:(NSString *)key)
{
  ScannedPeripheral * per = [self.peripherals valueForKey:key];
  if (per != nil)
  {
    [per setIsMatchd:false];
    [per setAutoConnect:false];
    if (per.isConnected == 1)
    {
      [self disconnectDevice:per.peripheral];
    }
  }
  [self.peripherals removeObjectForKey:key];
  [self save_matchd_data];
  [self send_to_rndata];
  
}


/*
 调整风速
 */
RCT_EXPORT_METHOD(rn_windSpeed:(NSString *)key speed:(int)speed)
{
  ScannedPeripheral * scannedperipheral = [self.peripherals objectForKey:key];
  if (scannedperipheral == nil)
  {
    return;
  }
  if (scannedperipheral.isConnected != 1)
  {
    return;
  }
  //[scannedperipheral setSpeed:speed];
  //[self send_to_rndata];
  uint8_t tmp_speed = speed;
  Byte buf[8] = {0};
  buf[0] = 0xaa;
  buf[1] = 0x10;
  buf[2] = tmp_speed;
  buf[7] = buf[0] + buf[1] + buf[2];
  NSData *adata = [[NSData alloc] initWithBytes:buf length:8];
  NSLog(@"send buf -- %@", adata);
  [self writeRX:adata per:scannedperipheral];
  
//  [self get_name_from_fireware:key];
}


/*
 关机
 */
RCT_EXPORT_METHOD(rn_shutDown:(NSString *)key)
{
  ScannedPeripheral * scannedperipheral = [self.peripherals objectForKey:key];
  if (scannedperipheral == nil)
  {
    return;
  }
  if (scannedperipheral.isConnected != 1)
  {
    return;
  }
  
  [scannedperipheral setAutoConnect:false];

  
  Byte buf[3] = {0};
  buf[0] = 0xaa;
  buf[1] = 0x01;
  buf[2] = buf[0] + buf[1];
  NSData *adata = [[NSData alloc] initWithBytes:buf length:3];
  NSLog(@"send buf -- %@", adata);
  [self writeRX:adata per:scannedperipheral];
  
  [scannedperipheral setIsConnected:0];
  [self send_to_rndata];
  [NSThread sleepForTimeInterval:2];
  [scannedperipheral setAutoConnect:true];
}

/*
 手机主动断开连接
 */
RCT_EXPORT_METHOD(rn_disconnect:(NSString *)key)
{
  ScannedPeripheral * scannedperipheral = [self.peripherals objectForKey:key];
  if (scannedperipheral == nil)
  {
    return;
  }
  if (scannedperipheral.isConnected != 1)
  {
    return;
  }
  
  Byte buf[3] = {0};
  buf[0] = 0xaa;
  buf[1] = 0x07;
  buf[2] = buf[0] + buf[1];
  NSData *adata = [[NSData alloc] initWithBytes:buf length:3];
  NSLog(@"send buf -- %@", adata);
  [self writeRX:adata per:scannedperipheral];
}

/*
 Esmart 控制:
  0 为单次采集，1为连续采集
 */

RCT_EXPORT_METHOD(rn_esmart:(NSString *)key model:(int)model)
{
  ScannedPeripheral * scannedperipheral = [self.peripherals objectForKey:key];
  if (scannedperipheral == nil)
  {
    return;
  }
  if (scannedperipheral.isConnected != 1)
  {
    return;
  }
  
  Byte buf[4] = {0};
  buf[0] = 0xaa;
  buf[1] = 0x16;
  buf[2] = model & 0xff;
  buf[3] = buf[0] + buf[1] + buf[2];
  NSData *adata = [[NSData alloc] initWithBytes:buf length:4];
  NSLog(@"send buf -- %@", adata);
  [self writeRX:adata per:scannedperipheral];
}

/*
 设置采集时间间隔
 */
RCT_EXPORT_METHOD(rn_setInterval:(NSString *)key space:(int)space)
{
  ScannedPeripheral * scannedperipheral = [self.peripherals objectForKey:key];
  if (scannedperipheral == nil)
  {
    return;
  }
  if (scannedperipheral.isConnected != 1)
  {
    return;
  }
  
  Byte * interval = [self number_to_byte_2:space];
  
  Byte buf[5] = {0};
  buf[0] = 0xaa;
  buf[1] = 0x08;
  buf[2] = interval[0];
  buf[3] = interval[1];
  buf[4] = buf[0] + buf[1] + buf[2] + buf[3];
  NSData *adata = [[NSData alloc] initWithBytes:buf length:5];
  NSLog(@"send buf -- %@", adata);
  [self writeRX:adata per:scannedperipheral];
}

/*
 设置系统时间
 */
RCT_EXPORT_METHOD(rn_setSystemClock:(NSString *)key time:(int)time)
{
  ScannedPeripheral * scannedperipheral = [self.peripherals objectForKey:key];
  if (scannedperipheral == nil)
  {
    return;
  }
  if (scannedperipheral.isConnected != 1)
  {
    return;
  }
  
  Byte * interval = [self number_to_byte_4:time];
  
  Byte buf[7] = {0};
  buf[0] = 0xaa;
  buf[1] = 0x09;
  buf[2] = interval[0];
  buf[3] = interval[1];
  buf[4] = interval[2];
  buf[5] = interval[3];
  buf[6] = buf[0] + buf[1] + buf[2] + buf[3] + buf[4] + buf[5];
  NSData *adata = [[NSData alloc] initWithBytes:buf length:7];
  NSLog(@"send buf -- %@", adata);
  [self writeRX:adata per:scannedperipheral];
}

/*
 设置定时开机

 */

RCT_EXPORT_METHOD(rn_setVehideTime:(NSString *)key minute:(int)minute hour:(int)hour week:(int)week serial:(int)serial model:(int)model)
{
  ScannedPeripheral * scannedperipheral = [self.peripherals objectForKey:key];
  if (scannedperipheral == nil)
  {
    return;
  }
  if (scannedperipheral.isConnected != 1)
  {
    return;
  }
  
  Byte buf[8] = {0};
  buf[0] = 0xaa;
  buf[1] = 0x0c;
  buf[2] = minute & 0xff;
  buf[3] = hour & 0xff;
  buf[4] = week & 0xff;
  buf[5] = serial & 0xff;
  buf[6] = model & 0xff;
  buf[7] = buf[0] + buf[1] + buf[2] + buf[3] + buf[4] + buf[5] + buf[6];
  NSData *adata = [[NSData alloc] initWithBytes:buf length:8];
  NSLog(@"send buf -- %@", adata);
  [self writeRX:adata per:scannedperipheral];
}


/*
 读取SN号
 */
RCT_EXPORT_METHOD(rn_readSN:(NSString *)key)
{
  ScannedPeripheral * scannedperipheral = [self.peripherals objectForKey:key];
  if (scannedperipheral == nil)
  {
    return;
  }
  if (scannedperipheral.isConnected != 1)
  {
    return;
  }
  
  Byte buf[3] = {0};
  buf[0] = 0xaa;
  buf[1] = 0x5b;
  buf[2] = buf[0] + buf[1];
  NSData *adata = [[NSData alloc] initWithBytes:buf length:3];
  NSLog(@"send buf -- %@", adata);
  [self writeRX:adata per:scannedperipheral];
}
 /*
  配置SN号
  */
RCT_EXPORT_METHOD(rn_setSN:(NSString *)key)
{
  ScannedPeripheral * scannedperipheral = [self.peripherals objectForKey:key];
  if (scannedperipheral == nil)
  {
    return;
  }
  if (scannedperipheral.isConnected != 1)
  {
    return;
  }
  
  Byte buf[19] = {0};
  buf[0] = 0xaa;
  buf[1] = 0x05;
  buf[18] = buf[0] + buf[1];
  NSData *adata = [[NSData alloc] initWithBytes:buf length:19];
  NSLog(@"send buf -- %@", adata);
  [self writeRX:adata per:scannedperipheral];
}

/*
 设备确认
 */
RCT_EXPORT_METHOD(rn_devCheck:(NSString *)key)
{
  ScannedPeripheral * scannedperipheral = [self.peripherals objectForKey:key];
  if (scannedperipheral == nil)
  {
    return;
  }
  if (scannedperipheral.isConnected != 1)
  {
    return;
  }
  Byte buf[3] = {0};
  buf[0] = 0xaa;
  buf[1] = 0x13;
  buf[2] = buf[0] + buf[1];
  NSData *adata = [[NSData alloc] initWithBytes:buf length:3];
  NSLog(@"send buf -- %@", adata);
  [self writeRX:adata per:scannedperipheral];
}


/*
 设备重命名
 */
RCT_EXPORT_METHOD(rn_devRename:(NSString *)key name:(NSString *)name)
{
  
  ScannedPeripheral * scannedperipheral = [self.peripherals valueForKey:key];
  if (scannedperipheral == nil)
  {
    return;
  }
  if (scannedperipheral.isConnected != 1)
  {
    return;
  }
  
  int name_length = [name length];
  
  NSLog(@"%d", name_length);
  
  NSMutableData *name_buf = [[NSMutableData alloc]init];
  for (int i = 0; i < name_length; i++)
  {
    NSData * tmp_buf_1 = [[name substringWithRange:NSMakeRange(i, 1)] dataUsingEncoding:NSUTF8StringEncoding];
    int tmp_length_1  = [tmp_buf_1 length];
    int name_length = [name_buf length];
    if (tmp_length_1 + name_length <= 12)
    {
        [name_buf appendData:tmp_buf_1];
    } else {
      name = [name substringToIndex:(i)];
      break;
    }
  }
  
  Byte buf1[16] = {0};
//  Byte buf2[19] = {0};
  
  buf1[0] = 0xaa;
  buf1[1] = 0x12;
  buf1[2] = [name_buf length];

//  buf2[0] = 0xaa;
//  buf2[1] = 0x12;
//  buf2[2] = 0x01;
  
  Byte * b_name_buf = [name_buf bytes];
  int b_name_buf_length = [name_buf length];
  
  for (int i = 0; i < b_name_buf_length; i++)
  {
    buf1[i+3] = b_name_buf[i];
//    if (i < 15)
//    {
//      buf1[i+3] = b_name_buf[i];
//    }
//    else
//    {
//      buf2[i+3-15] = b_name_buf[i];
//    }
  }
  
  for (int i = 0; i < 15; i++) {
    buf1[15] += buf1[i];
  }


  NSData *adata1 = [[NSData alloc] initWithBytes:buf1 length:16];
  NSLog(@"send buf -- %@", adata1);
  [self writeRX:adata1 per:scannedperipheral];
 

  
  scannedperipheral.name = name ;
  
  [self send_to_rndata];
  [self save_matchd_data];
  
  
}


/*
 md5 file
 */
RCT_EXPORT_METHOD(file_md5:(NSString*)path callback:(RCTResponseSenderBlock)callback)
{
  callback(@[[NSNull null], [Utils get_file_md5:path]]);
//  NSFileHandle *handle = [NSFileHandle fileHandleForReadingAtPath:path];
//  if( handle== nil )
//  {
//    callback(@[[NSNull null], @""]);
//      //return @"ERROR GETTING FILE MD5"; // file didnt exist
//  }
//  
//  CC_MD5_CTX md5;
//  
//  CC_MD5_Init(&md5);
//  
//  BOOL done = NO;
//  while(!done)
//  {
//    NSData* fileData = [handle readDataOfLength: 256 ];
//    CC_MD5_Update(&md5, [fileData bytes], [fileData length]);
//    if( [fileData length] == 0 ) done = YES;
//  }
//  unsigned char digest[CC_MD5_DIGEST_LENGTH];
//  CC_MD5_Final(digest, &md5);
//  NSString* s = [NSString stringWithFormat: @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
//                 digest[0], digest[1],
//                 digest[2], digest[3],
//                 digest[4], digest[5],
//                 digest[6], digest[7],
//                 digest[8], digest[9],
//                 digest[10], digest[11],
//                 digest[12], digest[13],
//                 digest[14], digest[15]];
//  
//  NSLog(@"--------%@", s);
//  callback(@[[NSNull null], s]);
}

/*
 升级后回复自动连接
 */
-(void)update_after_reautoconnect
{
  
  self.centralManager.delegate = self;
  self.mPercentage = 0;
  self.m_update_state = 0;
  ScannedPeripheral * per = [self.peripherals valueForKey:self.selectedPeripheral.identifier.UUIDString];
  
  if (per != nil) {
    //升级后固件版本号复位
    [per setVersion:0];
    [per setAutoConnect:true];
  }
}

/*
 * 10进制转换Byte［4］
 */
-(Byte *)number_to_byte_4:(double) number {
  int temp = number;
  Byte * buf = malloc(4);
//  Byte buf[4] = {0};
  
  int i = 0;
  while (temp) {
    buf[4 - ++i] = (temp % 256) & 0xff;
    temp /= 256;
    NSLog(@"temp:%d", temp);
  }
  
  NSLog(@"%d", buf[3]);
  NSLog(@"%d", buf[2]);
  NSLog(@"%d", buf[1]);
  NSLog(@"%d", buf[0]);
  int size = sizeof(buf);
  NSLog(@"size:%d",size);
  return buf;
}

/*
 * 10进制转换Byte［2］
 */
-(Byte *)number_to_byte_2:(double) number {
  int temp = number;
//  Byte buf[2] = {0};
  Byte * buf = malloc(2);
  
  int i = 0;
  while (temp) {
    buf[2 - ++i] = (temp % 256) & 0xff;
    temp /= 256;
    NSLog(@"temp:%d", temp);
  }
  NSLog(@"%d", buf[1]);
  NSLog(@"%d", buf[0]);
  int size = sizeof(buf);
  NSLog(@"size:%d",size);
  return buf;
}

//MARK : - DFUControllerDelegate
-(void) didProgressNumber : (NSInteger)progress {
  self.mPercentage = (int)progress;
  if(progress < 100)
    [self send_to_rndata];
}

-(void) didDFUFail {
  NSLog(@"didDFUFail");
  self.m_update_state = 102;
  [self send_to_rndata];
  [self update_after_reautoconnect];
}

-(void) didDFUSuccess {
  NSLog(@"didDFUSuccess");
  self.m_update_state = 101;
  [self send_to_rndata];
  [self update_after_reautoconnect];
}

/*
 is platform version 10 // true or false
 */
RCT_EXPORT_METHOD(is_platform_version_10:(RCTResponseSenderBlock)callback)
{
  
  if ([[NSProcessInfo processInfo] isOperatingSystemAtLeastVersion:(NSOperatingSystemVersion){.majorVersion = 10, .minorVersion = 0, .patchVersion = 0}]) {
    callback(@[[NSNull null], @true]);
  } else {
    callback(@[[NSNull null], @false]);
  }
  
}

@end
