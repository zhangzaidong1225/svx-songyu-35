//
//  ModelShare.m
//  feibao
//
//  Created by Wiel on 15/12/19.
//  Copyright © 2015年 Facebook. All rights reserved.
//

#import "ModelShare.h"

#import <ShareSDK/ShareSDK.h>
//#import <ShareSDKExtension/SSEShareHelper.h>
#import <ShareSDKUI/ShareSDK+SSUI.h>
#import <ShareSDKUI/SSUIShareActionSheetStyle.h>
#import <ShareSDKUI/SSUIShareActionSheetCustomItem.h>
#import <ShareSDK/ShareSDK+Base.h>
#import "DeviceCountShare.h"




@interface ViewShareController ()

@end

@implementation ViewShareController

//UIImage * img_count;

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (void)dealloc{
  if (label != nil) {

  }
}


- (void)viewDidAppear:(BOOL)animated{
  //1、创建分享参数（必要）
  NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
  NSLog(@"share start");
  NSLog(self.page);
  if ([self.page isEqual:@("home")]) {
    NSLog(@"home page share.");
    CGRect frame = CGRectMake(10, 15, 300, 20);
    label = [[UILabel alloc] initWithFrame:frame];
    [self.view addSubview:label];

    //NSArray* imageArray = @[[UIImage imageNamed:@"appicon.png"]];
    NSLog(@"%@", self.img);
    [shareParams SSDKSetupShareParamsByText:self.content
                                     images:[NSURL URLWithString:self.img]
                                        url:[NSURL URLWithString:self.url]
                                      title:self.title
                                       type:SSDKContentTypeAuto];
    // 定制新浪微博的分享内容
    [shareParams SSDKSetupSinaWeiboShareParamsByText:[self.content stringByAppendingString:self.url] title:self.title image:[NSURL URLWithString:self.img] url:[NSURL URLWithString:self.url] latitude:0 longitude:0 objectID:nil type:SSDKContentTypeAuto];
    
    [shareParams SSDKSetupWeChatParamsByText:self.content title:self.title url:[NSURL URLWithString:self.url] thumbImage:[NSURL URLWithString:self.img] image:[NSURL URLWithString:self.img] musicFileURL:nil extInfo:nil fileData:nil emoticonData:nil type:SSDKContentTypeWebPage forPlatformSubType:SSDKPlatformSubTypeWechatSession];// 微信好友子平台
    [shareParams SSDKSetupWeChatParamsByText:self.content title:self.title url:[NSURL URLWithString:self.url] thumbImage:[NSURL URLWithString:self.img] image:[NSURL URLWithString:self.img] musicFileURL:nil extInfo:nil fileData:nil emoticonData:nil type:SSDKContentTypeWebPage forPlatformSubType:SSDKPlatformSubTypeWechatTimeline];// 朋友圈

    
  } else {
    if([self.page isEqualToString:@("count")]) {
      CGRect frame = CGRectMake(10, 15, 200, 20);
      label = [[UILabel alloc] initWithFrame:frame];
      [self.view addSubview:label];
      
      //NSArray* imageArray = @[[UIImage imageNamed:@"shareImg.png"]];
      NSLog(@"%@", self.img);
      NSArray *img_count = @[[[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"devicecountshare"]];
      NSArray *img_count_thumb = @[[[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"devicecountshare_thumb"]];
      
      [shareParams SSDKSetupShareParamsByText:self.content
                                       images:img_count //[NSURL URLWithString:self.img]
                                          url:[NSURL URLWithString:self.url]
                                        title:self.title
                                         type:SSDKContentTypeImage];
//      DeviceCountShare * deviceCountShare = [DeviceCountShare sharedInstance];
//      UIImage * img = [deviceCountShare createShareImageWith:self.title filtration:self.filtration usetime:self.time filternum:self.filternum events:self.array];//[deviceCountShare createShareImage:@"Louis" name:@"" number:@"5" grade:@"33"];
//      [deviceCountShare createShareImageWith:self.title filtration:self.filtration usetime:self.time filternum:self.filternum events:self.array save:@"devicecountshare"];
//      UIImage * img = [DeviceCountShare get_photo_from_app:@"devicecountshare"];
      // 定制新浪微博的分享内容
      [shareParams SSDKSetupSinaWeiboShareParamsByText:self.content title:self.title image:img_count url:[NSURL URLWithString:self.url] latitude:0 longitude:0 objectID:nil type:SSDKContentTypeAuto];
      
      [shareParams SSDKSetupWeChatParamsByText:self.content title:self.title url:nil thumbImage:img_count_thumb image:img_count musicFileURL:nil extInfo:nil fileData:nil emoticonData:nil sourceFileExtension:nil sourceFileData:nil type:SSDKContentTypeImage forPlatformSubType:SSDKPlatformSubTypeWechatSession];// 微信好友子平台
      [shareParams SSDKSetupWeChatParamsByText:self.content title:self.title url:nil thumbImage:img_count_thumb image:img_count musicFileURL:nil extInfo:nil fileData:nil emoticonData:nil sourceFileExtension:nil sourceFileData:nil type:SSDKContentTypeImage forPlatformSubType:SSDKPlatformSubTypeWechatTimeline];// 朋友圈
//      [shareParams SSDKSetupWeChatParamsByText:self.content title:self.title url:[NSURL URLWithString:self.url] thumbImage:img_count image:img_count musicFileURL:nil extInfo:nil fileData:nil emoticonData:nil type:SSDKContentTypeImage forPlatformSubType:SSDKPlatformSubTypeWechatFav];// 收藏
      
//      [shareParams SSDKSetupWeChatParamsByText:self.content title:self.title url:[NSURL URLWithString:self.url] thumbImage:img_count image:img_count musicFileURL:nil extInfo:nil fileData:nil emoticonData:nil type:SSDKContentTypeImage forPlatformSubType:SSDKPlatformTypeQQ];// 收藏
    }
  }
  

  /*
  //1.2、自定义分享平台（非必要）
  NSMutableArray *activePlatforms = [NSMutableArray arrayWithArray:[ShareSDK activePlatforms]];
  //添加一个自定义的平台（非必要）
  SSUIShareActionSheetCustomItem *item = [SSUIShareActionSheetCustomItem itemWithIcon:[UIImage imageNamed:@"Icon.png"]
                                                                                label:@"自定义"
                                                                              onClick:^{
                                                                                
                                                                                //自定义item被点击的处理逻辑
                                                                                NSLog(@"=== 自定义item被点击 ===");
                                                                                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"自定义item被点击"
                                                                                                                                    message:nil
                                                                                                                                   delegate:nil
                                                                                                                          cancelButtonTitle:@"确定"
                                                                                                                          otherButtonTitles:nil];
                                                                                [alertView show];
                                                                              }];
  [activePlatforms addObject:item];
  */
  //设置分享菜单栏样式（非必要）
  //        [SSUIShareActionSheetStyle setActionSheetBackgroundColor:[UIColor colorWithRed:249/255.0 green:0/255.0 blue:12/255.0 alpha:0.5]];
  //        [SSUIShareActionSheetStyle setActionSheetColor:[UIColor colorWithRed:21.0/255.0 green:21.0/255.0 blue:21.0/255.0 alpha:1.0]];
  //        [SSUIShareActionSheetStyle setCancelButtonBackgroundColor:[UIColor colorWithRed:21.0/255.0 green:21.0/255.0 blue:21.0/255.0 alpha:1.0]];
  //        [SSUIShareActionSheetStyle setCancelButtonLabelColor:[UIColor whiteColor]];
  //        [SSUIShareActionSheetStyle setItemNameColor:[UIColor whiteColor]];
  //        [SSUIShareActionSheetStyle setItemNameFont:[UIFont systemFontOfSize:10]];
  //        [SSUIShareActionSheetStyle setCurrentPageIndicatorTintColor:[UIColor colorWithRed:156/255.0 green:156/255.0 blue:156/255.0 alpha:1.0]];
  //        [SSUIShareActionSheetStyle setPageIndicatorTintColor:[UIColor colorWithRed:62/255.0 green:62/255.0 blue:62/255.0 alpha:1.0]];
    
  //2、分享
  [ShareSDK showShareActionSheet:label
                           items:nil
                     shareParams:shareParams
             onShareStateChanged:^(SSDKResponseState state, SSDKPlatformType platformType, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error, BOOL end) {
               
               switch (state) {
                   
                 case SSDKResponseStateBegin:
                 {
                   //[theController showLoadingView:YES];
                   break;
                 }
                 case SSDKResponseStateSuccess:
                 {
                   UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"分享成功"
                                                                       message:nil
                                                                      delegate:nil
                                                             cancelButtonTitle:@"确定"
                                                             otherButtonTitles:nil];
                   [alertView show];
                   break;
                 }
                 case SSDKResponseStateFail:
                 {
                   if (platformType == SSDKPlatformTypeSMS && [error code] == 201)
                   {
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"分享失败"
                                                                     message:@"失败原因可能是：1、短信应用没有设置帐号；2、设备不支持短信应用；3、短信应用在iOS 7以上才能发送带附件的短信。"
                                                                    delegate:nil
                                                           cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil, nil];
                     [alert show];
                     break;
                   }
                   else if(platformType == SSDKPlatformTypeMail && [error code] == 201)
                   {
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"分享失败"
                                                                     message:@"失败原因可能是：1、邮件应用没有设置帐号；2、设备不支持邮件应用；"
                                                                    delegate:nil
                                                           cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil, nil];
                     [alert show];
                     break;
                   }
                   else
                   {
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"分享失败"
                                                                     message:[NSString stringWithFormat:@"%@",error]
                                                                    delegate:nil
                                                           cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil, nil];
                     [alert show];
                     break;
                   }
                   break;
                 }
                 case SSDKResponseStateCancel:
                 {
//                   UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"分享已取消"
//                                                                       message:nil
//                                                                      delegate:nil
//                                                             cancelButtonTitle:@"确定"
//                                                             otherButtonTitles:nil];
                   
//                   [alertView show];
                   
                   break;
                 }
                 default:
                   break;
               }
               
               if (state != SSDKResponseStateBegin)
               {
                 //[theController showLoadingView:NO];
                 //[theController.tableView reloadData];
               }
               [self dismissViewControllerAnimated:YES completion:nil];
               
             }];
  
  //另附：设置跳过分享编辑页面，直接分享的平台。
  //        SSUIShareActionSheetController *sheet = [ShareSDK showShareActionSheet:view
  //                                                                         items:nil
  //                                                                   shareParams:shareParams
  //                                                           onShareStateChanged:^(SSDKResponseState state, SSDKPlatformType platformType, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error, BOOL end) {
  //                                                           }];
  //
  //        //删除和添加平台示例
  //        [sheet.directSharePlatforms removeObject:@(SSDKPlatformTypeWechat)];
  //        [sheet.directSharePlatforms addObject:@(SSDKPlatformTypeSinaWeibo)];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */



@end


@implementation Share

RCT_EXPORT_MODULE();


- (UIViewController *)getPresentedViewController
{
  UIViewController* activityViewController = nil;
  
  UIWindow *window = [[UIApplication sharedApplication] keyWindow];
  if(window.windowLevel != UIWindowLevelNormal)
  {
    NSArray *windows = [[UIApplication sharedApplication] windows];
    for(UIWindow *tmpWin in windows)
    {
      if(tmpWin.windowLevel == UIWindowLevelNormal)
      {
        window = tmpWin;
        break;
      }
    }
  }
  
  NSArray *viewsArray = [window subviews];
  if([viewsArray count] > 0)
  {
    UIView *frontView = [viewsArray objectAtIndex:0];
    
    id nextResponder = [frontView nextResponder];
    
    if([nextResponder isKindOfClass:[UIViewController class]])
    {
      activityViewController = nextResponder;
    }
    else
    {
      activityViewController = window.rootViewController;
    }
  }
  return activityViewController;
}

-(void) create_img:(NSDictionary *) dic {
  //  img = [DeviceCountShare get_photo_from_app:@"devicecountshare"];
      DeviceCountShare * deviceCountShare = [DeviceCountShare sharedInstance];
     [deviceCountShare createShareImageWith:[dic objectForKey:@"title"] filtration:[dic objectForKey:@"filtration"] usetime:[dic objectForKey:@"usetime"] filternum:[dic objectForKey:@"filternum"] events:[dic objectForKey:@"array"] save:@"devicecountshare"];
      NSString * title = [dic objectForKey:@"title"];
      NSLog([@"调用了读取图片的操作呀。。。。。" stringByAppendingString: title]);
  
//     [self performSelectorOnMainThread:@selector(set_image:) withObject:img waitUntilDone:YES];
}

//-(void) set_image:(UIImage *) image {
//  img_count = image;
//}

RCT_EXPORT_METHOD(pullMenuCount:(NSString *)title img:(NSString *)img filtration:(nonnull NSNumber *) filtration usetime:(nonnull NSNumber *)time filternum:(nonnull NSNumber *)filternum array:(NSArray *) array) {
  NSDictionary * dic = [NSDictionary dictionaryWithObjectsAndKeys:
                        title, @"title",
                        img, @"img",
                        filtration, @"filtration",
                        time, @"usetime",
                        filternum, @"filternum",
                        array, @"array",
                        nil];
  
  [NSThread detachNewThreadSelector:@selector(create_img:) toTarget:self withObject:dic];
//  DeviceCountShare * deviceCountShare = [DeviceCountShare sharedInstance];
//
//  [deviceCountShare createShareImageWith:title filtration:filtration usetime:time filternum:filternum events:array save:@"devicecountshare"];

}

RCT_EXPORT_METHOD(pullMenu:(NSString *)title url:(NSString *)url content:(NSString *)content img:(NSString *)img page:(NSString *)page filtration:(nonnull NSNumber * ) filtration usetime:(nonnull NSNumber *)time filternum:(nonnull NSNumber *) filternum array:(NSArray *) array)
{
  
  dispatch_async(dispatch_get_main_queue(), ^{
    __weak UIViewController *theController = [self getPresentedViewController];
    
    ViewShareController * vc1 = [[ViewShareController alloc] init];
    vc1.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    //vc1.view.backgroundColor = [UIColor yellowColor];
    //vc1.view.alpha = 0.5f;
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>=8.0) {
      vc1.modalPresentationStyle=UIModalPresentationOverCurrentContext;
    }
    else{
      theController.modalPresentationStyle = UIModalPresentationCurrentContext;
    }
    
    if (![title isEqual:@""]) [vc1 setTitle:title];
    if (![url isEqual:@""]) [vc1 setUrl:url];
    if (![content isEqual:@""]) [vc1 setContent:content];
    if (![img isEqual:@""]) [vc1 setImg:img];
    if (![page isEqual:@""]) [vc1 setPage:page];
    if (filtration != nil)  [vc1 setFiltration:filtration];
    if (time != nil) [vc1 setTime:time];
    if (filternum != nil) [vc1 setFilternum:filternum];
    if ([array count] > 0) {
      [vc1 setArray:array];
    }
    
    [theController presentViewController:vc1 animated:YES completion:nil];

  });

  

}

@end
