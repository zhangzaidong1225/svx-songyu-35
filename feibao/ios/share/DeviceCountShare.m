//
//  DeviceCountShare.m
//  feibao
//
//  Created by Louis Liu on 16/7/8.
//  Copyright © 2016年 Lianluo. All rights reserved.
//

#import "DeviceCountShare.h"


@implementation DeviceCountShare

float PI = 3.1415926;
CGFloat height;
CGFloat width;

+ (DeviceCountShare *)sharedInstance
{
  static DeviceCountShare *sharedInstance = nil;
  if (sharedInstance == nil) {
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
      sharedInstance = [[self alloc] init];
    });
  }
  return sharedInstance;
}

//获取事件及相应描述字典
+ (NSDictionary *) get_event_describe {
  NSDictionary * event_describe = [[NSDictionary alloc] initWithObjectsAndKeys:
                                   @"您开启了忻风健康旅程",@"初次",
                                   @"相当于云南某森林氧吧呼吸60分钟",@"60min",
                                   @"相当于云南某森林氧吧漫步3小时",@"180min",
                                   @"相当于云南某森林氧吧自驾游半天",@"360min",
                                   @"相当于云南某森林氧吧露营1夜",@"720min",
                                   @"相当于云南某森林氧吧放松1天",@"1440min",
                                   @"相当于云南某森林氧吧游玩2天",@"2880min",
                                   @"相当于云南某森林氧吧畅游3天",@"4320min",
                                   @"相当于云南某森林氧吧旅游7天",@"10080min",
                                   @"忻风，雾霾不在等风来",@"10080min",
                                   @"相当于少抽了34支二手烟",@"1m³",
                                   @"相当于少吸1.8L汽车尾气10分钟",@"5m³",
                                   @"相当于少抽了680支二手烟",@"20m³",
                                   @"相当于少吸4.0L汽车尾气50分钟",@"50m³",
                                   @"相当于少吸10L卡车尾气60分钟",@"350m³",
                                   @"相当于少抽17000支二手烟",@"500m³",
                                   @"相当于少吸4.0L汽车尾气50小时",@"3000m³",
                                   @"相当于少抽20万支二手烟",@"6000m³",
                                   @"相当于少吸10L卡车尾气60小时",@"20000m³",
                                   @"呼吸的空气充满了整座央视大裤衩",@"House一座",
                                   @"您已经使用了1个滤芯",@"1个",
                                   @"您已经使用了2个滤芯",@"2个",
                                   @"您已经使用了5个滤芯",@"5个",
                                   @"您已经使用了8个滤芯",@"8个",
                                   @"您已经使用了10个滤芯",@"10个",
                                   @"您已经使用了12个滤芯",@"12个",
                                   @"您已经使用了24个滤芯",@"24个",
                                   @"您已经使用超过48个滤芯",@"48个",
                                   nil];
  return event_describe;
}

//根据事件描述获取相应描述
-(NSString *) getDescribeWithEvent:(NSString *) event {
//  NSString * describe = [[DeviceCountShare get_event_describe] objectForKey:event];
  return [[DeviceCountShare get_event_describe] objectForKey:event];
}

//根据节点计算绘制图形的高度
-(CGFloat) calculatePhotoHeightWith:(int) count {
  return (621 + count * 106 + 21 + (count - 1) * 15 + 61);
}

-(UIImage *) createShareImageWith:(NSString *) deviceName filtration:(NSNumber *) filtration usetime:(NSNumber *)time filternum:(NSNumber *)filternum events:(NSArray *) events{
  if (events == nil || [events count] == 0) {
    return nil;
  }
  
  //数组倒叙
//  events = [[events reverseObjectEnumerator] allObjects];
  //获取图片长度
  height = [self calculatePhotoHeightWith:[events count]];
  //添加背景图片
  UIImage * backgroundImage = [ UIImage imageNamed : @"shareGrade" ];
  width = backgroundImage.size.width;
  //定义画布大小 单位为px
  CGSize size= CGSizeMake (backgroundImage. size . width , height); // 画布大小
  //生成圆形图片
  UIImage *icon = [ UIImage imageNamed : @"appicon" ];
  icon = [DeviceCountShare createCircleImage:icon borderWidth:0 borderColor:[UIColor colorWithRed:0 green:0.0 blue:1 alpha:1]];

//  UIImage *xuanzhuan = [ UIImage imageNamed : @"ic_xuanzhuan" ];
//  xuanzhuan = [DeviceCountShare createCircleImage:xuanzhuan borderWidth:0 borderColor:[UIColor colorWithRed:0 green:0.0 blue:0 alpha:0]];
  
//  [DeviceCountShare save_photo_to_app:xuanzhuan name:[@"xuanzhuan" stringByAppendingString:@"_xuanzhuan_thumb"]];

  
  // 获取上下文对象
  UIGraphicsBeginImageContextWithOptions (size, NO , 0.0 );
  //绘制背景图片
  [backgroundImage drawInRect:CGRectMake(0, 0, backgroundImage.size.width, height)];
  //绘制头像图片
  [icon drawAtPoint : CGPointMake(60, 170)];
  //绘制设备名称
  [DeviceCountShare drawTextWithString:deviceName position:CGRectMake(300, 170 - 20 + icon.size.height / 2, 300, 50) size:40 color:[UIColor whiteColor] style:@"STHeitiSC-Light" alignment: NSTextAlignmentLeft];
  
  // 获得一个位图图形上下文
  CGContextRef context= UIGraphicsGetCurrentContext ();
  CGContextDrawPath (context, kCGPathStroke );
  
  //画数据区域
  //绘制数据背景色
  //矩形，并填弃颜色
  UIColor* backgroundColor = [UIColor colorWithRed:70/255.0 green:79/255.0 blue:134/255.0 alpha:0.4];
  [DeviceCountShare drawRectangleWithContext:context position:CGRectMake(0, 417, 750, 204) fillColor:backgroundColor storkeColor:backgroundColor lineWidth:0.0];
  //画分割线
  UIImage * septalLine = [ UIImage imageNamed : @"septal_line" ];
  [DeviceCountShare drawImageWithSource:septalLine position:CGRectMake(750 / 3, 446, 3, 147)];
  [DeviceCountShare drawImageWithSource:septalLine position:CGRectMake(750 * 2 / 3, 446, 3, 147)];
  //绘制数据
  //绘制净化空气量
  [DeviceCountShare drawTextWithString:[filtration stringValue] position:CGRectMake(0, 447, 125 - (50 * [self get_integer_digit:filtration] + 44) / 2 + 50 * [self get_integer_digit:filtration], 102) size:80 color:[UIColor whiteColor] style:@"STHeitiSC-Light" alignment:NSTextAlignmentRight];
  [DeviceCountShare drawTextWithString:@("m³") position:CGRectMake(125 - (50 * [self get_integer_digit:filtration] + 44) / 2 + 50 * [self get_integer_digit:filtration] + 1, 500, 44, 40) size:30 color:[UIColor whiteColor] style:@"STHeitiSC-Light" alignment:NSTextAlignmentLeft];
  UIImage * airClear = [UIImage imageNamed:@"air_clear"];
  [DeviceCountShare drawImageWithSource:airClear position:CGRectMake(50, 565, 150, 31)];
  
  //绘制使用时长
  [DeviceCountShare drawTextWithString:[time stringValue] position:CGRectMake(250, 447, 125 - (50 * [self get_integer_digit:time] + 64) / 2 + 50 * [self get_integer_digit:time], 102) size:80 color:[UIColor whiteColor] style:@"STHeitiSC-Light" alignment:NSTextAlignmentRight];
  [DeviceCountShare drawTextWithString:@("min") position:CGRectMake(375 - (50 * [self get_integer_digit:time] + 64) / 2 + 50 * [self get_integer_digit:time] + 1, 500, 64, 40) size:30 color:[UIColor whiteColor] style:@"STHeitiSC-Light" alignment:NSTextAlignmentLeft];
  UIImage * userTime = [UIImage imageNamed:@"user_time"];
  [DeviceCountShare drawImageWithSource:userTime position:CGRectMake(315, 565, 120, 31)];
  
  //绘制滤棉使用个数
  [DeviceCountShare drawTextWithString:[filternum stringValue]position:CGRectMake(500, 447, 125 - (50 * [self get_integer_digit:filternum] + 90) / 2 + 50 * [self get_integer_digit:filternum], 102) size:80 color:[UIColor whiteColor] style:@"STHeitiSC-Light" alignment:NSTextAlignmentRight];
  [DeviceCountShare drawTextWithString:@("pieces") position:CGRectMake(625 - (50 * [self get_integer_digit:filternum] + 90) / 2 + 50 * [self get_integer_digit:filternum] + 1, 500, 90, 40) size:30 color:[UIColor whiteColor] style:@"STHeitiSC-Light" alignment:NSTextAlignmentLeft];
  UIImage * filterNum = [UIImage imageNamed:@"filter_number"];
  [DeviceCountShare drawImageWithSource:filterNum position:CGRectMake(550, 565, 150, 31)];
  
  //绘制时间轴
  //绘制线
  for(int n = 1; n <= [events count] + 1; n++) {
    CGFloat x, y, w, h;
    if(n == 1) {
      x = 174;
      y = 621;
      w = 3;
      h = 106;
    } else {
      if(n == 2) {
        x = 174;
        y = 621 + (n - 1) * 106 + (n - 1) * 21;
        w = 3;
        h = 106;
      } else {
        if(n == [events count] + 1) {
          x = 174;
          y = 621 + (n - 1) * 106 + 21 + (n - 2) * 15;
          w = 3;
          h = height - y;
        } else {
          x = 174;
          y = 621 + (n - 1) * 106 + 21 + (n - 2) * 15;
          w = 3;
          h = 106;
        }
      }
    }
    [DeviceCountShare drawImageWithSource:septalLine position:CGRectMake(x, y, w, h)];
  }
  
  //绘制时间轴节点
  for (int n = 1; n < [events count] + 1; n++) {

    CGFloat x, y, w, h;
    if ([events count] == 1) {
      //绘制大点
      UIImage * bigPoint = [ UIImage imageNamed : @"big_point" ];
      x = 165;
      y = 621 + 106;
      w = 21;
      h = 21;
      [DeviceCountShare drawImageWithSource:bigPoint position:CGRectMake(x, y, w, h)];
      //绘制时间事件标记
      x = 0;
      y = y - 10;
      w = 145;
      h = 32;
      [DeviceCountShare drawTextWithString:@"初次" position:CGRectMake(x, y, w, h) size:28 color:[UIColor whiteColor] style:@"STHeitiSC-Light" alignment:NSTextAlignmentRight];
      
      //绘制右侧透明矩形
      x = 220;
      y = y + 10 - 42;
      w = 500;
      h = 102;
      UIColor* backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.4];
      [DeviceCountShare drawArcRectangleWith:context position:CGRectMake(x, y, w, h) fillColor:backgroundColor storkeColor:backgroundColor lineWidth:0.0 radius:10];
      
      //绘制右侧事件说明
      x = 267;
      y = y + 42 - 12;
      w = 500;
      h = 36;
      [DeviceCountShare drawTextWithString:[self getDescribeWithEvent:@"初次"] position:CGRectMake(x, y, w, h) size:30 color:[UIColor whiteColor] style:@"STHeitiSC-Light" alignment:NSTextAlignmentLeft];
      break;
    }
    if(n == 1) {
      //绘制大点
      UIImage * bigPoint = [ UIImage imageNamed : @"big_point" ];
      x = 165;
      y = 621 + 106;
      w = 21;
      h = 21;
      [DeviceCountShare drawImageWithSource:bigPoint position:CGRectMake(x, y, w, h)];
      //绘制时间事件标记
      x = 0;
      y = y - 10;
      w = 145;
      h = 32;
      [DeviceCountShare drawTextWithString:[events objectAtIndex:n - 1] position:CGRectMake(x, y, w, h) size:28 color:[UIColor whiteColor] style:@"STHeitiSC-Light" alignment:NSTextAlignmentRight];
      
      //绘制右侧透明矩形
      x = 220;
      y = y + 10 - 42;
      w = 500;
      h = 102;
      UIColor* backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.4];
      [DeviceCountShare drawArcRectangleWith:context position:CGRectMake(x, y, w, h) fillColor:backgroundColor storkeColor:backgroundColor lineWidth:0.0 radius:10];
      
      //绘制右侧事件说明
      x = 267;
      y = y + 42 - 12;
      w = 500;
      h = 36;
      [DeviceCountShare drawTextWithString:[self getDescribeWithEvent:[events objectAtIndex:n - 1]] position:CGRectMake(x, y, w, h) size:30 color:[UIColor whiteColor] style:@"STHeitiSC-Light" alignment:NSTextAlignmentLeft];
    } else {
      if (n == [events count]) {
        //绘制小点
        UIImage * smallPoint = [UIImage imageNamed:@"small_point"];
        x = 168;
        y = 621 + 21 + (n - 2) * 15 + n * 106;
        w = 15;
        h = 15;
        [DeviceCountShare drawImageWithSource:smallPoint position:CGRectMake(x, y, w, h)];
        //绘制时间事件标记
        x = 0;
        y = y - 13;
        w = 145;
        h = 32;
        [DeviceCountShare drawTextWithString:@"初次" position:CGRectMake(x, y, w, h) size:28 color:[UIColor whiteColor] style:@"STHeitiSC-Light" alignment:NSTextAlignmentRight];
        
        //绘制右侧透明矩形
        x = 220;
        y = y + 13 - 45;
        w = 500;
        h = 102;
        UIColor* backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.4];
        [DeviceCountShare drawArcRectangleWith:context position:CGRectMake(x, y, w, h) fillColor:backgroundColor storkeColor:backgroundColor lineWidth:0.0 radius:10];
        
        //绘制右侧事件说明
        x = 267;
        y = y + 45 - 15;
        w = 500;
        h = 36;
        [DeviceCountShare drawTextWithString:[self getDescribeWithEvent:@"初次"] position:CGRectMake(x, y, w, h) size:30 color:[UIColor whiteColor] style:@"STHeitiSC-Light" alignment:NSTextAlignmentLeft];
      } else {
        //绘制小点
        UIImage * smallPoint = [UIImage imageNamed:@"small_point"];
        x = 168;
        y = 621 + 21 + (n - 2) * 15 + n * 106;
        w = 15;
        h = 15;
        [DeviceCountShare drawImageWithSource:smallPoint position:CGRectMake(x, y, w, h)];
        //绘制时间事件标记
        x = 0;
        y = y - 13;
        w = 145;
        h = 32;
        [DeviceCountShare drawTextWithString:[events objectAtIndex:n - 1] position:CGRectMake(x, y, w, h) size:28 color:[UIColor whiteColor] style:@"STHeitiSC-Light" alignment:NSTextAlignmentRight];
      
        //绘制右侧透明矩形
        x = 220;
        y = y + 13 - 45;
        w = 500;
        h = 102;
        UIColor* backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.4];
        [DeviceCountShare drawArcRectangleWith:context position:CGRectMake(x, y, w, h) fillColor:backgroundColor storkeColor:backgroundColor lineWidth:0.0 radius:10];
      
        //绘制右侧事件说明
        x = 267;
        y = y + 45 - 15;
        w = 500;
        h = 36;
        [DeviceCountShare drawTextWithString:[self getDescribeWithEvent:[events objectAtIndex:n - 1]] position:CGRectMake(x, y, w, h) size:30 color:[UIColor whiteColor] style:@"STHeitiSC-Light" alignment:NSTextAlignmentLeft];
      }
      
    }
  }


  UIImage *newImage= UIGraphicsGetImageFromCurrentImageContext ();
  
  UIGraphicsEndImageContext ();
  
  //保存图片到相册中
  //  UIImageWriteToSavedPhotosAlbum(newImage, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
  
  return newImage;

}

-(void) createShareImageWith:(NSString *) deviceName filtration:(NSNumber *) filtration usetime:(NSNumber *)time filternum:(NSNumber *)filternum events:(NSArray *) events save:(NSString *) name {
   UIImage * img = [self createShareImageWith:deviceName filtration:filtration usetime:time filternum:filternum events:events];
  img = [UIImage imageWithData:[self imageWithImage:img scaledToSize:CGSizeMake(width / 2, height / 2)]];
  [DeviceCountShare save_photo_to_app:img name:name];
  UIImage * thumb_img = [UIImage imageWithData:[self imageWithImage:img scaledToSize:CGSizeMake(width / 8, height / 8)]];
  [DeviceCountShare save_photo_to_app:thumb_img name:[name stringByAppendingString:@"_thumb"]];


}

// ------这种方法对图片既进行压缩，又进行裁剪
- (NSData *)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
  UIGraphicsBeginImageContext(newSize);
  [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
  UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return UIImageJPEGRepresentation(newImage, 0.8);
}

- (int) get_integer_digit:(NSNumber *) value {
  if (value == nil) {
    return 0;
  }
  
  NSString * str = [value stringValue];
  return [str length];
}

//- ( UIImage *)createShareImage:( NSString *)str name:( NSString *)name number:( NSString *)number grade:( NSString *)grade
//
//{
//  
//  //TODO 由分享的数据 计算分享图片的长 和 宽
//  
//  //添加背景图片
//  UIImage *image = [ UIImage imageNamed : @"shareGrade" ];
//  
//  //定义画布大小 单位为px
//  CGFloat width = image.size.width;
//  CGFloat height =  1334;//image.size.height;
//  CGSize size= CGSizeMake (image. size . width , 1920 ); // 画布大小
//
//  //生成圆形图片
//  UIImage *icon = [ UIImage imageNamed : @"appicon" ];
//  icon = [DeviceCountShare createCircleImage:icon borderWidth:2 borderColor:[UIColor colorWithRed:0 green:0.0 blue:1 alpha:1]];
//  
//  // 获取上下文对象
//  UIGraphicsBeginImageContextWithOptions (size, NO , 0.0 );
//  
//  //绘制背景图片
////  [image drawAtPoint : CGPointMake ( 0 , 0 )];
//  [image drawInRect:CGRectMake(0, 0, image.size.width, 1920)];
//  //绘制头像图片
//  [icon drawAtPoint : CGPointMake((width - icon.size.width) / 2, height * 170 / 1334)];
//  // 获得一个位图图形上下文
//  CGContextRef context= UIGraphicsGetCurrentContext ();
//  CGContextDrawPath (context, kCGPathStroke );
//  
//  //画数据区域
//  //绘制数据背景色
//  //矩形，并填弃颜色
//  CGContextSetLineWidth(context, 0.0);//线的宽度
//  //背景颜色#464F86, 40%
//  UIColor* backgroundColor = [UIColor colorWithRed:70/255.0 green:79/255.0 blue:134/255.0 alpha:0.4];
//  CGContextSetFillColorWithColor(context, backgroundColor.CGColor);//填充颜色
//  CGContextSetStrokeColorWithColor(context, backgroundColor.CGColor);//线框颜色
//  CGContextAddRect(context,CGRectMake(0, height*417/1334, width, height*204/1334));//画方框 x, y, width, height
//  CGContextDrawPath(context, kCGPathFillStroke);//绘画路径加填充
//  
//  //画分割线
//  UIImage * septalLine = [ UIImage imageNamed : @"septal_line" ];
//  [septalLine drawInRect:CGRectMake(width / 3, height * 446 / 1334, width * 3 / 750, height * 147 / 1334)];
//  [septalLine drawInRect:CGRectMake(width * 2/ 3, height * 446 / 1334, width * 3 / 750, height * 147 / 1334)];
//  //绘制数据
//  //绘制净化空气量
//  [@("15") drawInRect:CGRectMake(width * 55 / 750, height * 447 / 1334, width * 132 / 750, height * 102 / 1334) withAttributes:@{ NSFontAttributeName :[ UIFont fontWithName : @"Arial-BoldMT" size : 90], NSForegroundColorAttributeName :[ UIColor whiteColor ]}];
//  [@("m³") drawInRect:CGRectMake(width * 155 / 750, height * 500 / 1334, width * 44 / 750, height * 34 / 1334) withAttributes:@{ NSFontAttributeName :[ UIFont fontWithName : @"Arial-BoldMT" size : 30 ], NSForegroundColorAttributeName :[ UIColor whiteColor ] }];
//  UIImage * airClear = [UIImage imageNamed:@"air_clear"];
//  [airClear drawInRect:CGRectMake(width * 50 / 750, height * 565 / 1334, width * 150 / 750, height * 31 / 1334)];
//  
//  //绘制使用时长
//  [@("15") drawInRect:CGRectMake(width * 302 / 750, height * 447 / 1334, width * 132 / 750, height * 102 / 1334) withAttributes:@{ NSFontAttributeName :[ UIFont fontWithName : @"Arial-BoldMT" size : 90 ], NSForegroundColorAttributeName :[ UIColor whiteColor ] }];
//  [@("min") drawInRect:CGRectMake(width * 402 / 750, height * 500 / 1334, width * 60 / 750, height * 34 / 1334) withAttributes:@{ NSFontAttributeName :[ UIFont fontWithName : @"Arial-BoldMT" size : 30 ], NSForegroundColorAttributeName :[ UIColor whiteColor ] }];
//  UIImage * userTime = [UIImage imageNamed:@"user_time"];
//  [userTime drawInRect:CGRectMake(width * 315 / 750, height * 565 / 1334, width * 120 / 750, height * 31 / 1334)];
//  
//  //绘制滤棉使用个数
//  [@("1") drawInRect:CGRectMake(width * 563 / 750, height * 447 / 1334, width * 90 / 750, height * 102 / 1334) withAttributes:@{ NSFontAttributeName :[ UIFont fontWithName : @"Arial-BoldMT" size : 90 ], NSForegroundColorAttributeName :[ UIColor whiteColor ] }];
//  [@("piec") drawInRect:CGRectMake(width * 613 / 750, height * 500 / 1334, width * 100 / 750, height * 34 / 1334) withAttributes:@{ NSFontAttributeName :[ UIFont fontWithName : @"Arial-BoldMT" size : 30 ], NSForegroundColorAttributeName :[ UIColor whiteColor ] }];
//  UIImage * filterNum = [UIImage imageNamed:@"filter_number"];
//  [filterNum drawInRect:CGRectMake(width * 550 / 750, height * 565 / 1334, width * 150 / 750, height * 31 / 1334)];
//  
//  //绘制时间轴
//  //绘制线
//  for(int n = 1; n <= 6; n++) {
//    CGFloat x, y, w, h;
//    if(n == 1) {
//      x = width * 174 / 750;
//      y = height * 621 / 1334;
//      w = width * 3 / 750;
//      h = height * 106 / 1334;
//    } else {
//      if(n == 2) {
//        x = width * 174 /750;
//        y = height * 621 / 1334 + height * (n - 1) * 106 / 1334 + height * (n - 1) * 21 / 1334;
//        w = width * 3 / 750;
//        h = height * 106 / 1334;
//      } else {
//        if(n == 6) {
//          x = width * 174 /750;
//          y = height * 621 / 1334 + height * (n - 1) * 106 / 1334 + height * 21 / 1334 + height * (n - 2) * 15 / 1334;
//          w = width * 3 / 750;
//          h = height - y;
//        }
//        x = width * 174 /750;
//        y = height * 621 / 1334 + height * (n - 1) * 106 / 1334 + height * 21 / 1334 + height * (n - 2) * 15 / 1334;
//        w = width * 3 / 750;
//        h = height * 106 / 1334;
//      }
//    }
//    [septalLine drawInRect:CGRectMake(x, y, w, h)];
//  }
//  
//  //绘制时间轴节点
//  for (int n = 1; n < 6; n++) {
//    CGFloat x, y, w, h;
//    if(n == 1) {
//      //绘制大点
//      UIImage * bigPoint = [ UIImage imageNamed : @"big_point" ];
//      x = 165;
//      y = 621 + 106;
//      w = 21;
//      h = 21;
//      [bigPoint drawInRect:CGRectMake(x, y, w, h)];
//      //绘制时间事件标记
//      x = 0;
//      y = y - 4;
//      w = 145;
//      h = 32;
//      NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
//      paragraph.alignment = NSTextAlignmentRight;
//      [@("初次") drawInRect:CGRectMake(x, y, w, h) withAttributes:@{ NSFontAttributeName :[ UIFont fontWithName : @"Arial-BoldMT" size : 28], NSForegroundColorAttributeName :[ UIColor whiteColor ], NSParagraphStyleAttributeName:paragraph}];
//
//      //绘制右侧透明矩形
//      x = 220;
//      y = y + 4 - 41;
//      w = 500;
//      h = 102;
//      UIColor* backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.4];
//      CGContextSetFillColorWithColor(context, backgroundColor.CGColor);//填充颜色
//      CGContextSetStrokeColorWithColor(context, backgroundColor.CGColor);//线框颜色
//      CGContextAddRect(context,CGRectMake(x, y, w, h));//画方框 x, y, width, height
//      CGContextDrawPath(context, kCGPathFillStroke);//绘画路径加填充
//      
//      //绘制右侧事件说明
//      x = 267;
//      y = y + 41 - 4;
//      w = 500;
//      h = 32;
//      paragraph.alignment = NSTextAlignmentLeft;
//      [@("相当于少吸1.8L汽车尾气16分钟") drawInRect:CGRectMake(x, y, w, h) withAttributes:@{ NSFontAttributeName :[ UIFont fontWithName : @"Arial-BoldMT" size : 30], NSForegroundColorAttributeName :[ UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0], NSParagraphStyleAttributeName:paragraph}];
//    } else {
//      //绘制小点
//      UIImage * smallPoint = [UIImage imageNamed:@"small_point"];
//      x = 168;
//      y = 621 + 21 + (n - 2) * 15 + n * 106;
//      w = 15;
//      h = 15;
//      [smallPoint drawInRect:CGRectMake(x, y, w, h)];
//      //绘制时间事件标记
//      x = 0;
//      y = y - 7;
//      w = 145;
//      h = 32;
//      NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
//      paragraph.alignment = NSTextAlignmentRight;
//      [@("2000min") drawInRect:CGRectMake(x, y, w, h) withAttributes:@{ NSFontAttributeName :[ UIFont fontWithName : @"Arial-BoldMT" size : 28], NSForegroundColorAttributeName :[ UIColor whiteColor ], NSParagraphStyleAttributeName:paragraph}];
//
//      //绘制右侧透明矩形
//      x = 220;
//      y = y + 7 - 44;
//      w = 500;
//      h = 102;
//      UIColor* backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.4];
//      CGContextSetFillColorWithColor(context, backgroundColor.CGColor);//填充颜色
//      CGContextSetStrokeColorWithColor(context, backgroundColor.CGColor);//线框颜色
//      CGContextAddRect(context,CGRectMake(x, y, w, h));//画方框 x, y, width, height
//      CGContextDrawPath(context, kCGPathFillStroke);//绘画路径加填充
//      
//      //绘制右侧事件说明
//      x = 267;
//      y = y + 44 - 7;
//      w = 500;
//      h = 32;
//      paragraph.alignment = NSTextAlignmentLeft;
//      [@("雾霾不再等风来，您已乘上了远离雾霾的忻风列车") drawInRect:CGRectMake(x, y, w, h) withAttributes:@{ NSFontAttributeName :[ UIFont fontWithName : @"STHeitiSC-Light" size : 30], NSForegroundColorAttributeName :[ UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0 ], NSParagraphStyleAttributeName:paragraph}];
//
//    }
//  }
//  
//  
//  // 画 打败了多少用户
////  [str drawAtPoint : CGPointMake ( 30 , image. size . height * 0.65 ) withAttributes : @{ NSFontAttributeName :[ UIFont fontWithName : @"Arial-BoldMT" size : 30 ], NSForegroundColorAttributeName :[ UIColor whiteColor ] } ];
//
//  //绘制文字右对齐
////  NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
////  paragraph.alignment = NSTextAlignmentCenter;
////  [@("15") drawInRect:CGRectMake(width * 55 / 750, height * 447 / 1334, width * 132 / 750, height * 102 / 1334) withAttributes:@{ NSFontAttributeName :[ UIFont fontWithName : @"Arial-BoldMT" size : 90], NSForegroundColorAttributeName :[ UIColor whiteColor ], NSParagraphStyleAttributeName:paragraph}];
//  // 返回绘制的新图形
//  UIImage *newImage= UIGraphicsGetImageFromCurrentImageContext ();
//  
//  UIGraphicsEndImageContext ();
//  
//  //保存图片到相册中
////  UIImageWriteToSavedPhotosAlbum(newImage, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
//  
//  return newImage;
//  
//}

//绘制文字封装
+(void) drawTextWithString: (NSString *) str position:(CGRect) pos size:(CGFloat) size color:(UIColor *) color style:(NSString *) style alignment: (NSTextAlignment) alignment{
  NSMutableParagraphStyle * paragraph = [[NSMutableParagraphStyle alloc] init];
  paragraph.alignment = alignment;
  [str drawInRect:pos withAttributes:@{NSFontAttributeName:[UIFont fontWithName:style size:size], NSForegroundColorAttributeName: color, NSParagraphStyleAttributeName:paragraph}];
}
//绘制图片
+(void) drawImageWithSource: (UIImage *) image position:(CGRect) pos {
  [image drawInRect:pos];
}

//绘制填充矩形
+(void) drawRectangleWithContext: (CGContextRef) context position:(CGRect) pos fillColor:(UIColor *) fillColor storkeColor: (UIColor *)storkeColor lineWidth:(CGFloat) lineWidth{
  CGContextSetLineWidth(context, lineWidth);//线的宽度
  CGContextSetFillColorWithColor(context, fillColor.CGColor);//填充颜色
  CGContextSetStrokeColorWithColor(context, storkeColor.CGColor);//线框颜色
  CGContextAddRect(context,pos);//画方框
  CGContextDrawPath(context, kCGPathFillStroke);//绘画路径加填充
}

//绘制圆角矩形
+(void) drawArcRectangleWith: (CGContextRef) context position: (CGRect) pos fillColor:(UIColor *) fillColor storkeColor: (UIColor *)storkeColor lineWidth:(CGFloat) lineWidth radius:(CGFloat) radius {
  CGContextSetFillColorWithColor(context, fillColor.CGColor);//填充颜色
  CGContextSetLineWidth(context, lineWidth);//线的宽度
  CGContextSetStrokeColorWithColor(context, storkeColor.CGColor);//线框颜色
  
  //找到各个角点
  CGPoint p_left_top = CGPointMake(pos.origin.x, pos.origin.y);//左上方顶点
  CGPoint p_right_top = CGPointMake(pos.origin.x + pos.size.width, pos.origin.y);//右上方顶点
  CGPoint p_left_bottom = CGPointMake(pos.origin.x, pos.origin.y + pos.size.height);//左下方顶点
  CGPoint p_right_botoom = CGPointMake(pos.origin.x + pos.size.width, pos.origin.y + pos.size.height);//右下方顶点
  
  //绘制圆角
  CGContextMoveToPoint(context, p_left_top.x, p_left_top.y + radius * 2);
  CGContextAddArcToPoint(context, p_left_top.x, p_left_top.y, p_left_top.x + radius*2, p_left_top.y, radius);
  CGContextAddArcToPoint(context, p_right_top.x, p_right_top.y, p_right_top.x, p_right_top.y  + radius*2, radius);
  CGContextAddArcToPoint(context, p_right_botoom.x, p_right_botoom.y, p_right_botoom.x - radius*2, p_right_botoom.y, radius);
  CGContextAddArcToPoint(context, p_left_bottom.x, p_left_bottom.y, p_left_bottom.x, p_left_bottom.y - radius*2, radius);

  CGContextClosePath(context);
  CGContextDrawPath(context, kCGPathFillStroke); //根据坐标绘制路径
}

//创建圆形图片
+(UIImage *)createCircleImage:(UIImage *)image borderWidth:(CGFloat)borderWidth borderColor:(UIColor *)borderColor {
  //1.开启上下文
  CGFloat imageW = image.size.width ;//+ 22 * borderWidth;
  CGFloat imageH = image.size.height ;//+ 22 * borderWidth;
  CGSize imageSize = CGSizeMake(imageW, imageH);
  UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0);
  
  //2.取得当前的上下文，这里得到的就是上面刚创建的那个图片上下文
  CGContextRef ctx = UIGraphicsGetCurrentContext();
  
  //3.画边框（大圆）
  [borderColor set];
  CGFloat bigRadius = imageW * 0.5;//大圆半径
  CGFloat centerX = bigRadius;//圆心
  CGFloat centerY = bigRadius;
  CGContextAddArc(ctx, centerX, centerY, bigRadius, 0, PI * 2, 0);
  CGContextFillPath(ctx);//画圆 As a side effect when you call this function, Quartz clears the current path.
  
  //4.小圆
  CGFloat smallRadius = bigRadius - borderWidth;
  CGContextAddArc(ctx, centerX, centerY, smallRadius, 0, PI * 2, 0);
  //5.裁剪（后面画的东西才会受裁剪的影响）
  CGContextClip(ctx);
  //6.画图
  [image drawInRect:CGRectMake(borderWidth, borderWidth, image.size.width, image.size.height)];
  //7.取图
  UIImage * newImage = UIGraphicsGetImageFromCurrentImageContext();
  //8.结束上下文
  UIGraphicsEndImageContext();
  return newImage;
}

//储存到app home下的Documents目录里
+(void) save_photo_to_app: (UIImage *) image name:(NSString *) name{
  NSString *path = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:name];
  [UIImagePNGRepresentation(image) writeToFile:path atomically:YES];
  //這樣就把你要處理的圖片, 以image.png這個檔名存到app home底下的Documents目錄裡
}

//读取储存在app home下的Documents目录里的图片
+(UIImage *) get_photo_from_app: (NSString *) name{
  NSString *path = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:name];
  UIImage * image = [[UIImage alloc]initWithContentsOfFile:path];
  return image;
}

////图片保存到相册报错时回调
////- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
////{
////  NSLog(@"error:%@",error);
////}
//
//
////两张图片合成一张
//- (void) composeImage {
//  UIImage * image1 = [UIImage imageNamed:@"1.png"];
//  
//  UIImage * image2 = [UIImage imageNamed:@"2.png"];
//  
//  CGSize size = CGSizeMake (image1. size . width + image2.size.width, image1. size . height + image2.size.height);
//  
//  UIGraphicsBeginImageContext(size);
//  
//  [image1 drawInRect:CGRectMake(0, 0, size.width, size.height)];
//  
//  [image2 drawInRect:CGRectMake(30, 30, size.width - 60, size.height - 60)];
//  
//  UIImage *resultingImage =UIGraphicsGetImageFromCurrentImageContext();
//  
//  
//  UIGraphicsEndImageContext();
//  }
//
////切割图片
//-(UIImage *)clipImage: (UIImage *)image inRect: (CGRect) rect
//{
//  //获取父图片要切的位置
//  CGImageRef imageRef = CGImageCreateWithImageInRect(image.CGImage, rect);
//  //用子图片来接收切割出来的图片
//  UIImage *subImage = [UIImage imageWithCGImage:imageRef];
//  
//  return subImage;
//}
//
////1.等比率縮放
//- (UIImage *)scaleImage:(UIImage *)image toScale:(float)scaleSize
//{
//  UIGraphicsBeginImageContext(CGSizeMake(image.size.width*scaleSize,image.size.height*scaleSize));
//  [image drawInRect:CGRectMake(0, 0, image.size.width * scaleSize, image.size.height *scaleSize)];
//  UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
//  UIGraphicsEndImageContext();
//  return scaledImage;
//}
//
////2.自定長寬
//- (UIImage *)reSizeImage:(UIImage *)image toSize:(CGSize)reSize
//{
//  UIGraphicsBeginImageContext(CGSizeMake(reSize.width, reSize.height));
//  [image drawInRect:CGRectMake(0, 0, reSize.width, reSize.height)];
//  UIImage *reSizeImage = UIGraphicsGetImageFromCurrentImageContext();
//  UIGraphicsEndImageContext();
//  return reSizeImage;
//}
//
//
//
//
////儲存到手機圖片庫裡
//-(void) save_photo_to_screen {
////CGImageRef screen = UIGetScreenImage();
////UIImage* image = [UIImage imageWithCGImage:screen];
////CGImageRelease(screen);
////UIImageWriteToSavedPhotosAlbum(image, self, nil, nil);
//  //UIGetScreenImage();原本是private(私有)api, 用來截取整個畫面
//  //不過SDK 4.0後apple就開放了
//  //另外儲存到手機的圖片庫裡, 必須在實機使用, 模擬器無法使用
//}
//
//
//
//
////设置图片局部拉伸
////[photoView setContentMode:UIViewContentModeScaleAspectFill];
////photoView.clipsToBounds = YES;﻿
//
//// 覆盖drawRect方法，你可以在此自定义绘画和动画
//- (void)drawRect:(CGRect)rect
//{
//  //An opaque type that represents a Quartz 2D drawing environment.
//  //一个不透明类型的Quartz 2D绘画环境,相当于一个画布,你可以在上面任意绘画
//  CGContextRef context = UIGraphicsGetCurrentContext();
//  
//  /*写文字*/
//  CGContextSetRGBFillColor (context,  1, 0, 0, 1.0);//设置填充颜色
//  UIFont  *font = [UIFont boldSystemFontOfSize:15.0];//设置
//  [@"画圆：" drawInRect:CGRectMake(10, 20, 80, 20) withFont:font];
//  [@"画线及孤线：" drawInRect:CGRectMake(10, 80, 100, 20) withFont:font];
//  [@"画矩形：" drawInRect:CGRectMake(10, 120, 80, 20) withFont:font];
//  [@"画扇形和椭圆：" drawInRect:CGRectMake(10, 160, 110, 20) withFont:font];
//  [@"画三角形：" drawInRect:CGRectMake(10, 220, 80, 20) withFont:font];
//  [@"画圆角矩形：" drawInRect:CGRectMake(10, 260, 100, 20) withFont:font];
//  [@"画贝塞尔曲线：" drawInRect:CGRectMake(10, 300, 100, 20) withFont:font];
//  [@"图片：" drawInRect:CGRectMake(10, 340, 80, 20) withFont:font];
//  
//  /*画圆*/
//  //边框圆
//  CGContextSetRGBStrokeColor(context,1,1,1,1.0);//画笔线的颜色
//  CGContextSetLineWidth(context, 1.0);//线的宽度
//  //void CGContextAddArc(CGContextRef c,CGFloat x, CGFloat y,CGFloat radius,CGFloat startAngle,CGFloat endAngle, int clockwise)1弧度＝180°/π （≈57.3°） 度＝弧度×180°/π 360°＝360×π/180 ＝2π 弧度
//  // x,y为圆点坐标，radius半径，startAngle为开始的弧度，endAngle为 结束的弧度，clockwise 0为顺时针，1为逆时针。
//  CGContextAddArc(context, 100, 20, 15, 0, 2*PI, 0); //添加一个圆
//  CGContextDrawPath(context, kCGPathStroke); //绘制路径
//  
//  //填充圆，无边框
//  CGContextAddArc(context, 150, 30, 30, 0, 2*PI, 0); //添加一个圆
//  CGContextDrawPath(context, kCGPathFill);//绘制填充
//  
//  //画大圆并填充颜
//  UIColor*aColor = [UIColor colorWithRed:1 green:0.0 blue:0 alpha:1];
//  CGContextSetFillColorWithColor(context, aColor.CGColor);//填充颜色
//  CGContextSetLineWidth(context, 3.0);//线的宽度
//  CGContextAddArc(context, 250, 40, 40, 0, 2*PI, 0); //添加一个圆
//  //kCGPathFill填充非零绕数规则,kCGPathEOFill表示用奇偶规则,kCGPathStroke路径,kCGPathFillStroke路径填充,kCGPathEOFillStroke表示描线，不是填充
//  CGContextDrawPath(context, kCGPathFillStroke); //绘制路径加填充
//  
//  /*画线及孤线*/
//  //画线
//  CGPoint aPoints[2];//坐标点
//  aPoints[0] =CGPointMake(100, 80);//坐标1
//  aPoints[1] =CGPointMake(130, 80);//坐标2
//  //CGContextAddLines(CGContextRef c, const CGPoint points[],size_t count)
//  //points[]坐标数组，和count大小
//  CGContextAddLines(context, aPoints, 2);//添加线
//  CGContextDrawPath(context, kCGPathStroke); //根据坐标绘制路径
//  
//  //画笑脸弧线
//  //左
//  CGContextSetRGBStrokeColor(context, 0, 0, 1, 1);//改变画笔颜色
//  CGContextMoveToPoint(context, 140, 80);//开始坐标p1
//  //CGContextAddArcToPoint(CGContextRef c, CGFloat x1, CGFloat y1,CGFloat x2, CGFloat y2, CGFloat radius)
//  //x1,y1跟p1形成一条线的坐标p2，x2,y2结束坐标跟p3形成一条线的p3,radius半径,注意, 需要算好半径的长度,
//  CGContextAddArcToPoint(context, 148, 68, 156, 80, 10);
//  CGContextStrokePath(context);//绘画路径
//  
//  //右
//  CGContextMoveToPoint(context, 160, 80);//开始坐标p1
//  //CGContextAddArcToPoint(CGContextRef c, CGFloat x1, CGFloat y1,CGFloat x2, CGFloat y2, CGFloat radius)
//  //x1,y1跟p1形成一条线的坐标p2，x2,y2结束坐标跟p3形成一条线的p3,radius半径,注意, 需要算好半径的长度,
//  CGContextAddArcToPoint(context, 168, 68, 176, 80, 10);
//  CGContextStrokePath(context);//绘画路径
//  
//  //右
//  CGContextMoveToPoint(context, 150, 90);//开始坐标p1
//  //CGContextAddArcToPoint(CGContextRef c, CGFloat x1, CGFloat y1,CGFloat x2, CGFloat y2, CGFloat radius)
//  //x1,y1跟p1形成一条线的坐标p2，x2,y2结束坐标跟p3形成一条线的p3,radius半径,注意, 需要算好半径的长度,
//  CGContextAddArcToPoint(context, 158, 102, 166, 90, 10);
//  CGContextStrokePath(context);//绘画路径
//  //注，如果还是没弄明白怎么回事，请参考：http://donbe.blog.163.com/blog/static/138048021201052093633776/
//  
//  /*画矩形*/
//  CGContextStrokeRect(context,CGRectMake(100, 120, 10, 10));//画方框
//  CGContextFillRect(context,CGRectMake(120, 120, 10, 10));//填充框
//  //矩形，并填弃颜色
//  CGContextSetLineWidth(context, 2.0);//线的宽度
//  aColor = [UIColor blueColor];//blue蓝色
//  CGContextSetFillColorWithColor(context, aColor.CGColor);//填充颜色
//  aColor = [UIColor yellowColor];
//  CGContextSetStrokeColorWithColor(context, aColor.CGColor);//线框颜色
//  CGContextAddRect(context,CGRectMake(140, 120, 60, 30));//画方框
//  CGContextDrawPath(context, kCGPathFillStroke);//绘画路径
//  
//  //矩形，并填弃渐变颜色
//  //关于颜色参考http://blog.sina.com.cn/s/blog_6ec3c9ce01015v3c.html
//  //http://blog.csdn.net/reylen/article/details/8622932
//  //第一种填充方式，第一种方式必须导入类库quartcore并#import <QuartzCore/QuartzCore.h>，这个就不属于在context上画，而是将层插入到view层上面。那么这里就设计到Quartz Core 图层编程了。
//  CAGradientLayer *gradient1 = [CAGradientLayer layer];
//  gradient1.frame = CGRectMake(240, 120, 60, 30);
//  gradient1.colors = [NSArray arrayWithObjects:(id)[UIColor whiteColor].CGColor,
//                      (id)[UIColor grayColor].CGColor,
//                      (id)[UIColor blackColor].CGColor,
//                      (id)[UIColor yellowColor].CGColor,
//                      (id)[UIColor blueColor].CGColor,
//                      (id)[UIColor redColor].CGColor,
//                      (id)[UIColor greenColor].CGColor,
//                      (id)[UIColor orangeColor].CGColor,
//                      (id)[UIColor brownColor].CGColor,nil];
////  [self.layer insertSublayer:gradient1 atIndex:0];
//  //第二种填充方式
//  CGColorSpaceRef rgb = CGColorSpaceCreateDeviceRGB();
//  CGFloat colors[] =
//  {
//    1,1,1, 1.00,
//    1,1,0, 1.00,
//    1,0,0, 1.00,
//    1,0,1, 1.00,
//    0,1,1, 1.00,
//    0,1,0, 1.00,
//    0,0,1, 1.00,
//    0,0,0, 1.00,
//  };
//  CGGradientRef gradient = CGGradientCreateWithColorComponents
//  (rgb, colors, NULL, sizeof(colors)/(sizeof(colors[0])*4));//形成梯形，渐变的效果
//  CGColorSpaceRelease(rgb);
//  //画线形成一个矩形
//  //CGContextSaveGState与CGContextRestoreGState的作用
//  /*
//   CGContextSaveGState函数的作用是将当前图形状态推入堆栈。之后，您对图形状态所做的修改会影响随后的描画操作，但不影响存储在堆栈中的拷贝。在修改完成后，您可以通过CGContextRestoreGState函数把堆栈顶部的状态弹出，返回到之前的图形状态。这种推入和弹出的方式是回到之前图形状态的快速方法，避免逐个撤消所有的状态修改；这也是将某些状态（比如裁剪路径）恢复到原有设置的唯一方式。
//   */
//  CGContextSaveGState(context);
//  CGContextMoveToPoint(context, 220, 90);
//  CGContextAddLineToPoint(context, 240, 90);
//  CGContextAddLineToPoint(context, 240, 110);
//  CGContextAddLineToPoint(context, 220, 110);
//  CGContextClip(context);//context裁剪路径,后续操作的路径
//  //CGContextDrawLinearGradient(CGContextRef context,CGGradientRef gradient, CGPoint startPoint, CGPoint endPoint,CGGradientDrawingOptions options)
//  //gradient渐变颜色,startPoint开始渐变的起始位置,endPoint结束坐标,options开始坐标之前or开始之后开始渐变
//  CGContextDrawLinearGradient(context, gradient,CGPointMake
//                              (220,90) ,CGPointMake(240,110),
//                              kCGGradientDrawsAfterEndLocation);
//  CGContextRestoreGState(context);// 恢复到之前的context
//  
//  //再写一个看看效果
//  CGContextSaveGState(context);
//  CGContextMoveToPoint(context, 260, 90);
//  CGContextAddLineToPoint(context, 280, 90);
//  CGContextAddLineToPoint(context, 280, 100);
//  CGContextAddLineToPoint(context, 260, 100);
//  CGContextClip(context);//裁剪路径
//  //说白了，开始坐标和结束坐标是控制渐变的方向和形状
//  CGContextDrawLinearGradient(context, gradient,CGPointMake
//                              (260, 90) ,CGPointMake(260, 100),
//                              kCGGradientDrawsAfterEndLocation);
//  CGContextRestoreGState(context);// 恢复到之前的context
//  
//  //下面再看一个颜色渐变的圆
//  CGContextDrawRadialGradient(context, gradient, CGPointMake(300, 100), 0.0, CGPointMake(300, 100), 10, kCGGradientDrawsBeforeStartLocation);
//  
//  /*画扇形和椭圆*/
//  //画扇形，也就画圆，只不过是设置角度的大小，形成一个扇形
//  aColor = [UIColor colorWithRed:0 green:1 blue:1 alpha:1];
//  CGContextSetFillColorWithColor(context, aColor.CGColor);//填充颜色
//  //以10为半径围绕圆心画指定角度扇形
//  CGContextMoveToPoint(context, 160, 180);
//  CGContextAddArc(context, 160, 180, 30,  -60 * PI / 180, -120 * PI / 180, 1);
//  CGContextClosePath(context);
//  CGContextDrawPath(context, kCGPathFillStroke); //绘制路径
//  
//  //画椭圆
//  CGContextAddEllipseInRect(context, CGRectMake(160, 180, 20, 8)); //椭圆
//  CGContextDrawPath(context, kCGPathFillStroke);
//  
//  /*画三角形*/
//  //只要三个点就行跟画一条线方式一样，把三点连接起来
//  CGPoint sPoints[3];//坐标点
//  sPoints[0] =CGPointMake(100, 220);//坐标1
//  sPoints[1] =CGPointMake(130, 220);//坐标2
//  sPoints[2] =CGPointMake(130, 160);//坐标3
//  CGContextAddLines(context, sPoints, 3);//添加线
//  CGContextClosePath(context);//封起来
//  CGContextDrawPath(context, kCGPathFillStroke); //根据坐标绘制路径
//  
//  /*画圆角矩形*/
//  float fw = 180;
//  float fh = 280;
//  
//  CGContextMoveToPoint(context, fw, fh-20);  // 开始坐标右边开始
//  CGContextAddArcToPoint(context, fw, fh, fw-20, fh, 10);  // 右下角角度
//  CGContextAddArcToPoint(context, 120, fh, 120, fh-20, 10); // 左下角角度
//  CGContextAddArcToPoint(context, 120, 250, fw-20, 250, 10); // 左上角
//  CGContextAddArcToPoint(context, fw, 250, fw, fh-20, 10); // 右上角
//  CGContextClosePath(context);
//  CGContextDrawPath(context, kCGPathFillStroke); //根据坐标绘制路径
//  
//  /*画贝塞尔曲线*/
//  //二次曲线
//  CGContextMoveToPoint(context, 120, 300);//设置Path的起点
//  CGContextAddQuadCurveToPoint(context,190, 310, 120, 390);//设置贝塞尔曲线的控制点坐标和终点坐标
//  CGContextStrokePath(context);
//  //三次曲线函数
//  CGContextMoveToPoint(context, 200, 300);//设置Path的起点
//  CGContextAddCurveToPoint(context,250, 280, 250, 400, 280, 300);//设置贝塞尔曲线的控制点坐标和控制点坐标终点坐标
//  CGContextStrokePath(context);
//  
//  
//  /*图片*/
//  UIImage *image = [UIImage imageNamed:@"apple.jpg"];
//  [image drawInRect:CGRectMake(60, 340, 20, 20)];//在坐标中画出图片
//  //    [image drawAtPoint:CGPointMake(100, 340)];//保持图片大小在point点开始画图片，可以把注释去掉看看
//  CGContextDrawImage(context, CGRectMake(100, 340, 20, 20), image.CGImage);//使用这个使图片上下颠倒了，参考http://blog.csdn.net/koupoo/article/details/8670024
//  
//  //    CGContextDrawTiledImage(context, CGRectMake(0, 0, 20, 20), image.CGImage);//平铺图
//  
//}
//
////drawRect用法
////CustomView *customView = [[CustomView alloc]initWithFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
////    [self.view addSubview:customView];
//
//
//
////生成圆角图片
//- (id)createRoundedRectImage:(UIImage*)image size:(CGSize)size radius:(NSInteger)r
//{
//  // the size of CGContextRef
//  int w = size.width;
//  int h = size.height;
//  
//  UIImage *img = image;
//  CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
//  CGContextRef context = CGBitmapContextCreate(NULL, w, h, 8, 4 * w, colorSpace, kCGImageAlphaPremultipliedFirst);
//  CGRect rect = CGRectMake(0, 0, w, h);
//  
//  CGContextBeginPath(context);
//  addRoundedRectToPath(context, rect, r, r);
//  CGContextClosePath(context);
//  CGContextClip(context);
//  CGContextDrawImage(context, CGRectMake(0, 0, w, h), img.CGImage);
//  CGImageRef imageMasked = CGBitmapContextCreateImage(context);
//  img = [UIImage imageWithCGImage:imageMasked];
//  
//  CGContextRelease(context);
//  CGColorSpaceRelease(colorSpace);
//  CGImageRelease(imageMasked);
//  
//  return img;
//}
//
////生成圆角图片
//void addRoundedRectToPath(CGContextRef context, CGRect rect, float ovalWidth,
//                          float ovalHeight)
//{
//  float fw, fh;
//  
//  if (ovalWidth == 0 || ovalHeight == 0)
//  {
//    CGContextAddRect(context, rect);
//    return;
//  }
//  
//  CGContextSaveGState(context);
//  CGContextTranslateCTM(context, CGRectGetMinX(rect), CGRectGetMinY(rect));
//  CGContextScaleCTM(context, ovalWidth, ovalHeight);
//  fw = CGRectGetWidth(rect) / ovalWidth;
//  fh = CGRectGetHeight(rect) / ovalHeight;
//  
//  CGContextMoveToPoint(context, fw, fh/2);  // Start at lower right corner
//  CGContextAddArcToPoint(context, fw, fh, fw/2, fh, 1);  // Top right corner
//  CGContextAddArcToPoint(context, 0, fh, 0, fh/2, 1); // Top left corner
//  CGContextAddArcToPoint(context, 0, 0, fw/2, 0, 1); // Lower left corner
//  CGContextAddArcToPoint(context, fw, 0, fw, fh/2, 1); // Back to lower right
//  
//  CGContextClosePath(context);
//  CGContextRestoreGState(context);
//}




@end
