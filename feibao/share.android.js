'use strict';

import {
  NativeModules,
} from 'react-native';


var share = {
	// 'pullMenu':function(title, url, content, img){
	// 	NativeModules.Share.pullMenu(title, url, content, img);
	// },
	'pullMenu':function(title, url, content, img, page, filtration, time, filternum, array){
		NativeModules.Share.pullMenu(title, url, content, img, page, filtration, time, filternum, array);
	},
	'CanvasBitmap':function(title,filtration,time,filternum,array){
		NativeModules.Share.CanvasBitmap(title,filtration,time,filternum,array);
	}
};
module.exports = share;