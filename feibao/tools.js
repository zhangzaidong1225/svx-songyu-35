/**
* Tools:
* alter,
* 
*/
'use strict';

var Debug = false;
var UMeng = require('./UMeng');


import React, { Component } from 'react';
import Language from './Language'
import {
	Alert,
	Platform,
	ToastAndroid,
} from 'react-native';


var tools = {
	'gotoOtherPage':function(title,hasUmeng,navigator){
	  navigator.push({
        id:title,
      });
      if(hasUmeng!==''){
         UMeng.onEvent(hasUmeng);
      }
	},
	'goBack':function(navigator){
      navigator.pop();
	},
	'alertShow':function(message: string){
		if (Platform.OS === 'android') {
			this.alertAndroidShow(message, ToastAndroid.SHORT);
		} else {
			this.alertIOSShow(message);
		}
	},

	// 'alertShow':function(message: string, duration: number){
	// 	if (Platform.OS === 'android') {
	// 		alterAndroidShow(message);
	// 	} else {
	// 		alterIOSShow(message);
	// 	}
	// },

	'alertAndroidShow':function(message: string, duration: number) {
		ToastAndroid.show(message, duration === null ? duration:ToastAndroid.SHORT);
	},

	'alertIOSShow':function(message: string) {
		Alert.alert(
			Language.prompt+'', 
			message, 
			[
              {text: Language.ok+'', onPress: () => {console.log('OK Pressed!');UMeng.onEvent('tools_01');}},
            ]
		)
	},

	'alertDebugShow': function(message: string) {
		if(Debug) {
			this.alertShow(message);
		}
	}
};


module.exports = tools;