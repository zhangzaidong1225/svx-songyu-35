'use strict';

var { NativeModules } = require('react-native');


var BlueToothUtil = {
	'auto_connect':function(){
		NativeModules.BlueToothUtil.auto_connect();
	},
	'sendMydevice':function(){
		NativeModules.BlueToothUtil.sendMydevice();
	},
	'scanner':function(){
		NativeModules.BlueToothUtil.scanner();
	},
	'stopScan':function(){
		NativeModules.BlueToothUtil.stopScan();
	},
	'stopConnect':function(address){
		NativeModules.BlueToothUtil.stopConnect(address);
	},
	'cancelBond':function(address){
		NativeModules.BlueToothUtil.cancelBond(address);
	},
	'connectDevice':function(address){
		NativeModules.BlueToothUtil.connectDevice(address);
	},
	'shutDown':function(address){
		NativeModules.BlueToothUtil.shutDown(address);
	},
	'windSpeed':function(address,speed){
		NativeModules.BlueToothUtil.windSpeed(address,speed);
	},
	'deviceUpdate':function(address,name,path){
		NativeModules.BlueToothUtil.deviceUpdate(address,name,path);
	},
	'getHardwareVersion':function(address){
		NativeModules.BlueToothUtil.getHardwareVersion(address);
	},
	'devCheck':function(address){
		NativeModules.BlueToothUtil.devCheck(address);
	},
	'devRename':function(address, name){
		NativeModules.BlueToothUtil.devRename(address, name);
	},
	'verifyInstallPackage':function (path,callback){
		NativeModules.BlueToothUtil.verifyInstallPackage(path,callback);
	},

	'disconnect':function(address){
		NativeModules.BlueToothUtil.disconnect(address);
	},
	'eSmart':function(address,model){
		NativeModules.BlueToothUtil.esmart(address,model);
	},
	'setInterval':function(address,space){
		NativeModules.BlueToothUtil.setInterval(address,space);
	},
	'setSystemClock':function(address,time){
		NativeModules.BlueToothUtil.setSystemClock(address,time);
	},
	
	'setVehideTime':function(address,hour,minute,week,type,mode){
		NativeModules.BlueToothUtil.setVehideTime(address,hour,minute,week,type,mode);
	},
	'unZip':function(path){
		NativeModules.BlueToothUtil.unZip(path);
	},
	'filterFactor':function(address,factor){
		NativeModules.BlueToothUtil.filterFactor(address,factor);
	},

};
module.exports = BlueToothUtil;