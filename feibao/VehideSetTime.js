'use strict'

var TimerMixin = require ('react-timer-mixin');
var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter'); /*用于界面组件之间传递消息*/
var AsyncStorage = require('./AsyncStorage');
var UMeng = require('./UMeng');

import Title from './title'
var BlueToothUtil= require('./BlueToothUtil');
import Language from './Language'

import React, { Component } from 'react';
import {
    AppRegistry,
    styles_sliderheet,
    Text,
    View,
    PanResponder,
    Image,
    Dimensions,
    TouchableOpacity,
    TouchableWithoutFeedback,
    TouchableNativeFeedback,
    DeviceEventEmitter,
    NativeModules,
    ToastAndroid,
    NativeAppEventEmitter,
    Platform,
    Switch,
} from 'react-native';


var deviceWidth  = Dimensions.get ('window').width;
var deviceHeight = Dimensions.get ('window').height;
var msg = '为您和家人设定的模式，可在上车前自动开启设备，进行高效的净化，让您和您的家人上车即享受新鲜的空气。';
// var Week = [Language.mon,Language.tue,Language.wed,Language.thu,Language.fri,Language.sat,Language.sun,];

var Week = [];

var first = 1;
var second = 2;
var third = 3;

var first_str = Language.weekday +'';
var second_str = Language.weekday + '';
var third_str = Language.weekday + '';

var first_num = '0';
var second_num = '0';
var third_num = '0';

// var first_back = '#FAFAFA';
// var secode_back = '#FAFAFA';
// var third_num = '#FAFAFA';

// var week = 9;
var week01 = null;
var week02 = null;
var week03 = null;

var switch01 = -1;
var switch02 = -1;
var switch03 = -1;

// var workday = '周一 周二 周三 周四 周五';
// var weekend = '周六 周日';


var sub_time = null;
var sub_currentDevice = null;

var VehideTime = React.createClass({



	getInitialState(){

		return {
			address:'',
  			eventSwitchIsOn01: false,
  			eventSwitchIsOn02: false,
  			eventSwitchIsOn03: false,
  			hour01:'00',
  			minute01:'00',
  		
  			hour02:'00',
  			minute02:'00',
  	
  			hour03:'00',
  			minute03:'00',
 
		}
	},
	componentWillMount:function() {
		//Week = [Language.mon,Language.tue,Language.wed,Language.thu,Language.fri,Language.sat,Language.sun,];
		Week = new Array(Language.mon+'',Language.tue+'',Language.wed+'',Language.thu+'',Language.fri+'',Language.sat+'',Language.sun+'',);
		// this.setState({address:this.props.device.addr});

	},

	componentDidMount:function(){

		BlueToothUtil.sendMydevice();


	    sub_time = RCTDeviceEventEmitter.addListener('vehide_time_week',(val) => {

		// console.log('星期``14``'+val.week+'``'+val.order+'```'+val.hour+'````'+val.minute);


	    	if (val != null ){
		    	if (val.order == 1){
		    		if (!this.state.eventSwitchIsOn01){
			    		this.setState({
			    			hour01:val.hour,
			    			minute01:val.minute,
			    			eventSwitchIsOn01:true,
			    		});
			    		switch01 = 1;
		    		}else {
		    			this.setState({
			    			hour01:val.hour,
			    			minute01:val.minute,
			    			// eventSwitchIsOn01:true,
			    		});
		    		}

		    	

		    		if (val.week == 0x1f){
		    			first_str = Language.weekday;
		    		} else if (val.week == 0x60){
		    			first_str = Language.weekend;
		    		} else {
		    			first_str = this._parseWeek(val.week);
		    		}

		    		first_num = this._parseWeekNum(val.week);
		    		// console.log('星期``15``'+'```'+this.state.hour01+'````'+this.state.minute01);
		    	}

		    	if (val.order == 2){

		    		if (!this.state.eventSwitchIsOn02){
			    		this.setState({
			    			hour02:val.hour,
			    			minute02:val.minute,
			    			eventSwitchIsOn02:true,
			    		});
			    		switch02 = 1;
		    		} else {
		    			this.setState({
			    			hour02:val.hour,
			    			minute02:val.minute,
			    			// eventSwitchIsOn02:true,
			    		});
		    		}


		    		if (val.week == 0x1f){
		    			second_str = Language.weekday;
		    		} else if (val.week == 0x60){
		    			second_str = Language.weekend;
		    		} else {
		    			second_str = this._parseWeek(val.week);
		    		}

		    		second_num = this._parseWeekNum(val.week);
					// console.log('星期``16``'+'```'+this.state.hour02+'````'+this.state.minute02);
		    	}

		    	if (val.order == 3){
		    		if (!this.state.eventSwitchIsOn03){
			    		this.setState({
			    			hour03:val.hour,
			    			minute03:val.minute,
			    			eventSwitchIsOn03:true,
			    		});
			    		switch03 = 1;
		    		} else {
		    			this.setState({
			    			hour03:val.hour,
			    			minute03:val.minute,
			    			// eventSwitchIsOn03:true,
			    		});
		    		}


		    		if (val.week == 0x1f){
		    			third_str = Language.weekday;
		    		} else if (val.week == 0x60){
		    			third_str = Language.weekend;
		    		} else {
		    			third_str =  this._parseWeek(val.week);
		    		}


		    		third_num = this._parseWeekNum(val.week);
		    		// console.log('星期``17``'+'```'+this.state.hour03+'````'+this.state.minute03);
		    	}

	    	}
	    });

		//切换设备的监听器
	    sub_currentDevice = RCTDeviceEventEmitter.addListener('message_select_item',(val)=>{
		      if(val != null){

		        if (val.timeFlag01 == 1){

				    var tmp_hour ='';
		            var tmp_minute = '';
		            var tmp_time_state = null;


		            if (val.vehide_hour < 10 ){
		                tmp_hour = '0'+val.vehide_hour;
		            }else {
		              tmp_hour = val.vehide_hour;
		            }
		            if (val.vehide_minute < 10){
		              tmp_minute = '0'+val.vehide_minute;
		            }else {
		              tmp_minute = val.vehide_minute;
		            }
		            //init
		            if (switch01 == -1){
			            if (val.vehide_time_state == 0){
							tmp_time_state = false;
			            }else {
		            		tmp_time_state = true;
			            }
		            }
		            //手动点击
					// console.log('星期week01:'+val.vehide_time_state+'```'+switch01);

		            if (switch01 == val.vehide_time_state){
						// console.log('星期week01a:'+val.vehide_time_state);
			            if (val.vehide_time_state == 0){
								tmp_time_state = false;
			            }else {
			            	// console.log('星期week0b:'+val.vehide_time_state);
			            		tmp_time_state = true;
			            	
			            }

		            }

		    //         if (val.vehide_time_state == 0){
						
						// console.log('星期week01a:'+val.vehide_time_state);
						// if (switch01 != 1){
						// 	tmp_time_state = false;
						// }
  	
		    //         }else {
		    //         	console.log('星期week0b:'+val.vehide_time_state);
		    //         	if (switch01 != 0){
		    //         		tmp_time_state = true;
		    //         	}
		            
		    //         }

		            if ((this.state.hour01 != tmp_hour) || (this.state.minute01 != tmp_minute) ){

						console.log('星期week01b:'+this.state.eventSwitchIsOn01 +'```'+tmp_time_state);
						if  (tmp_time_state != null ){
							this.setState({
				    			hour01:tmp_hour,
				    			minute01:tmp_minute,
				    			eventSwitchIsOn01:tmp_time_state,
				    		});
						} else {
							this.setState({
				    			hour01:tmp_hour,
				    			minute01:tmp_minute,
				    			// eventSwitchIsOn01:tmp_time_state,
				    		});
						}
	
		            }

		            if (week01 != val.vehide_week){
						week01 = val.vehide_week;

			    		if (week01 == 0x1f){
			    			first_str = Language.weekday;
			    		} else if (week01 == 0x60){
			    			first_str = Language.weekend;
			    		} else {
							first_str = this._parseWeek(week01);
			    		} 

						first_num = this._parseWeekNum(week01);
						console.log('星期week01:'+week01);
		            }



		        }

		        if (val.timeFlag02 == 2){
				    var tmp_hour ='';
		            var tmp_minute = '';
		            var tmp_time_state = null;

		            if (val.vehide_hour02 < 10 ){
		                tmp_hour = '0'+val.vehide_hour02;
		            }else {
		              tmp_hour = val.vehide_hour02;
		            }
		            if (val.vehide_minute02 < 10){
		              tmp_minute = '0'+val.vehide_minute02;
		            }else {
		              tmp_minute = val.vehide_minute02;
		            }

		            //init
		            if (switch02 == -1){
			            if (val.vehide_time_state02 == 0){
							tmp_time_state = false;
			            }else {
		            		tmp_time_state = true;
			            }
		            }
		            //手动点击
		            if (switch02 == val.vehide_time_state02){
			            if (val.vehide_time_state02 == 0){
							tmp_time_state = false;
			            }else {
			            	tmp_time_state = true;
			            }

		            }

		            // if (val.vehide_time_state02 == 0){
		            // 	if (switch02 != 1){
		            // 		tmp_time_state = false;
		            // 	}
		            // }else {
		            // 	if (switch02 != 0){
		            // 		tmp_time_state = true;
		            // 	}
		            // }


		            if ((this.state.hour02 != tmp_hour) || (this.state.minute02 != tmp_minute)){

		            	if (null != tmp_time_state) {

				    		this.setState({
				    			hour02:tmp_hour,
				    			minute02:tmp_minute,
				    			eventSwitchIsOn02:tmp_time_state,
				    		});
		            	} else {

				    		this.setState({
				    			hour02:tmp_hour,
				    			minute02:tmp_minute,
				    			// eventSwitchIsOn02:tmp_time_state,
				    		});
		            	}

		    		
		            }

		            if (week02 != val.vehide_week02){
		            	week02 = val.vehide_week02;
		            	
		            	if (week02 == 0x1f){
			    			second_str = Language.weekday;
			    		} else if (week02 == 0x60){
			    			second_str = Language.weekend;
			    		} else {
							second_str = this._parseWeek(week02);
			    		} 

		           		
						second_num = this._parseWeekNum(week02);
						console.log('星期week02:'+week02);
		            }

		        }

		        if (val.timeFlag03 == 3){
		        	var tmp_hour ='';
		            var tmp_minute = '';
 					var tmp_time_state = null;

		            if (val.vehide_hour03 < 10 ){
		                tmp_hour = '0'+val.vehide_hour03;
		            }else {
		              tmp_hour = val.vehide_hour03;
		            }
		            if (val.vehide_minute03 < 10){
		              tmp_minute = '0'+val.vehide_minute03;
		            }else {
		              tmp_minute = val.vehide_minute03;
		            }

		            //init
		            if (switch03 == -1){
			            if (val.vehide_time_state03 == 0){
							tmp_time_state = false;
			            }else {
		            		tmp_time_state = true;
			            }
		            }
		            //手动点击
		            if (switch03 == val.vehide_time_state03){
			            if (val.vehide_time_state03 == 0){
							tmp_time_state = false;
			            }else {
			            	tmp_time_state = true;
			            }

		            }


		            // if (val.vehide_time_state03 == 0){
		            // 	if (switch03 != 1){
		            // 		tmp_time_state = false;
		            // 	}
		            // }else {
		            // 	if (switch03 != 0){
		            // 		tmp_time_state = true;
		            // 	}
		            // }

		            if ((this.state.hour03 != tmp_hour) || (this.state.minute03 != tmp_minute) ){

		            	if (null != tmp_time_state) {
		            		this.setState({
				    			hour03:tmp_hour,
				    			minute03:tmp_minute,
				    			eventSwitchIsOn03:tmp_time_state,
				    		});
		            	} else {
		            		this.setState({
				    			hour03:tmp_hour,
				    			minute03:tmp_minute,
				    			// eventSwitchIsOn03:tmp_time_state,
				    		});
		            	}

		            }

		            if (week03 != val.vehide_week03){
    					week03 = val.vehide_week03;

    					if (week03 == 0x1f){
			    			third_str = Language.weekday;
			    		} else if (week03 == 0x60){
			    			third_str = Language.weekend;
			    		} else {
							third_str =  this._parseWeek(week03);
			    		}

						third_num = this._parseWeekNum(week03);
						console.log('星期week03:'+week03);
		            }
		        }

		        this.setState({address:val.addr});

		        // console.log('星期week01:'+val.vehide_week+val.vehide_week02+val.vehide_week03);
		      }
			
	    });

	},


	componentWillUnmount:function() {

		sub_time.remove();
		sub_currentDevice.remove();
	},


	//返回为1的位置
	_parseWeekNum:function(week){

		var rev = new Array();

		var str = '';
		var sec = parseInt(week).toString(2);
		rev = sec.split('').reverse();
		console.log('星期num0:'+rev.length);

		for (var i = 0;i < rev.length;i++){
			if (rev[i] == '1'){
					str += i + ',';
					console.log('星期num1:'+str);
			}	
			continue;
		}
		console.log('星期num2:'+str);

		return str;
	},


	//返回字符串
	_parseWeek:function(week){
		console.log('星期week:'+week);

		var str ='';
		var rev = new Array();

		var weekArry = [];
		var secondHex = parseInt(week).toString(2);
		console.log('星期2进制:'+secondHex);

		rev = secondHex.split('').reverse();

		for (var i = 0;i < rev.length;i++){
			if (rev[i] == '1'){
				weekArry.push(i);
				console.log('星期2进制b:'+i);
			}

		}


		for (var i = 0;i < Week.length;i++){
			for (var j =0; j< weekArry.length;j++){

				if (weekArry[j] == i){
					str += Week[i]+' '; 
					console.log('被选择的星期:'+Week[i]);
					console.log('星期···'+str);

				}
				continue;
			}
		}

		return str;
	},

	render(){
      var navigator = this.props.navigator;

      	console.log('星期``01```'+'```'+this.state.eventSwitchIsOn01+'``'+switch01);

		return (
			<View style = {{flex:1,backgroundColor:'#FFFFFF'}}>
				<Title title = {Language.timedswitch} hasGoBack = {true}  navigator = {this.props.navigator}/>

				<View style = {{height:deviceHeight /4}}>
					<Image 
						source = {require('image!ic_timer')}
						style  = {{width:deviceWidth,height:513/1080 * deviceWidth,resizeMode:'stretch'}}/>
				</View>

				<View style = {{height:30,marginTop:10,marginLeft:10,marginRight:10,marginBottom:10,}}>
					<Text style = {{color:'#999999',fontSize:14,textAlign:'auto'}}>{Language.start_to_purify}</Text>
				</View>

				<View style = {{marginTop:20,width:deviceWidth,marginLeft:10,height:20,
								alignItems:'flex-start',justifyContent:'flex-start'}}>
					<Text style = {{color:'#474747',fontSize:16,textAlign:'center'}}>{Language.settings}</Text>
				</View>

				<View style = {{backgroundColor:'#FAFAFA',width:deviceWidth,marginTop:10,}}>
					<View style = {{height:1,backgroundColor:'#C8C8C8',width:deviceWidth}}/>
						<View style = {{flexDirection:'row',height:50,}}>
							<TouchableOpacity 
								onPress = { () => {this._gotoEditTime(this.state.hour01,this.state.minute01,first,first_num)}}
								style = {{flex:1.5,alignItems:'flex-start',justifyContent:'center'}}>
							<View style = {{flexDirection:'column',marginLeft:20,}}>
								<Text style = {{color:'#2AB9F1',fontSize:20,}}>{this.state.hour01}{' : '}{this.state.minute01}</Text>
								<Text style = {{color:'#BCBCBC',fontSize:12,}}>{first_str}</Text>
							</View>
							</TouchableOpacity>

							<View style = {{flex:0.5,marginRight:20,alignItems:'flex-end',justifyContent:'center'}}>
					          <Switch
					            onValueChange={(value) => {this._changeSwitch01(this.state.address,value)}}
					            style={{marginBottom: 0}}
					            value={this.state.eventSwitchIsOn01} />
							</View>

						</View>
					<View style = {{height:1,backgroundColor:'#C8C8C8',marginLeft:10,width:deviceWidth - 10}}/>
						<View style = {{flexDirection:'row',height:50,}}>
							<TouchableOpacity 
								onPress = { () => {this._gotoEditTime(this.state.hour02,this.state.minute02,second,second_num)}}
								style = {{flex:1.5,alignItems:'flex-start',justifyContent:'center'}}>
							<View style = {{flexDirection:'column',marginLeft:20,}}>
								<Text style = {{color:'#2AB9F1',fontSize:20,}}>{this.state.hour02}{' : '}{this.state.minute02}</Text>
								<Text style = {{color:'#BCBCBC',fontSize:12,}}>{second_str}</Text>
							</View>
							</TouchableOpacity>

							<View style = {{flex:0.5,marginRight:20,alignItems:'flex-end',justifyContent:'center'}}>
					          <Switch
					            onValueChange={(value) => this._changeSwitch02(this.state.address,value)}
					            style={{marginBottom: 0}}
					            value={this.state.eventSwitchIsOn02} />
							</View>
						</View>

					<View style = {{height:1,backgroundColor:'#C8C8C8',marginLeft:10,width:deviceWidth - 10}}/>

						<View style = {{flexDirection:'row',height:50,}}>
							<TouchableOpacity 
								onPress = { () => {this._gotoEditTime(this.state.hour03,this.state.minute03,third,third_num)}}
								style = {{flex:1.5,alignItems:'flex-start',justifyContent:'center'}}>
							<View style = {{flexDirection:'column',marginLeft:20,}}>
								<Text style = {{color:'#2AB9F1',fontSize:20,}}>{this.state.hour03}{' : '}{this.state.minute03}</Text>
								<Text style = {{color:'#BCBCBC',fontSize:12,}}>{third_str}</Text>
							</View>
							</TouchableOpacity>

							<View style = {{flex:0.5,marginRight:20,alignItems:'flex-end',justifyContent:'center'}}>
								
						          <Switch
						            onValueChange={(value) => this._changeSwitch03(this.state.address,value)}
						            style={{marginBottom: 0}}
						            value={this.state.eventSwitchIsOn03} />
					         
							</View>

						</View>

					<View style = {{height:1,backgroundColor:'#C8C8C8',width:deviceWidth}}/>
				</View>
			</View>
		);
	},

	_gotoEditTime:function(hour,minute,order,week){

	console.log('hour``minute``type``week``1```'+hour+minute+order+week);
	
	var time = {hour:'',minute:'',order:'',week:''};

    time.hour   = hour;
     time.minute = minute;
    time.order   = order+'';
    time.week   = week;

	console.log('hour``minute``type``week``2```'+time.hour+time.minute+time.order+time.week);

    //RCTDeviceEventEmitter.emit('vehide_edit_time_week', time);

	this.props.navigator.push({
       id: 'VehideEditTime',
       params:{
           time:time,
           address:this.state.address,
          }
       });
	},

	_changeSwitch01:function(address,value){
		console.log('星期``01```'+address+'```'+value);
	
		this.setState({eventSwitchIsOn01: value});
		// console.log('星期``01```'+address+'``1`'+this.state.eventSwitchIsOn01);

       // BlueToothUtil.setVehideTime(address,parseInt(minute),parseInt(hour),parseInt(dex),parseInt(order),1);

		if (!value){
     		 switch01 = 0;

     		 BlueToothUtil.setVehideTime(address,parseInt(this.state.minute01),parseInt(this.state.hour01),0,first,0);

		}else {
			switch01 = 1;
			 BlueToothUtil.setVehideTime(address,parseInt(this.state.minute01),parseInt(this.state.hour01),0,first,1);
		}
		
	},

	_changeSwitch02:function(address,value){
			console.log('星期``02```'+address+'```'+value);
			this.setState({eventSwitchIsOn02: value});
				// console.log('星期``02```'+address+'```'+this.state.eventSwitchIsOn02);
		if (!value){
			switch02 = 0;
     		BlueToothUtil.setVehideTime(address,parseInt(this.state.minute02),parseInt(this.state.hour02),0,second,0);
		}else {
			switch02 = 1;
			BlueToothUtil.setVehideTime(address,parseInt(this.state.minute02),parseInt(this.state.hour02),0,second,1);
		}
		
	},

	_changeSwitch03:function(address,value){
			console.log('星期``03```'+address+'```'+value);
			this.setState({eventSwitchIsOn03: value});
			// console.log('星期``03```'+address+'```'+this.state.eventSwitchIsOn03);
		if (!value){
			switch03 = 0;

     		BlueToothUtil.setVehideTime(address,parseInt(this.state.minute03),parseInt(this.state.hour03),0,third,0);
		}else {
			switch03 = 1;
			BlueToothUtil.setVehideTime(address,parseInt(this.state.minute03),parseInt(this.state.hour03),0,third,1);
		}
		
	},
});

module.exports = VehideTime;

