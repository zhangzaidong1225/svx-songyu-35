/**
* 标题栏页面
*/

'use strict';

//TODO fetch style from style_login to style_title
// import style_login from './styles/style_login'
// import  styles from './styles/style_setting';

import tools from './tools'
import Language from './Language'
// import styles_feedback from './styles/style_feedback'



// import styles_health from './styles/style_health'

// import style_register from './styles/style_register'

import styles_title from  './styles/style_title'

// import Title from './title'
//<Title title = {Language.personal} hasGoBack = {true}  navigator = {this.props.navigator}/>


import React, { Component } from 'react';
import {
  StyleSheet,
  Dimensions,
  View,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';


var UMeng = require('./UMeng');

var deviceWidth = Dimensions.get('window').width;
var deviceHeight = Dimensions.get('window').height;
var Title = React.createClass({

    propTypes:{
      title :React.PropTypes.string,
      hasGoBack:React.PropTypes.bool,
      backAddress:React.PropTypes.any,
      goUpgradeBack:React.PropTypes.func,
    },

  	getDefaultProps: function () {
  		return {
  			title:'',
  			hasGoBack: false,
        backAddress: null,

  		};
  	},
    //<Image style={[styles_title.backImage,{resizeMode: 'stretch'}]} source={require('image!ic_triangle')}/>
	render: function() {
		return (

      <View style = {styles_title.title} >
        <View style = {styles_title.textContainer}>
          <Text allowFontScaling={false} style={[styles_title.text,]}>{this.props.title}</Text>
          
        </View >

        <View style = {styles_title.backContainer}>
            <TouchableOpacity
              activeOpacity = {this.props.hasGoBack?1:0}
              style = {styles_title.touch}
              onPress={()=>{this._goBack();UMeng.onEvent('title_01');}}>
              <Image style={this.props.hasGoBack?[styles_title.backImage,{resizeMode: 'stretch'}]:{width:0,height : 0}} source={require('image!ic_back')}/>
            </TouchableOpacity>
        </View>
      </View>
    	);
	},

  _goBack:function (event) {
    if (this.props.goUpgradeBack){
        this.props.goUpgradeBack(event);
    } else {
      if (this.props.backAddress == null) {
        this.props.navigator.pop();
      } else {
        this.props.navigator.replace({
          id: this.props.backAddress,
        });
      }
    }

  },

});

module.exports = Title;