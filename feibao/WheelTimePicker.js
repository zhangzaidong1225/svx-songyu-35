/** 
* Dispaly status of the filters which user has.
*/
'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Platform,
} from 'react-native';
 
import Picker from 'react-native-wheel-picker'
import styles_wheeltimepicker from './styles/style_wheeltimepicker'
import Language from './Language'
var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter');


var PickerItem = Picker.Item;

var mheight = Dimensions.get('window').height;

var hourList = ['00','01','02','03','04','05','06','07','08','09','10','11','12',
                '13','14','15','16','17','18','19','20','21','22','23'];
var minuteList = ['00','01','02','03','04','05','06','07','08','09','10',
                  '11','12','13','14','15','16','17','18','19','20',
                  '21','22','23','24','25','26','27','28','29','30',
                  '31','32','33','34','35','36','37','38','39','40',
                  '41','42','43','44','45','46','47','48','49','50',
                  '51','52','53','54','55','56','57','58','59'];


var WheelTimePicker = React.createClass({

  getDefaultProps: function() {
    return {
      // value : '10',
      hour:'00',
      minute:'00',
      showFlag : true,
    };

  },

  getInitialState: function() {
    return {
      hourSelectedItem : 0,
      minuteSelectedItem : 0,
      // selectedItem : 2,
      //itemList:['10','15', '20', '25', '30', '40', '50', '60', '90', '120'],

    }
  },

  componentDidMount: function() {

  },

  componentWillMount: function() {
    // this.timer = setTimeout(() => {
    var hourItem = parseInt(this.props.hour);
    var minuteItem = parseInt(this.props.minute);

    // if (hourList.indexOf(this.props.hour) == -1) {
    //   hourItem = hourList.indexOf('22');
    // } else {
    //   hourItem = hourList.indexOf(this.props.hour);
    // }

    // if (minuteList.indexOf(this.props.minute) == -1) {
    //   minuteItem = minuteList.indexOf('22');
    // } else {
    //   minuteItem = minuteList.indexOf(this.props.minute);
    // }

    this.setState({
      minuteSelectedItem : minuteItem,
      hourSelectedItem : hourItem,
    });

    // }, 1000);


    // if (this.state.itemList.indexOf(this.props.value) == -1) {
    //   this.setState({
    //     selectedItem: this.state.itemList.indexOf('20'),
    //   })
    // } else {
    //   this.setState({
    //     selectedItem: this.state.itemList.indexOf(this.props.value),
    //   })
    // }

  },

  componentWillUnmount: function() {
    // this.timer && clearTimeout(this.timer);
  },

  onHourSelected : function(index) {
    this.setState({
      hourSelectedItem : index,
    });
    this.setSelectedTime(index, this.state.minuteSelectedItem);

  },

  onMinuteSelected : function(index) {
    console.log('minute selected item:' + index);
    this.setState({
      minuteSelectedItem : index,
    });
    this.setSelectedTime(this.state.hourSelectedItem, index);
  },
 
  // onPikcerSelect: function(index) {
  //   this.setState({
  //     selectedItem: index,
  //   })
  // },
 
  // onAddItem: function() {
  //   var name = '司马懿'
  //   if (this.state.itemList.indexOf(name) == -1) {
  //     this.state.itemList.push(name)
  //   }
  //   this.setState({
  //     selectedItem: this.state.itemList.indexOf(name),
  //   })
  // },

  setSelectedTime : function(hour, minute) {
    var time = {hour:'',minute:''};
    time.hour = hourList[hour];
    time.minute = minuteList[minute];
    RCTDeviceEventEmitter.emit('wheel_time_picker', time);
    // RCTDeviceEventEmitter.emit('wheel_time_picker_close');
  },

        //   <View style={{position:'absolute', width: 200, height: 180,justifyContent: 'center',alignItems: 'center',}}>
        //   <Text style={{color: 'white',fontSize:24}}>
        //     {content}
        //   </Text>
        // </View>
 
  render: function() {
    //if(this.props.type!=1){
     // this.close();
    //}
    return (
      <View style={[styles_wheeltimepicker.container,this.props.showFlag?{}:{marginTop : -200, width : 0, height : 0}]}>

        <View style = {[styles_wheeltimepicker.picker, {backgroundColor : 'white',}, this.props.showFlag?{}:{width : 0, height : 0}]}>
          <Picker style={[styles_wheeltimepicker.hourpicker, this.props.showFlag?{}:{width : 0, height : 0}]}
            selectedValue={this.state.hourSelectedItem}
            itemStyle={Platform.OS === 'android'? {color:'#2AB9F1',} : [{color:'#2AB9F1',}, (this.props.showFlag?{height : mheight * 71.5 / 320}:{width : 0,height : 0})]}
            onValueChange={(index) => this.onHourSelected(index)}>
              {hourList.map((value, i) => (
                <PickerItem label={value} value={i} key={'hour'+value}/>
              ))}
          </Picker>
          <Picker style={[styles_wheeltimepicker.minutepicker, this.props.showFlag?{}:{width : 0, height : 0}]}
            selectedValue={this.state.minuteSelectedItem}
            itemStyle={Platform.OS === 'android'? {color:'#2AB9F1',} : [{color:'#2AB9F1',}, (this.props.showFlag?{height : mheight * 71.5 / 320}:{width : 0,height : 0})]}
            onValueChange={(index) => this.onMinuteSelected(index)}>
              {minuteList.map((value, i) => (
                <PickerItem label={value} value={i} key={'minute'+value}/>
              ))}
          </Picker>
        </View>



      </View>
    );
  },

});

        // <TouchableOpacity activeOpacity = {1} style = {[styles_wheeltimepicker.touchableOpacity, this.props.showFlag?{}:{width : 0, height : 0, marginTop : 0}]} onPress={() => {this.close();}}>
        //   <Text style = {[styles_wheeltimepicker.text, this.props.showFlag?{}:{width : 0, height : 0}]}>
        //     确认
        //   </Text>
        // </TouchableOpacity>


        // <Text style={{margin: 20, color: 'black',}}>
        //   你最喜欢的是：{this.state.itemList[this.state.selectedItem]}
        // </Text>
 
        // <Text style={{margin: 20, color: 'black'}}
        //     onPress={this.onAddItem}>
        //   怎么没有司马懿？
        // </Text>



module.exports = WheelTimePicker;