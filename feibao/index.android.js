/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    AppRegistry,
} from 'react-native';

import Navigation from './Navigation'

AppRegistry.registerComponent('feibao', () => Navigation);
