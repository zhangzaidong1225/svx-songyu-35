/**
* AsyncStorage:
* ios from AsyncStorage and android from RCTSZTYSharedPreferencesModule,
* 
*/

'use strict';
// var React = require('react-native');

// var {
// 	AsyncStorage,
// } = React;

var RCTAsyncStorage = require('./node_modules/react-native/Libraries/Storage/AsyncStorage');

var AsyncStorage = {

	getItem: function(
		key: string,
		// def: string,
    	callback?: ?(error: ?Error, result: ?string) => void) {

		RCTAsyncStorage.getItem(key, callback);

	},

	setItem: function(
		key: string,
    	value: string,
    	callback?: ?(error: ?Error) => void) {

		RCTAsyncStorage.setItem(key, value, callback);

	},


  removeItem: function(
    key: string,
    callback?: ?(error: ?Error) => void) {

  	RCTAsyncStorage.removeItem(key, callback);

  }
};

module.exports = AsyncStorage;
