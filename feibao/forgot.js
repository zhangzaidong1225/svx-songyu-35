/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

import styles from './styles/style_setting'
import styles_feedback from './styles/style_feedback'
import styles_selectList from './styles/style_selectList'
import  config from './config'
import tools from './tools'
import styles_health from './styles/style_health'
import style_register from './styles/style_register'
import Spinner from 'react-native-loading-spinner-overlay';
var TimerMixin = require('react-timer-mixin');
//var AsyncStorage = require('./AsyncStorage');
var UMeng = require('./UMeng');
import Title from './title'
import Language from './Language'
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  PixelRatio,
   AsyncStorage,
  TouchableOpacity,
  TouchableHighlight,
  ToastAndroid,
  Navigator,
  TextInput,
  ScrollView,
  NetInfo,
} from 'react-native';

var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter');
var sub_loading;
var isloading=0;
var wait=60;
var wait1;
var wait2;
var second;
var time;  
var iscount = true;
var deviceWidth = Dimensions.get('window').width;
var deviceHeight = Dimensions.get('window').height;
// var DEFAULT_URL = config.server_base+'/iface/user/register';
var Forgot = React.createClass({
  mixins: [TimerMixin],
  timer_delay:null,
  getInitialState(){
    return {
      type:1,
      phonenum:'',
      code:'',
      password:'',
      hidden:true,
      count:'',
      isCable:true,
      title:Language.retrievepassword,
      visible1: false,
    }
  },

  componentWillUnmount:function(){
    var myDate = new Date(); 
    time = Math.floor(parseInt(myDate.getTime())/1000); 
     sub_loading.remove();
    AsyncStorage.getItem('first',(error,result) => {
        if (error){}else {
          this.setState({
          count:Language.getsms,
        });
      }
    });
    var phonenum = '';
    if(this.state.phonenum != null){
    	phonenum = this.state.phonenum;
    }
    AsyncStorage.setItem('user_phonenum',
      phonenum,error => {

    });
  },
  componentWillMount:function(){
   var iscount = true;
    AsyncStorage.getItem('user_time',(error,result) => {
        if (error){}else {
          if(result==null){
              this.setState({
                count:Language.getsms,
              });
          }else{
                time=parseInt(result);
                var myDate = new Date(); 
                second = myDate.getTime();
                wait1 = Math.floor(parseInt(second)/1000-parseInt(time));//两次的时间差
                if(wait1>=60){
                    iscount = false;
                    this.setState({
                      count:Language.getsms,
                    });
                }else{
                    wait = 60-wait1;
                    if(this.state.count!=Language.getsms){
                      this.setState({
                        isCable:false,
                      });
                    }
                    this.countDown(); 
                }
              }
            }
        });

    AsyncStorage.getItem('user_phonenum',(error,result) => {
        if (error){}else {
          if (result == null) {return;}
          this.setState({
            phonenum:result,
          });
        }
    });
  },
  componentDidMount:function(){
    console.log('componentDidMount');
    isloading=0;
    this.setState({title:this.props.title});  
    sub_loading = RCTDeviceEventEmitter.addListener('loading',(val)=>{
      if(val==false){
        isloading=1;
        this.setState({
          visible1:val,
        });
      }
    });
  },
  hidden(){
    if (!this.state.hidden){
        this.setState({hidden:true});
    }else {
       this.setState({hidden:false});
    }
  },
  render(){
	var navigator = this.props.navigator;
  var img;
  if (this.state.hidden){
      img = require('image!ic_hide'); 
  }else{
      img = require('image!ic_show');
  }
  return(
    <View
        style = {style_register.container}>
        <ScrollView style={styles_health.scrollView} scrollEnabled={false}>
        <Spinner visible={this.state.visible1} />
        <Title title = {this.state.title} hasGoBack = {true}  navigator = {this.props.navigator}/>
        <View style={style_register.borderview1}>
            <View style={style_register.borderview2}>
                <View style={style_register.borderview3}>
                    <Image style={[style_register.borderview3_img,{resizeMode:'stretch',}]} source={require('image!ic_feedback')}/>
                </View>
                <View style={style_register.borderview4}>
                    <Image style={[style_register.borderview4_img,{resizeMode:'stretch',}]} source={require('image!ic_dividing_line')}/>
                </View>
                      <TextInput
                        style = {style_register.borderview5}
                        underlineColorAndroid='transparent'
                        textAlignVertical='center'
                        keyboardType ='numeric'
                        onChangeText = {(text) => this.setState({phonenum:text})}
                        value = {this.state.phonenum}
                        placeholderTextColor = '#CCCCCC'
                         maxLength={11}
                        placeholder = {Language.phonenumber}/>
                <TouchableOpacity 
                  style={style_register.borderview6} 
                  onPress={()=>{this.isClickable();UMeng.onEvent('forgot_02');}}>
                    <View style={style_register.borderview6_1}>
                        <Text allowFontScaling={false} style={style_register.borderview6_text}>{this.state.count}</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={style_register.view2}>
                <View style={style_register.view2_1}>
                    <Image style={[style_register.view2_1_img,{resizeMode:'stretch',}]} source={require('image!ic_codes')}/>
                </View>
                <View style={style_register.view2_2}>
                    <Image style={style_register.view2_2_img} source={require('image!ic_dividing_line')}/>
                </View>
                      <TextInput
                        style = {style_register.view2_3}
                        underlineColorAndroid='transparent'
                        textAlignVertical='center'
                        KeyboardType = 'default'
                        onChangeText = {(text) => this.setState({code:text})}
                        value = {this.state.code}
                        placeholderTextColor = '#CCCCCC'
                        placeholder = {Language.sms}/>
                    <View style={style_register.view3_2}/>
                    <TouchableOpacity 
                       style={style_register.view3_4}
                      onPress = {() => {this.setState({code:''});UMeng.onEvent('forgot_03');}}>
                        <Image style={[style_register.view2_4_img,{resizeMode:'stretch',}]} source={require('image!ic_delete')}/>
                    </TouchableOpacity>
            </View>
          <View style={style_register.view3}>
                <View style={style_register.view3_1}>
                    <Image style={[style_register.view3_1_img,{resizeMode:'stretch'}]} source={require('image!ic_lock')}/>
                </View>
                <View style={style_register.view3_2}>
                    <Image style={[style_register.view3_2_img,{resizeMode:'stretch',}]} source={require('image!ic_dividing_line')}/>
                </View>
                    <TextInput
                        style = {style_register.view3_3}
                        underlineColorAndroid='transparent'
                        textAlignVertical='center'
                        KeyboardType = 'default'
                        secureTextEntry ={this.state.hidden}
                        onChangeText = {(text) => this.setState({password:text})}
                        placeholderTextColor = '#CCCCCC'
                        maxLength={16}
                        placeholder = {Language.passwordcombination}/> 
                <View style={style_register.view3_2}>
                    <Image style={[style_register.view3_2_img,{resizeMode:'stretch',}]} source={require('image!ic_dividing_line')}/>
                </View>

                <TouchableOpacity 
                   style={style_register.view3_4}
                  onPress = {() => {this.hidden();UMeng.onEvent('forgot_04');}}>
                    <Image style={[style_register.view3_4_img,{resizeMode:'stretch',}]} source={img}/>
                </TouchableOpacity>
            </View>
        </View>
    </ScrollView>
        <View style = {styles_feedback.commitView}>
              <View style = {style_register.view4}>
                <TouchableOpacity
                    onPress = {this._handleSubmit}>
                   <View style = {styles_feedback.textBorder}>
                    <Text allowFontScaling={false} style={styles_feedback.commitText}>{Language.commit}</Text>
                   </View>
                </TouchableOpacity>
              </View>
        </View>
</View>
)},
  _handleSubmit(){
    UMeng.onEvent('forgot_05');
      if(this.state.phonenum === ''|| this.state.phonenum == null){
          tools.alertShow(Language.phonenumbernotempty);
          return;
      }
      if(this.state.code === ''|| this.state.code == null ){
          tools.alertShow(Language.verification_code_empty);
          return;
      }
      if(this.state.password === ''|| this.state.password == null ){
          tools.alertShow(Language.password_not_empty);
          return;
      }
      if(this.passwordNumOrAlpCheck(this.state.password)){
          tools.alertShow(Language.passwordNotFormat);
          return;
      }
      if (this.telRuleCheck2(this.state.phonenum) && this.passwordRuleCheck(this.state.password)){
        if(isloading==1){
          isloading=0;
          return;
        }
        this.setState({
          visible1: true,
        });      
      NetInfo.isConnected.fetch().done(
        (isConnected) => { 
              if (isConnected) {
                  fetch (config.server_base+ config.user_repass_uri +'?mobi='+this.state.phonenum+'&pass='+this.state.password+'&code='+this.state.code+'&version='+config.version+'&platform='+config.platform+'&phone_modal='+config.phone_modal+'&language='+config.language,{timeout: 10000})
                  .then ((response) => response.json())
                  .then((responseData) => {
                    if(responseData.code == 0){                     
                      this.goBack(responseData.msg+'');
                    }
                    if(responseData.code == 301){
                      this.setState({
                        visible1: false,
                      });                     
                      tools.alertShow(responseData.msg+'');
                    }
                    if(responseData.code == 305){
                      this.setState({
                        visible1: false,
                      });                      
                      tools.alertShow(responseData.msg+'');
                    }
                  })
                  .catch((error) => {
                    this.setState({
                      visible1: false,
                    });                 
                    tools.alertShow(Language.network_request_failed);
                  });
                  this.setState({
                    isConnect:true,
                  });
                }else{
                  this.setState({
                      visible1: false,
                    });                 
                  tools.alertShow(Language.connectnetwork);
                }
        });
      }else{
        tools.alertShow(Language.phone_password_error);
      }
  },
  _getCode(){

      iscount = true;
      if(isloading==1){
         isloading=0;
        return;
      }
      this.setState({
        visible1: true,
      });
      NetInfo.isConnected.fetch().done(
        (isConnected) => { 
              if (isConnected) {
                  fetch (config.server_base + config.user_getcode_uri+'?mobi='+this.state.phonenum+'&version='+config.version+'&platform='+config.platform+'&phone_modal='+config.phone_modal+'&language='+config.language,{timeout: 10000})
                  .then ((response) => response.json())
                  .then((responseData) => {
                    if(responseData.code == 0){
                      var myDate = new Date(); 
                      var user_time = Math.floor(parseInt(myDate.getTime())/1000);
                      AsyncStorage.setItem('user_time',
                        user_time+'',error => {
                      });
                      this.setState({
                        visible1: false,
                      });                    
                      tools.alertShow(responseData.msg+'');
                    }
                    if(responseData.code == 304){
                      this.setState({
                        visible1: false,
                      });                     
                      tools.alertShow(responseData.msg+'');
                    }
                    var nomassage = true;
                    if(responseData.code == 307){
                      this.setState({
                        visible1: false,
                      });
                      nomassage = false;
                      iscount = false;
                      this.setState({isCable:true});
                      tools.alertShow(responseData.msg+'');
                    }
                    if(nomassage){
                       this.setState({isCable:false});
                       wait = 60;
                       this.countDown(); 
                    } 
                  })
                  .catch((error) => {
                    this.setState({
                      visible1: false,
                    });                  
                    tools.alertShow(Language.network_request_failed);
                      this.setState({
                        isCable:true,
                      });
                  });

                }else{
                    this.setState({
                      visible1: false,
                    });                
                    tools.alertShow(Language.connectnetwork);
                }
        });
  },

  telRuleCheck2(string) {
    var pattern = /^1[34578]\d{9}$/;
    if (pattern.test(string)) {
        return true;
    }
    return false;
  },
  passwordRuleCheck(string) {
    var pattern = string.length;
    if (pattern>=6&&pattern<=16) {
        return true;
    }
    return false;
  },
  passwordNumOrAlpCheck(string) {
    //var pattern = /^[0-9a-zA-Z]*$/g;
    //var p = /(?!^\d+$)(?!^[a-zA-Z]+$)[0-9a-zA-Z]{4,23}/

    if (string.trim() == null||string.trim() == '') {
        return true;
    }
    return false;
  },
  mailCheck(string){
     var res= /\w@\w*\.\w/;
     if (res.test(string)){
       return true;
     }
     return false;
  },
  goBack:function(string){
    this.setState({
      visible1: false,
    }); 
    tools.alertShow(string);
    this.props.navigator.pop(); 
  },
  countDown:function(){
    if(wait>0){
      wait--;
      if(iscount){
        console.log('setTimeout');
          this.setTimeout(() => {
            this.setState({
              count:wait+'',
            });
            this.countDown();
          }, 1000);
      }
    }else if(wait<=0){
      this.setState({
        count:Language.getsms,
        isCable:true,
      });
      wait=60;
      iscount = false;
    }
},

  isClickable:function(){
    if (this.state.phonenum === ''|| this.state.phonenum == null ){
      tools.alertShow(Language.phonenumbernotempty);
      return;
    }
    if (!this.telRuleCheck2(this.state.phonenum)){
        tools.alertShow(Language.phone_password_error);
        return;
    }
    if (this.state.isCable&&this.state.phonenum!=''){
        this.setState({
          isCable:false,
        });
        this._getCode(); 
    }
  },

});


module.exports = Forgot;
