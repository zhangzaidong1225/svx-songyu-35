/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

var Log = require('./net');
import tools from './tools'
var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter'); /*用于界面组件之间传递消息*/
var AsyncStorage = require('./AsyncStorage');
//import PercentageCircle from 'react-native-percentage-circle';
import * as Progress from 'react-native-progress';
import  styles from './styles/style_singleicon'
import  styles1 from './styles/style_icon'
import Language from './Language'

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Animated,
  Easing,
  Alert,
  DeviceEventEmitter,
  NativeModules,
  ToastAndroid,
  TouchableOpacity,
  PanResponder,
  Dimensions,
  NativeAppEventEmitter
} from 'react-native';
var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;
var sub_getMode=null;
var wait = 0;
var pm25 = 250;
var isSwitch = true;
var misCharged = 0;
var mpm25Flag = -1;
var count = 0;
var mpm25PickTime = 0;
var msystemTime = 0;
var timeago = 0;
var mmodel = 0;
var animated;
var timeString = '';
var time_min;
var timenumber;
var power = 0;
var pm25String = '000';
var hasSingleString = ' ';
var isOpentools = true;
var BlueToothUtil= require('./BlueToothUtil');
var TimerMixin = require('react-timer-mixin');
var sub_currentDevice = null;
var mcover;
var SingleIcon= React.createClass({
  mixins: [TimerMixin],
  getInitialState: function() {
   return {
      pm25:250,
      time:20,
      isOpen:false,
      pm25Flag:0,
      ischarged:0,
      cover:0,
      color1:'#96FB69',
      address:'',
      isWait_V:false,
    };
  },
  componentWillUnmount:function(){
      sub_currentDevice.remove();
      sub_getMode.remove();
  },
  componentDidMount:function(){
    //this.countDown();
    this._run();
    sub_getMode = RCTDeviceEventEmitter.addListener('type_mode',(val)=>{
      if(val == null){
        console.log('val ····sub_getMode········NULL···');
      }else{
        if(val=='0'){
          this.setState({
            mode:0,
          });
        }else{
          this.setState({
            mode:1,
          });
        }
      }
      console.log('val ····sub_getMode···········'+val+'        '+this.state.mode);
    });
    sub_currentDevice = RCTDeviceEventEmitter.addListener('message_select_item',(val) => {
      if(val == null){
        console.log('val ···············');
      }else {
        var mcover=parseInt(val.battery_power);
        power = mcover/100;
        this.setState({
         pm25:val.Pm25,
         pm25Flag:val.Pm25Flag,
         pm25PickTime:val.Pm25PickTime,
         isCharged:val.ischarged,
         cover:val.battery_power,
         address:val.addr,
         systemTime:val.SystemTime,
         model:val.model,
         type:val.type,
         hasSingle:val.vehide_model,
         vehideModel:val.vehide_is_smart,
        });
        //hasSingle:val.vehide_model,
        //mcover=parseInt(this.state.cover);
        console.log('==是否充电'+this.state.isCharged);
        count=parseInt(this.state.pm25)/500;
        mmodel=parseInt(this.state.model);
        misCharged=parseInt(this.state.isCharged);
        mpm25Flag=parseInt(this.state.pm25Flag);
        mpm25PickTime=parseInt(this.state.pm25PickTime);
        msystemTime=parseInt(this.state.systemTime);
        if(Math.floor((msystemTime-mpm25PickTime)/60)<0){
            timeago = 0;
        }else{
            timeago = Math.floor((msystemTime-mpm25PickTime)/60);
        }
        if((this.state.hasSingle == 0)&&(this.state.type == 2)){
          if (Platform.OS === 'android') {
            pm25String = "     ?";
            hasSingleString = "           "+Language.plug_monitor;
          } else {
            pm25String = "   ?";
            hasSingleString = "Language.plug_monitor";            
          }

        }else{
            hasSingleString = '';
            if(parseInt(this.state.pm25)<=9&&parseInt(this.state.pm25)>=0){
                pm25String = '00'+parseInt(this.state.pm25);
            }else if(parseInt(this.state.pm25)<=99&&parseInt(this.state.pm25)>=10){
                pm25String = '0'+parseInt(this.state.pm25);
            }else{
                pm25String = parseInt(this.state.pm25);
            }
        }
        if(mcover>=30){
          this.setState({
            color1:'#2AB9F1',
          });
          console.log('==电池>30  '+mcover);
        }else{
          this.setState({
            color1:'#F86E68FF',
          });
          console.log('==电池<30  '+mcover);
        }
        this.setState({
            count1:count,
            mmodel1:mmodel,
            timeago1:timeago,
            pm25String1:pm25String,
            mpm25Flag1:mpm25Flag,
        });
        console.log('val ·111··············'+Math.floor((msystemTime-mpm25PickTime)/60)+'           '+pm25String+'       '+mpm25PickTime+'  msystemTime     '+msystemTime+'  timeago    '+timeago+' mmodel  '+mmodel);

      }
      
    });
  },
   _run: function(){
    animated=Animated.timing(this.anim, {
      toValue: 0.5 * 60 * 60 * 24,
      duration: 1000 * 60 * 60 * 24,
      easing: Easing.linear
    })
    animated.start(() => this._run());
  },
  render: function(){
  this.anim = this.anim || new Animated.Value(0);
    var img4;
    var length;
    var length1;
    
    if(misCharged==1){
      img4=require('image!ic_chargeing_sc');
      length=0;
      length1=0; 
    }else{
      img4=require('image!ic_charge_sc');
      length=20;
      //length1=20-this.state.power; 
     length1=17/180*deviceWidth*power;
    }
    if(this.state.mode == '0'||this.state.type == 2){
      if(this.state.vehideModel==1){
        time_min = Language.automation_on;
      }else{
         time_min = '';
         timeString = '';
      }      
      console.log('val ····'+this.state.vehideModel+''+timeString);
    }else{
       console.log('val ····sub_getMode·······aaaaa····'+this.state.mode);
      timeString = ' min ago';
      time_min = timeago+timeString;
    }

    console.log('=======22====='+length1+'   '+length+'   '+count);

   if(this.state.mpm25Flag1==2||this.state.mpm25Flag1==3){
      
      animated.stop();
      if(this.state.mpm25Flag1==2){
         isOpentools = true;
      }
      if(this.state.mpm25Flag1==3&&isOpentools){
        tools.alertShow(Language.operationfailed);
        isOpentools = false;
        console.log('地址----------------------'+isOpentools);
      }
      if(this.state.vehideModel==1||(this.state.model==1&&this.state.type==1)){
      console.log('isWait_V    界面222');

       return (
      <View style={[styles.background,{backgroundColor:'white'}]}>
         <Image style={[styles.image,styles.position,{resizeMode: 'stretch'}]} source={require('image!ic_single_circle')}/>
          <View style={[styles.image,styles.position]}>
             <Progress.Circle size={123/180*deviceWidth-12} color = {'#2AB9F1'} thickness={4} progress={count} borderWidth={0} borderColor={'transparent'}/> 
          </View>

                  <View style={{justifyContent:'center',alignItems: 'center',}}>
                  <View style={{height:10/320*deviceHeight,width:20/180*deviceWidth,}}>
                      <View style={{marginRight:1.5,
                            marginLeft:1.5,marginTop:1.5,marginBottom:1.5,width:length1,height:9/320*deviceHeight,backgroundColor:this.state.color1,
                            position:'absolute',justifyContent:'center',alignItems: 'center',}} />        
                      <Image style={{width:20/180*deviceWidth,height:10/320*deviceHeight, resizeMode: 'stretch',
                            position:'absolute',justifyContent:'center',alignItems: 'center',}} 
                            source={img4}/>            
                   </View>
                <View style={{height:110,justifyContent:'center',alignItems: 'center',}}>
                    <View  style={{flexDirection: 'row',alignItems: 'flex-end',marginTop:20}}>
                      <Text allowFontScaling={false} style={[styles.font,styles.fontSizeBig,{color:'#2AB9F1',justifyContent:'center',alignItems: 'center'}]}>  
                         {pm25String} 
                         <Text allowFontScaling={false} style={[styles.font,styles.fontSizeSmall,{color:'#939393',}]}>  
                             {' '}PM2.5{'         '} 
                              <Text allowFontScaling={false} style={[styles.font,styles.fontSizeSmall,{color:'#939393',}]}>{'\n'}{' '}{time_min}</Text> 
                          </Text>                    
                      </Text>  
                   </View>
               </View>
              <TouchableOpacity
                      onPress={()=>{this.close();}}>
                   <Image style={[styles.filterImage,{resizeMode: 'stretch',}]} source={require('image!ic_open')}/> 
              </TouchableOpacity>  
              </View>           
          </View>
      );

      }else{
        console.log('isWait_V    界面555');

       return (
      <View style={[styles.background,{backgroundColor:'white'}]}>
         <Image style={[styles.image,styles.position,{resizeMode: 'stretch'}]} source={require('image!ic_single_circle')}/>
          <View style={[styles.image,styles.position]}>
             <Progress.Circle size={123/180*deviceWidth-12} color = {'#2AB9F1'} thickness={4} progress={count} borderWidth={0} borderColor={'transparent'}/> 
          </View>

          <View style={{justifyContent:'center',alignItems: 'center',}}>
                  <View style={{height:10/320*deviceHeight,width:20/180*deviceWidth,}}>
                      <View style={{marginRight:1.5,
                            marginLeft:1.5,marginTop:1.5,marginBottom:1.5,width:length1,height:9/320*deviceHeight,backgroundColor:this.state.color1,
                            position:'absolute',justifyContent:'center',alignItems: 'center',}} />        
                      <Image style={{width:20/180*deviceWidth,height:10/320*deviceHeight, resizeMode: 'stretch',
                            position:'absolute',justifyContent:'center',alignItems: 'center',}} 
                            source={img4}/>            
                   </View>
                <View style={{height:110,justifyContent:'center',alignItems: 'center',}}>
                    <View  style={{flexDirection: 'row',alignItems: 'flex-end',marginTop:20}}>
                      <Text allowFontScaling={false} style={[styles.font,styles.fontSizeBig,{color:'#2AB9F1',justifyContent:'center',alignItems: 'center'}]}>  
                         {pm25String} 
                         <Text allowFontScaling={false} style={[styles.font,styles.fontSizeSmall,{color:'#939393',}]}>  
                             {' '}PM2.5{'         '}  
                          </Text>  
                          <Text allowFontScaling={false} style={{marginTop:20,fontSize:13,color:'#F1B02A'}}>{'\n    '}{hasSingleString}</Text>                    
                      </Text>  
                   </View>
               </View>
              <TouchableOpacity
                      onPress={()=>{this.close();}}>
                   <Image style={[styles.filterImage,{resizeMode: 'stretch',}]} source={require('image!ic_open')}/> 
              </TouchableOpacity>  
              </View>           
          </View>
      );

      }

   }else{
    console.log('isWait_V    界面444');
       return (
      <View style={[styles.background,{backgroundColor:'white'}]}>
          <View style={[styles.image,styles.position]}>
                <Animated.Image style={[styles.image,styles.position,{resizeMode: 'stretch'},{
                  transform: [
                    {rotate: this.anim.interpolate({
                    inputRange: [0, 1],
                    outputRange: [
                     '0deg', '360deg' 
                     ],
                     })},
                   ]}
                ]} source={require('image!ic_xuanzhuan')}/>
          </View>
          <View style={{justifyContent:'center',alignItems: 'center',}}>
                  <View style={{height:10/320*deviceHeight,width:20/180*deviceWidth,}}>
                      <View style={{marginRight:1.5,
                            marginLeft:1.5,marginTop:1.5,marginBottom:1.5,width:length1,height:9/320*deviceHeight,backgroundColor:this.state.color1,
                            position:'absolute',justifyContent:'center',alignItems: 'center',}} />        
                      <Image style={{width:20/180*deviceWidth,height:10/320*deviceHeight, resizeMode: 'stretch',
                            position:'absolute',justifyContent:'center',alignItems: 'center',}} 
                            source={img4}/>            
                   </View>
         <View style={{height:110,justifyContent:'center',alignItems: 'center'}}>
             <View  style={{flexDirection: 'row',marginTop:20}}>
                <Text allowFontScaling={false} style={[styles.font,styles.fontSizeBig,{color:'#2AB9F1',}]}>  
                   {pm25String} 
                   <Text allowFontScaling={false} style={[styles.font,styles.fontSizeSmall,{color:'#939393',}]}>  
                       {' '}PM2.5{'         '}  
                       <Text allowFontScaling={false} style={[styles.font,styles.fontSizeSmall,{color:'#939393',}]}>{'\n'}{' '}{time_min}</Text> 
                    </Text> 
                     <Text allowFontScaling={false} style={{marginTop:20,fontSize:13,color:'#F1B02A'}}>{'\n'}{hasSingleString}</Text>
                </Text> 
                 
             </View>
             
         </View>
          
        <TouchableOpacity
                onPress={()=>{this.close();}}>
             <Image style={[styles.filterImage,{resizeMode: 'stretch',}]} source={require('image!ic_open')}/> 
        </TouchableOpacity>  
        </View>       
      </View>
      );
   }
  },
  close:function() {
  console.log('===2==2='+this.state.address);
      if (this.state.address != null &&this.state.type == 1){
        Alert.alert(
          Language.prompt+'',
          Language.close_dev +'',
          [
            {text: Language.ok+'', onPress: () => {BlueToothUtil.shutDown(this.state.address);}},
            {text: Language.cancel+'', onPress: () => {return true;}},
          ]
        )
        //BlueToothUtil.shutDown(this.state.address);
        return;
      }
      if(this.state.type == 2){
        if(!this.state.isWait_V){
          //BlueToothUtil.windSpeed(this.state.address,0);
          RCTDeviceEventEmitter.emit('isWait_V',true);
          console.log('isWait_V    待机');
          this.setState({
            isWait_V:true,
          });
        }else{
          RCTDeviceEventEmitter.emit('isWait_V',false); 
          this.setState({
            isWait_V:false,
          });
          console.log('isWait_V    唤醒');
        } 
        return;  
      }
      
  },
});
             //<View  style={{flexDirection: 'row',alignItems: 'flex-end'}}>
                //<Text allowFontScaling={false} style={[styles.font,styles.fontSizeBig,{textAlignVertical:'bottom',}]}>{this.state.pm25}</Text>
                //<Text allowFontScaling={false} style={[styles.font,styles.fontSizeSmall,{textAlignVertical:'bottom'}]}>PM2.5</Text> 
             //</View>
                //<Text allowFontScaling={false} style={[styles.font,styles.fontSizeBig,{textAlignVertical:'bottom',color:'#2AB9F1',}]}>{this.state.pm25}</Text>
                //<Text allowFontScaling={false} style={[styles.font,styles.fontSizeSmall,{textAlignVertical:'bottom'}]}>PM2.5</Text> 
module.exports = SingleIcon;
