'use strict';


import React, { Component } from 'react';

import {
  AppRegistry,
  View,
  Navigator,
  Text,
  Image,
  BackAndroid,
  StyleSheet,
  Dimensions,
} from 'react-native';
import Language from './Language'
var UMeng = require('./UMeng');
var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;

import  TabNavigator from 'react-native-tab-navigator'
var Home = require ('./home');
var Login = require ('./login');
var Health = require ('./health');

export default class Tabbar extends React.Component {
  constructor(props) {
        super(props);
        this.state = {
            selectedTab: 'home'
        };
        
    }


  render() {
        return (
          <TabNavigator tabBarStyle = {{backgroundColor:'#E6E6E6',alignItems:'center',justifyContent:'center', height: deviceHeight * 0.1,}}>
            <TabNavigator.Item
              selected={this.state.selectedTab === 'home'}
              title= {Language.mydevice}
              titleStyle = {{fontSize:12,}}
              selectedTitleStyle = {{color:'#2AB9F1'}}
              renderIcon={() => <Image source={require('image!ic_dev_gray')} style = {styles.size}/>}
              renderSelectedIcon={() => <Image source={require('image!ic_dev_blue')} style = {styles.size}/>}
              onPress={() => {
                              this.setState({ selectedTab: 'home'});
                              UMeng.onEvent('tabbar_mydevice');
                            }
                      }>
              <Home navigator = {this.props.navigator}/>
            </TabNavigator.Item>
            <TabNavigator.Item
              selected={this.state.selectedTab === 'health'}
              title={Language.mall}
              titleStyle = {{fontSize:12,}}
              selectedTitleStyle = {{color:'#2AB9F1'}}
              renderIcon={() => <Image source={require('image!ic_health_gray')} style = {styles.size_mall}/>}
              renderSelectedIcon={() => <Image source={require('image!ic_health_blue')} style = {styles.size_mall}/>}
              onPress={() => {
                              this.setState({ selectedTab: 'health' });
                              UMeng.onEvent('tabbar_heath');
                            }
                      }>
               <Health navigator = {this.props.navigator}/>
            </TabNavigator.Item>
            <TabNavigator.Item
              selected={this.state.selectedTab === 'login'}
              title= {Language.personal}
              titleStyle = {{fontSize:12,}}
              selectedTitleStyle = {{color:'#2AB9F1'}}
              renderIcon={() => <Image source={require('image!ic_person_gray')} style = {styles.size}/>}
              renderSelectedIcon={() => <Image source={require('image!ic_person_blue')} style = {styles.size}/>}
              onPress={() => {
                              this.setState({ selectedTab: 'login' });
                              UMeng.onEvent('tabbar_login');
                            }
                      }>
              <Login navigator = {this.props.navigator}/>
            </TabNavigator.Item>
        </TabNavigator>
        );
    }

  shouldComponentUpdate(nextProps, nextState) {
    return nextState.selectedTab !== this.state.selectedTab;
  }
}

var styles = StyleSheet.create({
  size:{
    width: deviceWidth * 11.5 / 180,
    height:deviceHeight * 12 / 320,
    resizeMode:'stretch',
  },
  size_mall:{
    width: deviceWidth * 6.5 / 180,//19.5,
    height:deviceHeight * 12 / 320,//19.5,
    resizeMode:'stretch',
  }
});

//AppRegistry.registerComponent('feibao', () => Tabbar);
