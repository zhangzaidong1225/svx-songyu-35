/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

var RNFS = require('react-native-fs');
var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter'); /*用于界面组件之间传递消息*/
var DownLoad = require('./download');
import styles from './styles/style_setting'
import styles_feedback from './styles/style_feedback'
import styles_deviceUpload from './styles/style_deviceupload'
import Language from './Language'
import Title from './title'
import  config from './config'


var AsyncStorage = require('./AsyncStorage');
var UMeng = require('./UMeng');



import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  Alert,
  Dimensions,
  PixelRatio,
  TouchableOpacity,
  TouchableWithoutFeedback,
  // AsyncStorage,
  NativeModules,
  NetInfo,
  ToastAndroid,
  BackAndroid,
  DeviceEventEmitter,
  NativeAppEventEmitter,
  ListView,
  Platform,
} from 'react-native';



var mwidth=Dimensions.get('window').width;
var mheight=Dimensions.get('window').height;
var BlueToothUtil=require('./BlueToothUtil');
var path1 = RNFS.DocumentDirectoryPath + '/deviceupdate_33/dev_update.zip';
var path2 = RNFS.DocumentDirectoryPath + '/deviceupdate_34/xinfeng_dev_update.zip';
//android path
var xinfeng_path = RNFS.DocumentDirectoryPath + '/updatefile36/xinfeng_dev_update.zip';
var single_path = RNFS.DocumentDirectoryPath + '/updatefile36/single_dev_update.zip';
var car_path = RNFS.DocumentDirectoryPath + '/updatefile36/car_dev_update.zip';
//ios path
var xinfeng_path2 = RNFS.DocumentDirectoryPath + '/deviceupdate_34/xinfeng_dev_update.zip';
var single_path2 = RNFS.DocumentDirectoryPath + '/deviceupdate_34/single_dev_update.zip';
var car_path2 = RNFS.DocumentDirectoryPath + '/deviceupdate_34/car_dev_update.zip';
//var path1 = RNFS.DocumentDirectoryPath + '/dev_update.zip';
var alertMessage = '设备固件升级中，请稍后...';
var alertMessage1 = '设备升级失败，请重新升级';
var alertMessage2 = '设备升级成功';
var Download = require('./download');
var sub_ble_state = null;
var sub_ble_state1 = null;


// var testDir1Path_tmp = RNFS.DocumentDirectoryPath + '/deviceupdate';
// var testDir1Path = RNFS.DocumentDirectoryPath + '/deviceupdate/dev_update.zip';
// var path_tmp = RNFS.MainBundlePath + '/deviceupdate';
// var DownLoad = require('./download');

var ObligatoryDeviceUpload = React.createClass({

    getDefaultProps:function () {
    return {
          hasGoBack: true,
          backAddress: null,
      };
    },

  getInitialState(){

    return {
      deviceid:'123',
      devicename:'',
      msg:Language.clickgrade,
      percentage:'',
      isBack:true,
      disabled:false,
    };
  },

    componentWillUnmount:function(){
      sub_ble_state.remove();
      sub_ble_state1.remove();
    },

    init_data:function(data){
      if (data.dev_list.length == 0 && !this.state.isBack) {
            this.setState({msg:Language.upgradefail,isBack:true,disabled:true});
            //ToastAndroid.show(Language.upgradefail,ToastAndroid.SHORT);
                Alert.alert(
                    Language.prompt+'',
                    Language.upgradefailalert+'',
                    [
                      {text: Language.ok+'', onPress: () => {RCTDeviceEventEmitter.emit('change_obligatory', false);this.props.navigator.pop();UMeng.onEvent('DeviceUpload_02');}},
                     
                    ]
                  )
      };
      for (var i = 0;i < data.dev_list.length;i++){
        //console.log(data.dev_list[i].addr+'列表中的值');
        //console.log(this.props.deviceid+'传过来的值')

        if (data.dev_list[i].addr == this.props.deviceid){
          //console.log(data.dev_list[i].addr+'   '+data.dev_list[i].percentage+'百分比');
          //console.log(data.dev_list[i].version+'版本号')
          if (data.dev_list[i].percentage != 0  && data.dev_list[i].percentage != null){
            this.setState({msg:data.dev_list[i].percentage+'%',isBack:false});
            console.log(data.dev_list[i].percentage);
          }
          if (data.dev_list[i].update_state == 101){
            console.log(data.dev_list[i].update_state+Language.upgradesuccessed);
              this.setState({msg:Language.upgradesuccessed,isBack:true,disabled:true});
              RCTDeviceEventEmitter.emit('back_up',true);
              console.log('升级完成');
                 Alert.alert(
                    Language.prompt+'',
                    Language.upgradesuccess+'',
                    [
                      {text: Language.ok+'', onPress: () => {RCTDeviceEventEmitter.emit('change_obligatory', false);this.props.navigator.pop();UMeng.onEvent('DeviceUpload_01');}},
                     
                    ]
                  )
              
          }
          if (data.dev_list[i].update_state == 102){
             console.log(data.dev_list[i].update_state+Language.upgradefail);
            this.setState({msg:Language.upgradefail,isBack:true,disabled:true});
             RCTDeviceEventEmitter.emit('back_up',true);
            //ToastAndroid.show(Language.upgradefail,ToastAndroid.SHORT);
                Alert.alert(
                    Language.prompt+'',
                    alertMessage1,
                    [
                      {text: Language.ok+'', onPress: () => {RCTDeviceEventEmitter.emit('change_obligatory', false);this.props.navigator.pop();UMeng.onEvent('DeviceUpload_02');}},
                     
                    ]
                  )
          }

        }
      }
    },

  componentDidMount:function (){
    this.gotoUpload(this.props.deviceid,this.props.devicename,this.props.devicetype);
    this.setState({deviceid:this.props.deviceid,devicename:this.props.devicename,devicetype:this.props.devicetype});

    sub_ble_state = NativeAppEventEmitter.addListener('ble_state',(data) => {
      this.init_data(data);
    });

    sub_ble_state1 = DeviceEventEmitter.addListener('ble_state',(data) => {
      this.init_data(data);
    });

  },

  _onClick(){
    if (this.state.isBack){
        this.props.navigator.pop()
        this.setState({isBack:false});
    }else {
        Alert.alert(
        Language.prompt+'',
        Language.upgrading+'',
        [
          {text: Language.ok+'', onPress: () => {UMeng.onEvent('DeviceUpload_03');return true;}},
         
        ]
      )
    }
  },


  render(){
    var navigator = this.props.navigator;


    if (config.global_language =='en'){

      return (
        <View style = {styles_deviceUpload.container}>
        <Title 
          title = {Language.firmwareupdate} 
          hasGoBack = {this.props.hasGoBack} 
          backAddress = {this.props.backAddress} 
          goUpgradeBack = {() => {this._onClick();}}
          navigator = {this.props.navigator}/>

            <View style = {styles_deviceUpload.view1}>
              
                <View style = {styles_deviceUpload.borderView}>
                 <TouchableOpacity 
                      disabled = {this.state.disabled}
                      onPress = { () => {UMeng.onEvent('DeviceUpload_05');}}>
                    <View style = {{width:100,height:100,justifyContent:'center',alignItems:'center',backgroundColor:'white',borderWidth:0,}}>
                      <Text allowFontScaling={false} style = {{fontSize:18,color:'rgb(23, 201, 180)',}}>{this.state.msg}</Text>
                    </View>
                 </TouchableOpacity> 

                </View>

            </View>

               
            <View style = {{height:150,marginTop:20,marginLeft:20,marginRight:20,backgroundColor:'white',justifyContent:'center',alignItems:'center'}}>
                <View style = {{}}>
                  <Text allowFontScaling={false} style = {styles_deviceUpload.text2}>{Language.upgrademsg1}</Text>
                  <Text allowFontScaling={false} style = {styles_deviceUpload.text2}>{Language.upgrademsg2}</Text>
                  <Text allowFontScaling={false} style = {styles_deviceUpload.text2}>{Language.upgrademsg3}</Text>
                </View>
            </View>
    </View>
      );
    }



    return(
    
    <View style = {styles_deviceUpload.container}>

        <Title 
          title = {Language.firmwareupdate} 
          hasGoBack = {this.props.hasGoBack} 
          backAddress = {this.props.backAddress} 
          goUpgradeBack = {() => {this._onClick();}}
          navigator = {this.props.navigator}/>

          <View>
            <View style = {styles_deviceUpload.view1}>
              
                  <View style = {styles_deviceUpload.borderView}>
                 <TouchableOpacity 
                      disabled = {this.state.disabled}
                      onPress = { () => {UMeng.onEvent('DeviceUpload_05');}}>
                    <View style = {styles_deviceUpload.view2}>
                      <Text allowFontScaling={false} style = {styles_deviceUpload.text}>{this.state.msg}</Text>
                    </View>
              </TouchableOpacity> 

                  </View>

            </View>
          </View>
               
            <View style = {{height:150,marginTop:20,backgroundColor:'white',justifyContent:'center',alignItems:'center'}}>
                <View style = {{}}>
                  <Text allowFontScaling={false} style = {{fontSize:16,color:'#323232',paddingTop:15,}}>{Language.upgrademsg1}</Text>
                  <Text allowFontScaling={false} style = {{fontSize:16,color:'#323232',paddingTop:15,}}>{Language.upgrademsg2}</Text>
                  <Text allowFontScaling={false} style = {{fontSize:16,color:'#323232',paddingTop:15,}}>{Language.upgrademsg3}</Text>
                </View>
            </View>


    </View>
    );
  },

  gotoUpload:function(deviceid,devicename,devicetype){

    if (config.global_language == 'en'){
      this.setState({msg:Language.readygrade,isBack:false,disabled:true});
      RCTDeviceEventEmitter.emit('back_up',false);

            if (Platform.OS === 'android') {
          if(devicetype==0){
            path2 = xinfeng_path;
          }else if(devicetype==1){
            path2 = single_path;
          }else if(devicetype==2){
            path2 = car_path;
          }
            DownLoad.exists(path2, (result) =>{
                          console.log('====='+path2+'检测文件是否存在' + result);
                          if (result) {
                            BlueToothUtil.deviceUpdate(deviceid,devicename,path2);
                          }

        });
      }else{

        if(devicetype==0){
            path2 = xinfeng_path2;
          }else if(devicetype==1){
            path2 = single_path2;
          }else if(devicetype==2){
            path2 = car_path2;
          }

          BlueToothUtil.deviceUpdate(deviceid,devicename,path2);

        // BlueToothUtil.deviceUpdate(deviceid,devicename,path1);
      }



      // if (devicetype == 0){
      //       console.log('忻风升级');
      //      BlueToothUtil.deviceUpdate(deviceid,devicename,path1);
      // }else if (devicetype == 1){
      //       console.log('单品升级 ');
      //      BlueToothUtil.deviceUpdate(deviceid,devicename,path2);
      // }else {
      //     console.log('车载升级 ');
      //      BlueToothUtil.deviceUpdate(deviceid,devicename,path3);
      // }
    }
    else {
      this.setState({msg:Language.readyupgrade,isBack:false,disabled:true});
      RCTDeviceEventEmitter.emit('back_up',false);


        if (Platform.OS === 'android') {
          if(devicetype==0){
            path2 = xinfeng_path;
          }else if(devicetype==1){
            path2 = single_path;
          }else if(devicetype==2){
            path2 = car_path;
          }
            DownLoad.exists(path2, (result) =>{
                          console.log('====='+path2+'检测文件是否存在' + result);
                          if (result) {
                            BlueToothUtil.deviceUpdate(deviceid,devicename,path2);
                          }

        });
      }else{

        if(devicetype==0){
            path2 = xinfeng_path2;
          }else if(devicetype==1){
            path2 = single_path2;
          }else if(devicetype==2){
            path2 = car_path2;
          }

          BlueToothUtil.deviceUpdate(deviceid,devicename,path2);

        // BlueToothUtil.deviceUpdate(deviceid,devicename,path1);
      }


      // if (devicetype == 0){
      //       console.log('忻风升级');
      //      BlueToothUtil.deviceUpdate(deviceid,devicename,path1);
      // }else if (devicetype == 1){
      //       console.log('单品升级 ');
      //      BlueToothUtil.deviceUpdate(deviceid,devicename,path2);
      // }else {
      //     console.log('车载升级 ');
      //      BlueToothUtil.deviceUpdate(deviceid,devicename,path3);
      // }
    }



      // this.setState({msg:Language.readyupgrade,isBack:false,disabled:true});
      // RCTDeviceEventEmitter.emit('back_up',false);
          
      // console.log('文件下载完成');


     // BlueToothUtil.deviceUpdate(deviceid,devicename,path1);


    // NetInfo.isConnected.fetch().done(
    //   (isConnected) => {

    //     if (isConnected) {
    //       this.setState({msg:Language.readyupgrade,isBack:false,disabled:true});
    //       RCTDeviceEventEmitter.emit('back_up',false);
          
    //       console.log('文件下载完成');
    //       BlueToothUtil.deviceUpdate(deviceid,devicename,path1);


    //     }else {
    //       console.log('文件没有下载');
    //       ToastAndroid.show(Language.connectnetwork,ToastAndroid.SHORT);
    //     }
    //   }
    //   );
  },

});

module.exports = ObligatoryDeviceUpload;



