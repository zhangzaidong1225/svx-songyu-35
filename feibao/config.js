'use strict';

var config = {
	os : 0,
	platform : 0,
	xinfeng_factor : 1,
	car_factor : 2,
	language : 'zh',
	phone_modal : 0,
	global_language:'zh',
	version : '1.2.2',
	min_device_version : '40',
	single_device_minversion : '0',
	car_min_device_minversion : '0',
	server_base :'https://mops.lianluo.com/vida/xinfeng',
	//server_base_test: 'https://139.198.9.171/',
	server_base_test: 'https://mops-xinfeng-dev.lianluo.com/',
	server_base_normal: 'https://mops.lianluo.com/vida/xinfeng',
	upload_uri : '/iface/system/update',   //升级接口
	dev_update_uri:'/iface/system/dev_update',//固件升级
	privilege_uri:'/web/discount',
    health_uri:'/web/health', 
	ad_uri:'/iface/info/ad',
	feedback_uri:'/iface/info/feedback',//反馈
	area_list_uri:'/iface/info/area_list',
	share_uri:'/web/share',
	filter_buy:'/web/filter_buy',//滤芯购买
	user_protocol:'/web/agreement',//用户协议
	user_repass_uri: '/iface/user/repass', //修改密码
	user_login_uri: '/iface/user/login', //用户登录
	user_register_uri: '/iface/user/register', //用户注册
	user_getcode_uri: '/iface/user/getcode', //获取验证码
	user_captcha_uri: '/iface/info/find_captcha', //防伪码查询
	share_icon_uri: '/dist/img/mops-icon.png', //分享图标
	share_icon_uri_test:'/dist/img/mops-icon.png',
	weather_uri: '/iface/weather/v2', // 天气请求接口
	system_time:'/iface/info/getServerTime',
    filtor_factor:'/iface/weather/weaFactor',//滤芯因子

	//network 地区
	region_base:'https://api.thinkpage.cn/v3/',
	air_request_url:'air/now.json?key=BA57CE6C5B4FC19477D34CE78CD6E841&language=zh-Hans&scope=city&location=',
    weather_request_url:'weather/daily.json?key=BA57CE6C5B4FC19477D34CE78CD6E841&start=0&days=1&language=zh-Hans&unit=c&location=',
   	wind_request_url:'weather/now.json?key=BA57CE6C5B4FC19477D34CE78CD6E841&language=zh-Hans&unit=c&location=',
    city_key_url:'location/search.json?key=BA57CE6C5B4FC19477D34CE78CD6E841&q=',
	ip_to_city_url:'http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=json',
};

module.exports = config;
// http://feibao01.esh001.com/web/discount    优惠活动
// http://feibao01.esh001.com/web/health   健康生活
// http://feibao01.esh001.com/web/share 分享
//http://feibao01.esh001.com/web/filter_buy 
//http://mops.lianluo.com/vida/xinfeng/iface/system/dev_update?item_id=3
// http://139.198.9.171/iface/info/getServerTime---时间戳 