/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';


var Log = require('./net');
var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter'); /*用于界面组件之间传递消息*/
//var AsyncStorage = require('./AsyncStorage');


import  styles from './styles/style_icon'
import Language from './Language'
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Animated,
  Easing,
  DeviceEventEmitter,
  NativeModules,
  ToastAndroid,
  AsyncStorage,
  PanResponder,
  NativeAppEventEmitter
} from 'react-native';
var viewLength=221;
var BlueToothUtil= require('./BlueToothUtil');
var sub_currentDevice;
var sub_close;
var sub_wait;
var misCharged;
var mwind_time;
var filter_in;
var aaa;
var mwind_time11,mwind_time1;
var icon= React.createClass({

  getInitialState: function() {
   return {
      cover:100,
      isOpen:1,
      filter:0,
      S:'',
      num:28,
      wind_time:0,
      changefilter:'',
      power:null,
      color1:'#96FB69',
      isCharged:0,
      use_time_this:0,
    };
  },

  componentWillUnmount:function(){
      sub_currentDevice.remove();
      //sub_close.remove();
      //sub_wait.remove();
  },
  componentDidMount:function(){
    this._run();
    this.startAnimation();
    sub_currentDevice = RCTDeviceEventEmitter.addListener('message_select_item',(val)=>{

      if(val == null){
        this.setState({
         isOpen:1,
        });
      }else{
          var tmp_isopen = 0
          if (val.speed == 0) {tmp_isopen = 2};
          
          var mcover=parseInt(val.battery_power);
            this.setState({
             isOpen:tmp_isopen,
             cover:val.battery_power,
             power:mcover/5,
             num:val.use_time * 2,
             filter:val.use_time,
             isCharged:val.ischarged,
             wind_time:val.wind_time,
             use_time_this:val.use_time_this,
            });
            filter_in = val.filter_in;
            if(filter_in==1){
              aaa.stop();
            }
            console.log('====isOpen========'+this.state.isOpen);
            misCharged=parseInt(this.state.isCharged);
            mwind_time=parseInt(this.state.use_time_this)*1000;
            
          if(this.state.isOpen == 0||this.state.isOpen==2){
            if (parseInt(this.state.filter)>=198000*0.8) {
              this.setState({
                changefilter:'更换滤芯',
              });
            }else{
              this.setState({
                changefilter:'',
              });
            }

          }
            if(mcover>=60){
              this.setState({
                color1:'#96FB69',
              });
            }else if (mcover>=20) {
              this.setState({
                color1:'#D0FB69',
              });
            }else{
              this.setState({
                color1:'#F86E68',
              });
            }
          
      }
    }
    );

  },
   _run: function(){
    Animated.timing(this.anim, {
      toValue: 1 * 60 * 60 * 24,
      duration: 1000 * 60 * 60 * 24,
      easing: Easing.linear
    }).start();
  },
  startAnimation:function() {
      this.anim4.setValue(0);
      aaa = Animated.sequence([ 
          Animated.timing(this.anim4, {
              toValue: 1,
              duration: 1500,
              easing: Easing.linear
          }),     
          Animated.timing(this.anim4, {
              toValue: 0,
              duration: 1500,
              easing: Easing.linear
          }),
      ])
      aaa.start(() => this.startAnimation());
  },
  render: function(){
    var img1,img2,img3,img_3,img4;
    var styles1;
    var length;
    var length1;
    var mcover=parseInt(this.state.cover);
    var power=mcover/5;
    var mfilter=parseInt(this.state.filter);
    var string;
    
    styles1=[styles.image,styles.position,{resizeMode: 'stretch'}];
	  img2=require('image!ic_fan_open');
    img1=require('image!ic_energy_hundred');
    this.anim = this.anim || new Animated.Value(0);
    this.anim4 = this.anim || new Animated.Value(0);
        if(mwind_time<1000000){
          mwind_time1=Math.floor(mwind_time/1000);
          string='dm³'; 
        }else if(mwind_time>=1000000){
          mwind_time11=Math.floor(mwind_time/10000);
          if(mwind_time11>=1000){
             mwind_time1=Math.floor(mwind_time11/10)/10;
          }else{
            mwind_time1=mwind_time11/100;
          }
          if(mwind_time1>100&&mwind_time1<=999){
            mwind_time1 = Math.floor(mwind_time1*10/10);
          }else if(mwind_time1>999){
            mwind_time1 = 999;
          }
          string='m³';           
        }

        if(misCharged==1){
          img4=require('image!ic_chargeing');
          length=0;
          length1=0; 
        }else{
          img4=require('image!ic_power');
          length=20;
          length1=20-this.state.power; 
        }

    if(this.state.isOpen == 0){  //开机并未待机

        if(mcover>=60){
          img1=require('image!ic_energy_hundred');
        }else if(mcover>=20){
          img1=require('image!ic_energy_fifty1');
        }else{
          img1=require('image!ic_energy_ten');
        }
        if(filter_in==0){
          img_3=require('image!ic_nofilter'); 
        }else{
            if(mfilter>=198000*0.8){
              img3=require('image!ic_filter_red');
            }else if (mfilter>=198000*0.75) {
              img3=require('image!ic_filter_three'); 
            }else if (mfilter>=198000*0.50) {
              img3=require('image!ic_filter_two');           
            }else if (mfilter>=198000*0.25) {
              img3=require('image!ic_filter_one');        
            }else {
              img3=require('image!ic_filter_new');        
            }
        }

        return (
          <View style={styles.background}>
            <Image style={styles1} source={require('image!ic_circle_decorate')}/>
            <Image style={styles1} source={img1}/>
             <View style={[styles.image,styles.adjust2]}>
                <View style={styles.changefilterView}>
                   <Text allowFontScaling={false} style={[styles.font,{fontSize:13,}]}>{this.state.changefilter}</Text>
                </View>
                 <View style={styles.adjust1}>
                    <View style={{marginRight:2.5,
                          marginLeft:2.5,marginTop:2.5,marginBottom:2.5,width:length,height:7,backgroundColor:this.state.color1,
                          position:'absolute',justifyContent:'center',alignItems: 'center',}} />        
                    <View style={{marginLeft:2.5,marginTop:2.5,marginBottom:2.5,width:length1,height:7,backgroundColor:'#323c4b',
                          position:'absolute',justifyContent:'center',alignItems: 'center',}} />
                    <Image style={{width:25,height:12, resizeMode: 'stretch',
                          position:'absolute',justifyContent:'center',alignItems: 'center',}} 
                          source={img4}/>            
       
                 </View>
                 <Text allowFontScaling={false} style={[styles.font,styles.fontSizeSmall,styles.adjustMarginTop]}>已过滤空气</Text>
                 <View  style={[styles.view,{}]}>
                    <Text allowFontScaling={false} style={[styles.font,styles.fontSizeBig]}>
                       {mwind_time1}
                    </Text>
                 </View>
               <View style={styles.adjustMarginTop}>
                  <Text allowFontScaling={false} style={[styles.font,styles.fontSizeSmall]}>{string}</Text>
               </View>

               <View style={[styles.adjustMarginTop]}>
                <Animated.Image style={[styles.filterImage1,{resizeMode: 'stretch',},{
                      opacity: this.anim4.interpolate({
                        inputRange: [0, 1],
                        outputRange: [0, 1],
                      }) 
                  }
                  ]} source={img_3}/>
                  <Image style={[styles.filterImage1,styles.position1,{resizeMode: 'stretch'}]} source={img3}></Image>




               </View>
              </View>

              <Animated.Image style={[styles.image,styles.position,{resizeMode: 'stretch'},{
                    transform: [
                      {rotate: this.anim.interpolate({
                        inputRange: [0, 1],
                        outputRange: [
                          '0deg', '360deg' 
                        ],
                      })},
                    ]}
                  ]} source={require('image!ic_fan_open')}/>
            </View>
          ); 
    }
                      //<View style={[styles.changefilterView2, {position: 'absolute',}]}>
                    //<Text allowFontScaling={false} style={[styles.font,styles.adjustFont]}>{this.state.changefilter}</Text>
                  //</View>
    if(this.state.isOpen == 1){  //设备未连接
      img1=require('image!ic_energy_off');
      img2=require('image!ic_fan_off');
      return (
      <View style={styles.background}>
         <Image style={styles1} source={require('image!ic_circle_decorate')}/>
         <Image style={styles1} source={img1}/>
         <Image style={styles1} source={img2}/>
         <Text allowFontScaling={false} style={[styles.font,styles.fontSizeSmall]}>{Language.connect_smart_dev}</Text>
         <Image style={[styles.image,styles.imageOff]} source={require('image!ic_off')}/>
         <Text allowFontScaling={false} style={[styles.font,styles.fontSizeSmall]}>m³</Text> 
         <Image style={[styles.image,styles.filterImage,{resizeMode: 'stretch'}]} source={require('image!ic_filter_over')}/>
      </View>
      );
    }
    if(this.state.isOpen == 2){  //待机
       if(mcover>=60){
          img1=require('image!ic_energy_hundred');
        }else if(mcover>=20){
          img1=require('image!ic_energy_fifty1');
        }else{
          img1=require('image!ic_energy_ten');
        }
        if(filter_in==0){
          img_3=require('image!ic_nofilter');
        }else{
            if(mfilter>=198000*0.8){
              img3=require('image!ic_filter_red');
            }else if (mfilter>=198000*0.75) {
              img3=require('image!ic_filter_three'); 
            }else if (mfilter>=198000*0.50) {
              img3=require('image!ic_filter_two');            
            }else if (mfilter>=198000*0.25) {
              img3=require('image!ic_filter_one');        
            }else {
              img3=require('image!ic_filter_new');        
            }
        }

        return (
          <View style={styles.background}>
            <Image style={styles1} source={require('image!ic_circle_decorate')}/>
            <Image style={styles1} source={img1}/>
             <View style={[styles.image]}>


                <View style={styles.changefilterView1}>
                    <Text allowFontScaling={false} style={[styles.font,{fontSize:13,}]}>{this.state.changefilter}</Text>
                </View>

                 <View style={{height:12,width:25,}}>
                    <View style={{marginRight:2.5,
                          marginLeft:2.5,marginTop:2.5,marginBottom:2.5,width:length,height:7,backgroundColor:this.state.color1,
                          position:'absolute',justifyContent:'center',alignItems: 'center',}} />        
                    <View style={{marginLeft:2.5,marginTop:2.5,marginBottom:2.5,width:length1,height:7,backgroundColor:'#323c4b',
                          position:'absolute',justifyContent:'center',alignItems: 'center',}} />
                    <Image style={{width:25,height:12,resizeMode: 'stretch',
                          position:'absolute',justifyContent:'center',alignItems: 'center',}} 
                          source={img4}/>            
       
                 </View>
                 <Text allowFontScaling={false} style={[styles.font,styles.fontSizeSmall,styles.adjust3]}></Text>
                 <View  style={[styles.view]}>
                    <Text allowFontScaling={false} style={[styles.font,{fontSize:22}]}>
                       {Language.sleep}
                    </Text>
                 </View>
               <View style={styles.adjust3}>
                  <Text allowFontScaling={false} style={[styles.font,styles.fontSizeSmall]}></Text>
               </View>
               <View style={[styles.filterImage1,{marginTop:10,}]}>
                <Animated.Image style={[styles.filterImage1,{resizeMode: 'stretch',
                          position:'absolute',top:0,left:0},{
                      opacity: this.anim4.interpolate({
                        inputRange: [0, 1],
                        outputRange: [0, 1],
                      }) 
                  }
                  ]} source={img_3}/>
                  <Image style={[styles.filterImage1,{resizeMode: 'stretch',position:'absolute',top:0,left:0}]} source={img3}></Image>



               </View>
               </View>
            </View>
          );  

    }
  }

});
                  //<View style={[styles.changefilterView2, {position: 'absolute',}]}>
                   // <Text allowFontScaling={false} style={[styles.font,styles.adjustFont]}>{this.state.changefilter}</Text>
                  //</View>
module.exports = icon;
