'use strict'

var RNFS = require('react-native-fs');

var XF_Slider = require ('./xf_slider');
var Whether_Circle = require ('./whether_circle');
var SingleHome = require ('./singleHome');
var CarPage = require ('./carPage');
import Tabbar from './tabbar'
import ModalBox from './modal'
import styles_actionbar  from './styles/style_actionbar'
import styles_feedback from './styles/style_feedback'
import styles_modal1 from './styles/style_home_modal'
var share = require('./share');
import  config from './config'
import tools from './tools'
import HomeTitle from './homeTitle'
import Language from './Language'
import XinfengPage from './xinfengPage'

var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter'); /*用于界面组件之间传递消息*/
var UMeng = require('./UMeng');

var number = [];
var aa ;
var bb;

var subscription = null;
import React, { Component } from 'react';
import {
     AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    Navigator,
    AsyncStorage,
    TouchableHighlight,
    TouchableOpacity,
    PixelRatio,
    NativeModules,
    // AsyncStorage,
    ToastAndroid,
    NetInfo,
    ListView,
    Platform,
    Alert,
    Easing,
    Animated,
    // DeviceEventEmitter,
    NativeAppEventEmitter,
} from 'react-native';

var BlueToothUtil=require('./BlueToothUtil');
var Utils = require('./utils');

var  DownLoud= NativeModules.Share;
var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;


var timing = Animated.timing;
var aaa = null;
var speed = 6000;

// var viewHeight;
// var sel_height;
// var openView = false;
// var title_height;
// var image_height;
// var font_size;
// var line_height;
// var select_string;
// var margin_top;
// var margin_top_modal;
// var touch_color;

// var BlueToothUtil= require('./BlueToothUtil');

var DownLoad = require('./download');

//var DeviceVersion_URL = 'http://feibao.esh001.com/iface/system/dev_update';
//tmp
var testDir1Path_tmp = RNFS.DocumentDirectoryPath + '/upload_tmp';
//tmp存储路径
var testFile3Path_tmp = RNFS.DocumentDirectoryPath + '/upload_tmp/dev_update.zip';
//存储目录
var testDir1Path = RNFS.DocumentDirectoryPath + '/upload';
var testFile3Path = RNFS.DocumentDirectoryPath + '/upload/dev_update.zip';


var sub_ble_state = null;
// var sub_ble_state2=null;
var sub_currentWind  = null;//监听手动风速值

var subscription_title_modalBox=null;
var sub_currentDevice = null;
//同步成最新添加设备
var sub_addToCurrentDevice = null;
var subscription2=null;
var subscription3=null;

var connectd_list = [];
var isopen;
var type_img;

var queue_wind;//风速管理队列
var factorMap;

var Home1 = React.createClass({
    getInitialState:function (){

      return {
        dataSource: new ListView.DataSource({
          rowHasChanged: (row1, row2) => row1 !== row2,
        }),
        code:1,
        isConnected:null,
        isChange:false,
        name:Language.xinfeng,
        address:'',
        wait:Language.sleep,
        // pselect_item:null,
        localVer:1,
        version:1,
        url:'',
        md5:'',
        md5Message:'',
        info:'',
        type:-1,
        speed:-1,
      }
    },

    componentWillMount(){
      // console.log('滤芯--componentWillMount---');
      queue_wind = this.Queue();
      this.timer();
    },
  componentWillUnmount:function(){
    queue_wind._clear();
    sub_ble_state.remove();
    // sub_ble_state2.remove();
    sub_currentDevice.remove();
    sub_addToCurrentDevice.remove();
    subscription2.remove();
    subscription_title_modalBox.remove();
    sub_currentWind.remove();
    this.interval && clearInterval(this.interval);
  },
  timer() {
    // console.log('滤芯--timer()---');
      this.interval = setInterval(() => {
        for (let item of factorMap.entries()) {
          if(item[1] == 0) {
              this.getXinfeng_factor(item[0]);
          }
          if(item[1] == 2){
              this.getCar_factor(item[0]);
          }
          // console.log('滤芯--2222222---'+item[0]+"       "+item[1]);
        }
      }  , 600000); 
  },
  init_data:function(data){
          // console.log('滤芯--init_data----');
          connectd_list = [];
          for (var i = 0; i < data.dev_list.length; i++) {
            
            if(data.dev_list[i].isconnectd == 1){
              connectd_list.push(data.dev_list[i]);

              if(factorMap.has(data.dev_list[i].addr)){
              }else{
                factorMap.set(data.dev_list[i].addr, data.dev_list[i].type);
                // console.log(factorMap);

                if(data.dev_list[i].type == 0){
                    this.getXinfeng_factor(data.dev_list[i].addr);
            }
                if(data.dev_list[i].type == 2){
                    this.getCar_factor(data.dev_list[i].addr);
                }

          }

            }

            if(data.dev_list[i].isconnectd == 0){
              factorMap.delete(data.dev_list[i].addr);
              // console.log('滤芯map有忻风---delete--map-');
               console.log(factorMap);
            }

          }
          AsyncStorage.getItem('select_item_addr', (error, result)=>{
            var tmp_select_item = null;
            var tmp_number = [];
            for (var i = 0; i < connectd_list.length; i++) {
              tmp_number.push({id:connectd_list[i].addr, name:connectd_list[i].name,type:connectd_list[i].type});

              if(result == connectd_list[i].addr){
                tmp_select_item = connectd_list[i];
              }
            }
            if(tmp_select_item == null){
              if(connectd_list.length > 0){
                tmp_select_item = connectd_list[0];
              }
            }
            if(tmp_select_item == null){
              this.setState({
                  name:Language.xinfeng,
                  address:null,
                  type:-1,
                  dataSource: this.state.dataSource.cloneWithRows(tmp_number),
                });
            }else{
              var  tmp_str = Language.awake;
              // if (tmp_select_item.speed > 0) {tmp_str = Language.sleep;}

              if (queue_wind._size() > 0){

                if (queue_wind._getStart() > 0 ) {tmp_str = Language.sleep;}  
                  queue_wind._first();
                if (queue_wind._getStart() != tmp_select_item.speed) {
                  // console.log('home``2``'+queue_wind._quere());
                  BlueToothUtil.windSpeed(tmp_select_item.addr,parseInt(queue_wind._getStart()));
                } else {
                  queue_wind._clear();
                }
              } 
              else 
              {
                if (tmp_select_item.speed > 0) {tmp_str = Language.sleep;}
              }
              console.log('home``2``'+queue_wind._quere());

                // if ((this.state.wait != tmp_str) || (this.state.address != tmp_select_item.addr) || (this.state.pselect_item.speed != tmp_select_item.speed)){

                // }
                this.setState({
                    wait:tmp_str,
                    name:tmp_select_item.name,
                    address:tmp_select_item.addr,
                    type:tmp_select_item.type,
                    // pselect_item:tmp_select_item,
                    dataSource: this.state.dataSource.cloneWithRows(tmp_number),
                  },() => {
                    // console.log('home``wait```'+this.state.wait);
                  });

            }
            // console.log(tmp_select_item);
            RCTDeviceEventEmitter.emit('message_select_item',tmp_select_item); 
          });
  },
  componentDidMount:function (){
    factorMap = new Map();
    BlueToothUtil.auto_connect();
    this.check_update();

    //from xinfengSlider
    sub_currentWind = RCTDeviceEventEmitter.addListener('queue_wind',(val) => {
      console.log('home----queue_wind------'+val);
      queue_wind._push(val);

      // if (queue_wind._first() != this.state.pselect_item.speed){
      //     BlueToothUtil.windSpeed(this.state.pselect_item.addr,parseInt(val));
      // }
    });
    //from xinfengIcon
    subscription2 = RCTDeviceEventEmitter.addListener('alert',(val) => {

      console.log('alert---2---' +  val);

      var  tmp_str = Language.awake;
      // if (tmp_select_item.speed > 0) {tmp_str = Language.sleep;}

      if (queue_wind._size() > 0){
        if (queue_wind._getStart() > 0 ) {tmp_str = Language.sleep;}  
      } 
      else 
      {
        // console.log('home``4``null--'+queue_wind._quere() +'--'+this.state.pselect_item.speed);

        if (parseInt(val) > 0) {tmp_str = Language.sleep;}
      }
      // console.log('home``4``'+queue_wind._quere() +'--'+this.state.wait +'--'+tmp_str + '--'+this.state.pselect_item.speed);

      if (this.state.wait != tmp_str){
        this.setState({
          wait:tmp_str,
        });
      }

      this.refs.modal2.open();
    });
    subscription_title_modalBox = RCTDeviceEventEmitter.addListener('title_modalBox',(val) => {
      // console.log('-----  -alert------');
      UMeng.onEvent('home_02');
      this.refs.modal.open();

    });  


     
    sub_ble_state = NativeAppEventEmitter.addListener(
         'ble_state',
         (data) => {
            this.init_data(data);
            console.log('滤芯  ble_state------');
         }
     );  
    BlueToothUtil.sendMydevice();

    sub_addToCurrentDevice = NativeAppEventEmitter.addListener(
         'add_to_current_device',
         (data) => {
            Utils.getSelectItemAddr((error, data) => {
              console.log("address_send---send address - 4"+data);
                  AsyncStorage.setItem('select_item_addr', data, (error)=>{
                  });
            });
          console.log("address_send---send address - 2"+data);
         }
     );
    /*变换头部*/
    sub_currentDevice = RCTDeviceEventEmitter.addListener('msg_select_item',(val)=>{
      var select_item = null;
      if (this.state.address != val.id){
          queue_wind._clear();
      }


      console.log("pselect_item---name--"+val.name +'--size--'+ queue_wind._size());



      for (var i = 0; i < connectd_list.length; i++) {
        if(connectd_list[i].addr == val.id){
          select_item = connectd_list[i];
        }
      }
      var tmp_str = Language.awake;
      if (select_item.speed > 0) {tmp_str = Language.sleep;}

      console.log("pselect_item---speed--"+select_item.speed);

        this.setState({
          wait:tmp_str,
          address:val.id,
          name:val.name,
          type:val.type,
          // pselect_item:select_item,
        });
     // console.log("pselect_item--speed--"+this.state.pselect_item.speed);

      isopen=select_item;
      RCTDeviceEventEmitter.emit('message_select_item',select_item); 
      this.refs.modal.close();
      // if (Platform.OS == 'ios') {
      //   BlueToothUtil.isVersion10((flag) => {
      //     if (flag) {
      //     this.refs.modal.close();
      //     } else {
      //       this.close_view();
      //     }
      //   });
      // } else {
      //   this.refs.modal.close();

      // }
    });


  },

/**
 * [Queue]
 * @param {[Int]} size [队列大小]
 */
 Queue:function(size) {
    var queue_ = [];

    //向队列中添加数据
    queue_._push = function(data) {
      if (data == null) {
        return false;
      }
      //如果传递了size参数就设置了队列的大小
      if (size != null && !isNaN(size)) {
        if (queue_.length == size) {
          queue_.pop();
        }
      }
      queue_.unshift(data);//在前端添加
      return true;
    };

    //从队列中取出数据
    queue_._pop = function() {
      return queue_.pop();
    };

    //返回队列的大小
    queue_._size = function() {
        return queue_.length;
    }

    //返回队列的内容
    queue_._quere = function() {
        return queue_;
    }

    queue_._clear = function() {
      while (queue_.length > 0) {
        queue_.pop();
      }
      return true;
    }

    //保留第一个元素
    queue_._first = function(){
      for (let i = queue_.length - 1;i >0;i--){
          queue_.pop();
      }
      return true;
    }

    //获取队列最后一个元素
    queue_._getEnd = function() {
      if (queue_.length > 0) {
        return queue_[queue_.length - 1];
      } else {
        return null;
      }
    }

    //获取队列第一个元素
    queue_._getStart = function() {
      if (queue_.length > 0) {
        return queue_[0];
      } else  {
        return null;
      }
    }

    return queue_;
},


  check_update:function(){

    var REQUIRE_URL = config.server_base + config.upload_uri + '?var=' + config.version + '&os=' + config.os+'&version='+config.version+'&platform='+config.platform+'&phone_modal='+config.phone_modal+'&language='+config.language;
    console.log('REQUIRE_URL: ' + REQUIRE_URL);
    if (Platform.OS === 'android') {
          NetInfo.isConnected.fetch().done(
              (isConnected) => { 
                if (isConnected) {
                fetch (REQUIRE_URL)
              .then ((response) => response.json())
              .then((responseData) => {
                if(responseData.code == 101){
                  this.gotoUpdate();
                }
              })
              .catch((error) => {
                 //ToastAndroid.show(''+error,ToastAndroid.SHORT);
              });
            }
            }
          );
    }
 },
  getXinfeng_factor:function(address){
    var REQUIRE_URL = config.server_base + config.filtor_factor + '?item_id=' +config.xinfeng_factor;
    console.log('REQUIRE_URL: ' + REQUIRE_URL);
    NetInfo.isConnected.fetch().done(
        (isConnected) => { 
          if (isConnected) {
          fetch (REQUIRE_URL)
        .then ((response) => response.json())
        .then((responseData) => {
          if(responseData.code == 0){
            // console.log("滤芯 因子：" +responseData.data.factors);
            BlueToothUtil.filterFactor(address,responseData.data.factors);
          }
        })
        .catch((error) => {
           //ToastAndroid.show(''+error,ToastAndroid.SHORT);
        });
      }
      }
    );
 },
  getCar_factor:function(address){
    var REQUIRE_URL = config.server_base + config.filtor_factor + '?item_id=' +config.car_factor;
    console.log('REQUIRE_URL: ' + REQUIRE_URL);
    NetInfo.isConnected.fetch().done(
        (isConnected) => { 
          if (isConnected) {
          fetch (REQUIRE_URL)
        .then ((response) => response.json())
        .then((responseData) => {
          if(responseData.code == 0){
            BlueToothUtil.filterFactor(address,responseData.data.factors);
          }
        })
        .catch((error) => {
           //ToastAndroid.show(''+error,ToastAndroid.SHORT);
        });
      }
      }
    );
 },
    render:function (){

       var navigator = this.props.navigator;
       // console.log('type+++++++++2222'+this.state.type);
        if(this.state.type==0){
          var color1 = '#323C4B';
          // touch_color = '#FFFFFF33'
       }else{
          var color1 = '#FFFFFF';
          // touch_color = '#00000033'

       }
      return (
        <View  style = {{flexDirection:'column',backgroundColor:color1,height:deviceHeight * 0.9,}}>
        <HomeTitle  type = {this.state.type} devicename = {this.state.name} deviceaddress = {this.state.address}   navigator = {this.props.navigator}/>

        <SwitchPage type = {this.state.type} navigator = {this.props.navigator}/>


       <ModalBox 
          style = {styles_modal1.modal_container}  
          onClosed={this.onClose} 
          swipeToClose = {true}
          isDisabled = {false}
          swipeArea = {300}
          position={'center'} 
          ref={'modal'}
          animationDuration = {1}>
              <View style = {styles_modal1.modal_view}>
                <View style = {styles_modal1.modal_head}>
                  <View style= {{height:40,flex:1,}}></View>
                  <View style= {{height:40,flex:6,alignItems:'center',justifyContent:'center'}}>
                    <Text allowFontScaling={false} style= {[styles_modal1.modal_headtext,{color:'#8A8A8A'}]}>{Language.select_device}</Text>
                  </View>
                    <TouchableOpacity style= {{height:40,flex:1,alignItems:'center',justifyContent:'center'}} onPress={()=>{this.gotoDeviceStatus();UMeng.onEvent('home_08');}}>
                      <Image style={{height:26,width:27,resizeMode: 'stretch'}} source = {require ('image!ic_pluse')}/>
                    </TouchableOpacity> 
                </View>
                <View>
                  <View style= {styles_modal1.modal_line}/>
                </View>
                <View style = {styles_modal1.modal_borderview}>
                        <View style= {styles_modal1.modal_borderview2}>
                          <TouchableOpacity onPress={()=> {this.refs.modal.close();UMeng.onEvent('home_09');}}>
                             <View style = {styles_modal1.modal_borderview3}>
                              <Text allowFontScaling={false} style={[styles_feedback.commitText,{color:'rgb(23, 201, 180)'}]}>{Language.cancel}</Text>
                             </View>
                          </TouchableOpacity>
                        </View>
                </View>
                  <ListView
                          dataSource={this.state.dataSource}
                          renderRow={this.renderDevice}
                          style= {styles_modal1.modal_listview}
                          removeClippedSubviews={false}/>                           
              </View>
        </ModalBox>
              <ModalBox
                style = {styles_modal1.modal2_container}
                position={'center'} 
                ref={'modal2'}
                animationDuration = {1}>
                  <View style = {styles_modal1.modal2_view}>
                    <View style = {styles_modal1.modal2_view1}>
                      <Text allowFontScaling={false} style= {styles_modal1.modal2_text}>{Language.confirm_close_dev}</Text>
                    </View>
                    <View style = {styles_modal1.modal2_view2}>                    
                        <View style= {styles_modal1.modal2_close}>
                          <TouchableOpacity 
                          onPress={()=>{this.close();UMeng.onEvent('home_10');}}
                          style = {styles_modal1.modal2_touch}>
                             <View style = {styles_modal1.modal2_closetext}>
                              <Text allowFontScaling={false} style={[styles_feedback.commitText,{color:'black'}]}>{Language.close}</Text>
                             </View>
                          </TouchableOpacity>
                        </View>
                        <View style= {styles_modal1.modal2_wait}>
                          <TouchableOpacity 
                          onPress={()=>{this.switch();UMeng.onEvent('home_11');}}
                          style = {styles_modal1.modal2_touch}>
                             <View style = {styles_modal1.modal2_closetext}>
                              <Text allowFontScaling={false} style={[styles_feedback.commitText,{color:'black'}]}>{this.state.wait}</Text>
                             </View>
                          </TouchableOpacity>
                        </View>
                        <View style= {styles_modal1.modal2_cancel}>
                          <TouchableOpacity 
                          onPress={()=> {this.refs.modal2.close();UMeng.onEvent('home_12');}}
                          style = {styles_modal1.modal2_touch}>
                             <View style = {styles_modal1.modal2_closetext}>
                              <Text allowFontScaling={false} style={[styles_feedback.commitText,{color:'black'}]}>{Language.cancel}</Text>
                             </View>
                          </TouchableOpacity>
                        </View>
                    </View>
                  </View>
              </ModalBox>


        </View>
      );
    },
    switch:function() {

      if(this.state.wait == Language.sleep){
        this.refs.modal2.close();
        RCTDeviceEventEmitter.emit('slider',true);
        
      }else{
        this.refs.modal2.close();
        RCTDeviceEventEmitter.emit('slider',false);
      }
    },
    close:function() {
      this.refs.modal2.close(); 
      if (this.state.address != null ){
        BlueToothUtil.shutDown(this.state.address);
      }

    },


    gotoUpdate:function() {
      this.props.navigator.push({
        id: 'Update',
      });
    },

    gotoDeviceStatus:function(){
      this.props.navigator.push({
        id: 'DeviceStatus',
      });
    },

          /*已配对设备*/
   renderDevice: function(device) {
    if(device.id === this.state.address ){
      return <Item2 device = {device}  navigator = {this.props.navigator}/>
    }else{
      return <Item device = {device} navigator = {this.props.navigator}/>
    }
  },

});

//当前设备
var Item2 = React.createClass({

  render(){
  var navigator = this.props.navigator

    var type_img,image_style;
    if(this.props.device.type == 0){
        type_img = require('image!ic_xinfeng');
        image_style = {height:15/320 * deviceHeight,width:15/320 * deviceHeight,resizeMode:'stretch'};
    }else if(this.props.device.type == 1){
        type_img = require('image!ic_single');
        image_style = {height:15/320 * deviceHeight,width:15/320 * deviceHeight,resizeMode:'stretch'};
    }else{
        type_img = require('image!ic_car');
        image_style = {height:15/320 * deviceHeight,width:15/320 * deviceHeight,resizeMode:'stretch'};
    }
    console.log('type+++22++++++'+this.props.device.type);
      return (
        <View style = {{marginLeft:0,marginRight:0}}>
            <TouchableOpacity      
                onPress={()=> {this.renderMovie3(this.props.device);UMeng.onEvent('home_13');}}>
            <View style={{flexDirection: 'row',alignItems: 'center',height:40,}}>
                      <View style = {{width:13,marginLeft:5,alignItems: 'center',justifyContent:'center'}}>
              <Image style={image_style} source={type_img}/>
          </View>
              <Text allowFontScaling={false} style={{fontSize:15,left:0,color:'#50BCA1',marginLeft:10/180 * deviceWidth}}>{this.props.device.name}</Text>
              <Image  style={{position:'absolute',right:20,marginVertical:10,height:14,width:20.5,resizeMode: 'stretch'}} source = {require ('image!ic_select1')}/>
          </View> 
          </TouchableOpacity> 
          <View style={{backgroundColor:'#D9D9D9',height:1,width:deviceWidth-40}}/>
            </View>
      );
  },

    renderMovie3: function(device) {
      console.log(device);
      RCTDeviceEventEmitter.emit('msg_select_item',device); 
    },

});

//忻风界面内容
var SwitchPage = React.createClass({


    render(){
    if(this.props.type!==1){
        RCTDeviceEventEmitter.emit('wheel_timer_picker_close');
    }
    // console.log('type+++++++++'+this.props.type);
    
        if(this.props.type==0){
            return (
              <View style = {{}}>
                <XinfengPage navigator = {this.props.navigator}/>
              </View>
            );
      
          }else if(this.props.type==1){
            return (
              <View>
                  <SingleHome navigator = {this.props.navigator}/>
              </View>
            );
          }else if(this.props.type==2){
            return (
              <View>
                  <CarPage navigator = {this.props.navigator}/>
              </View>
            );
          }else{
            return (

              <View style = {{flex:1,alignItems:'center',justifyContent:'center',}}>
                  <TouchableOpacity style={{alignItems:'center',justifyContent:'center',}} onPress={()=>{this.gotoDeviceStatus();UMeng.onEvent('setting_10');}}>
                    <Image style={{height:70,width:70,resizeMode:'stretch'}} source={require('image!ic_jiahao')}/>
                    <View>
                       <Text allowFontScaling={false} style={{fontSize:15,marginTop:20}}>{Language.add_device}</Text>
                    </View>
                 
                  </TouchableOpacity> 
              </View>
            );
          }
    }, 


      gotoDeviceStatus:function(){
      // if(this.state.connect==='已连接'){
      //      this.props.navigator.jumpTo(this.props.navigator.getCurrentRoutes()[2]);
      // }else{
      this.props.navigator.push({
      id: 'DeviceStatus',
      });
      // }
   
    },

});

//未选择
var Item = React.createClass({

  getInitialState() {

      return {
      deviceName:'',
      }
    },

  render(){
  var navigator = this.props.navigator;
    var type_img,image_style;
    if(this.props.device.type == 0){
        type_img = require('image!ic_xinfeng');
        image_style = {height:15/320 * deviceHeight,width:15/320 * deviceHeight,resizeMode:'stretch'};
    }else if(this.props.device.type == 1){
        type_img = require('image!ic_single');
        image_style = {height:15/320 * deviceHeight,width:15/320 * deviceHeight,resizeMode:'stretch'};
    }else{
        type_img = require('image!ic_car');
        image_style = {height:15/320 * deviceHeight,width:15/320 * deviceHeight,resizeMode:'stretch'};
    }
    console.log('type+++11++++++'+this.props.device.type);
    return (
    <View  style = {{marginLeft:0,marginRight:0}}>
        <TouchableOpacity      
            onPress={()=> {this.renderMovie2(this.props.device);UMeng.onEvent('home_14');}}>
          <View style={{flexDirection: 'row',alignItems: 'center',height:40,}}>
                    <View style = {{width:13,marginLeft:5,alignItems: 'center',justifyContent:'center'}}>
              <Image style={image_style} source={type_img}/>
          </View>
            <Text allowFontScaling={false} style={{fontSize:15,left:0,color:this.state.bg,marginLeft:10/180 * deviceWidth}}>{this.props.device.name}</Text>
            <Text allowFontScaling={false} style={{position:'absolute',textAlign:'center',marginVertical:10,right:20,fontSize:13,color:'#969696',}}>{this.state.deviceName}</Text>
        </View> 
        </TouchableOpacity> 
          <View style={{backgroundColor:'#D9D9D9',height:1,width:deviceWidth-40}}/>
        </View>
    );
  },
  renderMovie2: function(device) {
    AsyncStorage.setItem('select_item_addr', device.id, (error)=>{
      RCTDeviceEventEmitter.emit('msg_select_item',device); 
    });
    Utils.setSelectItemAddr(device.id);
  },

});

var styles = StyleSheet.create ({
  container:{
      //backgroundColor:'blue',
      flexDirection:'column',
      backgroundColor:'#323C4B',
      height:deviceHeight * 0.9,//deviceHeight - 65,
  },

});



module.exports = Home1;
