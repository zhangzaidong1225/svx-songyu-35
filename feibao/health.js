'use strict';

import  config from './config'

import tools from './tools'
import WebViewPage from './webViewPage'
import style_register from './styles/style_register'
import styles_web from './styles/style_web'
import Language from './Language'
import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    ScrollView,
    TextInput,
    Dimensions,
    NetInfo,
    ToastAndroid,
    WebView,
    Platform,
} from 'react-native';

var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter');
var Utils = require('./utils');

var deviceWidth = Dimensions.get('window').width;
var deviceHeight = Dimensions.get('window').height;


var rctDeviceEventEmitter = null;

var firstClickTime = new Date().getTime();
var currentClickTime = null;

var Health = React.createClass({

  componentDidMount: function() {
    rctDeviceEventEmitter = RCTDeviceEventEmitter.addListener(
      'web_page_skip',
      (params) => {
        this._gotoWeb(params)
      }

    );
  },

  componentWillUnmount: function() {
    rctDeviceEventEmitter.remove();
  },
  
  render: function() {
    if (Platform.OS === 'android') {
      Utils.setInputModeForWebView();
    }

    // var DEFAULT_URL = config.server_base+config.health_uri;
    var DEFAULT_URL = config.server_base+config.health_uri+'?version='+config.version+'&platform='+config.platform+'&phone_modal='+config.phone_modal+'&language='+config.language;  //'https://baidu.com';

    // var DEFAULT_URL = 'http://test.mall.lianluo.com/test/xf_app/';
    // var DEFAULT_URL = 'https://wap.baidu.com';
    return (
      <View style = {style_register.container}>
        <WebViewPage title = {Language.mall} url={DEFAULT_URL} hasGoBack = {false} webviewStyle = {styles_web.webview_page}></WebViewPage>
      </View>
    );
//健康生活
  },

  _gotoWeb:function(params) {
      if (this._isFastClick()) {
        return;
      }
      // console.log('nativeEvent canGoBack');
      this.props.navigator.push({
        id: 'WebViewPage',
        params:params,
      });

  },

  _isFastClick:function () {
    currentClickTime = new Date().getTime();
    if (currentClickTime > firstClickTime + 1000) {
      firstClickTime = currentClickTime;
      // console.log('过滤掉了！！！！！');
      return false;
    } else {
      firstClickTime = currentClickTime;
      return true;
    }
  },

 });

module.exports = Health;
