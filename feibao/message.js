'use strict';


import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Component,
    Image,
} from 'react-native';

var Message = React.createClass({
  render: function() {
    return (
      <View style={styles.container}>
        <Text allowFontScaling={false} style={styles.welcome}>
          Welcome to React Native!
        </Text>
        <Text allowFontScaling={false} style={styles.instructions}>
          To get started, edit index.android.js
        </Text>
        <Text allowFontScaling={false} style={styles.instructions}>
          Shake or press menu button for dev menu
        </Text>
      </View>
    );
  }
});

var styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});




module.exports = Message;
