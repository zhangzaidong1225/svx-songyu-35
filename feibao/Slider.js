'use strict'

var TimerMixin = require ('react-timer-mixin');
var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter'); /*用于界面组件之间传递消息*/
var Log = require('./net');
import styles_slider from './styles/style_slider'
var AsyncStorage = require('./AsyncStorage');
var UMeng = require('./UMeng');
import Language from './Language'

import React, { Component } from 'react';
import {
    AppRegistry,
    styles_sliderheet,
    Text,
    View,
    PanResponder,
    Image,
    Dimensions,
    TouchableOpacity,
    TouchableWithoutFeedback,
    TouchableNativeFeedback,
    DeviceEventEmitter,
    NativeModules,
    ToastAndroid,
    NativeAppEventEmitter,
    // AsyncStorage,
    Platform,
  } from 'react-native';
var deviceWidth =Dimensions.get ('window').width;

var BlueToothUtil= require('./BlueToothUtil');


/*未连接状态*/
var Close = React.createClass({

  render:function (){
    return (
      <View>
        <View
          style = {{height:36,}}/>
        <View
          style = {styles_slider.container_gray}>

          <BackGround/>

          <Image
            source = {require('image!img_selecte_switch_gray')}
            style = {styles_slider.img}/>
        </View>
    </View>
    );
  }
});
var Wait = React.createClass({
   _handleClick:function (){

   RCTDeviceEventEmitter.emit('alert',true);
  },

  render:function (){
    return (
      <View>
        <View
          style = {{height:36,}}/>
        <View
          style = {styles_slider.container_gray}>

          <BackGround/>

            <TouchableOpacity
                onPress = {()=>{this._handleClick();UMeng.onEvent('Slider_01');}}>
              <Image
                source = {require ('image!img_selected_switch_blue')}
                style = {styles_slider.img}/>
              </TouchableOpacity>
        </View>
    </View>
    );
  }
});


var BackGround = React.createClass({

    render:function(){

      return (

              <View
                  style = {{
                  backgroundColor:'#8C9696',flexDirection:'row',height:40,width:deviceWidth-80,}}>
                  
                  <View style = {{flex:1.2,marginLeft:10,justifyContent:'flex-start',flexDirection:'row',alignItems:'center',}}>
                    <Text allowFontScaling={false} style = {styles_slider.textLH}>LOW</Text>
                    <Image source={require('image!img_small_wind')}
                        style = {{marginLeft:10,height:15,width:15,resizeMode: 'stretch'}}/>
                    </View>
                  <View style = {{flex:2}}/>
                  <View style = {{flex:1,marginRight:10,justifyContent:'flex-end',flexDirection:'row',alignItems:'center'}}>
                    <Image source={require('image!img_big_wind')}
                        style = {{marginRight:10,height:20,width:20,alignItems:'center',justifyContent:'center'}}/>
                    <Text allowFontScaling={false} style = {styles_slider.textLH}>HIGH</Text>
                  </View>
              </View>
      );
    }
});

/*
  0--表示无连接状态
  1--表示已连接转台，
  开关的关闭和开启。
  visible --true--开
  visible --false --关

deviceWidth-20-50-10
 */

var  factor = (deviceWidth-20-50-10)/70;

var sub_currentDevice;
var sub_close;
var sub_close1;
var sub_wait;
var Pan = React.createClass({

  mixins:[TimerMixin],

  _panResponder: {},

  timer_delay: null,

  timer_windSpeed : null,
  last_windSpeed: null,
 
  getInitialState() {

     DeviceEventEmitter.addListener('getconnectstate',(e)=>{
       this.setState({
        isOpen: e.connectstate,
       });
     });

      return {
          value:0,
          isOpen:'未连接',
          isVisiable:false,
          delay:true,
          pselect_item:null,
          mvalue:50,
          fanclose:0,
          mclose:false,
          state_text:'',
      }
  },  


  componentWillMount: function() {

    this._panResponder = PanResponder.create({
      // 要求成为响应者：
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,

      onPanResponderGrant: (evt, gestureState) => {
        // 开始手势操作。给用户一些视觉反馈，让他们知道发生了什么事情！
        var x_val = gestureState.x0/factor + 30;
        this.timer_delay && clearTimeout(this.timer_delay);

        if (x_val < 30) {
          x_val = 30;
          RCTDeviceEventEmitter.emit('slider_wait',x_val);
        }
        if (x_val > 100) {

          x_val = 100;
        }
        this.setState({isVisiable:true,delay:false});
        this.setState({value:x_val});
        
        // gestureState.{x,y}0 现在会被设置为0
      },
      onPanResponderMove: (evt, gestureState) => {
        // 最近一次的移动距离为gestureState.move{X,Y}
        var x_val = (gestureState.x0+gestureState.dx)/factor+ 30;
        if (x_val < 30) {x_val = 30;}
        if (x_val > 100) {x_val = 100;}
        this.setState({value:x_val});
        
        
        // 从成为响应者开始时的累计手势移动距离为gestureState.d{x,y}
      },
      onPanResponderTerminationRequest: (evt, gestureState) => true,
      onPanResponderRelease: (evt, gestureState) => {
        // 用户放开了所有的触摸点，且此时视图已经成为了响应者。
        // 一般来说这意味着一个手势操作已经成功完成。
        var x_val = (gestureState.x0+gestureState.dx)/factor+ 30;
        if (x_val < 30) {x_val = 30;}
        if (x_val > 100) {x_val = 100;}
        this._tran(Math.round(x_val));
        this.setState({isVisiable:false});
      },
      onPanResponderEnd:(evt, gestureState) => {
      },
      onPanResponderReject: (e, gestureState) => {
      },
      onPanResponderTerminate: (evt, gestureState) => {
        // 另一个组件已经成为了新的响应者，所以当前手势将被取消。
      },
      onShouldBlockNativeResponder: (evt, gestureState) => {
        // 返回一个布尔值，决定当前组件是否应该阻止原生组件成为JS响应者
        // 默认返回true。目前暂时只支持android。
        //console.log(gestureState);
        return true;
      },
    });
  }, 

  
  _isclose:function (){

      //BlueToothUtil.shutDown(this.state.pselect_item.addr);  
      /*发送关机界面消息*/
      //RCTDeviceEventEmitter.emit('message_close',false); 
        //ToastAndroid.show('你确定要关闭设备？',ToastAndroid.SHORT);
  }, 
  

  componentDidMount:function(){

    /*解决关机延缓*/
    /*
    sub_close = RCTDeviceEventEmitter.addListener('message_close',(val)=>{
      this.setState({
        isOpen:'未连接',
      });
    });
    
    sub_close1 = RCTDeviceEventEmitter.addListener('isclose',(val)=>{
      this.setState({
        isOpen:'未连接',
      });
      if(val){
        this._isclose();
      }
    });
    */
    //待机和唤醒的监听器
    sub_wait = RCTDeviceEventEmitter.addListener('slider',(val)=>{
      if (val) {
        this.setState({
          mvalue:this.state.value,
          value:0,
          mclose:val,
        });
         BlueToothUtil.windSpeed(this.state.pselect_item.addr,0);
      }else{
        this.setState({
          value:this.state.mvalue,
          mclose:val,
        });
         BlueToothUtil.windSpeed(this.state.pselect_item.addr,this.state.mvalue);
      };
    });
    //切换设备的监听器
    sub_currentDevice = RCTDeviceEventEmitter.addListener('message_select_item',(val)=>{
      if(val == null || val.speed <= 0){
        this.setState({'delay':true});
      }
      if (this.state.delay) {
          if(val == null){
              this.setState({
               isOpen:'未连接',
              });
          }else{

            var tmp_mclose = true;
            if(val.speed > 0){tmp_mclose = false;}
            this.setState({
             isOpen:'已连接',
             value:val.speed,
             pselect_item:val,
             mclose:tmp_mclose,
            });
      }
    }
      
    });

    // BlueToothUtil.getConnectState();
  },

  componentWillUnmount(){
    sub_currentDevice.remove();
    //sub_close.remove();
    //sub_close1.remove();
    sub_wait.remove();
    this.timer_delay &&  clearTimeout(this.timer_delay);
  },

  _handleClick:function (){

    //BlueToothUtil.shutDown(this.state.pselect_item.addr);  
  /*发送关机界面消息*/
  //RCTDeviceEventEmitter.emit('message_close',false); 
  //弹出关机待机窗口
   RCTDeviceEventEmitter.emit('alert',true);
    //ToastAndroid.show('你确定要关闭设备？',ToastAndroid.SHORT);
  },

  _tran:function(value){
      //ToastAndroid.show(value+'',ToastAndroid.SHORT);
    if (value>0 && value <30) {
      value = 30;
    }
    if (this.timer_windSpeed == null) {
      BlueToothUtil.windSpeed(this.state.pselect_item.addr,value); 

      console.log('aaa--'+this.state.pselect_item.addr+'--'+value);

      this.timer_windSpeed = this.setTimeout( () => {
        if(this.last_windSpeed != null){
          
          BlueToothUtil.windSpeed(this.state.pselect_item.addr,this.last_windSpeed);
          console.log('aaa--aa--'+this.state.pselect_item.addr+'--'+this.last_windSpeed);

        }
        this.timer_windSpeed = null;
        this.last_windSpeed = null;
      },200);  
    }else{

      this.last_windSpeed = value;
       console.log('aaa--aa-a-'+this.state.pselect_item.addr+'--'+this.last_windSpeed);
    }

    
    this.timer_delay = this.setTimeout( () => {
      this.setState({delay:true});
      //BlueToothUtil.sendMydevice();
    },3000); 
  },

    render:function (){

      var img;
      var wind_text;

      if (this.state.isVisiable === false){
          img = require('image!ic_wind_indected');
          wind_text = '';
          
      }else {
          img = require('image!img_wind_indected');
          wind_text=Language.wind_speed+Math.round (this.state.value);
      }

      if (this.state.isOpen == '未连接'){
        return (
          <View >
            <Close/>
          </View>
        );
      }else if(this.state.isOpen == '已连接' && this.state.mclose){
        return (
          <View >
            <Wait/>
          </View>
        );
      }else{

        return (
          <View
            style = {styles_slider.container} >
            <View style = {{width:deviceWidth-20,alignItems:'center',justifyContent:'center',marginBottom:5,height:18,}}>
              <Text allowFontScaling={false} style = {styles_slider.text}>{wind_text}</Text>
            </View>
            <View >
              <Image
                source = {img}
                style ={{left:((this.state.value-30)* factor) - 5,marginBottom:5,}}/>
            </View>


            <View style= {{flexDirection:'row'}}  >


              <View
                  style = {{
                  backgroundColor:'#8C9697',flexDirection:'row',height:40,width:deviceWidth-80,}} {...this._panResponder.panHandlers}>
                  
                <View style = {{flex:3,position:'absolute',left:0,right:0,height:40,width:(this.state.value-30)*factor,backgroundColor: '#50C8FF'}}>
                  
                </View>

                  <View style = {{flex:1,marginLeft:10,justifyContent:'flex-start',flexDirection:'row',alignItems:'center',backgroundColor:'#00000000'}}>
                    <Text allowFontScaling={false} style = {{fontSize:15,color:'white'}}>LOW</Text>
                    <Image source={require('image!img_small_wind')}
                        style = {{marginLeft:10,height:15,width:15,}}/>
                  </View>

                  <View style = {{flex:1,marginRight:10,justifyContent:'flex-end',flexDirection:'row',alignItems:'center',backgroundColor:'#00000000'}}>
                    <Image source={require('image!img_big_wind')}
                        style = {{marginRight:10,height:20,width:20,alignItems:'center',justifyContent:'center'}}/>
                    <Text allowFontScaling={false} style = {{fontSize:15,color:'white'}}>HIGH</Text>
                  </View>
              </View>
                  

              <TouchableOpacity
                onPress = {()=>{this._handleClick();UMeng.onEvent('Slider_02');}}>
              <Image
                source = {require ('image!img_selected_switch_blue')}
                style = {styles_slider.img}/>
              </TouchableOpacity>
              </View>

          </View>
        );
      }
      
  },
});


module.exports = Pan;
