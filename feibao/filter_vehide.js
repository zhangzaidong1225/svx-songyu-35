/** 
* Dispaly status of the filters which user has.
*/
'use strict';

import  styles from './styles/style_setting';
import  styles_up from './styles/style_update'
import  styles_filter from './styles/style_filter'
import styles_web from './styles/style_web'
import  tools from './tools'
import  config from './config'
var Filter_content = require ('./filter_content');
var UMeng = require('./UMeng');
var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;
import Title from './title'
import * as Progress from 'react-native-progress';
import Language from './Language'

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  Dimensions,
  PixelRatio,
  TouchableOpacity,
  TouchableWithoutFeedback,
  AsyncStorage,
  NativeModules,
  NetInfo,
  ToastAndroid,
  DeviceEventEmitter,
  NativeAppEventEmitter,
  ListView,
  Platform,
} from 'react-native';
var f01_in = 0;
var f02_in = 0;
var f01_time = 0;
var f02_time = 0;
var sub_currentDevice = null;
var mwidth;
var mheight;

var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter');
var BlueToothUtil= require('./BlueToothUtil');


var rate = 0;
var hour = 0;
var min = 0;
var img = require('image!ic_filter_no'); 
var hasFilter = Language.notdetected;
var useNumber = 0;

var rate2 = 0;
var hour2 = 0;
var min2 = 0;
var img2 = require('image!ic_filter_no'); 
var hasFilter2 = Language.notdetected;
var useNumber2 = 0;


var filter_vehide = React.createClass({
  
  getInitialState: function() {
    return {
      textColor1:'#B6B6B6',
      filter01_in:0,
      filter01_use_time:0,

      filter02_in:0,
      filter02_use_time:0,
      textColor2:'#B6B6B6',
    };
  },

  componentWillMount:function(){
    
    BlueToothUtil.sendMydevice();
  },



componentDidMount:function(){

    sub_currentDevice = RCTDeviceEventEmitter.addListener('message_select_item',(val) => {
      if(val == null){
        console.log('val ···············');
      }else {
        this.setState({
          type:val.type,
          filter01_in:val.filter01_in,
          filter01_use_time:val.filter01_use_time,
          filter02_in:val.filter02_in,
          filter02_use_time:val.filter02_use_time,
        });
      }
      // f01_in = this.state.filter01_in;
      // f02_in = this.state.filter02_in;
      // f01_time =  Math.floor(this.state.filter01_use_time/60);
      // f02_time =  Math.floor(this.state.filter02_use_time/60); 
        this._trannum(val.filter01_in,Math.floor(val.filter01_use_time/60));
        this._tranfilter(val.filter02_in,Math.floor(val.filter02_use_time/60));

      console.log('滤棉是否在位```'+f01_in);
    });
  },

  _tranfilter:function(status,number){
      rate2 =  Math.floor(number/60/90*100);
     hour2 = Math.floor(number/60);
     min2 = Math.floor(number%60);
    if(rate2>=100){
        rate2 = 100;
    }
    if(status == 0){
      hasFilter2 = Language.notdetected;
      img2 = require('image!ic_filter_no');
      this.setState({
        textColor2:'#B6B6B6',
      });
    }else{
      if(number<=72*60){
         hasFilter2 = Language.normal;
         img2 = require('image!ic_filter_normal');
        this.setState({
          textColor2:'#2DBAF1',
        });
      }
      if(number>72*60){
         hasFilter2 = Language.suggestedreplacement;
         img2 = require('image!ic_filter_abnormal'); 
        this.setState({
          textColor2:'#f1ab2d',
        });
      }

    }

  },

    _trannum :function (status,number){
     rate =  Math.floor(number/60/90*100);
     hour = Math.floor(number/60);
     min = Math.floor(number%60);
    if(rate>=100){
        rate = 100;
    }
    if(status == 0){
      hasFilter = Language.notdetected;
      img = require('image!ic_filter_no');
      this.setState({
        textColor:'#B6B6B6',
      });
    }else{
      if(number<=72*60){
         hasFilter = Language.normal;
         img = require('image!ic_filter_normal');
        this.setState({
          textColor:'#2DBAF1',
        });
      }
      if(number>72*60){
         hasFilter = Language.suggestedreplacement;
         img = require('image!ic_filter_abnormal'); 
        this.setState({
          textColor:'#f1ab2d',
        });
      }

    }
  },





  componentWillUnmount: function() {
    sub_currentDevice.remove();
    BlueToothUtil.sendMydevice();
  },
  render: function() {
      mwidth = Dimensions.get ('window').width;
      mheight = Dimensions.get('window').height;
      return (
        <View style={styles_filter.viewFilters}>
          <Title title = {Language.filterelement} hasGoBack = {true}  navigator = {this.props.navigator}/>
          <Text style = {{color:'#2DBAF1',fontSize:14,marginTop:18/320*mheight,marginLeft:8/180*mwidth}}>{Language.xinfengcarfilter}</Text>
        

                  <View style={{width:mwidth}}>
          <View style = {{marginLeft:8/180*mwidth}}>
          <View style = {{flexDirection: 'row',height:50/320*mheight,alignItems:'center',marginTop:15}}>
            <Image style={{resizeMode: 'stretch',height:42/320*mheight,width:30/180*mwidth}} source={img}/>
            <View style={{marginLeft:15/180*mwidth}}>
               <Text style = {{color:'#525252',fontSize:16,}}>
                 {Language.filterState}
                 <Text style = {{color:this.state.textColor,}}>{':'}{hasFilter}</Text>
               </Text>
               <View style = {{marginTop:10,}}>
                    <Text style = {{color:'#b6b6b6',fontSize:13,}}>{Language.hepafilter}</Text>
                    <Text style = {{color:'#b6b6b6',fontSize:13,}}>{Language.usage}{':'}{hour}{' '}{Language.h}{' '}{min}{' '}{Language.min}</Text>
               </View>

               <View style = {{width:118.5/180*mwidth,marginTop:10}}>
                    <View style = {{flex:1, flexDirection: 'row',justifyContent:'flex-end',alignItems: 'center',}}>
                        <Text style = {{color:'#2DBAF1',fontSize:12,}}>{Language.usage}{':'}{rate}%</Text>
                    </View>
                    <Progress.Bar progress={rate/100} borderWidth = {0} width={118.5/180*mwidth} unfilledColor={'#d8d8d8'} height={3} borderColor={'#d8d8d8'} color = {'#2DBAF1'}/>
               </View>

            </View>
          </View>
          </View>
        </View>


          <View style = {{marginTop:20/320*mheight}}>


                    <View style={{width:mwidth}}>
          <View style = {{marginLeft:8/180*mwidth}}>
          <View style = {{flexDirection: 'row',height:50/320*mheight,alignItems:'center',marginTop:15}}>
            <Image style={{resizeMode: 'stretch',height:42/320*mheight,width:30/180*mwidth}} source={img2}/>
            <View style={{marginLeft:15/180*mwidth}}>
               <Text style = {{color:'#525252',fontSize:16,}}>
                 {Language.filterState}
                 <Text style = {{color:this.state.textColor,}}>{':'}{hasFilter2}</Text>
               </Text>
               <View style = {{marginTop:10,}}>
                    <Text style = {{color:'#b6b6b6',fontSize:13,}}>{Language.hepafilter}</Text>
                    <Text style = {{color:'#b6b6b6',fontSize:13,}}>{Language.usage}{':'}{hour2}{' '}{Language.h}{' '}{min2}{' '}{Language.min}</Text>
               </View>

               <View style = {{width:118.5/180*mwidth,marginTop:10}}>
                    <View style = {{flex:1, flexDirection: 'row',justifyContent:'flex-end',alignItems: 'center',}}>
                        <Text style = {{color:'#2DBAF1',fontSize:12,}}>{Language.usage}{':'}{rate2}%</Text>
                    </View>
                    <Progress.Bar progress={rate2/100} borderWidth = {0} width={118.5/180*mwidth} unfilledColor={'#d8d8d8'} height={3} borderColor={'#d8d8d8'} color = {'#2DBAF1'}/>
               </View>

            </View>
          </View>
          </View>
        </View>


          </View>


          <View style = {[styles_filter.bottomView,]}>
            <View style = {styles_filter.uview4}>

                     <TouchableOpacity onPress={() => {this._gotoCottonPage();UMeng.onEvent('filters_04');}}>
                        <View style={styles_filter.uview5} >
                          <Text allowFontScaling={false} style={styles_filter.text1}>{Language.tutorial}</Text>
                        </View>
                    </TouchableOpacity>

            </View>
            <View style = {styles_filter.uview6}>
                     <TouchableOpacity onPress={() => {this._gotoFilterBuy();UMeng.onEvent('filters_05');}}>
                        <View style={styles_filter.uview5} >
                          <Text allowFontScaling={false} style={styles_filter.text1}>{Language.buy}</Text>
                        </View>
                    </TouchableOpacity>
               
            </View>
          </View> 


        </View>
        );
  },

  _gotoCottonPage:function(){
    this.props.navigator.push({
      id: 'CottonVehidePage',
    });
  },
  _gotoFilterBuy: function() {
    var FILTER_BUY = config.server_base+config.filter_buy;//滤芯购买
    console.log('FILTER_BUY: ' + FILTER_BUY);
    this.props.navigator.push({
      id: 'WebViewPage',
      params:{
          title:Language.filterpurchase,
          url: FILTER_BUY,
          hasGoBack: true,
          webviewStyle:styles_web.webview_protocol,
      }
    });
  },
});

module.exports = filter_vehide;
