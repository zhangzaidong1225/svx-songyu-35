'use strict'

var TimerMixin = require ('react-timer-mixin');
var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter'); /*用于界面组件之间传递消息*/
// var Log = require('./net');
import styles_slider from './styles/style_slider'
import style_vehidemodel from './styles/style_vehidemodel'

var AsyncStorage = require('./AsyncStorage');
var UMeng = require('./UMeng');
import tools from './tools'
import Language from './Language'

import React, { Component } from 'react';
import {
    AppRegistry,
    styles_sliderheet,
    Text,
    View,
    PanResponder,
    Image,
    Dimensions,
    PixelRatio,
    TouchableOpacity,
    TouchableWithoutFeedback,
    TouchableNativeFeedback,
    DeviceEventEmitter,
    NativeModules,
    ToastAndroid,
    NativeAppEventEmitter,
    // AsyncStorage,
    Platform,
    Alert,
} from 'react-native';


var deviceWidth =Dimensions.get ('window').width;

var BlueToothUtil= require('./BlueToothUtil');
var alertMsg = '滤棉更换中...';
var alertSingleMsg = '连接单品才会有智能模式哦';


/*未连接状态*/
var Close = React.createClass({

  getInitialState:function(){
    return {

    };

  },

  getDefaultProps: function () {
      return {
      // model:'',
      device:'',
      clock:'',
      openCap:'',
      };
    },

  render:function (){
    return (
      <View>
        <View
          style = {style_vehidemodel.close_container}>

          <BackGround openCap = {this.props.openCap}/>
          <SpaceVehide  
            openCap = {this.props.openCap} 
            clock = {this.props.clock} 
            device = {this.props.device} 
            navigator = {this.props.navigator}/>
        </View>
    </View>
    );
  },

});


var Wait = React.createClass({

  getInitialState:function(){
    return {

    }
  },

  getDefaultProps: function () {
    return {
      model:'',
      device:'',
      clock:'',
      openCap:'',
      esmart :'',
    };
  },

  render:function (){
       console.log(this.props.model+'aaaa11111');
    return (
      <View>
        <View
          style = {style_vehidemodel.wait}>

          <BackGround openCap = {this.props.openCap}/>
          <Space model = {this.props.model} esmart = {this.props.esmart} clock = {this.props.clock} device = {this.props.device} navigator = {this.props.navigator} />

        </View>
    </View>
    );
  }
});


var BackGround = React.createClass({


  getDefaultProps: function () {
    return {
      openCap:'',
      singleModel:'',
    };
  },


    render:function(){

      return (

      		<View style = {style_vehidemodel.back_container}>

            <TouchableOpacity
              onPress = {() => {this.lv_alert()}}
              style = {{flexDirection:'row',marginLeft:0,marginRight:0,}}>

              <View style = {style_vehidemodel.back_left}>
              	<View style = {style_vehidemodel.back_view1}>
                
                <Image source={require('image!ic_gray_wind_small')}
                    style = {style_vehidemodel.back_img1}/>
                </View>
              </View>


            <View style ={style_vehidemodel.back_center}>

		          <View style = {style_vehidemodel.back_view2}>
	              <View
	                  style = {style_vehidemodel.back_view3} >
	                  
	              </View>
	            </View>  

            </View>


          <View style = {style_vehidemodel.back_right}>
          	<View style = {style_vehidemodel.back_view4}>
            <Image source={require('image!ic_gray_wind_big')}
                style = {style_vehidemodel.back_view5}/>
       		</View>
          </View>

          </TouchableOpacity>
        </View>
      );
    },


  lv_alert:function(){

    if (this.props.openCap == 1){
      //tools.alertShow(alertMsg);
        Alert.alert(
          Language.prompt+'', 
          Language.filerreplace+'', 
          [
            {text: Language.ok+'', onPress: () => {return true;}},
          ]
        )
    }
  
  },


}); 

var SpaceVehide = React.createClass ({

    getInitialState:function(){
    return {
        
    }
  },

  getDefaultProps: function () {
    return {
      vehide_model:'',
      device:'',
      clock:'',
      openCap:'',

    };
  },


    render(){
    var tmp_smart_back,tmp_smart_txt;
    var tmp_clock_back,tmp_clock_txt;

    return(

          <View style = {style_vehidemodel.space_container}>

              <View style = {style_vehidemodel.space_line}/>

              <View style = {style_vehidemodel.space_left}>
              <TouchableOpacity
                onPress = {() => {this._tranTime()}}
                style = {{flex:1}}>

                <View style = {style_vehidemodel.space_view1}>
                      <Image 
                        source = {require('image!ic_gray_clock')}
                        style = {style_vehidemodel.space_img1}/>
                    
                    <Text allowFontScaling={false} style = {[style_vehidemodel.space_text1,{color:'#EBEBEB'}]}>{Language.timedswitch}</Text>
                </View>
                </TouchableOpacity>

                <View style = {style_vehidemodel.space_line1}/>

              <TouchableOpacity
                onPress = {() => {this._tranEsmart(this.props.device)}}
                style = {{flex:1}}>
                <View style = {style_vehidemodel.space_top2}>
                      <Image 
                        source = {require('image!ic_gray_smart')}
                        style = {style_vehidemodel.space_img2}/>

                        <Text allowFontScaling={false} style = {[style_vehidemodel.space_text2,{color:'#EBEBEB'}]}>{Language.automation}</Text>
                </View>
                </TouchableOpacity>
              </View>
          </View>
    );
  },

      //传输时间
  _tranTime:function(){
    if (this.props.openCap == 1){
       // tools.alertShow(alertMsg);
        Alert.alert(
          Language.prompt+'', 
          Language.filerreplace+'', 
          [
            {text: Language.ok+'', onPress: () => {return true;}},
          ]
        )

    } 
  },

  _tranEsmart:function(device){

    if (this.props.openCap == 1){
       // tools.alertShow(alertMsg);
      Alert.alert(
      Language.prompt+'', 
      Language.filerreplace+'', 
      [
        {text: Language.ok+'', onPress: () => {return true;}},
      ]
    )
    } 
  },



});

var Space = React.createClass({

  getInitialState:function(){
    return {
        
    }
  },

  getDefaultProps: function () {
    return {
      model:'',
      device:'',
      clock:'',
      esmart:'',
    };
  },

  render(){
    var tmp_smart_back,tmp_smart_txt;
    var tmp_clock_back,tmp_clock_txt;
   console.log(this.props.model+'aaaa22222');

    if ((this.props.model == 1) && (this.props.esmart == 1)){
        tmp_smart_back = require('image!ic_blue_smart');
        tmp_smart_txt = '#2AB9F1';

    }else {
        tmp_smart_back = require('image!ic_gray_smart');
        tmp_smart_txt = '#404040';
    }

    if (this.props.clock == 1){
        tmp_clock_back = require('image!ic_gray_clock');
        tmp_clock_txt = '#404040';
    }else {
        tmp_clock_back = require('image!ic_blue_clock');
        tmp_clock_txt = '#2AB9F1';
    }

    return(

          <View style = {style_vehidemodel.space_container}>

              <View style = {style_vehidemodel.space_line}/>

              <View style = {style_vehidemodel.space_left}>
              <TouchableOpacity
                onPress = {() => {this._tranTime()}}
                style = {{flex:1}}>

                <View style = {style_vehidemodel.space_view1}>
                    <View style = {{flex:1,alignItems:'center',marginLeft:13,justifyContent:'center',}}>
                      <Image 
                        source = {tmp_clock_back}
                        style = {style_vehidemodel.space_img1}/>
                    </View>

                    <View style = {{flex:1.2,alignItems:'flex-start',marginRight:15,justifyContent:'center',}}>
                        <Text allowFontScaling={false} style = {[style_vehidemodel.space_text1,{color:tmp_clock_txt}]}>{Language.timedswitch}</Text>
                    </View>
                </View>
                </TouchableOpacity>

                <View style = {style_vehidemodel.space_line1}/>

              <TouchableOpacity
                onPress = {() => {this._tranEsmart(this.props.device)}}
                style = {{flex:1}}>
                <View style = {style_vehidemodel.space_top2}>
                      <Image 
                        source = {tmp_smart_back}
                        style = {style_vehidemodel.space_img2}/>

                        <Text allowFontScaling={false} style = {[style_vehidemodel.space_text2,{color:tmp_smart_txt}]}>{Language.automation}</Text>
                </View>
                </TouchableOpacity>
              </View>
          </View>
    );
  },

    //传输时间
  _tranTime:function(){

      this.props.navigator.push({
       id: 'VehideTime',
       // params:{
       //     // device:device,
       //    }
       });
    

  },

  _tranEsmart:function(device){

    if (this.props.model == 1){

      this.props.navigator.push({
         id: 'Esmart',
         params:{
             device:device,
            }
         });

    }else {
      tools.alertShow(Language.plugdetectiondev);
    }
  },

});

/*
  0--表示无连接状态
  1--表示已连接转台，
  开关的关闭和开启。


deviceWidth-20-50-10
 */

var  factor = (deviceWidth-150) / 70;

var sub_currentDevice;
var sub_wait;
var Time;
var _txtback1,_txtback2;
var tmp_color = -1;//时钟模式，1--gray 0--blue


var clickwait= -1;

var SliderVehide = React.createClass({

  mixins:[TimerMixin],

  _panResponder: {},

  timer_delay: null,
  // timer_wait :null,

  timer_windSpeed : null,
  last_windSpeed: null,
 
  getInitialState() {

     DeviceEventEmitter.addListener('getconnectstate',(e)=>{
       this.setState({
        isOpen: e.connectstate,
       });
     });

      return {
          value:0,
          isOpen:'未连接',
          isVisiable:false,
          delay:true,
          pselect_item:null,
          mvalue:50,
          fanclose:0,
          mclose:false,
          state_text:'',
          hour:'  ',
          minute:'  ',
          vehide_model:null,
          isSingle:null,
          waitSpeed:-1,//待机和唤醒
          switch01:0,
          switch02:0,
          switch03:0,
          open_cap:0,
          wind01_speed:-1,
      }
  },  


  componentWillMount: function() {

    this._panResponder = PanResponder.create({
      // 要求成为响应者：
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,

      onPanResponderGrant: (evt, gestureState) => {
        // 开始手势操作。给用户一些视觉反馈，让他们知道发生了什么事情！
        console.log('onPanResponderGrant```1```'+gestureState.x0);
        // console.log('onPanResponderGrant```2'+ );
      
            
            var x_val = (gestureState.x0 - 65)/factor + 30;
            this.timer_delay && clearTimeout(this.timer_delay);

            if (x_val < 30) {
              x_val = 30;
              RCTDeviceEventEmitter.emit('slider_wait',x_val);
            }
            if (x_val > 100) {

              x_val = 100;
            }
            this.setState({isVisiable:true,delay:false});
            this.setState({value:x_val});
          

        

        
        // gestureState.{x,y}0 现在会被设置为0
      },

      onPanResponderMove: (evt, gestureState) => {
        // 最近一次的移动距离为gestureState.move{X,Y}
        //       console.log('onPanResponderGrant```1'+gestureState.x0);
        // console.log('onPanResponderGrant```2'+deviceWidth- 65);

        var x_val = (gestureState.x0+gestureState.dx -65)/factor+ 30;
        if (x_val < 30) {x_val = 30;}
        if (x_val > 100) {x_val = 100;}
        this.setState({value:x_val});
        
        
        // 从成为响应者开始时的累计手势移动距离为gestureState.d{x,y}
      },
      onPanResponderTerminationRequest: (evt, gestureState) => true,
      onPanResponderRelease: (evt, gestureState) => {
        // 用户放开了所有的触摸点，且此时视图已经成为了响应者。
        // 一般来说这意味着一个手势操作已经成功完成。
        console.log('onPanResponderRelease```1```'+gestureState.x0);
        // console.log('onPanResponderRelease```2'+deviceWidth- 65);
            if (!this.state.mclose) {
        var x_val = (gestureState.x0+gestureState.dx - 60)/factor+ 30;
        if (x_val < 30) {x_val = 30;}
        if (x_val > 100) {x_val = 100;}
        this._tran(Math.round(x_val));
        this.setState({isVisiable:false});
            }

      },
      onPanResponderEnd:(evt, gestureState) => {
      },
      onPanResponderReject: (e, gestureState) => {
      },
      onPanResponderTerminate: (evt, gestureState) => {
        // 另一个组件已经成为了新的响应者，所以当前手势将被取消。
      },
      onShouldBlockNativeResponder: (evt, gestureState) => {
        // 返回一个布尔值，决定当前组件是否应该阻止原生组件成为JS响应者
        // 默认返回true。目前暂时只支持android。
        //console.log(gestureState);
        return true;
      },
    });
  }, 

  componentDidMount:function(){

        //切换设备的监听器
    sub_currentDevice = RCTDeviceEventEmitter.addListener('message_select_item',(val)=>{
      if(val == null || val.wind01_speed <= 0){
        this.setState({'delay':true});
      }
      if (this.state.delay) {
          if(val == null){
              this.setState({
               isOpen:'未连接',
              });
          }else{

            var tmp_mclose = true;

            if(val.wind01_speed > 0){tmp_mclose = false;}

            if (val.open_cap == 1){
                this.setState({
                  open_cap:val.open_cap,
                  pselect_item:val,
                });
            } else {
              if (this.state.vehide_model != val.vehide_is_smart){
                this.setState({
                  waitSpeed:-1,
                });
              }

                this.setState({
                 isOpen:'已连接',
                  // value:val.wind01_speed,
                 pselect_item:val,
                 mclose:tmp_mclose,
                 vehide_model:val.vehide_is_smart,
                 isSingle:val.vehide_model,
                 switch01:val.vehide_time_state,
                 switch02:val.vehide_time_state02,
                 switch03:val.vehide_time_state03,
                 open_cap : val.open_cap,
                });

                // if (this.state.waitSpeed == val.wind01_speed){

                //   console.log('唤醒·····');
                //     this.setState({
                //       value : val.wind01_speed,
                //     });
                // } else {

                //   this.setState({
                //       value : val.wind01_speed,
                //     });
                  
                // }

                if (val.vehide_is_smart == 1){
                  //智能模式

                  if (this.state.waitSpeed == 0){
                    //待机
                    if (this.state.value != val.wind01_speed){
                      console.log("smart待机模式下需要更新的数据");
                  
                      this.setState({
                        value :val.wind01_speed,
                      });
                    }
                  } else if (this.state.waitSpeed == 1){
                    if (val.wind01_speed != 0 ){
                      //唤醒
                      console.log("smart唤醒模式下需要更新的数据");
                        this.setState({
                          value :val.wind01_speed,
                        });
                    }
                  }else {
                    console.log("smart智能模式下模式下需要更新的数据"+val.wind01_speed);
                      this.setState({
                        value :val.wind01_speed,
                      });
                  }
                } else {

                  if (this.state.waitSpeed == 0){
                    //待机
                    if (this.state.value != val.wind01_speed){
                      console.log("normal待机模式下多发的数据");
                  
                      this.setState({
                        value :val.wind01_speed,
                      });
                    }
                  } else if (this.state.waitSpeed == 1) {
                    if (val.wind01_speed != 0){
                      //唤醒
                      console.log("normal唤醒模式下需要更新的数据");
                        this.setState({
                          value :val.wind01_speed,
                        });
                    }
                  } else {
                    console.log("normal模式下需要更新的数据"+val.wind01_speed);
                      this.setState({
                        value :val.wind01_speed,
                      });
                  }
                }

            }
      }
    }
      
    });


    //待机和唤醒的监听器
    sub_wait = RCTDeviceEventEmitter.addListener('isWait_V',(val)=>{
      // console.log('待机和唤醒···'+val);

      if (val) {

        if (this.state.pselect_item.addr != null){

         BlueToothUtil.windSpeed(this.state.pselect_item.addr,0);

        console.log('ccccc```wait```');
          this.setState({
            mvalue:this.state.value,
            // value:0,
            mclose:val,
            waitSpeed:0,
          });

        }

            // console.log('待机和唤醒···1``'+this.state.value+this.state.mclose);

      }else{
        if (this.state.pselect_item.addr != null) {

          this.setState({
            // value:this.state.mvalue,
            mclose:val,
            waitSpeed:1,
          });
           // console.log('待机和唤醒··2``·'+this.state.value+this.state.mclose);

           BlueToothUtil.windSpeed(this.state.pselect_item.addr,this.state.mvalue);

            console.log('ccccc```awake```');
          }

        }
  });

  },

  // _tranClock:function(address,hour,minute){
  //     BlueToothUtil.setVehideTime(address,hour,minute);

  // },

  componentWillUnmount(){
    sub_currentDevice.remove();
    sub_wait.remove();
    this.timer_delay &&  clearTimeout(this.timer_delay);
    // this.timer_wait &&  clearTimeout(this.timer_wait);
  },


  _tran:function(value){
      //ToastAndroid.show(value+'',ToastAndroid.SHORT);
    if (value>0 && value <30) {
      value = 30;
    }
    if (this.timer_windSpeed == null) {
      BlueToothUtil.windSpeed(this.state.pselect_item.addr,value); 

      console.log('aaa--'+this.state.pselect_item.addr+'--'+value);

      this.timer_windSpeed = this.setTimeout( () => {
        if(this.last_windSpeed != null){
          
          BlueToothUtil.windSpeed(this.state.pselect_item.addr,this.last_windSpeed);
          console.log('aaa--aa--'+this.state.pselect_item.addr+'--'+this.last_windSpeed);

        }
        this.timer_windSpeed = null;
        this.last_windSpeed = null;
      },200);  
    }else{

      this.last_windSpeed = value;
    }

    
    this.timer_delay = this.setTimeout( () => {
      this.setState({delay:true});
      //BlueToothUtil.sendMydevice();
    },3000); 
  },

    render:function (){

      var navigator = this.props.navigator;

      var wind_text;
      var tmp_img,tmp_back;

      if (this.state.isVisiable === false){
          wind_text = '';
          
      }else {
          wind_text=Language.wind_speed+Math.round (this.state.value);
      }

      if ((this.state.switch01 == 0) && (this.state.switch02 == 0) && (this.state.switch03 == 0)){
         _txtback1 = '#404040';
        tmp_back = require('image!ic_gray_clock');
        tmp_color = 1;
      }else {
        tmp_color = 0;

        _txtback1 = '#2AB9F1';
        tmp_back = require('image!ic_blue_clock');
      }
  
      console.log('ccccc```1-'+this.state.vehide_model+'````'+this.state.isSingle);

      if ((this.state.vehide_model == 1) && (this.state.isSingle == 1)){

          tmp_img = require('image!ic_blue_smart');
          _txtback2 = '#2AB9F1';
      }else {
        tmp_img = require('image!ic_gray_smart');
        _txtback2 = '#404040';
        
      }

      if (this.state.open_cap == 1 ){
        //更换滤棉页面
        return (
          <View >
            <Close 
              openCap = {this.state.open_cap}
              clock = {1} 
              device = {this.state.pselect_item} 
              navigator = {this.props.navigator} />
          </View>
        );
      } else if(this.state.isOpen == '已连接' && this.state.mclose){
        console.log(this.state.isSingle+'aaaa');


        return(
<View style = {style_vehidemodel.slide_container}>

          <View
            style = {style_vehidemodel.slide_container2} >


              <View style = {style_vehidemodel.slide_left}>
                <View style = {style_vehidemodel.slide_view1}>
                
                <Image source={require('image!ic_gray_wind_small')}
                    style = {style_vehidemodel.slide_img1}/>
                </View>
              </View>


            <View style ={style_vehidemodel.back_center}>

              <View style = {style_vehidemodel.back_view2}>
                <View
                    style = {style_vehidemodel.back_view3} >
                    
                </View>
              </View>  

            </View>


              <View style = {style_vehidemodel.slide_right2}>
                <View style = {style_vehidemodel.slide_view6}>
                  <Image source={require('image!ic_gray_wind_big')}
                      style = {style_vehidemodel.slide_img3}/>
                </View>
              </View>


          </View>

          <View style = {style_vehidemodel.slide_container3}>

              <View style = {style_vehidemodel.slide_line1}/>

              <View style = {style_vehidemodel.slide_top}>
              <TouchableOpacity
                onPress = {() => {this._tranTime()}}
                style = {{flex:1}}>
                <View style = {style_vehidemodel.slide_view7}>
                      <Image 
                        source = {tmp_back}
                        style = {style_vehidemodel.slide_img4}/>

                    <Text allowFontScaling={false} style = {[style_vehidemodel.slide_text2,{color:_txtback1}]}>{Language.timedswitch}</Text>
                   
                </View>
                </TouchableOpacity>

                <View style = {style_vehidemodel.slide_line2}/>

              <TouchableOpacity
                onPress = {() => {this._tranEsmart(this.state.pselect_item)}}
                style = {{flex:1}}>
                <View style = {style_vehidemodel.slide_view8}>
                      <Image 
                        source = {tmp_img}
                        style = {style_vehidemodel.slide_img5}/>

                        <Text allowFontScaling={false} style = {[style_vehidemodel.slide_text3,{color:_txtback2}]}>{Language.automation}</Text>
                </View>
                </TouchableOpacity>
              </View>
          </View>
          </View>
        );

      } else {

        return (
          <View style = {style_vehidemodel.slide_container}>

          <View
            style = {style_vehidemodel.slide_container2} {...this._panResponder.panHandlers}>


              <View style = {style_vehidemodel.slide_left}>
              	<View style = {style_vehidemodel.slide_view1}>
                
                <Image source={require('image!ic_blue_wind_small')}
                    style = {style_vehidemodel.slide_img1}/>
                </View>
              </View>


            <View style= {style_vehidemodel.slide_center}>
             
		           <View style = {style_vehidemodel.slide_view2}>
		              <Text allowFontScaling={false} style = {style_vehidemodel.slide_text1}>{wind_text}</Text>
		           </View>

		        <View style = {style_vehidemodel.slide_view3} >
	              <View
	                  style = {style_vehidemodel.slide_view4} >
	                  
	                <View 
	                	style = {[style_vehidemodel.slide_view5,{width:(this.state.value-30)*factor}]}/>
	                  
	              </View>

	             </View>


	            <View style = {style_vehidemodel.slide_right} >

                <View style = {{width:19,height:19,backgroundColor:'#2AB9F1',left:((this.state.value-30)* factor) - 7.5,
                          borderWidth:1/PixelRatio.get(),borderRadius:9.5,borderColor:'#2AB9F1',}}/>

	            </View>
	                  
            </View>


              <View style = {style_vehidemodel.slide_right2}>
              	<View style = {style_vehidemodel.slide_view6}>
                  <Image source={require('image!ic_blue_wind_big')}
                      style = {style_vehidemodel.slide_img3}/>
           		  </View>
              </View>


          </View>

          <View style = {style_vehidemodel.slide_container3}>

              <View style = {style_vehidemodel.slide_line1}/>

              <View style = {style_vehidemodel.slide_top}>
              <TouchableOpacity
                onPress = {() => {this._tranTime()}}
                style = {{flex:1}}>
                <View style = {style_vehidemodel.slide_view7}>
                      <Image 
                        source = {tmp_back}
                        style = {style_vehidemodel.slide_img4}/>
                    <Text allowFontScaling={false} style = {[style_vehidemodel.slide_text2,{color:_txtback1}]}>{Language.timedswitch}</Text>
                    
                </View>
                </TouchableOpacity>

                <View style = {style_vehidemodel.slide_line2}/>

              <TouchableOpacity
                onPress = {() => {this._tranEsmart(this.state.pselect_item)}}
                style = {{flex:1}}>
                <View style = {style_vehidemodel.slide_view8}>
                      <Image 
                        source = {tmp_img}
                        style = {style_vehidemodel.slide_img5}/>

                        <Text allowFontScaling={false} style = {[style_vehidemodel.slide_text3,{color:_txtback2}]}>{Language.automation}</Text>
                </View>
                </TouchableOpacity>
              </View>
          </View>
          </View>
        );
      }
      
  },
  //传输时间
  _tranTime:function(){

    this.props.navigator.push({
       id: 'VehideTime',
       params:{
           // device:device,
          }
       });

  },

  _tranEsmart:function(device){


    if (this.state.isSingle == 1){
      
      this.props.navigator.push({
       id: 'Esmart',
       params:{
           device:device,
          }
       });

    }else {
      tools.alertShow(Language.plugdetectiondev);
    }
  },


   //传输时间
  _tranTimeWait:function(){

    console.log("_tranTimeWait````qqq");

      this.props.navigator.push({
       id: 'VehideTime',
       // params:{
       //     // device:device,
       //    }
       });
  },

  _tranEsmartWait:function(){
    console.log("_tranEsmartWait````qqq");

    if (this.state.isSingle == 1){

      this.props.navigator.push({
         id: 'Esmart',
         params:{
             device:this.state.pselect_item,
            }
         });

    }else {
      tools.alertShow(Language.plugdetectiondev);
    }
  },






});


module.exports = SliderVehide;

        // return (
        //   <View >
        //     <Wait 
        //         model = {this.state.isSingle}
        //         esmart = {this.state.vehide_model}
        //         openCap = {0}
        //         clock = {tmp_color} 
        //         device = {this.state.pselect_item}  
        //         navigator = {this.props.navigator}/>
        //   </View>
        // );

