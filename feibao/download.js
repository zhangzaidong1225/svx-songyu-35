'use strict'

import React, { Component } from 'react';
var RNFS = require('react-native-fs');

var testDir1Path = RNFS.DocumentDirectoryPath + '/upload';
//升级和下载路径
var testFile3Path = RNFS.DocumentDirectoryPath + '/upload/dev_update.zip';
var jobId1 = -1;
var statusCode = 0;
import Language from './Language'
//var download_url = 'http://feibao.esh001.com/public/dev_update_v13.zip';

import {
   NetInfo
} from 'react-native';


var Download = {

		exists: function(path, cb) {
			console.log('检测文件是否存在');
			return RNFS.exists(path).then(res => {
				console.log('检测文件是否存在' + res);
				cb(res);
	        });

		},

		mkdirTest: function(path) {
	      return RNFS.mkdir(path).then(success => {
	        var text = success.toString();
	        console.log(text+'先创建文件夹');
	      }).catch(err => this.showError(err));
	    },
	    writeNestedTest: function() {
   			 return RNFS.writeFile(testFile3Path, 'I am a file in the directory', 'ascii').then(() => {
     		 this.setState({ output: 'Files written successfully' });
   			 }).catch(err => this.showError(err));
  		},

	  	readNestedTest: function(path) {
	   	 return RNFS.readFile(path).then((data) => {
	      	console.log(data);

	   	 }).catch(err => this.showError(err));
	  	},
	  	moveFile:function(path1,path2){
	  		 return RNFS.moveFile(path1,path2).then(() => {
     		 	console.log('创建成功');
   			 }).catch(err => this.showError(err));

	  	},

	  	readDirNestedTest: function(path) {
	   	 return RNFS.readDir(path).then(files => {
	   	 	  var text = files.map(file => file.name + ' (' + file.size + ') (' + (file.isDirectory() ? 'd' : 'f') + ')').join('\n');
	   	 	  console.log(text);
	   		 }).catch(err => this.showError(err));
	 	 },

	    showError: function(err) {
	      console.log('react native fs---'+err.message);

	    },

	  	deleteDirTest: function(path) {
	   	 return RNFS.unlink(path).then(success => {
	   	   var text = success.toString();
	   	   console.log(text);
	   	 }).catch(err => this.showError(err));
	  	},

		downloadFileTest: function(url,path, cb) {
		    var progress1 = data => {
		      var text = JSON.stringify(data);
		    };

		    var begin1 = res => {
		      jobId1 = res.jobId;
		     
		    };
		    

		    RNFS.downloadFile(url, path, begin1, progress1)
		    .then(res => {

		      console.log(JSON.stringify(res));
		      cb();
		     
		    }).catch(err => this.showError(err));

		  

	  	},

};

module.exports = Download;



