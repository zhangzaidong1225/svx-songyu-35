'use strict'

import  styles_pm from './styles/style_pm'
var TimerMixin = require ('react-timer-mixin');
var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter'); /*用于界面组件之间传递消息*/
import tools from './tools'
import styles_actionbar  from './styles/style_actionbar'
var Slider = require ('./Slider');
var Whether_PM = require ('./whether_pm');
var UMeng = require('./UMeng');
import Language from './Language'
import React, { Component,PropTypes } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    NetInfo,
} from 'react-native';
import * as Progress from 'react-native-progress';

var mwidth = Dimensions.get ('window').width;
var mheight = Dimensions.get('window').height;
var hasFilter = Language.notdetected;
var useNumber = 0;
//var textColor = '#B6B6B6';
var img = require('image!ic_filter_no'); 
var rate = 0;
var hour = 0;
var min = 0;
var filter_content = React.createClass({
  PropTypes:{
      status:PropTypes.number,
      number:PropTypes.number,
  },
  getInitialState:function(){
    return {
      textColor:'#B6B6B6',
    }
  },
  
  componentWillReceiveProps:function(nextProps) {
    rate =  Math.floor(this.props.number/60/90*100);
    hour = Math.floor(this.props.number/60);
    min = Math.floor(this.props.number%60);
    if(rate>=100){
        rate = 100;
    }
    if(this.props.status == 0){
      hasFilter = Language.notdetected;
      img = require('image!ic_filter_no');
      this.setState({
        textColor:'#B6B6B6',
      });
    }else{
      if(this.props.number<=72*60){
         hasFilter = Language.normal;
         img = require('image!ic_filter_normal');
        this.setState({
          textColor:'#2DBAF1',
        });
      }
      if(this.props.number>72*60){
         hasFilter = Language.suggestedreplacement;
         img = require('image!ic_filter_abnormal'); 
        this.setState({
          textColor:'#f1ab2d',
        });
      }

    }
  },
  render:function (){
      return (
        <View style={{width:mwidth}}>
          <View style = {{marginLeft:8/180*mwidth}}>
          <View style = {{flexDirection: 'row',height:50/320*mheight,alignItems:'center',marginTop:15}}>
            <Image style={{resizeMode: 'stretch',height:42/320*mheight,width:30/180*mwidth}} source={img}/>
            <View style={{marginLeft:15/180*mwidth}}>
               <Text style = {{color:'#525252',fontSize:16,}}>
                 {Language.filterState}
                 <Text style = {{color:this.state.textColor,}}>{':'}{hasFilter}</Text>
               </Text>
               <View style = {{marginTop:10,}}>
                    <Text style = {{color:'#b6b6b6',fontSize:13,}}>{Language.hepafilter}</Text>
                    <Text style = {{color:'#b6b6b6',fontSize:13,}}>{Language.usage}{':'}{hour}{' '}{Language.h}{' '}{min}{' '}{Language.min}</Text>
               </View>

               <View style = {{width:118.5/180*mwidth,marginTop:10}}>
                    <View style = {{flex:1, flexDirection: 'row',justifyContent:'flex-end',alignItems: 'center',}}>
                        <Text style = {{color:'#2DBAF1',fontSize:12,}}>{Language.usage}{':'}{rate}%</Text>
                    </View>
                    <Progress.Bar progress={rate/100} borderWidth = {0} width={118.5/180*mwidth} unfilledColor={'#d8d8d8'} height={3} borderColor={'#d8d8d8'} color = {'#2DBAF1'}/>
               </View>

            </View>
          </View>
          </View>
        </View>
        );
  },  
});



module.exports = filter_content;
