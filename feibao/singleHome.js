/*单品主页*/
'use strict'

var RNFS = require('react-native-fs');
var TimerMixin = require('react-timer-mixin');
import AsyncStorage from './AsyncStorage'
import SingleWeather from './SingleWeather'
import SingleIcon from './singleIcon'
import SingleMode from './singleMode'
import  config from './config'
import Language from './Language'
import styles_singleweater from './styles/style_singleweather'


import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    Navigator,
    TouchableHighlight,
    TouchableOpacity,
    PixelRatio,
    NativeModules,
    // AsyncStorage,
    ToastAndroid,
    NetInfo,
    ListView,
    Platform,
    DeviceEventEmitter,
    NativeAppEventEmitter,
} from 'react-native';


var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter'); /*用于界面组件之间传递消息*/
var BlueToothUtil=require('./BlueToothUtil');
var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;



var SingleHome = React.createClass({

	getInitialState:function(){
		return {
			navigator:'',
			code:'',
			system_time:'',
		}
	},


	componentDidMount:function(){
		            //检测网络变化
    NetInfo.addEventListener(
        'change',
        this._handleConnectionInfoChange
    );

	this._fetchData();

	},
	_handleConnectionInfoChange: function(connectionInfo) {

    console.log(connectionInfo+'是否联网');
	this._fetchData();
  },

	_fetchData:function(){

		NetInfo.isConnected.fetch().done(
			(isConnected) => {
				if (isConnected){
					console.log('时间接口有网路啦');
				    var DEV_URL = config.server_base + config.system_time;
				    console.log(DEV_URL);
				    fetch (DEV_URL)
				    	.then ((response) => response.json())
				    	.then((responseData) => {
				    		if (responseData.code == 0){
				    			this.setState({
				    				system_time:responseData.data.server_time,
				    			});
  								 //console.log('时间戳·····'+this.state.system_time);
				    		}
				    	})
				    	.catch((error) => {
 							console.log('时间戳·····使用错误');
				    	})
				}
			}
		);
	},

	componentWillUnmount:function(){
	    NetInfo.removeEventListener(
	        'change',
	        this._handleConnectionInfoChange
	    );
	},

	render:function(){
     if (config.global_language =='en'){
     	return (
			<View style = {[styles.container,{alignItems: 'center'}]}>
				<View style = {styles_singleweater.view1}/>
				<SingleIcon navigator = {this.props.navigator}/>
				<SingleMode navigator = {this.props.navigator}/>
			</View>

		);
     } else {
     	return (
			<View style = {[styles.container,{alignItems: 'center'}]}>
				<SingleWeather navigator = {this.props.navigator}/>
				<SingleIcon navigator = {this.props.navigator}/>
				<SingleMode navigator = {this.props.navigator}/>
			</View>

		);
     }


	},

});

var styles = StyleSheet.create ({

	container:{
		flexDirection:'column',
		backgroundColor:'#FFFFFF',
		height:deviceHeight * 0.9,
	}
});

module.exports = SingleHome;

