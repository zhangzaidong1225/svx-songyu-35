/**
 * 单品模式切换
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
//var TimerMixin = require ('react-timer-mixin');
import TimerMixin from 'react-timer-mixin'

import styles_singlemodel  from './styles/style_singlemodel'
import tools from './tools'
import Language from './Language'
import TimerMap from './TimerMap'

import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  PanResponder,
  Dimensions,
  Image,
  TouchableOpacity,
} from 'react-native';

var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;

var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter'); /*用于界面组件之间传递消息*/
var AsyncStorage = require('./AsyncStorage');


var BlueToothUtil= require('./BlueToothUtil');
var Continue = 0;//连续采集
var timeShare = 1;//分时采集
var singleCheck = 2;//单次检测

var conFlag = 1;
var shareFlag = 1;
var singleFlag = 1;

var setModel =-1;//手动切换
// var addr;//全局地址
var setClick = -1;//手动单次检测


var sub_currentDevice = null;
var now_status = null;
var type_modeString = false;


var wheel_timer_picker;

var _back1,_back2;
var _txtback1,_txtback2;


var SingleMode  = React.createClass ({

  mixins:[TimerMixin],
  timer_delay_con:null,
  _addr:null,
  //timer_delay_share:null,

  getInitialState:function (){

    return {
      visible:true,
      address:'',
      model:'',
      timerSpace:null,
      type:'',
      flag:'',
      //now:true,//显示立即检测
      now_shot:false,//手动点击立即检测
      //disabled:true,
      Pm25PickTime:'',
      SystemTime:'',
      press:false,
    };
  },

  _panResponder: {},
  _previousLeft: 0,
  _previousTop: 0,
  _circleStyles: {},
  circle: (null:?{ setNativeProps(props: Object): void }),



  componentWillMount: function() {

    this._panResponder = PanResponder.create({
      onStartShouldSetPanResponder: this._handleStartShouldSetPanResponder,
      onMoveShouldSetPanResponder: this._handleMoveShouldSetPanResponder,
      onPanResponderGrant: this._handlePanResponderGrant,
      onPanResponderMove: this._handlePanResponderMove,
      onPanResponderRelease: this._handlePanResponderEnd,
      onPanResponderTerminate: this._handlePanResponderTerminate,
      onShouldBlockNativeResponder:this._handlePanResponderBlock,
    });
    this._previousLeft = 0;
    this._previousTop = 0;
    this._circleStyles = {
      style: {
        left: this._previousLeft,
        top: this._previousTop,
        backgroundColor: '#2AB9F1',
      }
    };

  },

  componentDidMount: function() {
    this._updateNativeStyles();

    sub_currentDevice = RCTDeviceEventEmitter.addListener('message_select_item',(val) => {

      if (val == null){
        // console.log('地址为空 ·····');
      }else {
        console.log('timer·····'+val.addr+'...'+val.model+'``'+setModel+'```'+this.state.model);

        if (this.state.model === 2 && val.model === 1){
          // console.log('timer····模式为2····');
          setModel  = val.model;
          RCTDeviceEventEmitter.emit('now_shot_click',false);
        }
        
        this.setState({
          address:val.addr,
          model:val.model,
          timerSpace:val.space,
          type:val.type,
          flag:val.Pm25Flag,
          Pm25PickTime:val.Pm25PickTime,
          SystemTime:val.SystemTime,

        });

        var floor = val.SystemTime - val.Pm25PickTime;

         this._handlerModel(this.state.model,this.state.address,floor);
      }


    });

    now_status = RCTDeviceEventEmitter.addListener(
         'now_shot_click',
         (value) => {
            this.setState({now_shot:value});
          if (value){
          // console.log('timer.'+value + this.state.model);

            setModel  = singleCheck;

          } else {
          // console.log('timer..'+value + this.state.model);

            setModel  = timeShare;

          }
         }   
    );

  },

  _handlerModel:function(model,address,floor){
    console.log('timer····.'+setModel+'····'+model);
    console.log('timer1······'+this._addr+'····'+address);

    if (this._addr != address){
      console.log('timer····地址变了····');
      this.setState({now_shot:false});

      setModel = -1;
      this._addr = address;
      setClick = -1;
    }

    if (setModel == model){

      if (model == 0){
        this.setState({
          visible:true,
          press:false,
        });
          this._circleStyles.style.left = 0 ;
        type_modeString = '0';
      } else {
        this.setState({
          visible:false,
          press:true,
        });
        type_modeString = '1';
        this._circleStyles.style.left = (deviceWidth-80)/2 + 0 ;
      }

      this._updateNativeStyles();
    }

    //初次进入
    if (setModel == -1){

      if (model == 0){
          this._circleStyles.style.left = 0 ;
          type_modeString = '0';
        this.setState({
          visible:true,
          press:false,
        });

      } else {

          // console.log('地址··只执行一次·');

        type_modeString = '1';
        this._circleStyles.style.left = (deviceWidth-80)/2 + 0 ;
        this.setState({
            visible:false,
            // now:false,
            press:true,
        });

        }

      }

      this._updateNativeStyles();

    // }
    RCTDeviceEventEmitter.emit('type_mode',type_modeString); 
  },

  componentWillUnmount:function(){
    sub_currentDevice.remove();
    now_status.remove();

    // if (this.timer_delay_con != null){
    //     this.timer_delay_con && clearTimeout(this.timer_delay_con);
    // }

  },


  render: function() {

    if (this.state.press){
      //分时
      _back1 = '#FFFFFF';
      _txtback1 = '#525252';
      _back2 = '#2AB9F1';

      _txtback2 = '#FFFFFF';
    }else {
      //时时
      _back2 = '#FFFFFF';
      _txtback1 = '#FFFFFF';
        _back1 = '#2AB9F1';
      _txtback2 = '#525252';

    }


    return (
      <View
        style={styles_singlemodel.container}>

          <View style = {styles_singlemodel.view1}>
            <Timer visible = {this.state.visible} address = {this.state.address} flag = {this.state.flag} timerSpace = {this.state.timerSpace}/>
          </View>

          <View style = {styles_singlemodel.view2}>
            <View style = {styles_singlemodel.view3}>
              <View 
                style = {styles_singlemodel.view4}
                  {...this._panResponder.panHandlers} >

                  <View
                    ref={(circle) => {
                      this.circle = circle;
                    }}
                    style={styles_singlemodel.circle}/>

                  <View style = {[styles_singlemodel.view5,]}>
                    <Text allowFontScaling={false} style = {[styles_singlemodel.text1,{color:_txtback1,}]}>{Language.real_time}</Text>
                  </View>

                   <View style = {[styles_singlemodel.view6,]}>
                      <Text allowFontScaling={false} style = {[styles_singlemodel.text2,{color:_txtback2,}]}>{Language.timed}</Text>
                  </View>
              </View>

            </View>
          </View>

      </View>
    );
  },

  _highlight: function() {
    this._circleStyles.style.backgroundColor = '#2AB9F1';
    this._updateNativeStyles();
  },

  _unHighlight: function() {
    this._circleStyles.style.backgroundColor = '#FFFFFF00';
    this._updateNativeStyles();
  },

  _updateNativeStyles: function() {
    this.circle && this.circle.setNativeProps(this._circleStyles);
  },

  _handlePanResponderTerminate:function(e: Object, gestureState: Object){
    return true;
  },

  _handlePanResponderBlock:function(e: Object, gestureState: Object){
      return false;
  },

  _handleStartShouldSetPanResponder: function(e: Object, gestureState: Object): boolean {
    // Should we become active when the user presses down on the circle?
    return true;
  },

  _handleMoveShouldSetPanResponder: function(e: Object, gestureState: Object): boolean {
    // Should we become active when the user moves a touch over the circle?
    return true;
  },

  _handlePanResponderGrant: function(e: Object, gestureState: Object) {
    // this._highlight();
  },
  _handlePanResponderMove: function(e: Object, gestureState: Object) {
    // this._circleStyles.style.left = this._previousLeft + gestureState.dx;
    // if (gestureState.x0 >= 40 && gestureState.x0 <= ((deviceWidth-80)/2 + 40)){
    //   // if (this.state.model == 1){
       

    //   // }
    //    //this._circleStyles.style.left = 40 ;
    //   //this.setState({visible:true});
    // }
    // if (gestureState.x0 >=((deviceWidth-80)/2 + 40) && gestureState.x0 <= (deviceWidth-40) ){
    //   // if (this.state.model == 0 || this.state.model == 2){
    //   // }
    //      //this._circleStyles.style.left = (deviceWidth-80)/2 + 40 ;

    // }
    //this.setState({visible:false});
    //this._circleStyles.style.top = this._previousTop + gestureState.dy;
    //this._updateNativeStyles();
  },
  _handlePanResponderEnd: function(e: Object, gestureState: Object) {

     //this._circleStyles.style.left = gestureState.x0 + gestureState.dx;

        if (this.state.model != 2 && !this.state.now_shot){
           // console.log('val ····sub_getMode······send··1···');
          //连续
          if (gestureState.x0 >= 40 && gestureState.x0 <= ((deviceWidth-80)/2 + 40)){

              type_modeString = '0';
              // console.log('地址 ··时时··');
              this._circleStyles.style.left = 0 ;
              this.setState({
                visible:true,
                press:false,
              });
              this._tranContinue();
              setModel = Continue;
            
            // if (this.timer_delay_con != null){
            //   this.timer_delay_con && clearTimeout(this.timer_delay_con);
            // }
          }
          //分时
         if (gestureState.x0 >=((deviceWidth-80)/2 + 40) && gestureState.x0 <= (deviceWidth-40) ){
            type_modeString = '1';
            // console.log('地址 ```分时·· `');
            this._circleStyles.style.left = (deviceWidth-80)/2 + 0 ;
            this.setState({
              visible:false,
              now:false,
              press:true,
            });

            this._tranTimeShare();
            setModel = timeShare;

            // this.timer_delay_con = this.setTimeout( () => {
             
            //   console.log('地理位置 ··bbbb2222··');
            //   this.setState({now:false});

            // },60000); 

          }
        }
        else {
            if (gestureState.x0 >= 40 && gestureState.x0 <= ((deviceWidth-80)/2 + 40)){
                tools.alertShow(Language.detecting);
            }


        }
      // console.log('val ····sub_getMode······send·····'+type_modeString);
      RCTDeviceEventEmitter.emit('type_mode',type_modeString); 


      //更新开关
      this._highlight();
  },
  //连续
  _tranContinue:function(){
    //console.log('地理位置 ··bbb0000··');
    BlueToothUtil.eSmart(this.state.address,Continue);


  },
  //分时
  _tranTimeShare:function(){
    //console.log('地理位置 ··ccc000··');
    BlueToothUtil.eSmart(this.state.address,timeShare);

  }
});


// var timerMap = new Map();

var Timer = React.createClass ({

  _timer:'10',
  _addr:'',

  getInitialState(){
    return {
      visible:true,
      address:'',
      timerSpace:null,
      // disabled:false,
      flag:'',
    };
  },

  componentWillMount:function(){
    // timerMap = new Map();
    this._timer = '10';
    // this._addr = null;


  },


  componentDidMount:function(){

    wheel_timer_picker = RCTDeviceEventEmitter.addListener(
         'wheel_timer_picker',
         (value) => {
          
          // console.log('timer··0··'+value);

          // this._timer = parseInt(value);

          TimerMap.set(this._addr,value);

          this.setState({timerSpace:value});

          this._tranTimeSpace(parseInt(value));  
         }   
    );
    
  },

  _tranTimeSpace:function(value){

    if ((this.props.address != null) && (value != null)){
      BlueToothUtil.setInterval(this.props.address,value);
    }

  },

  componentWillUnmount:function(){
    wheel_timer_picker.remove();
    // console.log('timer··3·3·'+TimerMap.get(this.props.address)+'``'+this.props.timerSpace +'--'+this._timer);

    // this._timer = null;
    // this._addr = null;
  },

  render() {

      // console.log('timer``'+this._addr + "--"+this.props.address + '--'+this._timer);
    
    if (this._addr != this.props.address && this.props.timerSpace != 0){


      if (!TimerMap.has(this.props.address)){

        TimerMap.set(this.props.address,this.props.timerSpace);
      }
      
      this._timer = TimerMap.get(this.props.address);

      this._addr = this.props.address;
      // console.log('timer··4·1·'+TimerMap.get(this.props.address)+'``'+this.props.timerSpace +'--'+this._timer);


    } else {
      // console.log('timer··3··'+TimerMap.get(this.props.address)+'``'+this.props.timerSpace +'--'+this._timer);
      if (typeof(this._timer) === 'undefined' || !TimerMap.has(this.props.address)){

        this._timer = '10';
      // console.log('timer··3·1·'+TimerMap.get(this.props.address)+'``'+this.props.timerSpace +'--'+this._timer);

      } else {
        this._timer = TimerMap.get(this._addr);
      }
    }


    if (!this.props.visible){

        return (
          <View style = {styles_singlemodel.timer_view1}>

            <View style = {styles_singlemodel.timer_view2}>

              <View style= {styles_singlemodel.timer_view3}/>
              <TouchableOpacity 
                  onPress = {()=>{this._tranTime()}}
                  style = {styles_singlemodel.timer_view4}>
                <View style= {styles_singlemodel.timer_view5}>
                  <Text allowFontScaling={false} style= {styles_singlemodel.timer_text1}>{typeof(this._timer) === 'undefined'?'10':this._timer}</Text>
                  <Text allowFontScaling={false} style= {styles_singlemodel.timer_text2}>{' min'}</Text>
                  <Image 
                    style= {styles_singlemodel.timer_img1}
                    source= {require('image!ic_trangle_timer')}/>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                
                onPress = {() => {this._NowCheck(this.props.address)}}
                disabled = {false} 
                style= {styles_singlemodel.timer_view6}>
                <View style= {[styles_singlemodel.timer_view7,{backgroundColor:'#2AB9F1',borderColor:'#2AB9F1'}]}>
                  <Text allowFontScaling={false} style= {styles_singlemodel.timer_text3}>{Language.detectnow}</Text>
                </View>
              </TouchableOpacity>
            </View>

          </View>
        );

    }else {
      return (
        <View/>
      );
    }
  },

  _tranTime:function(){

    console.log('timer··5··'+'--'+this._timer);

    RCTDeviceEventEmitter.emit('wheel_timer_picker_open',''+this._timer);


  },

  _NowCheck:function(deviceid){
    //单次检测

    // console.log('地理位置---aaa------');
    BlueToothUtil.eSmart(deviceid,singleCheck);

    //立即检测
    RCTDeviceEventEmitter.emit('now_shot_click',true);


  },
});


module.exports = SingleMode;

