/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

import  styles from './styles/style_setting';
//var AsyncStorage = require('./AsyncStorage');
var UMeng = require('./UMeng');
import Language from './Language'
import Title from './title'

  

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  Dimensions,
  PixelRatio,
  TouchableOpacity,
  ListView,
  ToastAndroid,
   AsyncStorage,
} from 'react-native';

var mwidth=Dimensions.get('window').width;
var mheight=Dimensions.get('window').height;
var setPage=require('./setting');
var NetWork=require('./NetWork');
var city_data = require('./city_data');
var async = require('async');


var mcityid; 
var mcity;
var mareaid;
var marea;

var Area= React.createClass({  
    getInitialState() {
        return {
           dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      loaded: false,
            value:mheight,
            m:mwidth,
            id:null,
        }
    },   

    componentDidMount: function() {
      this.fetchData();
    },

    getLocation:function(cb){

      async.parallel([
          function(callback){
            AsyncStorage.getItem('cityid',(error,result)=>{
              console.log('area-cityid:' + result);
            if(error){}else{
                callback(null, result);
               }
            });
            
          },
          function(callback){
          AsyncStorage.getItem('areaid',(error,result)=>{
            console.log('area-areaid:' + result);
            if(error){}else{
              callback(null, result);
             }
            });
          },
          function(callback){
          AsyncStorage.getItem('area',(error,result)=>{
            console.log('area-area:' + result);
            if(error){}else{
              callback(null, result);
             }
            });
          },
        ],
        function(err, results){
          mcityid=results[0];
          mareaid=results[1];
          marea=results[2];
          cb();
        });
    },
  
    
fetchData: function() {
      this.getLocation(()=>{
        var responseData = city_data.get_area_list('CH',this.props.provinceid,this.props.cityid);
        var index=-1;
        for(var i=0;i<responseData.length;i++){
          if(responseData[i].id===mareaid){
            index=i;
          }
        }
        if(index!=-1){
          responseData.splice(index,1);
        }
        this.setState({
          dataSource: this.state.dataSource.cloneWithRows(responseData),
          loaded: true,
        });
      });
  },

  rederview:function(){
    if(marea===null || mcityid!=this.props.cityid){
      var navigator = this.props.navigator;
    return (
      <View>    
        <Title title = {Language.locationtitle} hasGoBack = {true}  navigator = {this.props.navigator}/>    

        <ScrollView style={styles.item} >  
    <View  >
        <View style={styles.view} >
            <Text allowFontScaling={false} style={styles.font2}>{Language.allareas}</Text>  
       </View>
       <View style={styles.line2}/>

        <ListView
        dataSource={this.state.dataSource}
        renderRow={this.renderMovie}
        style={styles.listView}
        removeClippedSubviews={false}
      />     
      </View>   
    </ScrollView>
    </View>
    );}else{
var navigator = this.props.navigator;
    return (
      <View>
           
        <Title title = {Language.locationtitle} hasGoBack = {true}  navigator = {this.props.navigator}/>  

        <ScrollView style={styles.item}>   
    <View   >
        <View style={styles.view} >
            <Text allowFontScaling={false} style={styles.font2}>{Language.allareas}</Text>  
       </View>
       <View style={styles.line2}/>

        <TouchableOpacity style={styles.view} onPress={()=>this.saveAddress2()}>
            <Text allowFontScaling={false} style={styles.font1}>{marea}</Text>  
            <View style={styles.direction}>
            <Text allowFontScaling={false} style={styles.font2}>{Language.selected}</Text>
            </View>
       </TouchableOpacity>

       <View style={styles.line}/>


        <ListView
        dataSource={this.state.dataSource}
        renderRow={this.renderMovie}
        style={styles.listView}
        removeClippedSubviews={false}
      />     
      </View>   
    </ScrollView>
    </View>
    );}
 },


  render: function(){
    return this.rederview();
  },

  renderMovie: function(area) {
    return (
      <View>
      <TouchableOpacity style={styles.view} onPress={()=>this.saveAddress(area)}>      
          <Text allowFontScaling={false} style={styles.font1} >{area.name}</Text> 
      </TouchableOpacity>
      <View style={styles.line2}/>
      </View>
    );
  },
  
  saveAddress:function(area){
    UMeng.onEvent('area_02');
    var navigator = this.props.navigator;

    async.parallel([
          (callback)=>{
            AsyncStorage.setItem('area',area.name,(error)=>{
              callback(null, null);
            });
          },
          (callback)=>{
            AsyncStorage.setItem('areaid',area.id,(error)=>{
              callback(null, null);
            });
          },
          (callback)=>{
            AsyncStorage.setItem('province',this.props.province,(error)=>{
              callback(null, null);
            });
          },
          (callback)=>{
            AsyncStorage.setItem('provinceid',this.props.provinceid,(error)=>{
              callback(null, null);
            });
          },
          (callback)=>{
            AsyncStorage.setItem('city',this.props.city,(error)=>{
              callback(null, null);
            });
          },
          (callback)=>{
            AsyncStorage.setItem('cityid',this.props.cityid,(error)=>{
              callback(null, null);
            });
          },
          
        ],
        function(err, results){
            NetWork.get_city();
        });


    
    if (this.props.start_page == 0) {
      navigator.popToRoute(navigator.getCurrentRoutes()[1]);
    }else{
      navigator.popToRoute(navigator.getCurrentRoutes()[0]);
    }
    
 
  },

 saveAddress2:function(){
    UMeng.onEvent('area_02');
    var navigator = this.props.navigator;
    if (this.props.start_page == 0) {
      navigator.popToRoute(navigator.getCurrentRoutes()[1]);
    }else{
      navigator.popToRoute(navigator.getCurrentRoutes()[0]);
    }
  },
});
module.exports = Area;
