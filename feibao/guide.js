/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

var TimerMixin = require ('react-timer-mixin');
var RNFS = require('react-native-fs');
//var AsyncStorage = require('./AsyncStorage');
import styles_guide from './styles/style_guide'
import Language from './Language'

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ToastAndroid,
  TouchableOpacity,
  Image,
  Dimensions,
  Platform,
  AlertIOS,
   AsyncStorage,
  Navigator,
  NetInfo,
  WebView,
} from 'react-native';


import  config from './config'
import LangChange from './LangChange'

var deviceWidth = Dimensions.get('window').width;
var deviceHeight = Dimensions.get('window').height;

var BlueToothUtil= require("./BlueToothUtil");
import Utils  from './utils'


var IMG_LIST = [
  require('image!pic1'),
  require('image!pic2'),
  require('image!pic3'),
];

var WEBVIEW_REF = 'guide_webview';

function notifyMessage(msg: string) {
  if (Platform.OS === 'android') {
    //ToastAndroid.show(msg, ToastAndroid.SHORT)
  } else {
    AlertIOS.alert(msg);
  }
}



var GuidePage = React.createClass ({
    mixins:[TimerMixin],
  getInitialState: function () {
    //去掉log
    // this._console();

    return {
      status:'0',
      status1:'1.0',
      result:1,
      mNetInfo: null,
      isConnected:false
    };
  },

  // _console(){
  
  //     if (!__DEV__) {
  //     global.console = {
  //       info: () => {},
  //       log: () => {},
  //       warn: () => {},
  //       error: () => {},
  //     };
  //   }
  // },



  componentWillUnmount(){
    // if (Platform.OS === 'android') {
    //   this.timer && clearTimeout(this.timer);
    // }
    AsyncStorage.setItem('first',
    this.state.status1,error => {
      if (error){
      }else {
      }
    });
  },

    _handleCurrentLanguage() {
      if (Platform.OS === 'android'){
        var str = '';

      	Utils.getCurrentLanguage((msg) => {
        	console.log('BleApplication```' + msg);
        	// str = msg;
          LangChange.changeLang('zh');
          config.global_language = 'zh';
          
          config.platform = 0;
          config.phone_modal = 0;
  	      config.language = 'zh-CN';

  	      // if (msg == 'zh'){
  	      //      config.language = 'zh-CN';
  	      // } 
         //  else {
  	      //      config.language = 'en-US';
  	      // }
      	});
      } else {
        Utils.getCurrentLanguage((error, msg) => {
        console.log('BleApplication```' + msg);
        LangChange.changeLang('zh');
        config.global_language = 'zh';
        config.language = 'zh-CN';

        config.platform = 1;
        config.phone_modal = 0;
        
        // if (msg == 'zh'){
        //      config.language = 'zh-CN';
        // } else {
        //      config.language = 'en-US';
        // } 

        });
      }
  },


  componentWillMount(){

    this._handleCurrentLanguage();

    this.setState({isConnected:true});
    // if (Platform.OS === 'android') {
    //     this.timer = this.setTimeout(() => {
    //         this.props.navigator.replace({
    //           id: 'TabbarPage',
    //         });
    //         AsyncStorage.flushGetRequests();
    //       }, 500); 
    //   }
          AsyncStorage.getItem('isTestXinfeng', (error, result) => {
            if (error) {

            } else {
              if (result == '忻风测试') {
                config.server_base = config.server_base_test;
              } else {
                config.server_base = config.server_base_normal;
              }
            }
            AsyncStorage.getItem('first',(error,result) => {

              if (error){
              }else {
                if (Platform.OS === 'android') {
                        this.setState({
                          result:this.state.status1,
                        });
                        if (result == null) {
                          this.second(false);
                        } else {
                          this._isExitAdFile();
                        }
                } else {
                  this.setState({
                    result:result,
                  });
                  if (result != null){
                    this._isExitAdFile();
                  }
                }
            }
          });
            
          });

    // if (Platform.OS === 'android') {
    //       console.log('guide start.');
    //       this.setState({isConnected:true}); 
    //       AsyncStorage.getItem('first',(error,result) => {
    //         if (error){}else {
    //           this.setState({
    //           result:result,
    //         });
    //         if (result != null){
    //             RNFS.exists(RNFS.PicturesDirectoryPath + '/rctad.png')
    //             .then(rslt => { 
    //               console.log('_isExitAdFile start---');
    //               console.log(rslt);
    //               this.second(rslt);
    //             });
    //         }
    //       }
    //     });
    // } else {

    // NetInfo.isConnected.fetch().done(
    //     (isConnected) => { 
    //       this.setState({isConnected:isConnected}); 
    //       AsyncStorage.getItem('first',(error,result) => {
    //         if (error){}else {
    //           this.setState({
    //           result:result,
    //         });
    //         if (result != null){

    //             this.second(true);

    //         }
    //       }
    //     });
    //   }
    // );
    // }
  },



  render() {

    return (
      <Navigator 
        renderScene = {this.renderScene}
        navigator = {this.props.navigator}/>
    );

  },

  renderScene(route,navigator){

    if (this.state.result === null ) {
      // if (Platform.OS === 'android') {
       // return (

       // <View style = {{flex:1,backgroundColor:'transparent',}}>
       //    <ViewPager
       //      style={styles_guide.container}
       //      dataSource={this.state.dataSource}
       //      renderPage={this._renderPage}
       //      onChangePage={this._onChangePage}
       //      isLoop={false}
       //      autoPlay={false} />
       //  </View>
       // source={require('./guide/guide.html')}

     //    <WebView  
     //      ref={WEBVIEW_REF}
     //      source={{uri:'file:///android_asset/guide/guide.html'}}
     //      automaticallyAdjustContentInsets = {true} 
     //      javaScriptEnabled = {true}
     //      domStorageEnabled = {true}
     //      onShouldStartLoadWithRequest  = {(nativeEvent) => this.onShouldStartLoadWithRequest(nativeEvent)}
     //      scalesPageToFit={true}>
     //    </WebView>

     //  );
     // } else {
      var guide = RNFS.MainBundlePath+'/guide/guide.html';
      return (
        <WebView  
          ref={WEBVIEW_REF}
          source={{uri:guide}}
          automaticallyAdjustContentInsets = {true} 
          javaScriptEnabled = {true}
          domStorageEnabled = {true}
          onShouldStartLoadWithRequest  = {(nativeEvent) => this.onShouldStartLoadWithRequest(nativeEvent)}
          scalesPageToFit={true}
          style={{width:deviceWidth, height:deviceHeight,}}>
        </WebView>        
      );
     // }
    }
//{require('./guide/guide.html')} {{uri:'file:///android_asset/guide/guide.html'}}  {uri:'http://localhost:8081/guide/guide.html'}
// 'content://com.android.htmlfileprovider/sdcard/index.html'
    return (<View ></View>);

  },

  onShouldStartLoadWithRequest(nativeEvent) {
    // TODO 跳过，开始使用
    // TODO 区分ios和android
    console.log('guide webview test:');
    console.log(nativeEvent);
    if (Platform.OS === 'android') {
      nativeEvent = nativeEvent.nativeEvent;
    }
    if (nativeEvent.url == 'vida:\/\/directlyuse\/') {
      this.props.navigator.replace({
        id: 'TabbarPage',
      });
      return false;
    } else
      return true;

  },
  second(rslt:boolean){
    if(this.state.isConnected && rslt){
        this.props.navigator.replace({
            id: 'WebPage',
        });
    }
    else{
        this.props.navigator.replace({
            id: 'TabbarPage',
        });
    }
  },



  _renderPage:function(img:Object, pageId:number) {
  return (
      <View style={styles_guide.container} key={pageId}>
		<Image style={[styles_guide.img,{resizeMode: 'stretch'}]} source={img} />
      </View>
    );
  },

  _onChangePage:function (page:number) {
    if (page === 2){
      //ToastAndroid.show(page+'', ToastAndroid.SHORT);

    this.setTimeout(() => {
      this.props.navigator.replace({
        id: 'TabbarPage',
      });
    }, 1000);
    }

  },

  _isExitAdFile:function () {
              var fileDir = '';
              if (Platform.OS === 'android') {
                  fileDir = RNFS.PicturesDirectoryPath + '/rctad.png';
              } else {
                  fileDir = RNFS.CachesDirectoryPath + '/rctad.png';
              }
              RNFS.exists(fileDir)
                .then(rslt => { 
                  console.log('_isExitAdFile start---');
                  console.log(rslt);
                  this.second(rslt);
              });
  },

});




//AppRegistry.registerComponent('test', () => helloworld);

module.exports = GuidePage;

