package com.lianluo.qingfeng.BlueTooth;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.nfc.Tag;
import android.util.Log;

import com.facebook.react.ReactApplication;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import com.lianluo.qingfeng.MyReactPackage;
import com.lianluo.qingfeng.reactmodule.ShareModule;
import com.rnfs.BuildConfig;
import com.rnfs.RNFSPackage;
import com.zyu.ReactNativeWheelPickerPackage;
import com.umeng.analytics.MobclickAgent;


import java.io.File;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Nullable;

import static com.lianluo.qingfeng.MainActivity.TAG;

public class BleApplication extends Application implements ReactApplication {

    private static final String TAG = "BleApplication";
    public static BlueToothUtil blueToothUtil;
    public static ShareModule shareModule;

    public static BleApplication INSTANCE;
    public static Context context;
    public static String versionNumber = "1.2.0";

    public BleApplication() {
        INSTANCE = this;
    }

    public static BleApplication getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new BleApplication();
        }
        return INSTANCE;
    }

    /**
     * 获取全局context
     * @return 返回全局context对象
     */
    public static Context getContext(){
        return context = BleApplication.getInstance().getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SoLoader.init(this, /* native exopackage */ false);
        MobclickAgent.setScenarioType(this, MobclickAgent.EScenarioType.E_UM_NORMAL);
//        MobclickAgent.openActivityDurationTrack(false);
//        MobclickAgent.setDebugMode( true );
        MobclickAgent.enableEncrypt(true);//6.0.0版本及以后

        getVersion();

    }
    private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
        @Override
        protected boolean getUseDeveloperSupport() {
            return BuildConfig.DEBUG;
        }

        @Override
        protected List<ReactPackage> getPackages() {
            return Arrays.<ReactPackage>asList(
                    new MainReactPackage(),
                    new MyReactPackage(),
                    new BlueToothtPackage(),
                    new RNFSPackage(),
                    new ReactNativeWheelPickerPackage()
            );
        }

        @Override
        public ReactInstanceManager getReactInstanceManager() {
            Log.e("getReactInstanceManager","进入getReactInstanceManager");
            return super.getReactInstanceManager();
        }

        @Nullable
        @Override
        protected String getJSBundleFile() {
            //return super.getJSBundleFile();
            String jsBundleFile = getFilesDir().getAbsolutePath() + "/index.android.bundle";
            Log.e(TAG,"zip---"+jsBundleFile);
            String jsBundle = getFilesDir().getAbsolutePath() + "/hotupload/bundle/index.android.bundle";

            Log.e(TAG,"zip---"+jsBundle);
            File file = new File(jsBundle);
            Log.e(TAG,"zip---"+file.exists());
            return file != null && file.exists() ? jsBundle : null;
        }
    };

    @Override
    public ReactNativeHost getReactNativeHost() {
        return mReactNativeHost;
    }

    /**
     * 获取版本号
     * @return 当前应用的版本号
     */
    public  String getVersion() {
        String version = "1.2.0";
        try {
            PackageManager manager = getPackageManager();
            PackageInfo info = manager.getPackageInfo(this.getPackageName(), 0);
            version = info.versionName;
            versionNumber = version;
            Log.e(TAG, "version------"+version);
            return version;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return version;
    }
}
