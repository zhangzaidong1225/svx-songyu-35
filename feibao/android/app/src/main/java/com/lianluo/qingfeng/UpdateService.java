package com.lianluo.qingfeng;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.lianluo.qingfeng.utils.FileUtil;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import static com.lianluo.qingfeng.MainActivity.mContext;

public class UpdateService extends Service  {

	public static final String Install_Apk = "Install_Apk";
	private static final int down_step_custom = 2;
	public static boolean mStartd = false;
	private static final int TIMEOUT = 10 * 1000;//超时
	private static String down_url;
	private static final int DOWN_OK = 1;
	private static final int DOWN_ERROR = 0;
    public static DownLoadThread downloadThread=null;
	private String app_name;
	public static  NotificationManager notificationManager;
	private Intent updateIntent;
	private PendingIntent pendingIntent;
	private RemoteViews contentView;
	private NotificationCompat.Builder mBuilder;
    public String isStopUpdate;
	public  int requestFlag = 1;
	public static boolean isStopdownloadThread = true;
	public UpdateService() {
		super();
		// TODO Auto-generated constructor stub
		mBuilder = new NotificationCompat.Builder(this);
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		mStartd = true;
		app_name = intent.getStringExtra("Key_App_Name");
		down_url = intent.getStringExtra("Key_Down_Url");

		FileUtil.createFile(app_name);

		if (FileUtil.isCreateFileSucess == true) {
			if (downloadThread != null && downloadThread.isAlive()) {
		      Toast.makeText(this, "正在下载中......", Toast.LENGTH_SHORT).show();
		    }else{
			   createNotification();
			   createThread();
		    }


		} else {
			Toast.makeText(this, R.string.insert_card, Toast.LENGTH_SHORT)
					.show();
			stop();
		}

		return super.onStartCommand(intent, flags, startId);
	}

	private final Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case DOWN_OK:
				contentView.setImageViewResource(R.id.notificationImage, R.drawable.appicon);
				contentView.setTextViewText(R.id.notificationTitle, app_name
						+ getString(R.string.down_sucess));
				contentView.setTextViewText(R.id.notificationPercent, "100%");

				//安装
				installApk();
				clickinstallApk();
				break;

			case DOWN_ERROR:
				contentView.setImageViewResource(R.id.notificationImage, R.drawable.appicon);
				contentView.setTextViewText(R.id.notificationTitle, app_name
						+ getString(R.string.down_fail));
				stop();
				break;

			default:
				// stopService(updateIntent);
				/****** Stop service ******/
				// stopService(intentname)
				// stopSelf();
				break;
			}
		}
	};

	private void installApk() {
		Log.e("=========","正在安装apk");
		Uri uri = Uri.fromFile(FileUtil.updateFile);
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.setDataAndType(uri, "application/vnd.android.package-archive");
		UpdateService.this.startActivity(intent);
	}
	private void clickinstallApk(){
		Log.e("============","点击安装apk");
		//点击打开一个安装包
	    Intent apkIntent = new Intent();
		apkIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		apkIntent.setAction(android.content.Intent.ACTION_VIEW);
		Uri uri = Uri.fromFile(FileUtil.updateFile);
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(uri,
				"application/vnd.android.package-archive");
		pendingIntent = PendingIntent.getActivity(UpdateService.this,
				0, intent, 0);
		mBuilder.setContentIntent(pendingIntent);
		notificationManager.notify(R.layout.notification, mBuilder.build());
		stop();
	}
	public void createThread() {
			downloadThread = new DownLoadThread();
			downloadThread.start();
	}
	public static void stop(Context context){
            mStartd = false;
		    isStopdownloadThread=false;
			try{
				downloadThread.wait();
			}catch (Exception e){};
		   	UpdateService.notificationManager.cancelAll();
			context.stopService(new Intent(context, UpdateService.class));
	}
	public void stop() {
		stopSelf();
	}
	private class DownLoadThread extends Thread {
		@Override
		public void run() {
				// TODO Auto-generated method stub
				Message message = new Message();
				try {
					long downloadSize = downloadUpdateFile(down_url,
							FileUtil.updateFile.toString());
					if (downloadSize > 0) {
						// down success
						message.what = DOWN_OK;
						handler.sendMessage(message);
						mBuilder.setOngoing(true);
					}
				} catch (Exception e) {
					e.printStackTrace();
					message.what = DOWN_ERROR;
					handler.sendMessage(message);
				}
		}

	}
	public void createNotification() {
		contentView = new RemoteViews(getPackageName(),
				R.layout.notification);
		contentView.setImageViewResource(R.id.notificationImage, R.drawable.appicon);
		contentView.setTextViewText(R.id.notificationTitle, app_name
				+ getString(R.string.is_downing));
		contentView.setTextViewText(R.id.notificationPercent, "0%");
		contentView.setProgressBar(R.id.notificationProgress, 100, 0, false);

		mBuilder = new NotificationCompat.Builder(this);
		mBuilder.setWhen(System.currentTimeMillis())// 通知产生的时间，会在通知信息里显示
				.setContent(contentView).setAutoCancel(true)// 设置这个标志当用户单击面板就可以让通知自动取消
				.setSmallIcon(R.drawable.appicon);
		Notification nitify = mBuilder.build();
		nitify.contentView = contentView;
		notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(R.layout.notification, nitify);
	}
	public long httpUpdate(String down_url ,String file)throws Exception{
		int down_step = down_step_custom;
		int totalSize;
		long downloadCount = 0;
		int updateCount = 0;
		Log.e("===","下载====5");
		InputStream inputStream;
		OutputStream outputStream;

		URL url = new URL(down_url);

		Log.e("===","下载====7");
		HttpURLConnection httpURLConnection = (HttpURLConnection) url
				.openConnection();
		Log.e("===","下载====8");
		httpURLConnection.setConnectTimeout(TIMEOUT);
		httpURLConnection.setReadTimeout(TIMEOUT);
		Log.e("===","下载====9");
		totalSize = httpURLConnection.getContentLength();
		Log.e("===","下载====10"+totalSize);
		if (httpURLConnection.getResponseCode() == 404) {
			throw new Exception("fail!");
		}
		inputStream = httpURLConnection.getInputStream();
		outputStream = new FileOutputStream(file, false);

		byte buffer[] = new byte[1024];
		int readsize = 0;

		while ((readsize = inputStream.read(buffer)) != -1) {
			if(isStopdownloadThread){
				outputStream.write(buffer, 0, readsize);
				downloadCount += readsize;
				if (updateCount == 0
						|| (downloadCount * 100 / totalSize - down_step) >= updateCount) {
					updateCount += down_step;
					contentView.setTextViewText(R.id.notificationPercent,
							updateCount + "%");
					contentView.setProgressBar(R.id.notificationProgress, 100,
							updateCount, false);

					mBuilder.setProgress(100, updateCount, false); // 这个方法是显示进度条
					notificationManager.notify(R.layout.notification,
							mBuilder.build());
				}
			}
		}
		if (httpURLConnection != null) {
			httpURLConnection.disconnect();
		}
		inputStream.close();
		outputStream.close();

		return downloadCount;
	}
	public long httpsUpdate(String down_url ,String file) throws Exception{
		int down_step = down_step_custom;
		int totalSize;
		long downloadCount = 0;
		int updateCount = 0;
		Log.e("===","下载====5");
		InputStream inputStream;
		OutputStream outputStream;


		CertificateFactory cf = CertificateFactory.getInstance("X.509");
		Log.e("===","下载====6");
		// From
		// https://www.washington.edu/itconnect/security/ca/load-der.crt
		//InputStream caInput = mContext.getAssets().open("nginx.crt");
		InputStream caInput = mContext.getAssets().open("_.lianluo.com_bundle.crt");
		Log.e("===","下载====2");
		Certificate ca;
		try {
			ca = cf.generateCertificate(caInput);
		} finally {
			caInput.close();
		}
		Log.e("===","下载====4");
		// Create a KeyStore containing our trusted CAs
		String keyStoreType = KeyStore.getDefaultType();
		KeyStore keyStore = KeyStore.getInstance(keyStoreType);
		keyStore.load(null, null);
		keyStore.setCertificateEntry("ca", ca);

		// Create a TrustManager that trusts the CAs in our KeyStore
		String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
		TrustManagerFactory tmf = TrustManagerFactory
				.getInstance(tmfAlgorithm);
		tmf.init(keyStore);

		// Create an SSLContext that uses our TrustManager
		SSLContext context = SSLContext.getInstance("TLS");
		context.init(null, tmf.getTrustManagers(), null);

		Log.e("===","下载====3");




		//URL url = new URL(down_url);
		URL url = new URL(down_url);

		Log.e("===","下载====7");
		HttpsURLConnection httpURLConnection = (HttpsURLConnection) url
				.openConnection();
		Log.e("===","下载====8");
		httpURLConnection.setConnectTimeout(TIMEOUT);
		httpURLConnection.setReadTimeout(TIMEOUT);
		httpURLConnection.setSSLSocketFactory(context.getSocketFactory());
		Log.e("===","下载====9");
		totalSize = httpURLConnection.getContentLength();
		Log.e("===","下载====10"+totalSize);
		if (httpURLConnection.getResponseCode() == 404) {
			throw new Exception("fail!");
		}

		inputStream = httpURLConnection.getInputStream();
		outputStream = new FileOutputStream(file, false);

		byte buffer[] = new byte[1024];
		int readsize = 0;

		while ((readsize = inputStream.read(buffer)) != -1) {
			if(isStopdownloadThread){
				outputStream.write(buffer, 0, readsize);
				downloadCount += readsize;
				if (updateCount == 0
						|| (downloadCount * 100 / totalSize - down_step) >= updateCount) {
					updateCount += down_step;
					contentView.setTextViewText(R.id.notificationPercent,
							updateCount + "%");
					contentView.setProgressBar(R.id.notificationProgress, 100,
							updateCount, false);

					mBuilder.setProgress(100, updateCount, false); // 这个方法是显示进度条
					notificationManager.notify(R.layout.notification,
							mBuilder.build());
				}
			}
		}
		if (httpURLConnection != null) {
			httpURLConnection.disconnect();
		}
		inputStream.close();
		outputStream.close();

		return downloadCount;
	}
	public long downloadUpdateFile(String down_url, String file)
			throws Exception {
		long downloadCount = 0;
		Log.e("===","下载====1");
		//if(!down_url.startsWith("https:")){
			//downloadCount = httpUpdate(down_url,file);
		//}else {
			downloadCount = httpsUpdate(down_url,file);
		//}
		return  downloadCount;
	}

}