package com.lianluo.qingfeng.BlueTooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Parcel;
import android.os.ParcelUuid;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.util.SparseArray;

import com.lianluo.qingfeng.utils.SZTYSharedPreferences;
import com.lianluo.qingfeng.utils.UMengDeviceUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import no.nordicsemi.android.support.v18.scanner.BluetoothLeScannerCompat;
import no.nordicsemi.android.support.v18.scanner.ScanCallback;
import no.nordicsemi.android.support.v18.scanner.ScanFilter;
import no.nordicsemi.android.support.v18.scanner.ScanResult;
import no.nordicsemi.android.support.v18.scanner.ScanSettings;

/**
 * Created by zhangzaidong on 16/5/6.
 * 数据处理模块
 */
public class XinfengService extends UartService {

    private final static String TAG = XinfengService.class.getSimpleName();
//    private final static UUID BP_SERVICE_UUID = UUID.fromString("00007676-0000-1000-8000-00805f9b34fb");
//    private final static UUID BS_SERVICE_UUID = UUID.fromString("00007777-0000-1000-8000-00805f9b34fb");
//    private final static UUID BV_SERVICE_UUID = UUID.fromString("00007878-0000-1000-8000-00805f9b34fb");

    private final static String SHAREPREFERENCE_NAME = "XINGFENG";
    private final static String PARAM_UUID = "param_uuid";
//    private final static String PARAM_UUID_single = "param_uuid_single";
//    private final static String PARAM_UUID_vehide = "param_uuid_vehide";

    private static ParcelUuid mUuid,mUuidSingle,mUuidVehide;
    private static boolean scan_state = false;

    public static ConcurrentHashMap<String, BleDevice> mData;
    private Map<String, Boolean> mDeviceDisconnectedState;
    private DeviceMap deviceMap;

    private BluetoothAdapter mBluetoothAdapter;
    BluetoothLeScannerCompat scanner, scanerAuto,scanConn;
    ScanCallback scanCallback, scanCallbackAuto,scanCallbackConn;

    private static int tmp_num = 0;
    private static int tmp_num2 = 0;
    private Handler mHandler;

    public XinfengService() {

    }

    /***
     * 初始化 data 数据
     */
    public void InitData() {
        mData = new ConcurrentHashMap<String, BleDevice>();

        deviceMap = new DeviceMap();

        deviceMap.map = new HashMap<>();
        deviceMap.map = mData;
        mDeviceDisconnectedState = new HashMap<String, Boolean>();
        mHandler = new Handler();
        ReadData();
    }

    /***
     * 发送数据到 data Activity
     */
    public void SendData(String action) {
        final Intent intent = new Intent(action);
        deviceMap.map = mData;
        intent.putExtra("map", deviceMap);
        intent.putExtra("scan_state", scan_state);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public void sendData(String action, String address) {

        final Intent intent = new Intent(action);
        deviceMap.map = mData;
        intent.putExtra("map", deviceMap);
        intent.putExtra(DEVICE_ADDRESS, address);
        intent.putExtra("scan_state", scan_state);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

    }

    /***
     * 发送数据到 data Activity
     */
    public void SendData(String action, String address) {
        final Intent intent = new Intent(action);
        deviceMap.map = mData;
        intent.putExtra("map", deviceMap);
        intent.putExtra(DEVICE_ADDRESS, address);

        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    /***
     * 清除数据
     *
     * @param mark 0-清除所有  1-只清除未配对
     */
    public void ClearData(int mark) {
        if (mark == 0) {
            mData.clear();
            SharedPreferences sp = getSharedPreferences(SHAREPREFERENCE_NAME, Context.MODE_PRIVATE);

            SharedPreferences.Editor editor = sp.edit();
            editor.clear();
            editor.apply();
        }
        if (mark == 1) {
            if (mData.size() > 0) {
                for (Iterator iter = mData.entrySet().iterator(); iter.hasNext(); ) {
                    Map.Entry element = (Map.Entry) iter.next();
                    Object strKey = element.getKey();
                    Object strObj = element.getValue();

                    final BleDevice device = (BleDevice) strObj;

                    if (device.getIsconnectd() == 0 && !device.ismatchd()) {
                        iter.remove();
                    }
                }
            }
        }
    }

    /***
     * 清除一条数据
     * 取消配对时删除数据
     *
     * @param address
     */
    public void RemoveData(String address) {
        if (!mData.isEmpty() && (mData.containsKey(address)) && (address != null)) {
            Iterator iter = mData.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry entry = (Map.Entry) iter.next();
                Object key = entry.getKey();
                Object val = entry.getValue();
                BleDevice bleDevice = (BleDevice) val;
                if (bleDevice.getAddress().equals(address)) {
                    iter.remove();
                }
            }
        }

        //取消配对时，删除指定数据
        if (mData.isEmpty()) {
            ClearData(0);
        } else {
            SaveData();
        }
    }

    public void scanner() {
        scan_state = true;
        broadcastUpdate(START_SCAN);
        //点击搜索删除可用列表
//        Log.e(TAG, mData.size() + "");

        if (!mData.isEmpty()) {
            broadcastUpdate(CLEAR_DATA_1);
        }


        scanner = BluetoothLeScannerCompat.getScanner();
        final ScanSettings settings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY).setReportDelay(1000).setUseHardwareBatchingIfSupported(false).build();
        final List<ScanFilter> filters = new ArrayList<>();

        final Bundle args = new Bundle();

        mUuid = args.getParcelable(PARAM_UUID);
//        final Bundle xin = new Bundle();
//        final Bundle single = new Bundle();
//        final Bundle vehide = new Bundle();
//
//        xin.putParcelable(PARAM_UUID,new ParcelUuid(BP_SERVICE_UUID));
//        single.putParcelable(PARAM_UUID_single,new ParcelUuid(BS_SERVICE_UUID));
//        vehide.putParcelable(PARAM_UUID_vehide,new ParcelUuid(BV_SERVICE_UUID));
//
//        mUuid = xin.getParcelable(PARAM_UUID);
//        mUuidSingle = single.getParcelable(PARAM_UUID_single);
//        mUuidVehide = vehide.getParcelable(PARAM_UUID_vehide);


        scanCallback = new ScanCallback() {
            @Override
            public void onBatchScanResults(List<ScanResult> results) {
                //搜索到每个蓝牙对象是调用
                for (ScanResult result : results) {
//                    Log.e(TAG, "搜索到蓝牙设备--" + result.getDevice().getName() + " " + result.getDevice().getAddress());

                    List<ParcelUuid> uuids = result.getScanRecord().getServiceUuids();

                    byte[] manufactureData = result.getScanRecord().getManufacturerSpecificData(30326);
                    byte[] danManufactureData = result.getScanRecord().getManufacturerSpecificData(30583);

                    if (manufactureData != null && manufactureData[0] == 1) {
//                        Log.e(TAG, "该设备处于意外断开中" + result.getDevice().getName() + " " + result.getDevice().getAddress());
                        return;
                    }
                    boolean isQingfeng = false;
                    boolean isDanpin = false;
//                    boolean isVehide = false;

                    byte[] adData = result.getScanRecord().getManufacturerSpecificData(0x7676);
                    byte[] danData = result.getScanRecord().getManufacturerSpecificData(0x7777);
//                    byte[] vehideData = result.getScanRecord().getManufacturerSpecificData(0x7878);

//                    if (uuids != null){
//                        for (ParcelUuid uuid:uuids){
//                            if (uuid.equals(mUuid)){
//                                Log.e(TAG,"uuid--Xinfeng--"+uuid + "");
//                                if (adData != null) {
//                                    Log.e(TAG, "-----" + "--adData--" + adData[0]);
//                                    if (adData[0] == 0) {
//                                        Log.e(TAG, "--adData-IS TRUE--");
//                                        isQingfeng = true;
//                                    }
//                                }
//                            }
//                            if (uuid.equals(mUuidSingle)){
//                                Log.e(TAG,"uuid--Danpin--"+uuid + "");
//                                isDanpin = true;
//                            }
//                            if (uuid.equals(mUuidVehide)){
//                                Log.e(TAG,"uuid--Vehide--"+uuid + "");
//                                isVehide = true;
//                            }
//                            break;
//                        }
//                    }



                    //增加在设备直连的情况下，搜索不显示直连设备
                    if (uuids != null) {
                        if (uuids.size() > 0) {
                            String struuid = uuids.get(0).toString();
                            if (struuid.substring(0, 8).equals("00007676")) {
                                if (adData != null) {
                                    if (adData[0] == 0) {
                                        isQingfeng = true;
                                    }
                                } else {
                                    isQingfeng = true;
                                }

                            }
                            if (struuid.substring(0, 8).equals("00007777")) {
                                if (danData != null) {
                                    if (danData[0] == 0) {
                                        isDanpin = true;
                                    }
                                } else {
                                    isDanpin = true;
                                }
                            }
//                            if (struuid.substring(0, 8).equals("00007878")) {
//                                if (vehideData != null) {
//                                    Log.e(TAG, "-------vehideData--" + vehideData[0]);
//                                    if (vehideData[0] == 0) {
//                                        Log.e(TAG, "--danData-IS TRUE--");
//                                        isVehide = true;
//                                    } else {
//                                        Log.e(TAG, "-----" + result.getDevice().getName() + "--vehideData--" + vehideData[0]);
//                                    }
//                                } else {
//                                    Log.e(TAG, "---danData--" + "--adData--");
//                                    isVehide = true;
//                                }
//                            }

                        }
                    }

                    boolean is_contain = false;
                    for (String key : mData.keySet()) {
                        if (result.getDevice().getAddress().equals(key)) {
                            is_contain = true;
                            break;
                        }
                        is_contain = false;
                    }

                    if (!is_contain) {

                        if (isQingfeng) {
                            Log.e(TAG, "---isQinfeng---");
                            BleDevice device = new BleDevice();

                            if (result.getDevice().getName() == null) {
                                device.setName("未命名");
                            } else {
                                device.setName(result.getDevice().getName());
                            }
                            device.setAddress(result.getDevice().getAddress());
                            device.setIsmatchd(false);
                            device.setType(0);
                            AddData(result.getDevice().getAddress(), device);

                            broadcastUpdate(ADD_DEVICE, result.getDevice().getAddress());
                        }
                        if (isDanpin) {
                            Log.e(TAG, "---isDanpin---");
                            BleDevice device = new BleDevice();


                            if (result.getDevice().getName() == null) {
                                device.setName("未命名");
                            } else {
                                device.setName(result.getDevice().getName());
                            }
                            device.setAddress(result.getDevice().getAddress());
                            device.setIsmatchd(false);
                            device.setType(1);

                            AddData(result.getDevice().getAddress(), device);

                            //Log.e(TAG, mData.size() + "---MainActivity.mService.mData.size()---");

                            broadcastUpdate(ADD_DEVICE, result.getDevice().getAddress());

                        }
                        /*if (isVehide){

                            Log.e(TAG, "---isVehide---");
                            BleDevice device = new BleDevice();


                            if (result.getDevice().getName() == null) {
                                device.setName("未命名");
                            } else {
                                device.setName(result.getDevice().getName());
                            }
                            device.setAddress(result.getDevice().getAddress());
                            device.setIsmatchd(false);
                            device.setType(2);

                            AddData(result.getDevice().getAddress(), device);

                            //Log.e(TAG, mData.size() + "---MainActivity.mService.mData.size()---");

                            broadcastUpdate(ADD_DEVICE, result.getDevice().getAddress());
                        }*/
                    }
                    if (!isQingfeng) break;
                    if (!isDanpin) break;
//                    if (!isVehide) break;

                }
                super.onBatchScanResults(results);
            }
        };
        filters.add(new ScanFilter.Builder().setServiceUuid(mUuid).build());
//        filters.add(new ScanFilter.Builder().setServiceUuid(mUuidSingle).build());
//        filters.add(new ScanFilter.Builder().setServiceUuid(mUuidVehide).build());
        scanner.startScan(filters, settings, scanCallback);

        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                if (scanner != null ){
                    scanner.stopScan(scanCallback);
                    scan_state = false;
                    scanner = null;
                    scanCallback = null;
                    broadcastUpdate(STOP_SCAN);
                }
            }
        };
        timer.schedule(task, 5000);
    }

    public void stopScan() {
        if (scanner != null ){
            scanner.stopScan(scanCallback);
            scan_state = false;
            scanner = null;
            scanCallback = null;
            broadcastUpdate(STOP_SCAN);
        }

    }


    /**
     * 修改在自动连接时，设备的名称没有及时发生改变。
     */
    public void auto_Scanner() {

//        Log.e(TAG, "auto_Scanner---------");
        scanerAuto = BluetoothLeScannerCompat.getScanner();
        final ScanSettings settings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY).setReportDelay(1000).setUseHardwareBatchingIfSupported(false).build();
        final List<ScanFilter> filters = new ArrayList<>();

        final Bundle args = new Bundle();
        mUuid = args.getParcelable(PARAM_UUID);

//        final Bundle xin = new Bundle();
//        final Bundle single = new Bundle();
//        final Bundle vehide = new Bundle();

//        xin.putParcelable(PARAM_UUID,new ParcelUuid(BP_SERVICE_UUID));
//        single.putParcelable(PARAM_UUID_single,new ParcelUuid(BS_SERVICE_UUID));
//        vehide.putParcelable(PARAM_UUID_vehide,new ParcelUuid(BV_SERVICE_UUID));

//        mUuid = xin.getParcelable(PARAM_UUID);
//        mUuidSingle = single.getParcelable(PARAM_UUID_single);
//        mUuidVehide = vehide.getParcelable(PARAM_UUID_vehide);

        scanCallbackAuto = new ScanCallback() {
            @Override
            public void onBatchScanResults(List<ScanResult> results) {
                //搜索到每个蓝牙对象是调用
                for (ScanResult result : results) {
                    Log.e(TAG, "搜索到蓝牙设备---auto_Scanner---" + result.getDevice().getName() + " " + result.getDevice().getAddress());
                }
                super.onBatchScanResults(results);
            }
        };
        filters.add(new ScanFilter.Builder().setServiceUuid(mUuid).build());
//        filters.add(new ScanFilter.Builder().setServiceUuid(mUuidSingle).build());
//        filters.add(new ScanFilter.Builder().setServiceUuid(mUuidVehide).build());
        scanerAuto.startScan(filters, settings, scanCallbackAuto);


/*        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                Log.e(TAG,"auto_Scanner---------");
                scanner.stopScan(scanCallbackAuto);
                //scan_state=false;
                //broadcastUpdate(STOP_SCAN);

                //BleApplication.blueToothUtil.sendMydevice();
            }
        };
        timer.schedule(task, 5000);*/

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.e(TAG, "auto_Scanner---------STOP scanner---");
                //final BluetoothLeScannerCompat scanner = BluetoothLeScannerCompat.getScanner();
                if (scanerAuto != null){
                    scanerAuto.stopScan(scanCallbackAuto);
                    scanCallbackAuto = null;
                    scanerAuto = null;
                }
            }
        }, 5000);

    }

    /**
     * 自动连接时，扫描搜索，执行连接
     *
     */
    public void scanAddressAndConnect(final boolean connecting){
        Log.e(TAG, "scanAddressAndConnect---------");
        scanConn = BluetoothLeScannerCompat.getScanner();
        final ScanSettings settings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY).setReportDelay(1000).setUseHardwareBatchingIfSupported(false).build();
        final List<ScanFilter> filters = new ArrayList<>();
//
        final Bundle args = new Bundle();
        mUuid = args.getParcelable(PARAM_UUID);

//        final Bundle xin = new Bundle();
//        final Bundle single = new Bundle();
//        final Bundle vehide = new Bundle();

//        xin.putParcelable(PARAM_UUID,new ParcelUuid(BP_SERVICE_UUID));
//        single.putParcelable(PARAM_UUID_single,new ParcelUuid(BS_SERVICE_UUID));
//        vehide.putParcelable(PARAM_UUID_vehide,new ParcelUuid(BV_SERVICE_UUID));

//        mUuid = xin.getParcelable(PARAM_UUID);
//        mUuidSingle = single.getParcelable(PARAM_UUID_single);
//        mUuidVehide = vehide.getParcelable(PARAM_UUID_vehide);

         scanCallbackConn = new ScanCallback() {
            @Override
            public void onBatchScanResults(List<ScanResult> results) {
                //搜索到每个蓝牙对象是调用

                for (ScanResult result : results) {

                    //Log.e(TAG, "搜索到蓝牙设备---scanAddressAndConnect---" + result.getDevice().getName() + " " + result.getDevice().getAddress());

                    for (String key : mData.keySet()) {
                        if (result.getDevice().getAddress().equals(key)) {
                            BleDevice device = mData.get(key);
                            if (device.ismatchd()){
                                if ((device.getIsconnectd() == 0) && (device.isAutoConnect())){
                                    if (scanConn != null ){
                                        scanConn.stopScan(scanCallbackConn);
                                        scanConn = null;
                                        scanCallbackConn = null;
                                    }
                                    if (mBluetoothGattMap.isEmpty() ){
                                        //app启动执行搜索
                                        BleApplication.blueToothUtil.connect(device.getAddress(),connecting);
                                    }
                                    if (!mBluetoothGattMap.containsKey(key)){
                                        BleApplication.blueToothUtil.connect(device.getAddress(),connecting);
                                    }
                                    break;
                                }
                            }
                        }

                    }
                }
                super.onBatchScanResults(results);
            }
        };
        filters.add(new ScanFilter.Builder().setServiceUuid(mUuid).build());
//        filters.add(new ScanFilter.Builder().setServiceUuid(mUuidSingle).build());
//        filters.add(new ScanFilter.Builder().setServiceUuid(mUuidVehide).build());
        //在频繁的scan，会有scanConn和scanCallbackConn为空的情况，
        Log.e(TAG,"scanConn: "+scanConn + "``" + "scanCallbackConn: " + scanCallbackConn);

        if (scanConn != null && scanCallbackConn != null){
            scanConn.startScan(filters, settings, scanCallbackConn);
        }

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                if (scanConn != null){
                    scanConn.stopScan(scanCallbackConn);
                    scanConn = null;
                    scanCallbackConn = null;
                }
            }
        }, 3500);
    }


    /***
     * 添加一条数据
     * 每连接成功一个就添加进去
     *
     * @param address
     * @param item
     */
    public void AddData(String address, BleDevice item) {
        mData.put(address, item);
        SaveData();
    }

    /***
     * 更新数据
     * 状态改变时，更新数据
     *
     * @param address
     * @param item
     */
    public void UpdateData(String address, Object item) {
        mData.put(address, (BleDevice) item);
        SaveData();
    }

    public void UpdateData(String address, Object item, Boolean value) {
        mData.put(address, (BleDevice) item);
    }


    /***
     * 序列化数据
     * 将连接成功的数据 进行存储
     */

    public void SaveData() {

        JSONArray jsonArray = new JSONArray();
        Collection values = mData.values();
        for (Iterator iterator = values.iterator(); iterator.hasNext(); ) {
            Object obj = iterator.next();
            BleDevice device = (BleDevice) obj;
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("address", device.getAddress());
                jsonObject.put("name", device.getName());
                jsonObject.put("autoconnect", device.isAutoConnect());
                jsonObject.put("matched", device.ismatchd());
                jsonObject.put("type",device.getType());
                jsonObject.put("model",device.getModel());
//                jsonObject.put("space",device.getSpace());
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            SharedPreferences sp = getSharedPreferences(SHAREPREFERENCE_NAME, MODE_APPEND);
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("BLE_DATA", jsonArray.toString());
            editor.apply();
        }
    }

    /***
     * 反序列化数据
     * app启动的时候读取数据
     */
    public void ReadData() {
        mData.clear();
        SharedPreferences sp = getSharedPreferences(SHAREPREFERENCE_NAME, MODE_APPEND);
        String result = sp.getString("BLE_DATA", "");
        if ( result.equals("")){
            return;
        }

        try {
            JSONArray jsonArray = new JSONArray(result);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject itemObject = jsonArray.getJSONObject(i);
                BleDevice device = new BleDevice();
                device.setAddress((String) itemObject.get("address"));
                device.setName((String) itemObject.get("name"));
                device.setAutoConnect((boolean) itemObject.get("autoconnect"));
                device.setIsmatchd((boolean) itemObject.get("matched"));
                device.setType((Integer) itemObject.get("type"));
                device.setModel((Integer) itemObject.get("model"));
//                device.setSpace((Integer) itemObject.get("space"));
                device.setIsconnectd(0);
                device.setUpdate_state(0);

                AddData(device.getAddress(), device);
                broadcastUpdate(ADD_DEVICE, device.getAddress());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public class XinfengLocalBinder extends Binder {
        public XinfengService getService() {
            return XinfengService.this;
        }
    }

    public final IBinder mXinfengBinder = new XinfengLocalBinder();

    @Override
    public IBinder onBind(Intent intent) {
        return mXinfengBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }


    @Override
    protected void broadcastUpdate(String action) {
        super.broadcastUpdate(action);

        if (action.equals(CLEAR_DATA_1)) {
            ClearData(1);
            SendData(CLEAR_DATA_1);
        }

        if (action.equals(START_SCAN)) {
            SendData(START_SCAN);
        }

        if (action.equals(STOP_SCAN)) {
            SendData(STOP_SCAN);
        }

    }

    @Override
    protected void broadcastUpdate(final String action, final String address) {

        final BleDevice device = mData.get(address);
        switch (action) {
            case ADD_DEVICE:
                SendData(ADD_DEVICE);
                break;
            case ACTION_GATT_CONNECTED:
//                Log.d(TAG, "连接成功");

                device.setIsconnectd(1);
                device.setIsmatchd(true);
                device.setAutoConnect(true);
                device.setUpdate_state(0);
                UpdateData(address, device);
                sendData(ACTION_GATT_CONNECTED, address);

                /**
                 * 保存最后一次连接成功的
                 */
                SZTYSharedPreferences.getInstance().putString("address",address);
                sendData(DEVICE_ADDRESS, address);
                break;
            case ACTION_GATT_DISCONNECTED:
//                Log.d(TAG, "断开连接");
                if (device.ismatchd()) {
                    device.setIsconnectd(0);
                    device.setUpdate_state(0);

                    if(device.getType() == 1){
                        device.setSpace(0);
                    }

                    UpdateData(address, device);
//                    Log.d(TAG, "断开连接----ismatched=true-释放资源");
                    if (mDeviceDisconnectedState.get(address) != null) {
                        mDeviceDisconnectedState.remove(address);
                    }
                    close(address);
                } else {

                    if (device.getIsconnectd() == 1) {
                        RemoveData(address);
                        close(address);
//                        Log.e(TAG, "取消配对--ismatched=false-释放资源");
                    } else {
                        device.setIsconnectd(0);
                        device.setUpdate_state(0);

                        UpdateData(address, device);
                        close(address);
//                        Log.e(TAG, "可用设备列表---ismatched=false-释放资源");
                    }
                }

                sendData(ACTION_GATT_DISCONNECTED, address);
                break;
            case ACTION_GATT_SERVICES_DISCOVERED:
                sendData(ACTION_GATT_SERVICES_DISCOVERED, address);
                break;
            case DEVICE_DOES_NOT_SUPPORT_UART:
//                Log.e(TAG, "DEVICE_DOES_NOT_SUPPORT_UART.......");

//                disconnect(address);

                sendData(DEVICE_DOES_NOT_SUPPORT_UART, address);

                break;
            default:
                break;
        }


    }

    @Override
    protected void broadcastUpdate(final String action,
                                   final String address,
                                   final BluetoothGattCharacteristic characteristic) {
        BleDevice bleDevice = mData.get(address);
        switch (action) {
            case ACTION_DATA_AVAILABLE:
                // This is handling for the notification on TX Character of NUS service
                if (TX_CHAR_UUID.equals(characteristic.getUuid())) {
                    byte[] Value = characteristic.getValue();
                    if (parseData(Value, bleDevice)) {
                        UpdateData(address, bleDevice);
                        SendData(ACTION_DATA_AVAILABLE);
                    }
                }
                break;
            default:
                break;
        }
    }


    public boolean parseData(byte[] Value, BleDevice bleDevice) {
        boolean is_change = false;

        if (Value[1] == 0x01) {
//            Log.e(TAG, "关机命令" + Value[1] + "");
            if (mDeviceDisconnectedState.get(bleDevice.getAddress()) == null) {
            disconnect(bleDevice.getAddress());
                mDeviceDisconnectedState.put(bleDevice.getAddress(), true);
            is_change = true;
            }
        }

        if (Value[1] == 0x03) {
//            Log.e(TAG, "蓝牙关闭```" + Value[1] + "");
            bleDevice.setAutoConnect(false);
            //针对三星S6，关闭设备蓝牙，app显示仍然连接问题。
            disconnect(bleDevice.getAddress());
            is_change = true;

        }
/*        if (Value[1] == 0x07) {
            Log.e(TAG, "手动断开连接```````````" + bleDevice.getAddress());

        }
        if (Value[1] == 0x0C){
            Log.e(TAG, "车载时间```````````");

        }*/
        if (Value[1] == 0x15) {
            byte[] dataName = new byte[Value[2]];

            for (int i = 0; i < Value[2]; i++) {
                dataName[i] = Value[3 + i];
            }
            String devName = new String(dataName);

            if (!bleDevice.getName().equals(devName)) {
                Log.e(TAG, "名字---长度---" + Value[2] + "----" + devName);
                bleDevice.setName(devName);
                is_change = true;
            }
        }

/*        if (Value[1] == 0x16) {
            Log.e(TAG, "Esmaty---长度---");
        }

        if (Value[1] == 0x5C) {
            Log.e(TAG, "SN码---长度---");

        }*/
        /**
         * 接收滤芯因子返回数据
         */
        if (Value[1] == 0x5F) {
//            Log.d(TAG,"滤芯因子数据");
        }
        if (Value[1] == 0x54) {
            //获取固件版本
            //增加固件版本为小数
            int firstByte = (0x000000FF & ((int) Value[2]));
            int secondByte = (0x000000FF & ((int) Value[3]));
            int unsignedInt = ((int) (secondByte | firstByte << 8));

            if (bleDevice.getVersion() != unsignedInt) {
                Log.e(TAG,"设备版本号```"+unsignedInt + "");
                bleDevice.setVersion(unsignedInt);
                is_change = true;
            }
        }
        if (Value[1] == 0x50) {

            if (Value[2] == 0x1) {

                if (bleDevice.getSpeed() != Value[5]) {
                    Log.e(TAG,"风扇等级```"+Value[5] + "");
                    bleDevice.setSpeed(Value[5]);
                    is_change = true;
                }
                int firstByte = (0x000000FF & ((int) Value[14]));
                int secondByte = (0x000000FF & ((int) Value[13]));
                int threeByte = (0x000000FF & ((int) Value[12]));
                int fourByte = (0x000000FF & ((int) Value[11]));

                int unsignedInt = ((int) (firstByte | secondByte << 8 | threeByte << 16 | fourByte << 24));

                if ((unsignedInt - bleDevice.getUse_time()) > 60 || bleDevice.getUse_time() - unsignedInt > 0) {
                    Log.e(TAG,"滤棉使用时长```"+unsignedInt + "");
                    bleDevice.setUse_time(unsignedInt);
                    is_change = true;
                }

                int firstByteThis = (0x000000FF & ((int) Value[8]));
                int secondByteThis = (0x000000FF & ((int) Value[7]));
                int unsignedIntThis = ((int) (firstByteThis | secondByteThis << 8));

                if ((bleDevice.getUse_time_this() - unsignedIntThis > 0) || (unsignedIntThis - bleDevice.getUse_time_this() > 10)) {
                    Log.e(TAG,"本次滤棉使用时长```"+unsignedIntThis + "");
                    bleDevice.setUse_time_this(unsignedIntThis);
                    is_change = true;
                }

                if (bleDevice.getFilter_in() != Value[15]) {
                    Log.e(TAG,"滤棉在位````"+Value[15] + "");
                    bleDevice.setFilter_in(Value[15]);
                    is_change = true;
                }

                int firstByteNumber = (0x000000FF & ((int) Value[10]));
                int secondByteNumber = (0x000000FF & ((int) Value[9]));
                int unsignedIntNumber = ((int) (firstByteNumber | secondByteNumber << 8));
                if (bleDevice.getFilter_number() != unsignedIntNumber){
                    Log.e(TAG,"滤棉使用个数```"+unsignedIntNumber + "");
                    bleDevice.setFilter_number(unsignedIntNumber);
                    is_change = true;
                }
            }

            if (Value[2] == 0x3) {

                int firstByte = (0x000000FF & ((int) Value[10]));
                int secondByte = (0x000000FF & ((int) Value[9]));
                int threeByte = (0x000000FF & ((int) Value[8]));
                int fourByte = (0x000000FF & ((int) Value[7]));

                int unsignedInt = ((int) (firstByte | secondByte << 8 | threeByte << 16 | fourByte << 24));

                if ((unsignedInt - bleDevice.getFilter_use_time_this() > 60 ) || (bleDevice.getFilter_use_time_this() - unsignedInt > 0)){
                    Log.e(TAG,"本次滤棉使用总时长```"+unsignedInt + "   ");
                    bleDevice.setFilter_use_time_this(unsignedInt);
                    is_change = true;
                }

                int firstByteFan = (0x000000FF & ((int) Value[14]));
                int secondByteFan = (0x000000FF & ((int) Value[13]));
                int threeByteFan = (0x000000FF & ((int) Value[12]));
                int fourByteFan = (0x000000FF & ((int) Value[11]));

                int unsignedIntFan = ((int) (firstByteFan | secondByteFan << 8 | threeByteFan << 16 | fourByteFan << 24));

                if ((unsignedIntFan - bleDevice.getFilters_maxfan_usetime()) > 60 || (bleDevice.getFilters_maxfan_usetime() -unsignedIntFan > 0)){
                    Log.e(TAG,"滤棉折合全风速使用时长````"+unsignedIntFan + "   ");
                    bleDevice.setFilters_maxfan_usetime(unsignedIntFan);
                    is_change = true;
                }
                int firstByteFilter = (0x000000FF & ((int) Value[18]));
                int secondByteFilter = (0x000000FF & ((int) Value[17]));
                int threeByteFilter = (0x000000FF & ((int) Value[16]));
                int fourByteFilter = (0x000000FF & ((int) Value[15]));

                int unsignedIntFilter = ((int) (firstByteFilter | secondByteFilter << 8 | threeByteFilter << 16 | fourByteFilter << 24));

                if ((unsignedIntFilter - bleDevice.getFilter_use_time_real()) > 60 || (bleDevice.getFilter_use_time_real() -unsignedIntFilter > 0)){
                    Log.e(TAG,"滤棉使用真实时长````"+unsignedIntFilter + "   ");
                    bleDevice.setFilter_use_time_real(unsignedIntFilter);
                    is_change = true;
                }


            }

            if (Value[2] == 0x4) {

                //防止电量重复发送
                if (bleDevice.getBattery_power() != Value[3]) {
                    Log.e(TAG,"电量```"+Value[3] + "");
                    bleDevice.setBattery_power(Value[3]);
                    is_change = true;
                }

                if (bleDevice.getBattery_charge() != Value[6]) {
                    Log.e(TAG,"充电中```"+Value[6] + "");
                    bleDevice.setBattery_charge(Value[6]);
                    is_change = true;
                }

                if ((Value[17] & 0x01) == 0x01) {
                    Log.e(TAG, (Value[17] & 0x01) + "--01--电压过压");
                    bleDevice.setBattery_over(1);
                    is_change = true;

                } else if ((Value[17] & 0x10) == 0x10) {
                    Log.e(TAG, (Value[17] & 0x10) + "--10--电流过低");
                    bleDevice.setBattery_over(1);
                    is_change = true;
                } else {
                    if (Value[17] == 0){
                        if (bleDevice.getBattery_over() != Value[17]){
                            Log.e(TAG, (Value[17]) + "--0--电压正常");
                            bleDevice.setBattery_over(0);
                            is_change = true;
                        }
                    }
                }
            }

            if (Value[2] == 0x5) {
                int firstByte = (0x000000FF & ((int) Value[11]));
                int secondByte = (0x000000FF & ((int) Value[12]));

                int unsignedInt = ((int) (firstByte << 8 | secondByte));

                if ((unsignedInt - bleDevice.getWind_use_time_this()) > 10 || (bleDevice.getWind_use_time_this() - unsignedInt) > 0) {
                    Log.e(TAG,"本次风扇使用时长````"+unsignedInt + "   ");
                    bleDevice.setWind_use_time_this(unsignedInt);
                    is_change = true;
                }
            }

            if (Value[2] == 0x6) {
                int firstByte = (0x000000FF & ((int) Value[3]));
                int secondByte = (0x000000FF & ((int) Value[4]));
                int unsignedInt = ((int) (secondByte | firstByte << 8));

                if (bleDevice.getPm25() != unsignedInt) {
                    Log.e(TAG,"Pm2.5标准浓度````"+unsignedInt + "");
                    bleDevice.setPm25(unsignedInt);
                    is_change = true;
                }

                if (bleDevice.getPm25Flag() != Value[11]){
                    Log.e(TAG,"Pm2.5标准浓度--时间戳标志位````"+Value[11]+"");
                    bleDevice.setPm25Flag(Value[11]);
                    is_change = true;
                }

                int firstByteTimer = (0x000000FF & ((int) Value[15]));
                int secondByteTimer = (0x000000FF & ((int) Value[14]));
                int threeByteTimer = (0x000000FF & ((int) Value[13]));
                int fourByteTimer= (0x000000FF & ((int) Value[12]));

                int unsignedIntTimer = ((int) (firstByteTimer | secondByteTimer << 8 | threeByteTimer << 16 | fourByteTimer << 24));

                if ((unsignedIntTimer - bleDevice.getSystemTime() > 10) || (bleDevice.getSystemTime() - unsignedIntTimer > 0)){
                    Log.e(TAG,"系统时间戳```"+unsignedIntTimer + "");
                    bleDevice.setSystemTime(unsignedIntTimer);
                    is_change = true;
                }

                int firstByteFan = (0x000000FF & ((int) Value[10]));
                int secondByteFan = (0x000000FF & ((int) Value[9]));
                int threeByteFan = (0x000000FF & ((int) Value[8]));
                int fourByteFan = (0x000000FF & ((int) Value[7]));

                int unsignedIntFan = ((int) (firstByteFan | secondByteFan << 8 | threeByteFan << 16 | fourByteFan << 24));

                if ((unsignedIntFan - bleDevice.getPm25Time() > 10) || (bleDevice.getPm25Time() - unsignedIntFan > 0)){
                    Log.e(TAG,"Pm2.5标准浓度采集--时间戳```"+unsignedIntFan + "");
                    bleDevice.setSystemTime(unsignedIntTimer);
                    bleDevice.setPm25Time(unsignedIntFan);
                    is_change = true;
                }

            }

            if (Value[2] == 0x7){
                int firstByte = (0x000000FF & ((int) Value[3]));
                int secondByte = (0x000000FF & ((int) Value[4]));
                int unsignedInt = ((int) (secondByte | firstByte << 8));

                if (bleDevice.getSpace() != unsignedInt){
                    Log.e(TAG,"采集时间间隔```"+unsignedInt + "");
                    bleDevice.setSpace(unsignedInt);
                    is_change = true;
                }

                if (bleDevice.getType() == 1){
                    if (bleDevice.getModel() != Value[5]){
                        Log.e(TAG,"采集单品模式```"+Value[5]);
                        bleDevice.setModel(Value[5]);
                        is_change = true;
                    }
                }else if (bleDevice.getType() == 2){
                    if (bleDevice.getCarModel() != Value[5]){
                        Log.e(TAG,"采集车载模式```"+Value[5]);
                        bleDevice.setCarModel(Value[5]);
                        is_change = true;
                    }
                }

//                if (bleDevice.getHour() != Value[6]){
//                    Log.e(TAG,"车载时间小时```"+Value[6]);
//
//                     bleDevice.setHour(Value[6]);
//                    is_change = true;
//                }
//
//                if (bleDevice.getMinute() != Value[7]){
//                    Log.e(TAG,"车载时间分钟```"+Value[7]);
//                    bleDevice.setMinute(Value[7]);
//                    is_change = true;
//                }
                if (bleDevice.getVehideModel() != Value[9]){
                    Log.e(TAG,"车载模式```"+Value[9]);
                    bleDevice.setVehideModel(Value[9]);
                    is_change = true;
                }

                if (bleDevice.getOpen_cap() != Value[10]){
                    Log.e(TAG,"车载开盖···" + Value[10]);
                    bleDevice.setOpen_cap(Value[10]);
                    is_change = true;
                }

            }

            if (Value[2] == 0x8){

//                     bleDevice.setFilter01_in(1);
//                     bleDevice.setFilter02_in(1);

                if (bleDevice.getFilter01_in() != Value[3]){
                    Log.e(TAG,"滤棉1在位``` " + Value[3]);
                    bleDevice.setFilter01_in(Value[3]);
                    is_change = true;
                }
                if (bleDevice.getFilter02_in() != Value[4]){
                    Log.e(TAG,"滤棉2在位``` " +  Value[4]);
                    bleDevice.setFilter02_in(Value[4]);
                    is_change = true;
                }

                int firstByteFilter01Time = (0x000000FF & ((int) Value[8]));
                int secondByteFilter01Time = (0x000000FF & ((int) Value[7]));
                int threeByteFilter01Time = (0x000000FF & ((int) Value[6]));
                int fourByteFilter01Time = (0x000000FF & ((int) Value[5]));

                int unsignedIntFilter01 = ((int) (firstByteFilter01Time | secondByteFilter01Time << 8 | threeByteFilter01Time << 16 | fourByteFilter01Time << 24));


                if ((unsignedIntFilter01 - bleDevice.getFilter01_use_time() > 60 ) || bleDevice.getFilter01_use_time() - unsignedIntFilter01 > 0){
                    Log.e(TAG,"滤棉1使用总时长```"+unsignedIntFilter01 + "   ");
                    int tmp_num = unsignedIntFilter01 + 10;
                    bleDevice.setFilter01_use_time(tmp_num );
                    is_change = true;
                }

                //滤棉2使用时长
                int firstByteFilter02Time = (0x000000FF & ((int) Value[12]));
                int secondByteFilter02Time = (0x000000FF & ((int) Value[11]));
                int threeByteFilter02Time = (0x000000FF & ((int) Value[10]));
                int fourByteFilter02Time = (0x000000FF & ((int) Value[9]));

                int unsignedIntFilter02 = ((int) (firstByteFilter02Time | secondByteFilter02Time << 8 | threeByteFilter02Time << 16 | fourByteFilter02Time << 24));


                if ((unsignedIntFilter02 - bleDevice.getFilter02_use_time() > 60 ) || bleDevice.getFilter02_use_time() - unsignedIntFilter02 > 0){
                    Log.e(TAG,"滤棉2使用总时长```"+unsignedIntFilter02 + "   ");
                    bleDevice.setFilter02_use_time(unsignedIntFilter02);
                    is_change = true;
                }
                //本次滤棉1使用时长

                int firstByteFilter01This = (0x000000FF & ((int) Value[14]));
                int secondByteFilter01This = (0x000000FF & ((int) Value[13]));

                int unsignedIntFilter01This = ((int) (firstByteFilter01This | secondByteFilter01This << 8 ));

                if ((unsignedIntFilter01This - bleDevice.getFilter01_use_time_this() > 10 ) || bleDevice.getFilter01_use_time_this() - unsignedIntFilter01This > 0){
                    Log.e(TAG,"滤棉1本次使用时长```"+unsignedIntFilter01This + "   ");
                    bleDevice.setFilter01_use_time_this(unsignedIntFilter01This);
                    is_change = true;
                }
                //本次滤棉2使用时长

                int firstByteFilter02This = (0x000000FF & ((int) Value[16]));
                int secondByteFilter02This = (0x000000FF & ((int) Value[15]));

                int unsignedIntFilter02This = ((int) (firstByteFilter02This | secondByteFilter02This << 8 ));

                if ((unsignedIntFilter02This - bleDevice.getFilter02_use_time_this() > 10 ) || bleDevice.getFilter02_use_time_this() - unsignedIntFilter02This > 0){
                    Log.e(TAG,"滤棉2本次使用时长```"+unsignedIntFilter02This + "   ");
                    bleDevice.setFilter02_use_time_this(unsignedIntFilter02This);
                    is_change = true;
                }

            }
            if (Value[2] == 0x9){
                //滤棉使用总时长
                int firstByteFilterTimeTotal = (0x000000FF & ((int) Value[6]));
                int secondByteFilterTimeTotal = (0x000000FF & ((int) Value[5]));
                int threeByteFilterTimeTotal = (0x000000FF & ((int) Value[4]));
                int fourByteFilterTimeTotal = (0x000000FF & ((int) Value[3]));

                int unsignedIntFilterTotal = ((int) (firstByteFilterTimeTotal | secondByteFilterTimeTotal << 8 | threeByteFilterTimeTotal << 16 | fourByteFilterTimeTotal << 24));

                if ((unsignedIntFilterTotal - bleDevice.getVehide_filter_use_time_total() > 60 ) || bleDevice.getVehide_filter_use_time_total() - unsignedIntFilterTotal > 0){
                    Log.e(TAG,"滤棉使用总时长```"+unsignedIntFilterTotal + "   ");
                    bleDevice.setVehide_filter_use_time_total(unsignedIntFilterTotal);
                    is_change = true;
                }
                //滤棉1净化总时长
                int firstByteFilter01TimeTotal = (0x000000FF & ((int) Value[10]));
                int secondByteFilter01TimeTotal = (0x000000FF & ((int) Value[9]));
                int threeByteFilter01TimeTotal = (0x000000FF & ((int) Value[8]));
                int fourByteFilter01TimeTotal = (0x000000FF & ((int) Value[7]));

                int unsignedIntFilter01Total = ((int) (firstByteFilter01TimeTotal | secondByteFilter01TimeTotal << 8 | threeByteFilter01TimeTotal << 16 | fourByteFilter01TimeTotal << 24));

                if ((unsignedIntFilter01Total - bleDevice.getFilter01_use_time_total() > 60 ) || bleDevice.getFilter01_use_time_total() - unsignedIntFilter01Total > 0){
                    Log.e(TAG,"滤棉1使用总时长```"+unsignedIntFilter01Total + "   ");
                    bleDevice.setFilter01_use_time_total(unsignedIntFilter01Total);
                    is_change = true;
                }
                //滤棉2净化总时长
                int firstByteFilter02TimeTotal = (0x000000FF & ((int) Value[14]));
                int secondByteFilter02TimeTotal = (0x000000FF & ((int) Value[13]));
                int threeByteFilter02TimeTotal = (0x000000FF & ((int) Value[12]));
                int fourByteFilter02TimeTotal = (0x000000FF & ((int) Value[11]));

                int unsignedIntFilter02Total = ((int) (firstByteFilter02TimeTotal | secondByteFilter02TimeTotal << 8 | threeByteFilter02TimeTotal << 16 | fourByteFilter02TimeTotal << 24));

                if ((unsignedIntFilter02Total - bleDevice.getFilter02_use_time_total() > 60 ) || bleDevice.getFilter02_use_time_total() - unsignedIntFilter02Total > 0){
                    Log.e(TAG,"滤棉2使用总时长```"+unsignedIntFilter02Total + "   ");
                    bleDevice.setFilter02_use_time_total(unsignedIntFilter02Total);
                    is_change = true;
                }

                //滤棉使用个数

                int firstByteFilterNum = (0x000000FF & ((int) Value[16]));
                int secondByteFilterNum = (0x000000FF & ((int) Value[15]));

                int unsignedIntFilterNum = ((int) (firstByteFilterNum | secondByteFilterNum << 8 ));

                if (bleDevice.getVehide_filter_num() != unsignedIntFilterNum){
                    Log.e(TAG,"滤棉使用个数``` "+unsignedIntFilterNum);
                    bleDevice.setVehide_filter_num(unsignedIntFilterNum);
                    is_change = true;
                }
                //风扇等级1
                if (bleDevice.getWind1_speed() != Value[17]){
                    Log.e(TAG,"风扇1等级``` "+Value[17]);
                    bleDevice.setWind1_speed(Value[17]);
                    is_change = true;
                }
                //风扇等级2
                if (bleDevice.getWind2_speed() != Value[18]){
                    Log.e(TAG,"风扇2等级``` "+Value[18]);
                    bleDevice.setWind2_speed(Value[18]);
                    is_change = true;
                }
            }

            if (Value[2] == 0xA){

                if (bleDevice.getVehide_time_state() != Value[3]){
                    Log.e(TAG,"车载时间01开关标志```"+Value[3]);
                    bleDevice.setVehide_time_state(Value[3]);
                    bleDevice.setTimeFlag01(1);
                    is_change = true;
                }
                if (bleDevice.getMinute() != Value[4]){
                    Log.e(TAG,"车载时间01分钟```"+Value[4]);
                    bleDevice.setMinute(Value[4]);
                    bleDevice.setTimeFlag01(1);
                    is_change = true;
                }

                if (bleDevice.getHour() != Value[5]){
                    Log.e(TAG,"车载时间01小时```"+Value[5]);
                     bleDevice.setHour(Value[5]);
                    bleDevice.setTimeFlag01(1);
                    is_change = true;
                }

                if (bleDevice.getWeek() != Value[6]){
                    Log.e(TAG,"车载时间01星期```"+Value[6]);
                    bleDevice.setWeek(Value[6]);
                    bleDevice.setTimeFlag01(1);
                    is_change = true;
                }

                if (bleDevice.getVehide_time_state02() != Value[7]){
                    Log.e(TAG,"车载时间02开关标志```"+Value[7]);
                    bleDevice.setVehide_time_state02(Value[7]);
                    bleDevice.setTimeFlag02(2);
                    is_change = true;

                }

                if (bleDevice.getMinute02() != Value[8]){
                    Log.e(TAG,"车载时间02分钟```"+Value[8]);
                    bleDevice.setMinute02(Value[8]);
                    bleDevice.setTimeFlag02(2);
                    is_change = true;
                }

                if (bleDevice.getHour02() != Value[9]){
                    Log.e(TAG,"车载时间02小时```"+Value[9]);
                    bleDevice.setHour02(Value[9]);
                    bleDevice.setTimeFlag02(2);
                    is_change = true;
                }

                if (bleDevice.getWeek02() != Value[10]){
                    Log.e(TAG,"车载时间02星期```"+Value[10]);
                    bleDevice.setWeek02(Value[10]);
                    bleDevice.setTimeFlag02(2);
                    is_change = true;
                }

                if (bleDevice.getVehide_time_state03() != Value[11]){
                    Log.e(TAG,"车载时间03开关标志```"+Value[11]);
                    bleDevice.setVehide_time_state03(Value[11]);
                    bleDevice.setTimeFlag03(3);
                    is_change = true;
                }

                if (bleDevice.getMinute03() != Value[12]){
                    Log.e(TAG,"车载时间03分钟```"+Value[12]);
                    bleDevice.setMinute03(Value[12]);
                    bleDevice.setTimeFlag03(3);
                    is_change = true;
                }

                if (bleDevice.getHour03() != Value[13]){
                    Log.e(TAG,"车载时间03小时```"+Value[13]);
                    bleDevice.setHour03(Value[13]);
                    bleDevice.setTimeFlag03(3);
                    is_change = true;
                }

                if (bleDevice.getWeek03() != Value[14]){
                    Log.e(TAG,"车载时间03星期```"+Value[14]);
                    bleDevice.setWeek03(Value[14]);
                    bleDevice.setTimeFlag03(3);
                    is_change = true;
                }
            }
        }
       return  is_change;
    }
}
