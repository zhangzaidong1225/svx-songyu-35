package com.lianluo.qingfeng.BlueTooth;

/**
 * 设备属性
 * Created by zhangzaidong on 2016/11/2.
 */

public class DeviceMsg {
    public String address;
    public String name;
    public int action;
    public int speed;
    public int model;
    public int space;
    public int time;
    public int hour;
    public int minute;
    public int week;
    public int type;
    public int mode;
    public int factor;

    public  static  class Builder {
        //必须初始化的参数
        private String address;
        public int action;
        public String name;
        //option param
        public int speed;
        public int model;
        public int space;
        public int time;
        public int hour;
        public int minute;
        public int week;
        public int type;
        public int mode;
        public int factor;

        public Builder (String address,String name,int action){
            this.address = address;
            this.action = action;
            this.name   = name;
        }

        public Builder addSpeed (int speed){
            this.speed = speed;
            return  this;
        }
        public Builder addFactor (int factor) {
            this.factor = factor;
            return  this;
        }
        public Builder addModel (int model){
            this.model = model;
            return this;
        }
        public Builder addTime (int minute,int hour,int week,int type,int mode){
            this.hour = hour;
            this.minute = minute;
            this.week = week;
            this.type = type;
            this.mode = mode;
            return this;
        }
        public Builder addSpace (int space){
            this.space = space;
            return this;
        }
        public Builder addSystemTime (int time){
            this.time = time;
            return this;
        }

        public DeviceMsg create(){
            return new DeviceMsg(this);
        }
    }

    public DeviceMsg (Builder builder){
        this.address = builder.address;
        this.action  = builder.action;
        this.name    = builder.name;
        this.speed   = builder.speed;
        this.model   = builder.model;
        this.space   = builder.space;
        this.time    = builder.time;
        this.hour    = builder.hour;
        this.minute  = builder.minute;
        this.week    = builder.week;
        this.type    = builder.type;
        this.mode   = builder.mode;
        this.factor  = builder.factor;

    }
}
