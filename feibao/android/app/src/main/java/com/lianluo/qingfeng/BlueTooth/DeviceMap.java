package com.lianluo.qingfeng.BlueTooth;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;

public class DeviceMap implements Parcelable{

    public Map<String,BleDevice> map = new HashMap<String,BleDevice>();

    public DeviceMap(){}

    protected DeviceMap(Parcel in) {

        map = in.readHashMap(HashMap.class.getClassLoader());

    }

    public static final Creator<DeviceMap> CREATOR = new Creator<DeviceMap>() {
        @Override
        public DeviceMap createFromParcel(Parcel in) {
            return new DeviceMap(in);
        }

        @Override
        public DeviceMap[] newArray(int size) {
            return new DeviceMap[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeMap(map);
    }
}
