package com.lianluo.qingfeng.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.lianluo.qingfeng.BlueTooth.BleApplication;

/**
 * SharedPreferences signal instance.
 *
 * Created by louis on 16/4/25.
 */
public class SZTYSharedPreferences {

    //storage variables

    //marking ad is load or not.
    public static final String AD_URL = "ad_url";
    public static final String IS_LOAD = "is_load";
    public static final String AD_ACTION = "ad_action";
    public static final String HOT_UPLOAD_VER = "hot_upload_ver";


    private static final String SP_NAME = "lianluoqingfeng";

    public static final String FIRST_START = "first_start";

    private static SZTYSharedPreferences instance = new SZTYSharedPreferences();

    public SZTYSharedPreferences() {
    }

    private static synchronized void syncInit() {
        if (instance == null) {
            instance = new SZTYSharedPreferences();
        }
    }

    public static SZTYSharedPreferences getInstance() {
        if (instance == null) {
            syncInit();
        }
        return instance;
    }

    private SharedPreferences getSp() {
        return BleApplication.getInstance().getSharedPreferences(SP_NAME,
                Context.MODE_PRIVATE);
    }

    public int getInt(String key, int def) {
        try {
            SharedPreferences sp = getSp();
            if (sp != null)
                def = sp.getInt(key, def);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return def;
    }

    public void putInt(String key, int val) {
        try {
            SharedPreferences sp = getSp();
            if (sp != null) {
                SharedPreferences.Editor e = sp.edit();
                e.putInt(key, val);
                e.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public long getLong(String key, long def) {
        try {
            SharedPreferences sp = getSp();
            if (sp != null)
                def = sp.getLong(key, def);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return def;
    }

    public void putLong(String key, long val) {
        try {
            SharedPreferences sp = getSp();
            if (sp != null) {
                SharedPreferences.Editor e = sp.edit();
                e.putLong(key, val);
                e.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getString(String key, String def) {
        try {
            SharedPreferences sp = getSp();
            if (sp != null)
                def = sp.getString(key, def);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return def;
    }

    public void putString(String key, String val) {
        try {
            SharedPreferences sp = getSp();
            if (sp != null) {
                SharedPreferences.Editor e = sp.edit();
                e.putString(key, val);
                e.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean getBoolean(String key, boolean def) {
        try {
            SharedPreferences sp = getSp();
            if (sp != null)
                def = sp.getBoolean(key, def);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return def;
    }

    public void putBoolean(String key, boolean val) {
        try {
            SharedPreferences sp = getSp();
            if (sp != null) {
                SharedPreferences.Editor e = sp.edit();
                e.putBoolean(key, val);
                e.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void remove(String key) {
        try {
            SharedPreferences sp = getSp();
            if (sp != null) {
                SharedPreferences.Editor e = sp.edit();
                e.remove(key);
                e.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void putString(String keyToken, String Toaekval, String keyMsg,
                          String Msgkval) {
        try {
            SharedPreferences sp = getSp();
            if (sp != null) {
                SharedPreferences.Editor e = sp.edit();
                e.putString(keyToken, Toaekval);
                e.putString(keyMsg, Msgkval);
                e.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
