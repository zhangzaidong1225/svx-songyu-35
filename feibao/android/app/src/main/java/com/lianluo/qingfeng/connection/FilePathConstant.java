package com.lianluo.qingfeng.connection;

import android.app.Activity;
import android.content.Context;

import java.io.File;


public class FilePathConstant {

    private static final String TAG = "FilePathConstant";

    /*    获取当前程序路径
        getApplicationContext().getFilesDir().getAbsolutePath();
    获取该程序的安装包路径
        String path=getApplicationContext().getPackageResourcePath();
    获取程序默认数据库路径
        getApplicationContext().getDatabasePath(s).getAbsolutePath();*/
    //安装包文件路径
    private  String mFilePath;
    //临时文件下载目录
    public  String tmpFilePath;
    //临时文件下载路径（文件名称）
    public  String downloadPath;
    //复制目录
    public  String downloadPathCopy;
    //解压缩临时路径
    public  String unzipPath;
    //解压缩绝对路径
    public  String unzipPathSave;

    private static FilePathConstant sInstance = null;

    public FilePathConstant (){
    }

    public  void init(Activity context){
        mFilePath =  context.getApplicationContext().getFilesDir().getAbsolutePath();
        tmpFilePath = mFilePath + "/hot_upload_tmp/";
        downloadPathCopy = mFilePath + "/hot_upload/";
        unzipPath = mFilePath + "/hotupload/";
    }


    public static FilePathConstant  getInstance(){
        if (sInstance == null){
             sInstance = new FilePathConstant();
        }
        return sInstance;
    }

}
