package com.lianluo.qingfeng;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.JavaScriptModule;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;

import com.facebook.react.uimanager.ViewManager;
import com.lianluo.qingfeng.reactmodule.RCTSZTYSharedPreferencesModule;
import com.lianluo.qingfeng.reactmodule.RCTUMengModule;
import com.lianluo.qingfeng.reactmodule.ShareModule;
import com.lianluo.qingfeng.reactmodule.UtilsModule;

/**
 * Package defining basic modules and view managers.
 */
public class MyReactPackage implements ReactPackage {

  @Override
  public List<NativeModule> createNativeModules(ReactApplicationContext reactContext) {
    List<NativeModule> modules = new ArrayList<>();

    modules.add(new ShareModule(reactContext));
    modules.add(new UtilsModule(reactContext));
    modules.add(new RCTSZTYSharedPreferencesModule(reactContext));
    modules.add(new RCTUMengModule(reactContext));
    return modules;
//    return Arrays.<NativeModule>asList(
//      new ShareModule(reactContext));
  }

  @Override
  public List<Class<? extends JavaScriptModule>> createJSModules() {
    return Collections.emptyList();
  }

  @Override
  public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
    return Arrays.<ViewManager>asList();
  }
}
