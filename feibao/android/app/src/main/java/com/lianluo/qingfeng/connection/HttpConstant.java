package com.lianluo.qingfeng.connection;

import com.lianluo.qingfeng.BlueTooth.BleApplication;
import com.lianluo.qingfeng.SplashActivity;

import java.util.Locale;

/**
 * Relative final common variables about http.
 *
 * Created by louis on 16/4/22.
 */
public class HttpConstant {
    public static SplashActivity splashActivity = new SplashActivity();
    public static String HTTP_OPR = "http_opr";
    public static String HTTP_RESULT = "http_result";
    public static String versionName = BleApplication.versionNumber;
    public static int phone_modal = 0;
    public static int platform = 0;
    public static String SERVER_BASE_NORMAL = "https://mops.lianluo.com/vida/xinfeng";
    public static String SERVER_BASE_TEST = "https://mops-xinfeng-dev.lianluo.com";
    //public static String SERVER_BASE_TEST = "https://139.198.9.171/";
    public static String AD_URI = SERVER_BASE_NORMAL + "/iface/info/ad"+"?version="+versionName+"&platform="+platform+"&phone_modal="+phone_modal+"&language="+Locale.getDefault().getLanguage();
    public static String AD_URI_TEST = SERVER_BASE_TEST + "/iface/info/ad"+"?version="+versionName+"&platform="+platform+"&phone_modal="+phone_modal+"&language="+Locale.getDefault().getLanguage();
    //https://mops-xinfeng-dev.lianluo.com/iface/system/hot_update?version=1.0.0&platform=0
    public static String HOT_UPLOAD_URI_TEST = SERVER_BASE_TEST + "/iface/system/hot_update"+"?version="+versionName+"&platform="+"0";
    public static String HOT_UPLOAD_URI = SERVER_BASE_NORMAL + "/iface/system/hot_update"+"?version="+versionName+"&platform="+"0";


}
