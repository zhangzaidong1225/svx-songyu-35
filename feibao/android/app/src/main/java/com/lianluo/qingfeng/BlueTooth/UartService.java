
/*
 * Copyright (c) 2015, Nordic Semiconductor
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.lianluo.qingfeng.BlueTooth;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.nfc.Tag;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.lianluo.qingfeng.utils.SZTYSharedPreferences;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Service for managing connection and data communication with a GATT server hosted on a
 * given Bluetooth LE device.
 */
public class UartService extends Service {
    private final static String TAG = UartService.class.getSimpleName();

    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;

    public List<String> mAddressList;
    public static Map<String, BluetoothGatt> mBluetoothGattMap;
    public static Map<String, Object> mConnectionStateMap;
    public static Map<String, BluetoothGattService> mBluetoothGattService;
    public static Map<String, BluetoothGattCharacteristic> RXBluetoothGattCharacteristic;
    public static Map<String, BluetoothGattCharacteristic> TXBluetoothGattCharacteristic;


    protected static final int STATE_DISCONNECTED = 0;
    protected static final int STATE_CONNECTING = 1;
    protected static final int STATE_CONNECTED = 2;

    public final static String ACTION_GATT_CONNECTING =
            "com.nordicsemi.nrfUART.ACTION_GATT_CONNECTING";
    public final static String ACTION_GATT_CONNECTED =
            "com.nordicsemi.nrfUART.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED =
            "com.nordicsemi.nrfUART.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED =
            "com.nordicsemi.nrfUART.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE =
            "com.nordicsemi.nrfUART.ACTION_DATA_AVAILABLE";
    public final static String EXTRA_DATA =
            "com.nordicsemi.nrfUART.EXTRA_DATA";
    public final static String DEVICE_DOES_NOT_SUPPORT_UART =
            "com.nordicsemi.nrfUART.DEVICE_DOES_NOT_SUPPORT_UART";
    public final static String DEVICE_ADDRESS =
            "com.nordicsemi.nrfUART.DEVICE_ADDRESS";
    public final static String ADD_DEVICE =
            "com.nordicsemi.nrfUART.ADD_DEVICE";
    public final static String CLEAR_DATA_1 =
            "com.nordicsemi.nrfUART.CLEAR_DATA_1";
    public final static String STOP_SCAN =
            "com.nordicsemi.nrfUART.STOP_SCAN";
    public final static String START_SCAN =
            "com.nordicsemi.nrfUART.START_SCAN";

    public static final UUID CCCD = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    public static final UUID RX_SERVICE_UUID = UUID.fromString("6e400001-b5a3-f393-e0a9-e50e24dcca9e");
    public static final UUID RX_CHAR_UUID = UUID.fromString("6e400002-b5a3-f393-e0a9-e50e24dcca9e");
    public static final UUID TX_CHAR_UUID = UUID.fromString("6e400003-b5a3-f393-e0a9-e50e24dcca9e");

    private Handler mHandler;
    private int count = 0;


    private BluetoothGattCallback createGattCallback() {
        return new BluetoothGattCallback() {
            @Override
            public void onConnectionStateChange(final BluetoothGatt gatt, int status, int newState) {
                String intentAction;
                Log.d(TAG, "Connected to GATT server.``````" + status + "------" + newState);
                if (status == BluetoothGatt.GATT_SUCCESS) {
                    if (newState == BluetoothProfile.STATE_CONNECTED) {
                        mConnectionStateMap.put(gatt.getDevice().getAddress(), STATE_CONNECTED);

                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                // Some proximity tags (e.g. nRF PROXIMITY) initialize bonding automatically when connected.
                                if (gatt.getDevice().getBondState() != BluetoothDevice.BOND_BONDING) {
                                    gatt.discoverServices();
                                }
                            }
                        }, 600);

                    } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                        intentAction = ACTION_GATT_DISCONNECTED;
                        mConnectionStateMap.put(gatt.getDevice().getAddress(), STATE_DISCONNECTED);
                        Log.d(TAG, "Disconnected from GATT server.");
                        broadcastUpdate(intentAction, gatt.getDevice().getAddress());
                    }
                } else {
                    if(status == 133) {
                        if (count == 0) {
                            gatt.disconnect();
                            connect(gatt.getDevice().getAddress());
                            count++;
                        } else
                        {
                            // 遇到一个问题，在三星s6&5.1.1上，当设备距离过远时，函数执行到此，马上会执行连接操作，
                            // 导致页面没有及时的显示断开连接。
//                            System.gc();
                            count = 0;
                            Log.d(TAG, "onConnectionStateChange received: " + status);
                            if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                                mConnectionStateMap.put(gatt.getDevice().getAddress(), STATE_DISCONNECTED);
                                Log.d(TAG, "Disconnected from GATT server```.---非正常断开");
                                gatt.disconnect();
                                broadcastUpdate(ACTION_GATT_DISCONNECTED, gatt.getDevice().getAddress());
                            }
                        }
                    } else {
                        // 遇到一个问题，在三星s6&5.1.1上，当设备距离过远时，函数执行到此，马上会执行连接操作，
                        // 导致页面没有及时的显示断开连接。
                        Log.d(TAG, "onConnectionStateChange received: " + status);
                        gatt.disconnect();
                        if (newState == BluetoothProfile.STATE_DISCONNECTED){
                            Log.d(TAG, "Disconnected from GATT server```.---非正常断开");
                            mConnectionStateMap.put(gatt.getDevice().getAddress(), STATE_DISCONNECTED);
                            broadcastUpdate(ACTION_GATT_DISCONNECTED, gatt.getDevice().getAddress());
                        }
                    }
                }
            }

            @Override
            public void onServicesDiscovered(final BluetoothGatt gatt, int status) {
                if (status == BluetoothGatt.GATT_SUCCESS) {
                    if (gatt.getDevice() == null){
                        gatt.disconnect();
                        return;
                    }

                    if (isRequiredServiceSupported(gatt)) {
                        enableTXNotification(gatt.getDevice().getAddress());
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (gatt.getDevice() != null){
                                    if (!XinfengService.mData.isEmpty() && XinfengService.mData.containsKey(gatt.getDevice().getAddress())){
                                        if (XinfengService.mData.get(gatt.getDevice().getAddress()).getIsconnectd() != 0){
                                            if (gatt.getDevice().getBondState() != BluetoothDevice.BOND_BONDING) {
                                                Log.e(TAG,"-b-" + gatt.getDevice().getBondState());
                                                getName(gatt.getDevice().getAddress());
                                            }
                                        }
                                    }
                                }
                            }
                        }, 3000);
                    }else {
                        //disconnect(gatt.getDevice().getAddress());
                        broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_UART, gatt.getDevice().getAddress());
                        refreshDeviceCache(gatt);
                    }
                } else {
                    Log.e(TAG, "onServicesDiscovered received: " + status);
                }
            }

            @Override
            public void onCharacteristicRead(BluetoothGatt gatt,
                                             BluetoothGattCharacteristic characteristic,
                                             int status) {
                if (status == BluetoothGatt.GATT_SUCCESS) {
                    broadcastUpdate(ACTION_DATA_AVAILABLE, gatt.getDevice().getAddress(), characteristic);
                }
            }

            @Override
            public void onCharacteristicChanged(BluetoothGatt gatt,
                                                BluetoothGattCharacteristic characteristic) {
                byte[] value  = characteristic.getValue();
                String data10 = "";
                String data16 = "";

                for (int i = 0;i < value.length;i++){
                    int aa = value[i] & 0xFF;
                    String hex = Integer.toHexString(aa);
                    data16 += hex+"--";
                    //data10 += aa+"--";
                }
                Log.i("dataSource","来数据啦··16··"+data16);
               //Log.e(TAG,"来数据啦··10··"+data10);

                broadcastUpdate(ACTION_DATA_AVAILABLE, gatt.getDevice().getAddress(), characteristic);

            }

            @Override
            public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                super.onCharacteristicWrite(gatt, characteristic, status);
                if(status == BluetoothGatt.GATT_SUCCESS){
                Log.d(TAG, "onCharacteristicWrite`````success " + gatt.getDevice().getAddress() + status);

                }else {
                    Log.e(TAG,"onCharacteristicWrite error "+status);
                }
            }

            @Override
            public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
                super.onDescriptorRead(gatt, descriptor, status);
            }

            @Override
            public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
                super.onDescriptorWrite(gatt, descriptor, status);
            }
        };
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mHandler = new Handler();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    protected void broadcastUpdate(final String action, final String address) {}

    protected void broadcastUpdate(final String action) {}

    protected void broadcastUpdate(final String action,
                                   final String address,
                                   final BluetoothGattCharacteristic characteristic) {
    }

    public class LocalBinder extends Binder {
        UartService getService() {
            return UartService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        // After using a given device, you should make sure that BluetoothGatt.close() is called
        // such that resources are cleaned up properly.  In this particular example, close() is
        // invoked when the UI is disconnected from the Service.
        close();
        return super.onUnbind(intent);
    }

    private final IBinder mBinder = new LocalBinder();

    /**
     * Initializes a reference to the local Bluetooth adapter.
     *
     * @return Return true if the initialization is successful.
     */
    public boolean initialize() {
        // For API level 18 and above, get a reference to BluetoothAdapter through
        // BluetoothManager.

        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
            return false;
        }

        mAddressList = new ArrayList<String>();
        mBluetoothGattMap = new HashMap<String, BluetoothGatt>();
        mConnectionStateMap = new HashMap<String, Object>();

        mBluetoothGattService = new HashMap<String, BluetoothGattService>();
        RXBluetoothGattCharacteristic = new HashMap<String, BluetoothGattCharacteristic>();
        TXBluetoothGattCharacteristic = new HashMap<String, BluetoothGattCharacteristic>();

        return true;
    }

    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     *
     * @param address The device address of the destination device.
     * @return Return true if the connection is initiated successfully. The connection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public  boolean connect(final String address) {
        if (mBluetoothAdapter == null || address == null) {
            Log.e(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }

        final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        if (device == null) {
            Log.e(TAG, "Device not found.  Unable to connect.");
            return false;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
//            Log.e(TAG, "版本为6.0");
            BluetoothGatt gatt = device.connectGatt(UartService.this,false,createGattCallback(),BluetoothDevice.TRANSPORT_LE);
            mBluetoothGattMap.put(address, gatt);
        }else {
            BluetoothGatt gatt = device.connectGatt(UartService.this, false, createGattCallback());
            mBluetoothGattMap.put(address, gatt);
        }

//        Log.e(TAG, "Trying to create a new connection.");
        if (mAddressList.indexOf(address) == -1) {
            mAddressList.add(address);
        }
        mConnectionStateMap.put(address, STATE_CONNECTING);

        return true;
    }


    /**
     * Disconnects an existing connection or cancel a pending connection. The disconnection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public Boolean disconnect(String address) {

        if (mBluetoothAdapter == null) {
            Log.e(TAG, "BluetoothAdapter not initialized---diconnect");
            return false;
        }
        if (mBluetoothGattMap.get(address) == null){
            Log.e(TAG,"disconnect----不包含");
            return false;
        }
        mBluetoothGattMap.get(address).disconnect();

        return  true;
    }

    /**
     * After using a given BLE device, the app must call this method to ensure resources are
     * released properly.
     */
    public void close() {

        Iterator iter = mBluetoothGattMap.entrySet().iterator();

        while (iter.hasNext()) {
//            Log.e(TAG,"针对S6-------");
            Map.Entry entry = (Map.Entry) iter.next();
            Object key = entry.getKey();
            Object val = entry.getValue();
            BluetoothGatt gatt = (BluetoothGatt) val;
            //mAddressList.remove(gatt.getDevice().getAddress());
            //针对s6新加断开连接的公共方法
            gatt.disconnect();

            gatt.close();
        }

        mAddressList.clear();
        mBluetoothGattMap.clear();
        mConnectionStateMap.clear();

        mBluetoothGattService.clear();
        RXBluetoothGattCharacteristic.clear();
        TXBluetoothGattCharacteristic.clear();

        mBluetoothManager = null;

//        Log.e(TAG,"close--mBluetoothGattMap``-"+mBluetoothGattMap.size());

    }

    public void close(String address) {

        if (mBluetoothGattMap.get(address) == null) {
            Log.e(TAG, "mBluetoothGattMap not contain---close");
            return;
        }
        mAddressList.remove(address);
//        mBluetoothGattMap.get(address).disconnect();
        mBluetoothGattMap.get(address).close();
        mBluetoothGattMap.remove(address);

        Log.e(TAG,"close-addr-mBluetoothGattMap-"+mBluetoothGattMap.size());

    }


    /**
     * 清楚蓝牙缓存
     * @param gatt
     * @return
     */
    private boolean refreshDeviceCache (BluetoothGatt gatt){

        try {
            BluetoothGatt localBluetoothGatt = gatt;
            Method localMethod = localBluetoothGatt.getClass().getMethod("refresh",new Class[0]);
            localMethod.setAccessible(true);

            if (localMethod != null){
                boolean bool = ((Boolean) localMethod.invoke(localBluetoothGatt,new Object[0])).booleanValue();
//                Log.e(TAG, "refreshDeviceCache----"+bool);
                return bool;
            }

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return false;

    }

    /**
     * 获取设备名字命令
     * @param address
     */
    private void getName (String address){

        if (address == null ||address.equals("")){
            return;
        }

        byte head = (byte) 0xAA;
        byte command = 0x14;
        byte length = 0;
        byte name = 0;
        byte check = (byte) (head + command + length + name);
        byte[] data = new byte[]{head, command, length,name, check};

        writeRXCharacteristic(address, data);

    }



    /**
     * Request a read on a given {@code BluetoothGattCharacteristic}. The read result is reported
     * asynchronously through the {@code BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
     * callback.
     *
     * @param characteristic The characteristic to read from.
     */
    public void readCharacteristic(String address, BluetoothGattCharacteristic characteristic) {

        if (mBluetoothAdapter == null || mBluetoothGattMap.get(address) == null) {
            Log.e(TAG, "BluetoothAdapter not initialized-----readCharacteristic");
            return;
        }
        mBluetoothGattMap.get(address).readCharacteristic(characteristic);
    }

    /**
     * Enables or disables notification on a give characteristic.
     */
    public boolean isRequiredServiceSupported(final BluetoothGatt gatt) {

        final BluetoothGattService service = gatt.getService(RX_SERVICE_UUID);
        BluetoothGattCharacteristic mRXCharacteristic, mTXCharacteristic;

        if (service != null) {
            mBluetoothGattService.put(gatt.getDevice().getAddress(), service);

            mRXCharacteristic = service.getCharacteristic(RX_CHAR_UUID);
            RXBluetoothGattCharacteristic.put(gatt.getDevice().getAddress(), mRXCharacteristic);
            mTXCharacteristic = service.getCharacteristic(TX_CHAR_UUID);
            TXBluetoothGattCharacteristic.put(gatt.getDevice().getAddress(), mTXCharacteristic);
        }else {
//            broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_UART, gatt.getDevice().getAddress());
            return false;
        }

        boolean writeRequest = false;
        boolean writeCommand = false;

        if (mRXCharacteristic != null) {
            final int rxProperties = mRXCharacteristic.getProperties();

            writeRequest = (rxProperties & BluetoothGattCharacteristic.PROPERTY_WRITE) > 0;
            writeCommand = (rxProperties & BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE) > 0;

            // Set the WRITE REQUEST type when the characteristic supports it. This will allow to send long write (also if the characteristic support it).
            // In case there is no WRITE REQUEST property, this manager will divide texts longer then 20 bytes into up to 20 bytes chunks.
            if (writeRequest)
                mRXCharacteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
        }
        boolean isService =  mRXCharacteristic != null && mTXCharacteristic != null && (writeRequest || writeCommand);

        return isService;
    }

    /**
     * Enable Notification on TX characteristic
     *
     * @return
     */
    public void enableTXNotification(String address) {

        BluetoothGattCharacteristic TxChar = TXBluetoothGattCharacteristic.get(address);

        mBluetoothGattMap.get(address).setCharacteristicNotification(TxChar, true);

        BluetoothGattDescriptor descriptor = TxChar.getDescriptor(CCCD);
        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        mBluetoothGattMap.get(address).writeDescriptor(descriptor);

        //调整发送广播的位置
        broadcastUpdate(ACTION_GATT_CONNECTED, address);

    }


    public boolean writeRXCharacteristic(String address, byte[] value) {

        if ((address == null || address.equals("") ) || (value == null)){
            return false;
        }

        BluetoothGattCharacteristic RxChar = null;
        if (address != null){
             RxChar = RXBluetoothGattCharacteristic.get(address);
            RxChar.setValue(value);
        }

        boolean status = false;

        if (address != null && RxChar != null){

             status = mBluetoothGattMap.get(address).writeCharacteristic(RxChar);
        }

        Log.d(TAG, "writeRXCharacteristic`````````====" + status);
        return status;

    }


    /**
     * Retrieves a list of supported GATT services on the connected device. This should be
     * invoked only after {@code BluetoothGatt#discoverServices()} completes successfully.
     *
     * @return A {@code List} of supported services.
     */
    public List<BluetoothGattService> getSupportedGattServices(String address) {

        if (mBluetoothGattMap.get(address) == null) return null;

        return mBluetoothGattMap.get(address).getServices();
    }

}
