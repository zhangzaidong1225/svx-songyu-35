package com.lianluo.qingfeng.preLoader;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import com.facebook.common.logging.FLog;
import com.facebook.react.ReactActivity;
import com.facebook.react.ReactActivityDelegate;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactRootView;
import com.facebook.react.common.ReactConstants;
import com.facebook.react.devsupport.DoubleTapReloadRecognizer;
import com.facebook.react.modules.core.DefaultHardwareBackBtnHandler;
import com.facebook.react.modules.core.PermissionAwareActivity;
import com.lianluo.qingfeng.BuildConfig;
import com.lianluo.qingfeng.preLoader.ReactInfo;
import com.lianluo.qingfeng.preLoader.ReactPreLoader;

/**
 * Created by songyu on 2017/1/18.
 */

public abstract class MrReactActivity extends ReactActivity
        implements DefaultHardwareBackBtnHandler, PermissionAwareActivity {
    public ReactRootView mReactRootView;
    public DoubleTapReloadRecognizer mDoubleTapReloadRecognizer;
    public String TAG = "==MrReactActivity";
    public String REDBOX_PERMISSION_MESSAGE = "REDBOX_PERMISSION_MESSAGE";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate   view");
        if (getUseDeveloperSupport() && Build.VERSION.SDK_INT >= 23) {
            // Get permission to show redbox in dev builds.
            if (!Settings.canDrawOverlays(this)) {
                Intent serviceIntent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
                startActivity(serviceIntent);
                FLog.w(ReactConstants.TAG, REDBOX_PERMISSION_MESSAGE);
                //Toast.makeText(this, REDBOX_PERMISSION_MESSAGE, Toast.LENGTH_LONG).show();
            }
        }

        mReactRootView = ReactPreLoader.getRootView(getReactInfo());

        if (mReactRootView != null) {
            Log.i(TAG, "use pre-load view");
        } else {
            Log.i(TAG, "createRootView");
            mReactRootView = createRootView(this);
            if (mReactRootView != null) {
                mReactRootView.startReactApplication(
                        getReactNativeHost().getReactInstanceManager(),
                        getReactInfo().getMainComponentName(),
                        getReactInfo().getLaunchOptions());
            }
        }

        setContentView(mReactRootView);

        mDoubleTapReloadRecognizer = new DoubleTapReloadRecognizer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy   view");
        if (mReactRootView != null) {
            mReactRootView.unmountReactApplication();
            mReactRootView = null;
            ReactPreLoader.onDestroy(getReactInfo());
        }
        getReactNativeHost().clear();
    }
    protected ReactRootView createRootView(Context context/*Application application*/) {
        return new ReactRootView(context);
    }
    protected boolean getUseDeveloperSupport() {
        return BuildConfig.DEBUG;
    }
    public abstract ReactInfo getReactInfo();
}