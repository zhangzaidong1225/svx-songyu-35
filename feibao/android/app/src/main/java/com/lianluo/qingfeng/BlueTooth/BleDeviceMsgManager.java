package com.lianluo.qingfeng.BlueTooth;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.facebook.react.bridge.ReactMethod;
import com.lianluo.qingfeng.MainActivity;
import com.lianluo.qingfeng.utils.BytesToIntUtil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by louis on 16/9/2.
 */
public class BleDeviceMsgManager {
    private final static String TAG = "BleDeviceMsgManager";
    public static BleDeviceMsgManager sInstance;

    public ConcurrentHashMap<String, DeviceMsgManager> map = new ConcurrentHashMap<String, DeviceMsgManager>();

    public boolean stopFlag = false;
    private managerThread thread;

    public Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg == null) {
                return;
            }
            DeviceMsg deviceMsg = (DeviceMsg) msg.obj;
            Log.e(TAG, "handler...."+msg.what);

            switch (msg.what) {
                case DeviceMsgAction.WindSpeed:

                    windSpeed(deviceMsg.address, deviceMsg.speed);
                    break;

                case DeviceMsgAction.DevCheck:
                    devCheck(deviceMsg.address);
                    break;

                case DeviceMsgAction.DevRename:
                    devRename(deviceMsg.address, deviceMsg.name);
                    break;

                case DeviceMsgAction.Disconnect:
                    disconnect(deviceMsg.address);
                    break;

                case DeviceMsgAction.Esmart:
                    eSmart(deviceMsg.address,deviceMsg.model);
                    break;

                case DeviceMsgAction.SetInterval:
                    setInterval(deviceMsg.address,deviceMsg.space);
                    break;

                case DeviceMsgAction.SetSystemClock:
                    setSystemClock(deviceMsg.address,deviceMsg.time);
                    break;
                case DeviceMsgAction.SetTime:
                    setTime(deviceMsg.address,deviceMsg.minute,deviceMsg.hour,deviceMsg.week,deviceMsg.type,deviceMsg.mode);
                    break;
                case DeviceMsgAction.FilterFactor:
                    filterFactor(deviceMsg.address,deviceMsg.factor);
                    break;
                default:
                    break;
            }
        }
    };
    /**
     * 滤芯因子
     */
    public void filterFactor(String address, int factor) {
        Log.e(TAG, "filterFactor=====" + factor + "地址" + address);
        if (!isConnected(address)) {
            Log.e(TAG, "滤芯因子---断开连接.....");
            return;
        }
        byte head = (byte) 0xAA;
        byte command = 0x5F;
        byte fa = (byte) factor;
        Log.e(TAG, "bytefactor-----" + fa + "");
        byte check = (byte) (head + command + fa );
        byte[] data = new byte[]{head, command, fa, check};

        MainActivity.mService.writeRXCharacteristic(address, data);

    }

    public void windSpeed(String address, int speed) {
        Log.e(TAG, "windspeed=====" + speed + "地址" + address);

        if (!isConnected(address)) {
            Log.e(TAG, "风速---断开连接.....");
            return;
        }

        byte head = (byte) 0xAA;
        byte command = 0x10;
        byte sp = (byte) speed;
        Log.e(TAG, "bytespeed-----" + sp + "");
        byte time = 0;
        byte check = (byte) (head + command + sp + time);
        byte[] data = new byte[]{head, command, sp, time, check};

        MainActivity.mService.writeRXCharacteristic(address, data);
    }

    public void devCheck(String address) {

        Log.e(TAG, "devCheck```````````");

        byte head = (byte) 0xAA;
        byte command = 0x13;
        byte check = (byte) (head + command);
        byte[] data = new byte[]{head, command, check};


        if (!isConnected(address)) {
            Log.e(TAG, "断开连接.......devcheck");
            return;
        }

        MainActivity.mService.writeRXCharacteristic(address, data);

    }

    public void devRename(final String address, String rename) {
        Log.e(TAG, "devRename```````````");
        //最大为4个汉字，16字节
        byte head = (byte) 0xAA;
        byte command = 0x12;
        byte[] buf1 = new byte[16];
        byte[] buf_name = new byte[30];
        int len = rename.length();
        int byteLen = rename.getBytes().length;
        Log.e(TAG,"devRename``"+len);

        int nameLen = (len > 4) ? 4:len;
        int byteNameLen = byteLen > 12 ? 12:byteLen;

        byte[] tmpName = new byte[byteLen];
        byte[] names = rename.getBytes();

        try {
            int buf_length = 0;
            for (int i = 0;i < byteNameLen;i++){
                tmpName[i] = names[i];
            }
            String subname = new String(tmpName,"UTF-8");

            for (int i = 0; i < subname.length(); i++) {
                String tmp = subname.substring(i, i + 1);
                Log.e(TAG, "devRename--" + tmp +"```"+i);

                byte[] tmp_buf = tmp.getBytes("UTF8");
                if (buf_length + tmp_buf.length <= 30) {
                    for (int j = 0; j < tmp_buf.length; j++) {
                        buf_name[buf_length] = tmp_buf[j];
                        buf_length++;
                    }
                }
            }
            Log.e(TAG, "devRename---" + buf_length);

            for (int i = 0; i < 30; i++) {
                Log.e(TAG, "devRename--" + String.format("%x", buf_name[i]));
                if (i < 12) {
                    buf1[i + 3] = buf_name[i];
                }
            }
        } catch (Exception e) {
        }
        buf1[0] = head;
        buf1[1] = command;
        buf1[2] = (byte) byteNameLen;
        Log.e(TAG, "devRename--"+buf1[2]);

        //校验位
        for (int i = 0; i < 15; i++) {
            buf1[15] += buf1[i];
        }
        if (!isConnected(address)) {
            return;
        }

        MainActivity.mService.writeRXCharacteristic(address, buf1);
        Log.e(TAG, "devRename-----ok");

    }


    public void disconnect(String address) {

        Log.e(TAG, "disconnect```````````" + address);

        if (!isConnected(address)) {
            Log.e(TAG, "断开连接.......disconnect");
            return;
        }

        byte head = (byte) 0xAA;
        byte command = 0x07;
        byte check = (byte) (head + command);
        byte[] data = new byte[]{head, command, check};

        MainActivity.mService.writeRXCharacteristic(address, data);
    }

    public Boolean isConnected(String address) {

        if (!XinfengService.mData.isEmpty() && (XinfengService.mData.containsKey(address)) && (address != null)) {
            Log.e(TAG, "isConnected..........");

            //Object bleObj = XinfengService.mData.get(address);
            BleDevice device = XinfengService.mData.get(address);
            if (device.getIsconnectd() == 0) {
                return false;
            } else if (device.getIsconnectd() == 1) {
                return true;
            }
        }


        return false;
    }


    public void eSmart(String address,int model){

        Log.e(TAG, "eSmart``````````");

        if (!isConnected(address)) {
            Log.e(TAG, "断开连接.......disconnect");
            return;
        }

        byte head = (byte) 0xAA;
        byte command = 0x16;
        byte st = (byte) model;
        byte check = (byte) (head + command + st);
        byte[] data = new byte[]{head, command, st, check};

        MainActivity.mService.writeRXCharacteristic(address, data);
    }

    public void setInterval(String address,int space){

        if (!isConnected(address)) {
            Log.e(TAG, "断开连接.......disconnect");
            return;
        }
        byte head = (byte) 0xAA;
        byte command = 0x08;
        byte st = (byte) space;

        Log.e(TAG, "setInterval``````````"+space);
        byte time = 0;
        byte check = (byte) (head + command + st + time);
        byte[] data = new byte[]{head, command,time,st,check};

        MainActivity.mService.writeRXCharacteristic(address, data);
    }

    /**
     * 时钟
     * @param address
     * @param time
     */
    public void setSystemClock(String address,int time){
        byte[] buf1 = new byte[7];

        //long time = Math.round(System.currentTimeMillis() / 1000);
        Log.e(TAG, "---" + time);
        String ad = Long.toHexString(time);
        Log.e(TAG, "---" + ad);

        byte[] st = BytesToIntUtil.HexString2Bytes(ad);

        //String str = BytesToIntUtil.byte2HexStr(st);

        for (int i = 0; i < st.length; i++) {
            buf1[i + 2] = st[i];
            Log.e(TAG,"--"+st[i]);
        }

        byte head = (byte) 0xAA;
        byte command = 0x09;
        buf1[0] = head;
        buf1[1] = command;
        for (int i = 0; i < 6; i++) {
            buf1[6] += buf1[i];
        }

        MainActivity.mService.writeRXCharacteristic(address, buf1);
    }
    //车载时间

    /**
     * 车载时间
     * @param address
     * @param hour
     * @param minute
     */
    private void setTime(String address,int minute,int hour,int week,int type,int mode){

        if (!isConnected(address)) {
            Log.e(TAG, "断开连接.......disconnect");
            return;
        }
        byte head = (byte) 0xAA;
        byte command = 0x0C;
        byte st = (byte) minute;
        byte shour = (byte) hour;
        byte day = (byte) week;
        byte tt = (byte) type;
        byte model = (byte) mode;
        byte check = (byte) (head + command + shour + st+day+tt+model);
        byte[] data = new byte[]{head, command,st,shour,day,tt,model,check};

        MainActivity.mService.writeRXCharacteristic(address, data);

    }

//    public ArrayList<DeviceMsg> windSpedQueue = new ArrayList<DeviceMsg>();
//    public ConcurrentLinkedQueue<DeviceMsg> otherQueue = new ConcurrentLinkedQueue<DeviceMsg>();
//    public ConcurrentLinkedQueue<DeviceMsg> shutDownQueue = new ConcurrentLinkedQueue<DeviceMsg>();


    private BleDeviceMsgManager() {
        thread = new managerThread();
        thread.start();
    }

    public static BleDeviceMsgManager getInstance() {
        if(sInstance == null)
            sInstance = new BleDeviceMsgManager();
        return sInstance;
    }

    public void stopThread() {
        this.stopFlag = true;
    }


    private class managerThread extends Thread {

        @Override
        public void run() {
            while(!stopFlag) {
                synchronized(this) {
                    for (Iterator iter = map.entrySet().iterator(); iter.hasNext(); ) {
                        Map.Entry element = (Map.Entry) iter.next();
                        DeviceMsgManager msgManager = (DeviceMsgManager) element.getValue();
                        if (!msgManager.isEmpty()) {
                            DeviceMsg predeviceMsg = msgManager.peekOneMsg();
                            DeviceMsg deviceMsg = predeviceMsg;
                            //以最后一个为准，之前都清掉
                            if (predeviceMsg.action == DeviceMsgAction.WindSpeed) {
                                while(predeviceMsg.action == DeviceMsgAction.WindSpeed) {
                                    deviceMsg = msgManager.pollOneMsg();
                                    if (!msgManager.isEmpty()) {
                                        predeviceMsg = msgManager.peekOneMsg();
                                    } else {
                                        break;
                                    }
                                }
                            } else {
                                deviceMsg = msgManager.pollOneMsg();
                            }
//                        Message msg = handler.obtainMessage();
//                        Bundle bundle = new Bundle();
                            handler.obtainMessage(deviceMsg.action, deviceMsg).sendToTarget();
                        }
                    }
                    try {
                        wait(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public void addMsg2Manager(String address, String name, int action, int change,int hour,int week,int type,int mode) {
//        Log.e(TAG, "addMsg2Manager.....");
        if (map.containsKey(address)) {
//            Log.e(TAG, "addMsg2Manager...111111..");
            map.get(address).addOneMsg(address, name, action, change,hour,week,type,mode);
        } else {
//            Log.e(TAG, "addMsg2Manager...22222..");
            map.put(address, new DeviceMsgManager(address));
            map.get(address).addOneMsg(address, name, action, change,hour,week,type,mode);
        }
        synchronized(thread) {

            if (thread.getState() == Thread.State.WAITING) {
                thread.notify();
            }

        }
    }

    public DeviceMsgManager createDeviceMsgManager(String address) {
        return new DeviceMsgManager(address);
    }


    private class DeviceMsgManager {
        public String address;

        public ConcurrentLinkedQueue<DeviceMsg> queue = new ConcurrentLinkedQueue<DeviceMsg>();

        public DeviceMsgManager(String address) {
            this.address = address;
        }

        public void addOneMsg(String address, String name, int action, int change,int hour,int week,int type,int mode) {
//            Log.e(TAG, "DeviceMsgManager.....");
            queue.add(createDeviceMsg(address, name, action, change,hour,week,type,mode));
        }

        public DeviceMsg peekOneMsg() {
            return queue.peek();
        }

        public DeviceMsg pollOneMsg() {
            return queue.poll();
        }

        public void clearMsg() {
            queue.clear();
        }

        public boolean isEmpty() {
            return queue.isEmpty();
        }


//        private DeviceMsg createDeviceMsg(String address, String name, int action, int speed,int model,int space,int time) {
//            Log.e(TAG, "createDeviceMsg.....");
//
//            return new DeviceMsg(address, name, action, speed,model,space,time);
//        }

        private DeviceMsg createDeviceMsg(String address,String name,int action,int change,int hour,int week,int type,int mode){
            //change--speed,modal,space,time
            DeviceMsg.Builder builder = new DeviceMsg.Builder(address,name,action);
            DeviceMsg deviceMsg = builder.create();
//            DeviceMsg.Builder builder = new DeviceMsg.Builder(address,name,action);
//            DeviceMsg deviceMsg = new DeviceMsg.Builder(address,name,action).create();

            switch (action){
                case DeviceMsgAction.WindSpeed:
//                    Log.e(TAG, "createDeviceMsg.....WindSpeed");
                    deviceMsg = builder.addSpeed(change).create();
                        //return  builder.addSpeed(change).create();
                    break;
                case DeviceMsgAction.DevCheck:
//                    Log.e(TAG, "createDeviceMsg.....DevCheck");
                    deviceMsg = builder.create();
                    break;
                case DeviceMsgAction.DevRename:
//                    Log.e(TAG, "createDeviceMsg.....DevRename");
                    deviceMsg =  builder.create();
                    break;
                case DeviceMsgAction.Disconnect:
//                    Log.e(TAG, "createDeviceMsg.....Disconnect");
                    deviceMsg =  builder.create();
                    break;
                case DeviceMsgAction.Esmart:
//                    Log.e(TAG, "createDeviceMsg.....Esmart");
                    deviceMsg =  builder.addModel(change).create();
                    break;
                case DeviceMsgAction.SetInterval:
//                    Log.e(TAG, "createDeviceMsg.....SetInterval");
                    deviceMsg =  builder.addSpace(change).create();
                    break;
                case DeviceMsgAction.SetSystemClock:
//                    Log.e(TAG, "createDeviceMsg.....SetSystemClock");
                    deviceMsg = builder.addSystemTime(change).create();
                    break;
                case DeviceMsgAction.SetTime:
//                    Log.e(TAG, "createDeviceMsg.....SetTime");
                    deviceMsg =  builder.addTime(change,hour,week,type,mode).create();
                    break;
                case DeviceMsgAction.FilterFactor:
//                    Log.e(TAG, "creatDeviceMsg.....FiltorFactor");
                    deviceMsg = builder.addFactor(change).create();
                    break;
                default:
                    break;
            }
            return deviceMsg;
        }
    }


//    public class DeviceMsg {
//        public String address;
//        public String name;
//        public int action;
//        public int speed;
//        public int model;
//        public int space;
//        public int time;
//        public int minute;
//
//        public DeviceMsg(String address, String name, int action, int speed,int model,int space,int time) {
//            this.address = address;
//            this.name = name;
//            this.action = action;
//            this.speed = speed;
//            this.model = model;
//            this.space = space;
//            this.time = time;
//
//        }
//    }


    public static class DeviceMsgAction {

        public static final int WindSpeed      = 0x01;
        public static final int DevCheck       = 0x02;
        public static final int DevRename      = 0x03;
        public static final int Disconnect     = 0x04;
        public static final int Esmart         = 0x05;
        public static final int SetInterval    = 0x06;
        public static final int SetSystemClock = 0x07;
        public static final int SetTime        = 0x08;
        public static final int FilterFactor   = 0x09;
    }

}
