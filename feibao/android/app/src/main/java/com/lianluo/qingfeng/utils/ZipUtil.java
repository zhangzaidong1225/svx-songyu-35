package com.lianluo.qingfeng.utils;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ZipUtil {

    private static final int BUFFER_SIZE = 4096;

    public ZipUtil() {

    }

    public static void unzip(String zipFile, String location) {
        int size;
        byte[] buffer = new byte[BUFFER_SIZE];

        try{

        if (!location.endsWith("/")){
            location += "/";
        }

        File f = new File(location);
        if (!f.isDirectory()){
            f.mkdirs();
        }
            ZipInputStream zin = new ZipInputStream(new BufferedInputStream(new FileInputStream(zipFile), BUFFER_SIZE));

            try {
            ZipEntry ze = null;
            while ((ze = zin.getNextEntry()) != null){
                String path = location + ze.getName();
                File unzipFile = new File(path);
                if (ze.isDirectory()){
                    if (!unzipFile.isDirectory()){
                        unzipFile.mkdirs();
                    }
                }
                else
                {
                    File parentDir = unzipFile.getParentFile();
                    if (null != parentDir){
                        if (!parentDir.isDirectory()){
                            parentDir.mkdirs();
                        }
                    }

                    FileOutputStream out = new FileOutputStream(unzipFile,false);
                    BufferedOutputStream fout = new BufferedOutputStream(out,BUFFER_SIZE);

                    try {
                        while ((size = zin.read(buffer,0,BUFFER_SIZE)) != -1){
                            fout.write(buffer,0,size);
                        }
                        zin.closeEntry();

                    }finally {
                        fout.flush();
                        fout.close();
                    }

                }

            }

        }
        finally {
                zin.close();
        }

        } catch (Exception e){
            e.printStackTrace();
        }
        Log.e("zip", "生成的MD5------finished");

    }

    public static  void traverseFolder2(String path){
        File file  = new File(path);
        if (file.exists()){
            File[] files = file.listFiles();
            if (files.length == 0){
                Log.e("zip","文件夹是空的");
                return;
            }
            else
            {
                for (File file2:files){
                    if (file2.isDirectory()){
                        Log.e("zip","文件夹"+file2.getAbsolutePath());
                        traverseFolder2(file2.getAbsolutePath());
                    }
                    else
                    {
                        Log.e("zip","文件"+file2.getAbsolutePath());
                    }
                }
            }
        }
        else
        {
            Log.e("zip","文件夹不存在");
        }
    }

}
