package com.lianluo.qingfeng.module;

/**
 * Created by louis on 16/4/25.
 */
public class AdDataModule {

    private String url = "";
    private String action = "";


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
