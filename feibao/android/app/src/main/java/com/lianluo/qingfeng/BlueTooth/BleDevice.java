package com.lianluo.qingfeng.BlueTooth;

import android.bluetooth.BluetoothDevice;

/**
 * Created by Administrator on 2016/5/16.
 */
public class BleDevice {

    //private BluetoothDevice bluetoothDevice;

    //滤棉使用时长
    private int use_time;

    //本次虑棉使用时长
    private int use_time_this;

    //本次滤棉使用总时长
    private int filter_use_time_this;

    //滤棉折合全风速的使用量
    private int filters_maxfan_usetime;


    //风扇等级
    private int speed;
    //电量
    private int battery_power;
    //是否已连接
    private int isconnectd;
    //是否匹配
    private boolean ismatchd;
    //是否自动连接
    private boolean autoConnect;
    private boolean autoConnect1;


    //设备地址
    private String address;
    //设备名
    private String name;
    //版本
    private int version;
    //百分比
    private int percentage;
    //Pm2.5标准浓度
    private int pm25;

    //失败信息
    //101---升级成功
    //102---升级失败
    //103---升级中
    //104---升级开始
    private int update_state;

    //充电标志
    private int battery_charge;

    //本次风扇使用时长
    private int wind_use_time_this;

    //滤棉在位
    private int filter_in;

    //充电电压过低
    private int battery_over;

    //滤棉使用个数
    private int filter_number;

    //设备类型
    //0---忻风
    //1--单品
    //2--车载
    private int type;

    //pm2.5浓度采集时间戳
    private int pm25Time;

    //pm25浓度标志位
    /*
    * 0----读取失败
    * 1---读取中
    * 2---读取成功
    * */
    private int pm25Flag;

    // 系统时间戳
    private int systemTime;

    //采集时间间隔
    private int space;

    //采集模式
    private  int model;

    private int carModel;

    //滤棉1在位
    private int filter01_in;

    //滤棉1使用时长
    private int filter01_use_time;

    //滤棉1本次滤棉时长
    private int filter01_use_time_this;
    //滤棉1使用总时长
    private int filter01_use_time_total;
    //风扇1等级
    private int wind1_speed;
    //风扇2等级
    private int wind2_speed;

    //滤棉2在位
    private int filter02_in;

    //滤棉2使用时长
    private int filter02_use_time;

    //滤棉2本次滤棉时长
    private int filter02_use_time_this;
    //滤棉2使用总时长
    private int filter02_use_time_total;

    //车载设备使用滤棉总时长
    private int vehide_filter_use_time_total;

    //车载滤棉使用个数
    private int vehide_filter_num;

    //车载模式
    /**
     * 0-关闭
     * 1--开启
     */
    private int vehideModel;
    //车载时间
    //车载星期
    private int week;

    private int hour;

    private int minute;

    private int timeFlag01;

    private int vehide_time_state;

    //车载时钟02
    private int week02;

    private int hour02;

    private int minute02;

    private int timeFlag02;

    private int vehide_time_state02;

    //车载时钟03
    private int week03;

    private int hour03;

    private int minute03;

    private int timeFlag03;

    private int vehide_time_state03;

    //是否开盖
    private int open_cap;

    private int filter_use_time_real;

    public int getFilter_use_time_real() {
        return filter_use_time_real;
    }

    public void setFilter_use_time_real(int filter_use_time_real) {
        this.filter_use_time_real = filter_use_time_real;
    }

    public int getOpen_cap() {
        return open_cap;
    }

    public void setOpen_cap(int open_cap) {
        this.open_cap = open_cap;
    }

    public int getCarModel() {
        return carModel;
    }

    public void setCarModel(int carModel) {
        this.carModel = carModel;
    }

    public int getVehide_time_state() {
        return vehide_time_state;
    }

    public void setVehide_time_state(int vehide_time_state) {
        this.vehide_time_state = vehide_time_state;
    }

    public int getVehide_time_state02() {
        return vehide_time_state02;
    }

    public void setVehide_time_state02(int vehide_time_state02) {
        this.vehide_time_state02 = vehide_time_state02;
    }

    public int getVehide_time_state03() {
        return vehide_time_state03;
    }

    public void setVehide_time_state03(int vehide_time_state03) {
        this.vehide_time_state03 = vehide_time_state03;
    }

    public int getWeek() {
        return week;
    }

    public void setWeek(int week) {
        this.week = week;
    }

    public int getTimeFlag01() {
        return timeFlag01;
    }

    public void setTimeFlag01(int timeFlag01) {
        this.timeFlag01 = timeFlag01;
    }

    public int getWeek02() {
        return week02;
    }

    public void setWeek02(int week02) {
        this.week02 = week02;
    }

    public int getHour02() {
        return hour02;
    }

    public void setHour02(int hour02) {
        this.hour02 = hour02;
    }

    public int getMinute02() {
        return minute02;
    }

    public void setMinute02(int minute02) {
        this.minute02 = minute02;
    }

    public int getTimeFlag02() {
        return timeFlag02;
    }

    public void setTimeFlag02(int timeFlag02) {
        this.timeFlag02 = timeFlag02;
    }

    public int getWeek03() {
        return week03;
    }

    public void setWeek03(int week03) {
        this.week03 = week03;
    }

    public int getHour03() {
        return hour03;
    }

    public void setHour03(int hour03) {
        this.hour03 = hour03;
    }

    public int getMinute03() {
        return minute03;
    }

    public void setMinute03(int minute03) {
        this.minute03 = minute03;
    }

    public int getTimeFlag03() {
        return timeFlag03;
    }

    public void setTimeFlag03(int timeFlag03) {
        this.timeFlag03 = timeFlag03;
    }

    public int getVehideModel() {
        return vehideModel;
    }

    public void setVehideModel(int vehideModel) {
        this.vehideModel = vehideModel;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getWind1_speed() {
        return wind1_speed;
    }

    public void setWind1_speed(int wind1_speed) {
        this.wind1_speed = wind1_speed;
    }

    public int getWind2_speed() {
        return wind2_speed;
    }

    public void setWind2_speed(int wind2_speed) {
        this.wind2_speed = wind2_speed;
    }

    public int getFilter02_use_time() {
        return filter02_use_time;
    }

    public void setFilter02_use_time(int filter02_use_time) {
        this.filter02_use_time = filter02_use_time;
    }

    public int getFilter01_in() {
        return filter01_in;
    }

    public void setFilter01_in(int filter01_in) {
        this.filter01_in = filter01_in;
    }

    public int getFilter01_use_time() {
        return filter01_use_time;
    }

    public void setFilter01_use_time(int filter01_use_time) {
        this.filter01_use_time = filter01_use_time;
    }

    public int getFilter01_use_time_this() {
        return filter01_use_time_this;
    }

    public void setFilter01_use_time_this(int filter01_use_time_this) {
        this.filter01_use_time_this = filter01_use_time_this;
    }

    public int getFilter01_use_time_total() {
        return filter01_use_time_total;
    }

    public void setFilter01_use_time_total(int filter01_use_time_total) {
        this.filter01_use_time_total = filter01_use_time_total;
    }

    public int getFilter02_in() {
        return filter02_in;
    }

    public void setFilter02_in(int filter02_in) {
        this.filter02_in = filter02_in;
    }

    public int getFilter02_use_time_this() {
        return filter02_use_time_this;
    }

    public void setFilter02_use_time_this(int filter02_use_time_this) {
        this.filter02_use_time_this = filter02_use_time_this;
    }

    public int getFilter02_use_time_total() {
        return filter02_use_time_total;
    }

    public void setFilter02_use_time_total(int filter02_use_time_total) {
        this.filter02_use_time_total = filter02_use_time_total;
    }

    public int getVehide_filter_use_time_total() {
        return vehide_filter_use_time_total;
    }

    public void setVehide_filter_use_time_total(int vehide_filter_use_time_total) {
        this.vehide_filter_use_time_total = vehide_filter_use_time_total;
    }

    public int getVehide_filter_num() {
        return vehide_filter_num;
    }

    public void setVehide_filter_num(int vehide_filter_num) {
        this.vehide_filter_num = vehide_filter_num;
    }

    public int getSpace() {
        return space;
    }

    public void setSpace(int space) {
        this.space = space;
    }

    public int getModel() {
        return model;
    }

    public void setModel(int model) {
        this.model = model;
    }

    public int getPm25Time() {
        return pm25Time;
    }

    public void setPm25Time(int pm25Time) {
        this.pm25Time = pm25Time;
    }

    public int getPm25Flag() {
        return pm25Flag;
    }

    public void setPm25Flag(int pm25Flag) {
        this.pm25Flag = pm25Flag;
    }

    public int getSystemTime() {
        return systemTime;
    }

    public void setSystemTime(int systemTime) {
        this.systemTime = systemTime;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getFilters_maxfan_usetime() {
        return filters_maxfan_usetime;
    }

    public void setFilters_maxfan_usetime(int filters_maxfan_usetime) {
        this.filters_maxfan_usetime = filters_maxfan_usetime;
    }

    public int getFilter_use_time_this() {
        return filter_use_time_this;
    }

    public void setFilter_use_time_this(int filter_use_time_this) {
        this.filter_use_time_this = filter_use_time_this;
    }

    public int getFilter_number() {
        return filter_number;
    }

    public void setFilter_number(int filter_number) {
        this.filter_number = filter_number;
    }

    public int getBattery_over() {
        return battery_over;
    }

    public void setBattery_over(int battery_over) {
        this.battery_over = battery_over;
    }

    public boolean isAutoConnect1() {
        return autoConnect1;
    }

    public void setAutoConnect1(boolean autoConnect) {
        this.autoConnect1 = autoConnect;
    }

//    public BluetoothDevice getBluetoothDevice() {
//        return bluetoothDevice;
//    }
//
//    public void setBluetoothDevice(BluetoothDevice bluetoothDevice) {
//        this.bluetoothDevice = bluetoothDevice;
//    }

    public int getUse_time() {
        return use_time;
    }

    public void setUse_time(int use_time) {
        this.use_time = use_time;
    }

    public int getUse_time_this() {
        return use_time_this;
    }

    public void setUse_time_this(int use_time_this) {
        this.use_time_this = use_time_this;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getBattery_power() {
        return battery_power;
    }

    public void setBattery_power(int battery_power) {
        this.battery_power = battery_power;
    }

    public int getIsconnectd() {
        return isconnectd;
    }

    public void setIsconnectd(int isconnectd) {
        this.isconnectd = isconnectd;
    }

    public boolean ismatchd() {
        return ismatchd;
    }

    public void setIsmatchd(boolean ismatchd) {
        this.ismatchd = ismatchd;
    }

    public boolean isAutoConnect() {
        return autoConnect;
    }

    public void setAutoConnect(boolean autoConnect) {
        this.autoConnect = autoConnect;
        this.autoConnect1 = autoConnect;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }

    public int getPm25() {
        return pm25;
    }

    public void setPm25(int pm25) {
        this.pm25 = pm25;
    }

    public int getUpdate_state() {
        return update_state;
    }

    public void setUpdate_state(int update_state) {
        this.update_state = update_state;
    }

    public int getBattery_charge() {
        return battery_charge;
    }

    public void setBattery_charge(int battery_charge) {
        this.battery_charge = battery_charge;
    }

    public int getWind_use_time_this() {
        return wind_use_time_this;
    }

    public void setWind_use_time_this(int wind_use_time_this) {
        this.wind_use_time_this = wind_use_time_this;
    }

    public int getFilter_in() {
        return filter_in;
    }

    public void setFilter_in(int filter_in) {
        this.filter_in = filter_in;
    }
}
