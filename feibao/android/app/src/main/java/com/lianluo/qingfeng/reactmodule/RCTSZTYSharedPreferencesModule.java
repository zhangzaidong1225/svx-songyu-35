package com.lianluo.qingfeng.reactmodule;


import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.lianluo.qingfeng.utils.SZTYSharedPreferences;

/**
 * SharedPreferences is used in ReactNative.
 * Created by louis on 16/5/30.
 */
public class RCTSZTYSharedPreferencesModule extends ReactContextBaseJavaModule {

    private SZTYSharedPreferences mSZTYSharedPreferences = SZTYSharedPreferences.getInstance();

    public RCTSZTYSharedPreferencesModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "SZTYSharedPreferences";
    }

//    @ReactMethod
//    public void getInt(String key, int def, Callback callback) {
//        callback.invoke(mSZTYSharedPreferences.getInt(key, def));
//    }
//
//    @ReactMethod
//    public void putInt(String key, int val) {
//        mSZTYSharedPreferences.putInt(key, val);
//    }
//
//    @ReactMethod
//    public void getLong(String key, long def, Callback callback) {
//        callback.invoke(mSZTYSharedPreferences.getLong(key, def));
//    }
//
//    @ReactMethod
//    public void putLong(String key, long val) {
//        mSZTYSharedPreferences.putLong(key, val);
//    }

    @ReactMethod
    public void getString(String key, String def, Callback callback) {
        callback.invoke(null, mSZTYSharedPreferences.getString(key, def));
    }

    @ReactMethod
    public void putString(String key, String val, Callback callback) {
        mSZTYSharedPreferences.putString(key, val);
        callback.invoke(null, null);
    }

//    @ReactMethod
//    public void getBoolean(String key, boolean def, Callback callback) {
//        callback.invoke(mSZTYSharedPreferences.getBoolean(key, def));
//    }
//
//    @ReactMethod
//    public void putBoolean(String key, boolean val) {
//        mSZTYSharedPreferences.putBoolean(key, val);
//    }

    @ReactMethod
    public void remove(String key, Callback callback) {
        mSZTYSharedPreferences.remove(key);
        callback.invoke(null, null);
    }

}
