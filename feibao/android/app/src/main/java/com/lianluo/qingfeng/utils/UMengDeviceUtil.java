package com.lianluo.qingfeng.utils;

import android.content.Context;
import android.util.Log;

import com.lianluo.qingfeng.BlueTooth.BleApplication;
import com.umeng.analytics.MobclickAgent;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by louis on 16/6/16.
 */
public class UMengDeviceUtil {

    private static UMengDeviceUtil instance = new UMengDeviceUtil();

    private boolean[] flagArray = {false, false, false, false, false};

    private Map<String, boolean[]> flagMap = new HashMap<String, boolean[]>();

    private static int[] first = {2, 1, 1, 2, 2, 4, 1};
    private static String prexFirst = "first";
    private static int[] second = {4, 4, 4, 4};
    private static String prexSecond = "second";
    private static int[] third = {4};
    private static String prexThird = "third";
    private static int[] forth = {1, 1, 1, 1, 1, 2, 2, 2, 1, 2, 1};
    private static String prexForth = "forth";
    private static int[] fifth = {4, 4, 2, 4};
    private static String prexFifth = "fifth";

    public UMengDeviceUtil() {
    }

    private static synchronized void syncInit() {
        if (instance == null) {
            instance = new UMengDeviceUtil();
        }
    }

    public static UMengDeviceUtil getInstance() {
        if (instance == null) {
            syncInit();
        }
        return instance;
    }

    public void parseData(byte[] data, String mac){

        if (data == null || data.length < 2) {
            return;
        }

        if (data[1] != 0x50) {
            return;
        }

        if (!flagMap.containsKey(mac)) {
            flagMap.put(mac, flagArray);
        }

        switch (data[2]) {
            case 0x1: {
                if (flagMap.get(mac)[0]) {
                    return;
                }
                parseData(data, first, prexFirst, mac);
                flagMap.get(mac)[0] = true;
                break;
            }
            case 0x2: {
                if (flagMap.get(mac)[1]) {
                    return;
                }
                parseData(data, second, prexSecond, mac);
                flagMap.get(mac)[1] = true;
                break;
            }

            case 0x3: {
                if (flagMap.get(mac)[2]) {
                    return;
                }
                parseData(data, third, prexThird, mac);
                flagMap.get(mac)[2] = true;
                break;
            }

            case 0x4: {
                if (flagMap.get(mac)[3]) {
                    return;
                }
                parseData(data, forth, prexForth, mac);
                flagMap.get(mac)[3] = true;
                break;
            }

            case 0x5: {
                if (flagMap.get(mac)[4]) {
                    return;
                }
                parseData(data, fifth, prexFifth, mac);
                flagMap.get(mac)[4] = true;
                break;
            }
        }

    }

    private void parseData(byte[] data, int[] guide, String prex, String mac) {
        if (data == null || data.length == 0) {
            return;
        }

        if (guide == null) {
            return;
        }

        int sum = 3;
        for (int i = 0; i < guide.length; i++) {

            if (data.length < sum + guide[i]) {
                return;
            }

            Log.e("UMengDeviceUtil", prex + String.valueOf(i+1));
            switch (guide[i]) {
                case 1: {
                    int value = BytesToIntUtil.hexArrayToInt(data[sum]);
                    onEventValue(prex + String.valueOf(i+1), mac, value);
                    break;
                }

                case 2: {
                    int value = BytesToIntUtil.hexArrayToInt(data[sum], data[sum + 1]);
                    onEventValue(prex + String.valueOf(i+1), mac, value);
                    break;
                }

                case 4: {
                    int value = BytesToIntUtil.hexArrayToInt(data[sum], data[sum + 1], data[sum + 2], data[sum + 3]);
                    onEventValue(prex + String.valueOf(i+1), mac, value);
                    break;
                }

            }

            sum += guide[i];
        }
    }


    public void onEventValue(String id, String mac, int value) {
        Map<String, String> map_value = new HashMap<String, String>();
        map_value.put("MAC" , mac );
        MobclickAgent.onEventValue(BleApplication.getInstance(), id, map_value, value);
    }
}
