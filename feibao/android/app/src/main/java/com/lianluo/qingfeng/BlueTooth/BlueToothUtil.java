package com.lianluo.qingfeng.BlueTooth;


import android.app.NotificationManager;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.lianluo.qingfeng.MainActivity;
import com.lianluo.qingfeng.Update.DfuService;
import com.lianluo.qingfeng.utils.ZipUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import no.nordicsemi.android.dfu.DfuProgressListener;
import no.nordicsemi.android.dfu.DfuProgressListenerAdapter;
import no.nordicsemi.android.dfu.DfuServiceInitiator;
import no.nordicsemi.android.dfu.DfuServiceListenerHelper;

/**
 * Created by zhangzaidong on 2016/1/13 0013.
 */
public class BlueToothUtil extends ReactContextBaseJavaModule{

    private final static String TAG = "BlueToothUtil";
    public final static int REQUEST_SCAN = 110;
    public final static int REQUEST_CONNECT = 111;
    private static boolean scan_state = false;
    private static ReactApplicationContext reactApplicationContext;

    private BluetoothAdapter adapter;
    private static DeviceMap mdeviceMap = new DeviceMap();
    private HashMap<String, Timer> timer = new HashMap<String, Timer>();
    private boolean isUpdating = false;

    private  static String FILE_SAVEPATH = null;
    public static String pathfile = null;

    public BlueToothUtil(ReactApplicationContext reactContext) {
        super(reactContext);
        reactApplicationContext = reactContext;
        BleApplication.blueToothUtil = this;
        LocalBroadcastManager.getInstance(getReactApplicationContext()).registerReceiver(UARTStatusChangeReceiver, makeGattUpdateIntentFilter());
        FILE_SAVEPATH = this.getReactApplicationContext().getFilesDir().getAbsolutePath();
        pathfile = FILE_SAVEPATH + "/hotupload";
//        Log.e("zip","......"+FILE_SAVEPATH);
//        Log.e("zip","......"+pathfile);

    }


    public final BroadcastReceiver UARTStatusChangeReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, final Intent intent) {
            String action = intent.getAction();
            //Log.e(TAG, action);
            final Intent mIntent = intent;
            mdeviceMap = mIntent.getParcelableExtra("map");
            scan_state = mIntent.getBooleanExtra("scan_state", false);
            String mAddress = mIntent.getStringExtra(UartService.DEVICE_ADDRESS);
            //Log.e(TAG,mAddress+"------");

            switch (action) {
                case UartService.ADD_DEVICE:
                case UartService.CLEAR_DATA_1:
                case UartService.START_SCAN:
                case UartService.STOP_SCAN:
                case UartService.ACTION_DATA_AVAILABLE:
                    sendMydevice();
                    break;
                case UartService.ACTION_GATT_CONNECTED:

                    Log.e(TAG, "结束时间---ACTION_GATT_CONNECTED--");
                    stopTimer(mAddress);

                    sendMydevice();
                    break;
                case UartService.ACTION_GATT_DISCONNECTED:
                    stopTimer(mAddress);
                    sendMydevice();
                    break;
                case UartService.DEVICE_DOES_NOT_SUPPORT_UART:
                    Log.e(TAG, "DEVICE_DOES_NOT_SUPPORT_UART.......");

                    MainActivity.mService.disconnect(mAddress);
                    stopTimer(mAddress);
//                    try {
//                        Thread.sleep(500);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
                /*    MainActivity.mService.close(mAddress);*/

                    break;
                case UartService.DEVICE_ADDRESS:
                    BlueToothUtil.getRRRContext()
                            .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                            .emit("add_to_current_device",mAddress);
                    Log.e("address_send", "---send address - 2 :" + mAddress);
                    break;
                default:
                    break;

            }

        }
    };


    public IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();

        intentFilter.addAction(UartService.ADD_DEVICE);
        intentFilter.addAction(UartService.CLEAR_DATA_1);
        intentFilter.addAction(UartService.START_SCAN);
        intentFilter.addAction(UartService.STOP_SCAN);
        intentFilter.addAction(UartService.ACTION_GATT_CONNECTING);
        intentFilter.addAction(UartService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(UartService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(UartService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(UartService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(UartService.DEVICE_DOES_NOT_SUPPORT_UART);
        intentFilter.addAction(UartService.DEVICE_ADDRESS);
        return intentFilter;
    }


    @ReactMethod
    public void auto_connect() {

        final Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                //Log.e(TAG, "自动连接.....starting.....");

                adapter = BluetoothAdapter.getDefaultAdapter();
                if (!adapter.isEnabled()) return;

                if (XinfengService.mData.isEmpty()) {
                    Log.e(TAG, "---AutoConnect---kong");
                    return;
                }

                boolean isNeedAutoConnectFlag = false;
                for (String key : XinfengService.mData.keySet()) {
                    BleDevice device = XinfengService.mData.get(key);
                    if (device.ismatchd()){
                        if ((device.getIsconnectd() == 0) && (device.isAutoConnect())){
                            Log.e(TAG, "---AutoConnect---Need" + device.getAddress()+ device.getName());
                            isNeedAutoConnectFlag = true;

                        }
                    }
                }

                if (!isNeedAutoConnectFlag) {
                    Log.e(TAG, "---AutoConnect---NoNeed");
                    return;
                }

                if (MainActivity.mService != null){
                    if (!isUpdating) {
                        MainActivity.mService.scanAddressAndConnect(false);
                    }
                }

            }
        };
        timer.schedule(task, 100, 5000);
    }

    @Override
    public String getName() {
        return "BlueToothUtil";
    }

    public static ReactApplicationContext getRRRContext() {
        return reactApplicationContext;
    }


    @ReactMethod
    public void sendMydevice() {
        //Log.e(TAG, "来数据啦---发送数据····");
        //---这个问题---Receiving map already consumed
        //https://github.com/marcshilling/react-native-image-picker/issues/142

        WritableArray writableArray = Arguments.createArray();
        Iterator iter = mdeviceMap.map.entrySet().iterator();

        while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry) iter.next();

            BleDevice device = (BleDevice) entry.getValue();

            WritableMap writableMap = Arguments.createMap();

            writableMap.putInt("use_time", device.getUse_time());
            writableMap.putInt("use_time_this", device.getUse_time_this());
            writableMap.putInt("speed", device.getSpeed());
            writableMap.putInt("battery_power", device.getBattery_power());
            writableMap.putInt("isconnectd", device.getIsconnectd());
            writableMap.putBoolean("ismatchd", device.ismatchd());
            writableMap.putBoolean("autoConnect", device.isAutoConnect());
            writableMap.putString("addr", device.getAddress());
            writableMap.putString("name", device.getName());
            writableMap.putInt("version", device.getVersion());
            writableMap.putInt("percentage", device.getPercentage());
            writableMap.putInt("update_state", device.getUpdate_state());
            writableMap.putInt("ischarged", device.getBattery_charge());
            writableMap.putInt("wind_time", device.getWind_use_time_this());
            writableMap.putInt("battery_over", device.getBattery_over());
            writableMap.putInt("filter_in", device.getFilter_in());
            writableMap.putInt("filter_number", device.getFilter_number());
            writableMap.putInt("filter_use_time_this", device.getFilter_use_time_this());
            writableMap.putInt("filters_maxfan_usetime", device.getFilters_maxfan_usetime());
            writableMap.putInt("filter_use_time_real",device.getFilter_use_time_real());

            writableMap.putInt("type",device.getType());
            writableMap.putInt("Pm25", device.getPm25());
            writableMap.putInt("Pm25Flag",device.getPm25Flag());
            writableMap.putInt("Pm25PickTime",device.getPm25Time());
            writableMap.putInt("SystemTime",device.getSystemTime());
            writableMap.putInt("space",device.getSpace());
            writableMap.putInt("model",device.getModel());

/*            writableMap.putInt("filter01_in",device.getFilter01_in());
            writableMap.putInt("filter01_use_time",device.getFilter01_use_time());
            writableMap.putInt("filter01_use_time_this",device.getFilter01_use_time_this());
            writableMap.putInt("filter01_use_time_total",device.getFilter01_use_time_total());
            writableMap.putInt("wind01_speed",device.getWind1_speed());

            writableMap.putInt("filter02_in",device.getFilter02_in());
            writableMap.putInt("filter02_use_time",device.getFilter02_use_time());
            writableMap.putInt("filter02_use_time_this",device.getFilter02_use_time_this());
            writableMap.putInt("filter02_use_time_total",device.getFilter02_use_time_total());
            writableMap.putInt("wind02_speed",device.getWind2_speed());

            writableMap.putInt("filter_use_time_vehide",device.getVehide_filter_use_time_total());
            writableMap.putInt("filter_num_vehide",device.getVehide_filter_num());
            writableMap.putInt("vehide_model",device.getVehideModel());
            writableMap.putInt("vehide_is_smart",device.getCarModel());
            writableMap.putInt("open_cap",device.getOpen_cap());

            writableMap.putInt("timeFlag01",device.getTimeFlag01());
            writableMap.putInt("vehide_time_state",device.getVehide_time_state());
            writableMap.putInt("vehide_week",device.getWeek());
            writableMap.putInt("vehide_hour",device.getHour());
            writableMap.putInt("vehide_minute",device.getMinute());

            writableMap.putInt("timeFlag02",device.getTimeFlag02());
            writableMap.putInt("vehide_time_state02",device.getVehide_time_state02());
            writableMap.putInt("vehide_week02",device.getWeek02());
            writableMap.putInt("vehide_hour02",device.getHour02());
            writableMap.putInt("vehide_minute02",device.getMinute02());
            
            writableMap.putInt("timeFlag03",device.getTimeFlag03());
            writableMap.putInt("vehide_time_state03",device.getVehide_time_state03());
            writableMap.putInt("vehide_week03",device.getWeek03());
            writableMap.putInt("vehide_hour03",device.getHour03());
            writableMap.putInt("vehide_minute03",device.getMinute03());*/

            writableArray.pushMap(writableMap);
        }

        WritableMap params = Arguments.createMap();
        params.putBoolean("scan_state", scan_state);
        params.putArray("dev_list", writableArray);

        if (reactApplicationContext.hasActiveCatalystInstance()){
            BlueToothUtil.getRRRContext()
                    .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                    .emit("ble_state", params);
        }


    }

    public void sendMyDevice(DeviceMap deviceMap) {
        mdeviceMap = deviceMap;
        sendMydevice();
    }

    public void write(){
        WritableArray writableArray = Arguments.createArray();
        WritableMap writableMap = Arguments.createMap();

//        WritableArray writableArray = Arguments.createArray();
        Iterator iter = mdeviceMap.map.entrySet().iterator();
        WritableArray writableArrayBase = Arguments.createArray();
        WritableArray writableArrayXin = Arguments.createArray();
        WritableArray writableArraySingle = Arguments.createArray();
        WritableArray writableArrayVehide = Arguments.createArray();


        while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry) iter.next();
            BleDevice device = (BleDevice) entry.getValue();

            //ble公共数据

            WritableMap writableMapBase = Arguments.createMap();
            writableMapBase.putInt("isconnectd", device.getIsconnectd());
            writableMapBase.putBoolean("ismatchd", device.ismatchd());
            writableMapBase.putBoolean("autoConnect", device.isAutoConnect());
            writableMapBase.putString("addr", device.getAddress());
            writableMapBase.putString("name", device.getName());
            writableMapBase.putInt("version", device.getVersion());
            writableMapBase.putInt("percentage", device.getPercentage());
            writableMapBase.putInt("update_state", device.getUpdate_state());
            writableMapBase.putInt("ischarged", device.getBattery_charge());
            writableMapBase.putInt("battery_power", device.getBattery_power());
            writableMapBase.putInt("battery_over", device.getBattery_over());
            writableMapBase.putInt("type",device.getType());
            writableMapBase.putInt("Pm25", device.getPm25());
            writableMapBase.putInt("SystemTime",device.getSystemTime());
            writableArrayBase.pushMap(writableMapBase);

            if ( device.getType() == 0){

                WritableMap writableMapXin = Arguments.createMap();
                writableMapXin.putInt("use_time", device.getUse_time());
                writableMapXin.putInt("use_time_this", device.getUse_time_this());
                writableMapXin.putInt("speed", device.getSpeed());
                writableMapXin.putInt("wind_time", device.getWind_use_time_this());
                writableMapXin.putInt("filter_in", device.getFilter_in());
                writableMapXin.putInt("filter_number", device.getFilter_number());
                writableMapXin.putInt("filter_use_time_this", device.getFilter_use_time_this());
                writableMapXin.putInt("filters_maxfan_usetime", device.getFilters_maxfan_usetime());
                writableArrayXin.pushMap(writableMapXin);

            }else if ( device.getType() == 1){

                WritableMap writableMapSingle = Arguments.createMap();

                writableMapSingle.putInt("Pm25Flag",device.getPm25Flag());
                writableMapSingle.putInt("Pm25PickTime",device.getPm25Time());
                writableMapSingle.putInt("space",device.getSpace());
                writableMapSingle.putInt("model",device.getModel());
                writableArraySingle.pushMap(writableMapSingle);

            }else if ( device.getType() == 2 ){

                WritableMap writableMapVehide = Arguments.createMap();
                writableMapVehide.putInt("filter01_in",device.getFilter01_in());
                writableMapVehide.putInt("filter01_use_time",device.getFilter01_use_time());
                writableMapVehide.putInt("filter01_use_time_this",device.getFilter01_use_time_this());
                writableMapVehide.putInt("filter01_use_time_total",device.getFilter01_use_time_total());
                writableMapVehide.putInt("wind01_speed",device.getWind1_speed());

                writableMapVehide.putInt("filter02_in",device.getFilter02_in());
                writableMapVehide.putInt("filter02_use_time",device.getFilter02_use_time());
                writableMapVehide.putInt("filter02_use_time_this",device.getFilter02_use_time_this());
                writableMapVehide.putInt("filter02_use_time_total",device.getFilter02_use_time_total());
                writableMapVehide.putInt("wind02_speed",device.getWind2_speed());

                writableMapVehide.putInt("filter_use_time_vehide",device.getVehide_filter_use_time_total());
                writableMapVehide.putInt("filter_num_vehide",device.getVehide_filter_num());
                writableMapVehide.putInt("vehide_model",device.getVehideModel());

                writableMapVehide.putInt("timeFlag01",device.getTimeFlag01());
                writableMapVehide.putInt("vehide_time_state",device.getVehide_time_state());
                writableMapVehide.putInt("vehide_week",device.getWeek());
                writableMapVehide.putInt("vehide_hour",device.getHour());
                writableMapVehide.putInt("vehide_minute",device.getMinute());

                writableMapVehide.putInt("timeFlag02",device.getTimeFlag02());
                writableMapVehide.putInt("vehide_time_state02",device.getVehide_time_state02());
                writableMapVehide.putInt("vehide_week02",device.getWeek02());
                writableMapVehide.putInt("vehide_hour02",device.getHour02());
                writableMapVehide.putInt("vehide_minute02",device.getMinute02());

                writableMapVehide.putInt("timeFlag03",device.getTimeFlag03());
                writableMapVehide.putInt("vehide_time_state03",device.getVehide_time_state03());
                writableMapVehide.putInt("vehide_week03",device.getWeek03());
                writableMapVehide.putInt("vehide_hour03",device.getHour03());
                writableMapVehide.putInt("vehide_minute03",device.getMinute03());
                writableArrayVehide.pushMap(writableMapVehide);

            }


        }

        WritableMap params = Arguments.createMap();
        params.putBoolean("scan_state", scan_state);
        params.putArray("dev_list", writableArrayBase);
        params.putArray("dev_xin",writableArrayXin);
        params.putArray("dev_single",writableArraySingle);
        params.putArray("dev_vehide",writableArrayVehide);

        BlueToothUtil.getRRRContext()
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit("ble_state", params);
    }

    @ReactMethod
    public void scanner() {
        Log.e(TAG, "scanner.......STARTING...111");
        adapter = BluetoothAdapter.getDefaultAdapter();
        if (adapter.isEnabled()) {
            MainActivity.mContext.enableRequestLocation();
        } else {
            MainActivity.mContext.requestBluetooth(REQUEST_SCAN, "");
        }

    }


    @ReactMethod
    public void stopScan() {
        MainActivity.mService.stopScan();
    }


    public Boolean isConnected(String address) {

        if (!XinfengService.mData.isEmpty() && (XinfengService.mData.containsKey(address)) && (address != null)) {

            BleDevice device = XinfengService.mData.get(address);
            if (device.getIsconnectd() == 0) {
                return false;
            } else if (device.getIsconnectd() == 1) {
                return true;
            }
        }


        return false;
    }

    public void Disconnected(String address) {

        if (!XinfengService.mData.isEmpty() && XinfengService.mData.containsKey(address)) {
            final BleDevice device = XinfengService.mData.get(address);
            if (device.getIsconnectd() == 1) {
                device.setAutoConnect(false);
            }
        }
    }


    @ReactMethod
    public void stopConnect(String address) {

        Log.e(TAG, "stopConnect-------");

        if (!isConnected(address)) {
            Log.e(TAG, "断开连接。。。。。。");
            return;
        }
        this.Disconnected(address);
        MainActivity.mService.disconnect(address);
    }


    @ReactMethod
    public void cancelBond(String address) {
        Log.e(TAG, "---cancelBonded---");

        Iterator iter = XinfengService.mData.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry) iter.next();
            Object key = entry.getKey();
            Object val = entry.getValue();
            BleDevice bleDevice = (BleDevice) val;

            if (bleDevice.getAddress().equals(address)) {
//                bleDevice.setIsmatchd(false);
                Log.e(TAG, "---cancelBonded---" + bleDevice.getIsconnectd());
                if (bleDevice.getIsconnectd() == 1){
                    Log.e(TAG, "---cancelBonded---设备连接成功");
                    bleDevice.setIsmatchd(false);
                    disconnect(address);
                    stopConnect(address);

                    break;
                } else if (bleDevice.getIsconnectd() == 3){
                    Log.e(TAG, "---cancelBonded---设备连接中");
//                    bleDevice.setIsmatchd(false);
//                    disconnect(address);
//                    stopConnect(address);

                } else if (bleDevice.getIsconnectd() == 0) {
                    bleDevice.setIsmatchd(false);
                    Log.e(TAG, "---cancelBonded---设备未连接");
                    iter.remove();
                    MainActivity.mService.RemoveData(address);
                }
            }

        }

        mdeviceMap.map = XinfengService.mData;
        sendMyDevice(mdeviceMap);
    }

    /**
     * 连接蓝牙  传入选择的position
     *
     * @param
     */
    @ReactMethod
    public void connectDevice(final String address) {
        connectDevice(address, true);
    }

    public void connectDevice(final String address, boolean connecting) {
        connect(address, connecting);
    }

    public void connect(final String address, final boolean connecting) {

        adapter = BluetoothAdapter.getDefaultAdapter();
        if (adapter.isEnabled()) {

            Log.e(TAG, "---connect---" + XinfengService.mData.size());

            if (!XinfengService.mData.isEmpty() && XinfengService.mData.containsKey(address)) {

                Object bleObj = XinfengService.mData.get(address);
                BleDevice device = (BleDevice) bleObj;

                if (device.getIsconnectd() == 0) {
                    if (connecting) {
                        Log.e(TAG, "--手动点击------");
                        device.setIsconnectd(2);
                    } else {
                        Log.e(TAG, "--自动连接starting--3----");
                        device.setIsconnectd(3);
                    }
                    Log.e(TAG, "---connect--getIsconnectd-" + device.getIsconnectd() + "---" + device.getName());
                    mdeviceMap.map = XinfengService.mData;
                    sendMyDevice(mdeviceMap);
                    //将连接放到手动点击和自动连接中，当自动连接状态下，被点击时，不需要执行连接操作。
                    MainActivity.mService.connect(address);

//                    mTimer = new Timer();
////                    Log.e(TAG,"开始时间");
//                    mTimer.schedule(new BleTask(), 60000);
                    if(connecting) {
                        Timer temp = new Timer();
                        temp.schedule(new BleTask(device.getAddress()), 60000);
                        timer.put(device.getAddress(), temp);
                        Log.e(TAG, "````开始时间-----" + device.getAddress());
                    }


                } else {
                    if (device.getIsconnectd() == 3) {
                        if (connecting) {
                            Log.e(TAG, "--手动点击------");
                            device.setIsconnectd(2);
                        }
                        mdeviceMap.map = XinfengService.mData;
                        sendMyDevice(mdeviceMap);
                        if(connecting) {
                            Timer temp = new Timer();
                            temp.schedule(new BleTask(device.getAddress()), 60 * 1000);
                            timer.put(device.getAddress(), temp);
                            Log.e(TAG, "开始时间-----");
                        }
                    }
                }
            }


        } else {
            MainActivity.mContext.requestBluetooth(REQUEST_CONNECT, address);
        }
    }

    class BleTask extends TimerTask {

        public String address;
        public BleTask(String address) {
            this.address = address;
        }

        @Override
        public void run() {
            Log.e(TAG, "--连接中-OVER--");

                if (!XinfengService.mData.isEmpty() && XinfengService.mData.containsKey(address)) {

                    Object bleObj = XinfengService.mData.get(address);
                    final BleDevice device = (BleDevice) bleObj;

                    Log.e(TAG, "--连接中---OVER----" + device.getIsconnectd());

                    if ((device.getIsconnectd() == 2) || (device.getIsconnectd() == 3)) {
                        device.setIsconnectd(0);

                        MainActivity.mService.disconnect(address);

                        MainActivity.mService.close(address);

                        Log.e(TAG, "--连接中-OVER-close-" + device.getName());
                        mdeviceMap.map = XinfengService.mData;
                        sendMyDevice(mdeviceMap);
                    }
                }
        }
    }

    public void stopTimer(String address) {
        Log.e(TAG, "````stopTimer" + address);
        if (timer.containsKey(address)) {
            timer.get(address).cancel();
            timer.get(address).purge();
            timer.remove(address);
        }
    }


    @ReactMethod
    public void shutDown(String address) {
        Log.e(TAG, "shutdown```````````" + address);

        if (!isConnected(address)) {
            Log.e(TAG, "断开连接.......shutDown");
            return;
        }

        byte head = (byte) 0xAA;
        byte command = 0x01;
        byte check = (byte) (head + command);
        byte[] data = new byte[]{head, command, check};

        MainActivity.mService.writeRXCharacteristic(address, data);
    }

    @ReactMethod
    public void disconnect(String address) {

        Log.e(TAG, "disconnect```````````" + address);
        //BleDeviceMsgManager.getInstance().addMsg2Manager(address, "", BleDeviceMsgManager.DeviceMsgAction.Disconnect, 0,0);

        if (!isConnected(address)) {
            Log.e(TAG, "断开连接.......disconnect");
            return;
        }

        byte head = (byte) 0xAA;
        byte command = 0x07;
        byte check = (byte) (head + command);
        byte[] data = new byte[]{head, command, check};

        MainActivity.mService.writeRXCharacteristic(address, data);

    }

    /**
     * @param address
     * @param factor
     */
    @ReactMethod
    public void filterFactor(String address,int factor){
        Log.e("address_send", "DEVICE_ADDRESS......factor. ："+factor);
        if (address != null){
            Log.e(TAG,"filterFactor...."+address+"...."+factor);
            BleDeviceMsgManager.getInstance().addMsg2Manager(address, "", BleDeviceMsgManager.DeviceMsgAction.FilterFactor,factor,0,0,0,0);

        }

    }

    /**
     * @param address
     * @param speed
     */

    @ReactMethod
    public void windSpeed(String address, int speed) {
        Log.e(TAG, "windspeed=====" + speed + "地址" + address);

        if (address != null){
            BleDeviceMsgManager.getInstance().addMsg2Manager(address, "", BleDeviceMsgManager.DeviceMsgAction.WindSpeed, speed,0,0,0,0);
        }

//        if (!isConnected(address)) {
//            Log.e(TAG, "风速---断开连接。。。。。。");
//            return;
//        }
//
//        byte head = (byte) 0xAA;
//        byte command = 0x10;
//        byte sp = (byte) speed;
//        Log.e(TAG, "bytespeed-----" + sp + "");
//        byte time = 0;
//        byte check = (byte) (head + command + sp + time);
//        byte[] data = new byte[]{head, command, sp, time, check};
//
//        MainActivity.mService.writeRXCharacteristic(address, data);
//        try {
//            Thread.sleep(300);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }


    @ReactMethod
    public void devCheck(String address) {

        Log.e(TAG, "devCheck```````````"+address);
        if (address != null ){
            BleDeviceMsgManager.getInstance().addMsg2Manager(address, "", BleDeviceMsgManager.DeviceMsgAction.DevCheck,0,0,0,0,0);
        }

        //SingleMsg.getInstance().systemTime(address);

//        byte head = (byte) 0xAA;
//        byte command = 0x13;
//        byte check = (byte) (head + command);
//        byte[] data = new byte[]{head, command, check};
//
//
//        if (!isConnected(address)) {
//            Log.e(TAG, "断开连接.......devcheck");
//            return;
//        }
//
//        MainActivity.mService.writeRXCharacteristic(address, data);

//        try {
//            Thread.sleep(300);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }


    @ReactMethod
    public void devRename(final String address, String rename) {

        Log.e(TAG, "devRename```````````");

//        //最大为4个汉字，12字节
//
//        byte head = (byte) 0xAA;
//        byte command = 0x12;
//
//        //Log.e(TAG,"汉字的长度----"+length);
//
//
//        byte[] buf1 = new byte[16];
//
//        //final byte[] buf2 = new byte[19];
//        byte[] buf_name = new byte[30];
//
//        try {
//            int buf_length = 0;
//            for (int i = 0; i < rename.length(); i++) {
//                String tmp = rename.substring(i, i + 1);
//                Log.e(TAG, "devRename----" + tmp);
//
//                byte[] tmp_buf = tmp.getBytes("UTF8");
//                if (buf_length + tmp_buf.length <= 30) {
//                    for (int j = 0; j < tmp_buf.length; j++) {
//                        buf_name[buf_length] = tmp_buf[j];
//                        buf_length++;
//                    }
//                }
//            }
//
//            for (int i = 0; i < 30; i++) {
//                Log.e(TAG, "devRename--" + String.format("%x", buf_name[i]));
//                if (i < 12) {
//                    buf1[i + 3] = buf_name[i];
//                } else {
//                    //buf2[i + 3 - 15] = buf_name[i];
//                }
//            }
//
//        } catch (Exception e) {
//        }
//        buf1[0] = head;
//        buf1[1] = command;
//        buf1[2] = (byte) buf_name.length;
//
//
////        buf2[0] = head;
////        buf2[1] = command;
////        buf2[2] = 0x01;
//        //校验位
//        for (int i = 0; i < 15; i++) {
//            buf1[15] += buf1[i];
//            //buf2[18] += buf2[i];
//        }
////        for (int i = 0; i < 19; i++) {
////            //Log.e("devRename", String.format("%x", buf1[i]));
////        }
////
////        for (int i = 0; i < 19; i++) {
////            //Log.e("devRename", String.format("%x", buf2[i]));
////        }
//
//
//        //byte check= (byte) (head+command);
//        //byte[] bbb=new byte[]{head,command,check};
//        if (!isConnected(address)) {
//            return;
//        }
//
//
//        MainActivity.mService.writeRXCharacteristic(address, buf1);

//        try {
//            Thread.sleep(300);
//        } catch (Exception e) {
//        }

        //有可能重命名失败在S6上，
        //MainActivity.mService.writeRXCharacteristic(address, buf2);




        //获得特定的key
        Object bleObj = XinfengService.mData.get(address);
        final BleDevice device = (BleDevice) bleObj;


        try {
            int renameByte = rename.getBytes("UTF-8").length > 12 ? 12 :rename.getBytes("UTF-8").length;
            Log.e(TAG,"rename---" + renameByte);
            byte[] buf = new byte[renameByte];
            byte[] nameBytes = rename.getBytes("UTF-8");

            byte[] tmp10 = new byte[3];
            boolean chinese = false;
            if (nameBytes.length >= 12){
                for (int i = 9;i < nameBytes.length;i++ ){
                    if (i < 12) {
                        tmp10[i - 9] = nameBytes[i];
                    }
                }
                String tmp12 = new String(tmp10,"UTF-8");

                chinese = isChineseCharacter(tmp12);
                Log.e(TAG,"rename--12-" + tmp12 +"--"+tmp12.length()+"--"+ chinese);
            }


            if (chinese){
                renameByte = 12;
                for (int i = 0;i < renameByte;i++){
                    buf[i] = nameBytes[i];
                }
                String tmp = new String(buf,"UTF-8");
                Log.e(TAG,"rename---" + tmp);
                device.setName(tmp);
                if (address != null){
                    BleDeviceMsgManager.getInstance().addMsg2Manager(address, tmp, BleDeviceMsgManager.DeviceMsgAction.DevRename, 0,0,0,0,0);
                }
            } else {
                String tmpStr = "";
                if (rename.length() > 4){
                    tmpStr = rename.substring(0,4);
                    device.setName(tmpStr);
                } else {
                    tmpStr = rename;
                    device.setName(rename);
                }
                if (address != null){
                    BleDeviceMsgManager.getInstance().addMsg2Manager(address, tmpStr, BleDeviceMsgManager.DeviceMsgAction.DevRename, 0,0,0,0,0);
                }
            }


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
//        if (rename.length() > 4){
//            device.setName(rename.substring(0,4));
//        } else {
//            device.setName(rename);
//        }


        mdeviceMap.map = XinfengService.mData;
        sendMyDevice(mdeviceMap);
        Log.e(TAG, "devRename-----ok");
    }

    public static final boolean isChineseCharacter(String chineseStr) {
        char[] charArray = chineseStr.toCharArray();
        for (int i = 0; i < charArray.length; i++) {
            if ((charArray[i] >= 0x4e00) && (charArray[i] <= 0x9fbb)) {
                return true;
            }
        }
        return false;
    }

    @ReactMethod
    public void esmart(String address,int model){
        /*
        * 0--连续
        * 1--分时
        * 2---单次
        * */
        if (address != null){
            Log.e(TAG,"...ESMART...."+address+"...."+model);

            BleDeviceMsgManager.getInstance().addMsg2Manager(address, "", BleDeviceMsgManager.DeviceMsgAction.Esmart,model,0,0,0,0);

        }

    }
    @ReactMethod
    public void setInterval(String address,int space){
        if (address != null){
            Log.e(TAG,"...时间间隔"+address+"...."+space);

            BleDeviceMsgManager.getInstance().addMsg2Manager(address, "", BleDeviceMsgManager.DeviceMsgAction.SetInterval,space,0,0,0,0);

        }
    }

    @ReactMethod
    public void setSystemClock(String address,int time){
        if (address != null){
            Log.e(TAG,"...系统时间"+address+"...."+time);

            BleDeviceMsgManager.getInstance().addMsg2Manager(address, "", BleDeviceMsgManager.DeviceMsgAction.SetSystemClock,time,0,0,0,0);

        }
    }
    @ReactMethod
    public void setVehideTime (String address,int minute,int hour,int week,int type,int mode){
        Log.e(TAG,".车载时间.."+address+"...."+hour +"``"+minute+week+type+mode);

        if (address != null){
            Log.e(TAG,".车载时间.."+address+"...."+hour +"``"+minute);
            BleDeviceMsgManager.getInstance().addMsg2Manager(address, "", BleDeviceMsgManager.DeviceMsgAction.SetTime,minute,hour,week,type,mode);
        }
    }



    /**
     * MD5校验
     *
     * @param packagePath
     * @return
     */
    @ReactMethod
    public static void verifyInstallPackage(String packagePath, final Callback callback1) {

        try {
            MessageDigest sig = MessageDigest.getInstance("MD5");
            File packageFile = new File(packagePath);
            InputStream signedData = new FileInputStream(packageFile);
            byte[] buffer = new byte[4096];
            long toRead = packageFile.length();
            long soFar = 0;
            boolean interrupted = false;

            while (soFar < toRead) {
                interrupted = Thread.interrupted();
                if (interrupted) break;
                int read = signedData.read(buffer);
                soFar += read;
                sig.update(buffer, 0, read);
            }

            final byte[] digest = sig.digest();
            String digestStr = bytesToHexString(digest);
            digestStr = digestStr.toLowerCase();
            Log.e(TAG, "生成的MD5------" + digestStr);
            callback1.invoke(digestStr);

        } catch (Exception e) {
            Log.e(TAG, "CRC Error");
            callback1.invoke(false);
        }
    }
    @ReactMethod
    public void unZip(final String packagePath){
        Log.e(TAG, "生成的MD5------" + packagePath);
        new Thread(new Runnable() {
            @Override
            public void run() {
                ZipUtil.unzip(packagePath,pathfile);
//                Zip.traverseFolder2(pathfile);
            }
        }).start();

    }


    public static String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }
        int i = 0;
        while (i < src.length) {
            int v;
            String hv;
            v = (src[i] >> 4) & 0x0F;
            hv = Integer.toHexString(v);
            stringBuilder.append(hv);

            v = src[i] & 0x0F;
            hv = Integer.toHexString(v);
            stringBuilder.append(hv);
            i++;
        }
        return stringBuilder.toString();
    }

    /**
     * 设备固件升级方法，，，，
     *
     * @param address 需要升级设备的mac地址
     * @param name    需要升级设备的mac地址
     * @param path    升级所需的zip文件路径
     */
    @ReactMethod
    public void deviceUpdate(String address, String name, String path) {

        DfuServiceListenerHelper.registerProgressListener(MainActivity.mContext, mDfuProgressListener);
        final DfuServiceInitiator starter = new DfuServiceInitiator(address)
                .setDeviceName(name)
                .setKeepBond(true);
        Uri uri = Uri.fromFile(new File(path));
        starter.setZip(uri, path);
        starter.start(MainActivity.mContext, DfuService.class);

    }
    public static File getFilePath(String filePath,
                                   String fileName) {
        File file = null;
        makeRootDirectory(filePath);
        try {
            file = new File(filePath + fileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }

    public static void makeRootDirectory(String filePath) {
        File file = null;
        try {
            file = new File(filePath);
            if (!file.exists()) {
                file.mkdir();
            }
        } catch (Exception e) {

        }
    }
    /**
     *  将assets目录下指定的文件拷贝到sdcard中
     *  @return 是否拷贝成功,true 成功；false 失败
     *  @throws IOException
     */
    public static final String ASSET_LIST_FILENAME = "assets.lst";

    public static Context mContext = BleApplication.getInstance().getApplicationContext();
    public static AssetManager mAssetManager =  mContext.getAssets();
    public static File mAppDirectory;
    public static boolean copy(File path) throws IOException {
        Log.e("===s===","file.exists  "+path.exists());
        List<String> srcFiles = new ArrayList<String>();

        //获取系统在SDCard中为app分配的目录，eg:/sdcard/Android/data/$(app's package)
        //该目录存放app相关的各种文件(如cache，配置文件等)，unstall app后该目录也会随之删除
        mAppDirectory = path;
        if (null == mAppDirectory) {
            return false;
        }

        //读取assets/$(subDirectory)目录下的assets.lst文件，得到需要copy的文件列表
        List<String> assets = getAssetsList();
        for( String asset : assets ) {
            //如果不存在，则添加到copy列表
            Log.e("===s===","asset  "+asset);
            if( ! new File(mAppDirectory,asset).exists() ) {
                srcFiles.add(asset);
            }
        }

        //依次拷贝到App的安装目录下
        for( String file : srcFiles ) {
            File s = copy(file);
            Log.e("===s===","s  "+s);

        }

        return true;
    }

    /**
     *  获取需要拷贝的文件列表（记录在assets/assets.lst文件中）
     *  @return 文件列表
     *  @throws IOException
     */
    public static List<String> getAssetsList() throws IOException {

        List<String> files = new ArrayList<String>();

        InputStream listFile = mAssetManager.open(new File(ASSET_LIST_FILENAME).getPath());
        BufferedReader br = new BufferedReader(new InputStreamReader(listFile));
        String path;
        while (null != (path = br.readLine())) {
            files.add(path);
        }

        return files;
    }

    /**
     *  执行拷贝任务
     *  @param asset 需要拷贝的assets文件路径
     *  @return 拷贝成功后的目标文件句柄
     *  @throws IOException
     */
    public static File copy( String asset ) throws IOException {
        InputStream source = mAssetManager.open(new File(asset).getPath());
        File destinationFile = new File(mAppDirectory, asset);
        destinationFile.getParentFile().mkdirs();
        OutputStream destination = new FileOutputStream(destinationFile);
        byte[] buffer = new byte[1024];
        int nread;

        while ((nread = source.read(buffer)) != -1) {
            if (nread == 0) {
                nread = source.read();
                if (nread < 0)
                    break;
                destination.write(nread);
                continue;
            }
            destination.write(buffer, 0, nread);
        }
        destination.close();

        return destinationFile;
    }
    public static void  copyFileFromAssets(String name, String filePath,String fileName){
        boolean flag = false;
        FileOutputStream fos;
        try {
            InputStream is = BleApplication.getInstance().getApplicationContext().getAssets().open("dev_update.zip");
            Log.e("======","is  "+is);

            File file = getFilePath(filePath,fileName);
            if (file.exists()) {
                flag = true;
                Log.e("======","file.exists  "+file.exists());
            }
            if(!flag){
                Log.e("======"," copying  "+file);
                fos = new FileOutputStream(file);
                Log.e("======","fos  "+fos);
                byte[] buffer = new byte[1024];
                int count = 0;
                while (true) {
                    count++;
                    int len = is.read(buffer);
                    if (len == -1) {
                        break;
                    }
                    fos.write(buffer, 0, len);
                }
                fos.flush();
                fos.close();
            }
            is.close();

        }catch (Exception e){
            Log.e("======","err  "+e.getMessage());
        }
    }

    //设备固件升级的监听事件
    private final DfuProgressListener mDfuProgressListener = new DfuProgressListenerAdapter() {
        BleDevice device = new BleDevice();

        @Override
        public void onDeviceConnecting(final String deviceAddress) {
            Log.e("change,,", "onDeviceConnecting");
            Object bleObj = XinfengService.mData.get(deviceAddress);
            final BleDevice device = (BleDevice) bleObj;
            Log.e("change,,", "onDeviceConnecting....." + device.getUpdate_state());

        }

        @Override
        public void onDfuProcessStarting(final String deviceAddress) {
            Log.e("change,,", "onDfuProcessStarting");
            //获得特定的key
            Object bleObj = XinfengService.mData.get(deviceAddress);
            final BleDevice device = (BleDevice) bleObj;
            device.setUpdate_state(104);
            //sendMydevice();
            mdeviceMap.map = XinfengService.mData;
            sendMyDevice(mdeviceMap);

            isUpdating = true;

        }

        @Override
        public void onDeviceDisconnecting(final String deviceAddress) {
            Log.e("change,,", "onDeviceDisconnecting.....");

            Object bleObj = XinfengService.mData.get(deviceAddress);
            final BleDevice device = (BleDevice) bleObj;
            Log.e("change,,", "onDeviceDisconnecting....." + device.getUpdate_state());

            isUpdating = false;

            if (device.getUpdate_state() == 104) {
                device.setUpdate_state(102);
                //清零
                device.setPercentage(0);
                //sendMydevice();
                mdeviceMap.map = XinfengService.mData;
                sendMyDevice(mdeviceMap);
                device.setUpdate_state(0);

                // let's wait a bit until we cancel the notification. When canceled immediately it will be recreated by service again.
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //onTransferCompleted();

                        // if this activity is still open and upload process was completed, cancel the notification
                        final NotificationManager manager = (NotificationManager) MainActivity.mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                        manager.cancel(DfuService.NOTIFICATION_ID);
                    }
                }, 200);
            }
        }

        @Override
        public void onDfuCompleted(final String deviceAddress) {
            Log.e("change", "成功了");
            Object bleObj = XinfengService.mData.get(deviceAddress);
            final BleDevice device = (BleDevice) bleObj;
            device.setPercentage(0);
            device.setUpdate_state(101);
            device.setVersion(0);
            //sendMydevice();
            mdeviceMap.map = XinfengService.mData;
            sendMyDevice(mdeviceMap);
            device.setUpdate_state(0);


            isUpdating = false;

            // let's wait a bit until we cancel the notification. When canceled immediately it will be recreated by service again.
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    //onTransferCompleted();

                    // if this activity is still open and upload process was completed, cancel the notification
                    final NotificationManager manager = (NotificationManager) MainActivity.mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                    manager.cancel(DfuService.NOTIFICATION_ID);
                }
            }, 200);


        }

        @Override
        public void onDfuAborted(final String deviceAddress) {
            Log.e("change", "onDfuAborted");
        }

        @Override
        public void onProgressChanged(final String deviceAddress, final int percent, final float speed, final float avgSpeed, final int currentPart, final int partsTotal) {
            Log.e("change", "onProgressChanged" + "   " + percent);

            Object bleObj = XinfengService.mData.get(deviceAddress);
            final BleDevice device = (BleDevice) bleObj;
            device.setPercentage(percent);
            device.setUpdate_state(103);
            //sendMydevice();
            mdeviceMap.map = XinfengService.mData;
            sendMyDevice(mdeviceMap);
            isUpdating = true;
            //Log.e("MyApplication", "percentage " + "   " + device.getPercentage());

        }

        @Override
        public void onError(final String deviceAddress, final int error, final int errorType, final String message) {
            Log.e("change", "失败了...."+error +"..."+errorType+"..."+message);


            //获得特定的key
            Object bleObj = XinfengService.mData.get(deviceAddress);
            final BleDevice device = (BleDevice) bleObj;
            device.setUpdate_state(102);
            //清零
            device.setPercentage(0);
            //sendMydevice();
            mdeviceMap.map = XinfengService.mData;
            sendMyDevice(mdeviceMap);
            device.setUpdate_state(0);

            isUpdating = false;

            // let's wait a bit until we cancel the notification. When canceled immediately it will be recreated by service again.
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    //onTransferCompleted();

                    // if this activity is still open and upload process was completed, cancel the notification
                    final NotificationManager manager = (NotificationManager) MainActivity.mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                    manager.cancel(DfuService.NOTIFICATION_ID);
                }
            }, 200);
        }
    };

}
