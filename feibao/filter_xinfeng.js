/** 
* Dispaly status of the filters which user has.
*/
'use strict';

import  styles from './styles/style_setting';
import  styles_up from './styles/style_update'
import  styles_filter from './styles/style_filter'
import styles_web from './styles/style_web'
import  tools from './tools'
import  config from './config'
var Filter_content = require ('./filter_content');
var UMeng = require('./UMeng');
var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;
import Title from './title'
import * as Progress from 'react-native-progress';
import Language from './Language'
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  Dimensions,
  PixelRatio,
  TouchableOpacity,
  TouchableWithoutFeedback,
   AsyncStorage,
  NativeModules,
  NetInfo,
  ToastAndroid,
  DeviceEventEmitter,
  NativeAppEventEmitter,
  ListView,
  Platform,
} from 'react-native';
var f01_in = 0;
var f02_in = 0;
var f01_time = 0;
var f02_time = 0;
var sub_currentDevice = null;

var mwidth;// =Dimensions.get ('window').width;
var mheight;// = Dimensions.get('window').height;

var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter');
var BlueToothUtil= require('./BlueToothUtil');
var rate = 0;
var hour = 0;
var min = 0;
var img = require('image!ic_filter_no'); 
var hasFilter = Language.notdetected;
var useNumber = 0;
var filter01_in;

var filter_xinfeng = React.createClass({
  
  getInitialState: function() {
    return {
      filter01_in:0,
      filter01_use_time:0,
      textColor:'#B6B6B6',
    };
  },

  componentWillMount:function(){
    
    BlueToothUtil.sendMydevice();
  },

componentDidMount:function(){

    sub_currentDevice = RCTDeviceEventEmitter.addListener('message_select_item',(val) => {
      if(val == null){
        console.log('val ···············');
      }else {
        filter01_in = val.filter_in;
        console.log('vvv1'+' 滤芯状态：'+filter01_in+' 滤棉使用时长：'+Math.floor(val.use_time/60));
        this._trannum(val.filter_in,Math.floor(val.use_time/60),Math.floor(val.filter_use_time_real / 60));

        
        this.setState({
          // filter01_in:val.filter_in,
          // filter01_use_time:val.use_time,
          deviceid:val.addr,
        });
      }
      

    });


  },

  _trannum :function (status,number,time_real){
    
    console.log('vvv2'+'滤芯状态：'+status+'滤棉使用时长：'+number);

     rate =  Math.floor(time_real/60/45*100);
     hour = Math.floor(number/60);
     min = Math.floor(number%60);
    if(rate>=100){
        rate = 100;
    }
    if(status == 0){
      hasFilter = Language.notdetected+'';
      img = require('image!ic_filter_no');
      this.setState({
        textColor:'#B6B6B6',
      });
    }else{
      if(number<=40*60){
         hasFilter = Language.normal+'';
         img = require('image!ic_filter_normal');
        this.setState({
          textColor:'#2DBAF1',
        });
      }
      if(number>40*60){
         hasFilter = Language.suggestedreplacement+'';
         img = require('image!ic_filter_abnormal'); 
        this.setState({
          textColor:'#f1ab2d',
        });
      }

    }
  },

  componentWillUnmount: function() {
    sub_currentDevice.remove();
  },
  render: function() {
      mwidth = Dimensions.get ('window').width;
      mheight = Dimensions.get('window').height;
      return (
        <View style={styles_filter.viewFilters}>
          <Title title = {Language.filterelement} hasGoBack = {true}  navigator = {this.props.navigator}/>
          <Text style = {{color:'#2DBAF1',fontSize:14,marginTop:18/320*mheight,marginLeft:8/180*mwidth}}>{Language.xinfengairboxfilter}</Text>

        <View style={{width:mwidth}}>
          <View style = {{marginLeft:8/180*mwidth}}>
          <View style = {{flexDirection: 'row',height:50/320*mheight,alignItems:'center',marginTop:15}}>
            <Image style={{resizeMode: 'stretch',height:42/320*mheight,width:30/180*mwidth}} source={img}/>
            <View style={{marginLeft:15/180*mwidth}}>
               <Text style = {{color:'#525252',fontSize:16,}}>
                 {Language.filterState}
                 <Text style = {{color:this.state.textColor,}}>{':'}{hasFilter}</Text>
               </Text>
               <View style = {{marginTop:10,}}>
                    <Text style = {{color:'#b6b6b6',fontSize:13,}}>{Language.hepafilter}</Text>
                    <Text style = {{color:'#b6b6b6',fontSize:13,}}>{Language.usage}{':'}{hour}{' '}{Language.h}{' '}{min}{' '}{Language.min}</Text>
               </View>

               <View style = {{width:118.5/180*mwidth,marginTop:10}}>
                    <View style = {{flex:1, flexDirection: 'row',justifyContent:'flex-end',alignItems: 'center',}}>
                        <Text style = {{color:'#2DBAF1',fontSize:12,}}>{rate}%</Text>
                    </View>
                    <Progress.Bar progress={rate/100} borderWidth = {0} width={118.5/180*mwidth} unfilledColor={'#d8d8d8'} height={3} borderColor={'#d8d8d8'} color = {'#2DBAF1'}/>
               </View>

            </View>
          </View>
          </View>
        </View>


          <TouchableOpacity style = {{height:19/320*mheight,marginTop:95/320*mheight+15,}}
                onPress = {()=>{this.devCheck(this.state.deviceid);}}>
              <View style = {{flex:1, flexDirection: 'row',justifyContent:'flex-end',alignItems: 'center',}}>
                   <Image style={{resizeMode: 'stretch',height:19/320*mheight,width:41/180*mwidth,justifyContent:'center',alignItems: 'center',}} source={require('image!ic_finddevice_b')}>
                     <Text style = {{backgroundColor: '#2DBAF1',color:'#FFFFFF',fontSize:12,}}>{Language.finddevice}</Text>
                   </Image>
              </View>
          </TouchableOpacity>
          <View style = {[styles_filter.bottomView,]}>
            <View style = {styles_filter.uview4}>

                     <TouchableOpacity  onPress={() => {this._gotoCottonPage();UMeng.onEvent('filters_04');}}>
                        <View style={styles_filter.uview5} >
                          <Text allowFontScaling={false} style={styles_filter.text1}>{Language.tutorial}</Text>
                        </View>
                    </TouchableOpacity>

            </View>
            <View style = {styles_filter.uview6}>
 
                     <TouchableOpacity  onPress={() => {this._gotoFilterBuy();UMeng.onEvent('filters_05');}}>
                        <View style={styles_filter.uview5} >
                          <Text allowFontScaling={false} style={styles_filter.text1}>{Language.buy}</Text>
                        </View>
                    </TouchableOpacity>
                
            </View>
          </View> 


        </View>
        );
  },

  _gotoCottonPage:function(){
    this.props.navigator.push({
      id: 'CottonXinfengPage',
    });
  },
  devCheck:function(address){
    BlueToothUtil.devCheck(address);
  },
  _gotoFilterBuy: function() {
    var FILTER_BUY = config.server_base+config.filter_buy;//滤芯购买
    console.log('FILTER_BUY: ' + FILTER_BUY);
    this.props.navigator.push({
      id: 'WebViewPage',
      params:{
          title:Language.filterpurchase,
          url: FILTER_BUY,
          hasGoBack: true,
          webviewStyle:styles_web.webview_protocol,
      }
    });
  },
});

module.exports = filter_xinfeng;
//          <Filter_content status = {parseInt(this.state.filter01_in)} number = {Math.floor(this.state.filter01_use_time/60)}/>