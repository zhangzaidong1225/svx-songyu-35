//忻风改版主页
'use strict'

var RNFS = require('react-native-fs');
var TimerMixin = require('react-timer-mixin');

import SingleWeather from './SingleWeather'
// import VehideIcon from './vehideIcon'
// import VehideModel from './vehideModel'

import XinfengIcon from './XinfengIcon'
import XinfengSlider from './XinfengSlider'
// import XinfengWave from './XinfengWave'
import  config from './config'
import Language from './Language'
import Wave from './Wave'
import React, { Component } from 'react';
import styles_singleweater from './styles/style_singleweather'


import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    Navigator,
    TouchableHighlight,
    TouchableOpacity,
    PixelRatio,
    NativeModules,
    AsyncStorage,
    ToastAndroid,
    NetInfo,
    ListView,
    Platform,
    DeviceEventEmitter,
    NativeAppEventEmitter,
    Alert,
    Easing,
    Animated,
} from 'react-native';
var π = 3.1415926;
var sub_currentDevice;
var sub_wait;
var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter'); /*用于界面组件之间传递消息*/
var BlueToothUtil=require('./BlueToothUtil');
var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;
var timing = Animated.timing;
var animation1 = null;
var speed = 6000;
var isWait = false;
var yf = 1;
var ys = 1;
// var A = 0;
// var savespeed[];
// var savespeed;
// var count = -1;
var XinfengPage = React.createClass({

	getInitialState:function (){
		return {
			navigator : '',
			stop : false,
  		ΔFast : π/20,
  		ΔSlow : π/40, 
       runningDogeTrans1 : new Animated.ValueXY({
            x: 0,
            y: 20
        }),
        runningDogeTrans2 : new Animated.ValueXY({
            x: Dimensions.get('window').width-0.5,
            y:  20
        }), 
        runningDogeTrans3 : new Animated.ValueXY({
            x: -Dimensions.get('window').width+0.5,
            y: 20
        }),
        runningDogeTrans4 : new Animated.ValueXY({
            x: 0,
            y: 20
        }), 
		}
	},
  // timer() {
  //     this.interval = setInterval(() => {
  //       count = count + value;
  //     }  , 100); 
  // },
  componentDidMount:function(){
    // for(var i = 0 ; i <= 1 ; i++){
    //     savespeed[i] = -1;
    // }
    console.log('==speed==componentDidMount');
        //切换设备的监听器
    sub_currentDevice = RCTDeviceEventEmitter.addListener('message_select_item',(val)=>{
      console.log(' 待机和唤醒··1133·'+val);
      // for(var i = 0 ; i <= 1 ; i++){
      //   if(savespeed[i] == -1){
      //     savespeed[i] == val.speed;
      //   }         
      // }
      // if(savespeed[1] != val.speed ){
      //     savespeed = savespeed[0];
      //     savespeed[0] = savespeed[1];
      //     savespeed[1] = val.speed;
      // }
      // if(count == savespeed[0]){
      //   savespeed = savespeed [0];
      // }else{
      //   value = (savespeed[0] - savespeed)/30;
        
      // }

      if(val == null || val.speed <= 0){
      	  isWait = true;
          console.log('== 待机  val.speed '+val.speed );
          //ΔFast : π/20,
          //ΔSlow : π/40,
          //A_value : 0, 
          this.setState({
            stop : true,
          });
      }else{
      	isWait = false;
      	// if(val.speed >= 70){
      	//    this.setState({
       //      A : 20,
       //     });
      	// }else if(val.speed >= 40){
       //     this.setState({
       //      A : 10,
       //     });
      	// }else{
       //     this.setState({
       //      A : 5,
       //     });
      	// }
        ys = -0.48*(val.speed)+80;
        yf = -0.24*(val.speed)+40;
        //A  = parseInt(val.speed/10);
      	this.setState({
         	stop : false,
        });
        //A_value : A, 
        //if(val.speed >= 50){
      		this.setState({
      			ΔFast : π/yf,
      			ΔSlow : π/ys,
      		});
      	//}else{
      		// this.setState({
      		// 	ΔFast : π/40,
      		// 	ΔSlow : π/80, 
      		// });
      	//}
      	// if(val.speed >= 50){
      	// 	this.setState({
      	// 		ΔFast : π/10,
      	// 		ΔSlow : π/20, 
      	// 	});
      	// }else{
      	// 	this.setState({
      	// 		ΔFast : π/20,
      	// 		ΔSlow : π/40, 
      	// 	});
      	// }
      	
      }
      
    });
    sub_wait = RCTDeviceEventEmitter.addListener('slider',(val)=>{
      

      if (val) {
      	 isWait = true;
         speed = 6000;
         this.setState({
         	stop : true,
         });
      }else{//正常
      	 this.setState({
         	stop : false,
         });
      	 isWait = false;
      	 //speed = 10;
      }
      console.log(' 待机和唤醒··11·'+val+''+this.state.stop);
    });
	  this.startAnimation1();
	},

	componentWillMount:function (){
      //this.timer();
	},

	componentWillUnmount(){
		sub_currentDevice.remove();
		sub_wait.remove();
    },
	startAnimation1:function() {
      if(!isWait){
      	      this.state.runningDogeTrans1.setValue({x: 0, y: 20});

		      this.state.runningDogeTrans2.setValue({x: Dimensions.get('window').width-0.5, y: 20});
		      this.state.runningDogeTrans3.setValue({x: -Dimensions.get('window').width+0.5, y: 20});

		      this.state.runningDogeTrans4.setValue({x: 0, y: 20});

      }else{
		      this.state.runningDogeTrans2.setValue({x: 0, y: 20});

		      this.state.runningDogeTrans4.setValue({x: Dimensions.get('window').width-0.5, y: 20});
		      this.state.runningDogeTrans1.setValue({x: -Dimensions.get('window').width+0.5, y: 20});

		      this.state.runningDogeTrans3.setValue({x: 0, y: 20});
      }


      animation1 = Animated.parallel([ 
            timing(this.state.runningDogeTrans1, {
                toValue: {
                    x: -Dimensions.get('window').width,
                    y:20
                },
                duration: speed,
                easing: Easing.linear
            }),     
            timing(this.state.runningDogeTrans2, {
                toValue: {
                    x: 0,
                    y: 20
                },
                duration: speed,
                easing: Easing.linear
            }),
            timing(this.state.runningDogeTrans3, {
                toValue: {
                    x: 0,
                    y: 20
                },
                duration: speed,
                easing: Easing.linear
            }),    
            timing(this.state.runningDogeTrans4, {
                toValue: {
                    x: Dimensions.get('window').width,
                    y: 20
                },
                duration: speed,
                easing: Easing.linear
            }),
      ])
      animation1.start(() => this.startAnimation1());
  },
	render:function (){
		var navigator = this.props.navigator;
    
     if (config.global_language =='en'){
    return (
      <View style = {[styles.container,{alignItems: 'center'}]}>
        <View style = {styles_singleweater.view1}/>
        <XinfengIcon navigator = {this.props.navigator}/>
        <XinfengSlider navigator = {this.props.navigator}/>
        <Wave style = {{position: 'absolute', bottom:deviceHeight * 0.13,height:60,width: deviceWidth,}} height = {60} width = {deviceWidth} 
             ΔFast = {this.state.ΔFast} ΔSlow = {this.state.ΔSlow}  A = {10} stop = {this.state.stop} φx = {0} />
      </View>

    );
     } else {
          return (
      <View style = {[styles.container,{alignItems: 'center'}]}>
        <SingleWeather navigator = {this.props.navigator}/>
        <XinfengIcon navigator = {this.props.navigator}/>
        <XinfengSlider navigator = {this.props.navigator}/>
        <Wave style = {{position: 'absolute', bottom:deviceHeight * 0.13,height:60,width: deviceWidth,}} height = {60} width = {deviceWidth} 
             ΔFast = {this.state.ΔFast} ΔSlow = {this.state.ΔSlow}  A = {10} stop = {this.state.stop} φx = {0} />
      </View>

    );
     }

	},
});
				// <Animated.View style={[{position: 'absolute',borderWidth: 0,}, {
    //                 transform: this.state.runningDogeTrans1.getTranslateTransform()
    //             }]}>
    //                 <Image style = {{height:292/1620*deviceWidth,width:deviceWidth}} source={require('./img/rect1.png')}/>
    //             </Animated.View>
    //             <Animated.View style={[{position: 'absolute',borderWidth: 0,}, {
    //                 transform: this.state.runningDogeTrans2.getTranslateTransform()
    //             }]}>
    //                 <Image style = {{height:292/1620*deviceWidth,width:deviceWidth}} source={require('./img/rect1.png')}/>
    //             </Animated.View>
    //                             <Animated.View style={[{position: 'absolute',borderWidth: 0,}, {
    //                 transform: this.state.runningDogeTrans3.getTranslateTransform()
    //             }]}>
    //                 <Image style = {{height:292/1620*deviceWidth,width:deviceWidth}} source={require('./img/rect2.png')}/>
    //             </Animated.View>
    //             <Animated.View style={[{position: 'absolute',borderWidth: 0,}, {
    //                 transform: this.state.runningDogeTrans4.getTranslateTransform()
    //             }]}>
    //                 <Image style = {{height:292/1620*deviceWidth,width:deviceWidth}} source={require('./img/rect2.png')}/>
    //             </Animated.View>
var styles = StyleSheet.create ({

	container:{
		flexDirection:'column',
		backgroundColor:'#FFFFFF',
		height:deviceHeight * 0.9,
	},
    view:{
		flex:1,
		alignItems:'center',
		justifyContent:'center',
		backgroundColor:'#FFFFFF',
	}
});
module.exports = XinfengPage;
