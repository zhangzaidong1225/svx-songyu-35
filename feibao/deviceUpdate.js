/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 固件升级界面
 */
'use strict';

var RNFS = require('react-native-fs');
var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter'); /*用于界面组件之间传递消息*/

import styles from './styles/style_setting'
import styles_feedback from './styles/style_feedback'
import  config from './config'
import style_deviceupdate from './styles/style_deviceupdate'
// var AsyncStorage = require('./AsyncStorage');
var UMeng = require('./UMeng');
var DownLoad = require('./download');
import Language from './Language'
import Title from './title'
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  Dimensions,
  PixelRatio,
  TouchableOpacity,
  TouchableWithoutFeedback,
  AsyncStorage,
  NativeModules,
  NetInfo,
  Alert,
  ToastAndroid,
  // DeviceEventEmitter,
  NativeAppEventEmitter,
  ListView,
  BackAndroid,
} from 'react-native';

var type = 1;
var deviceWidth=Dimensions.get('window').width;
var deviceHeight=Dimensions.get('window').height;
// var sub_ble_state1 = null;
var sub_ble_state = null;
var sub_currentDevice = null;
var device_selected = {type:'',name:''};
var text_color = '#aa33cc';

//var REQUIRE_URL = 'http://feibao.esh001.com/iface/system/dev_update';

//var DEV_URL = config.server_base + config.dev_update_uri;

//var DeviceVersion_URL = 'http://feibao.esh001.com/iface/system/dev_update';

//tmp
const testDir1Path_tmp = RNFS.DocumentDirectoryPath + '/upload_tmp';
//tmp存储路径
const testFile3Path_tmp = RNFS.DocumentDirectoryPath + '/upload_tmp/dev_update.zip';
//存储目录
const testDir1Path = RNFS.DocumentDirectoryPath + '/upload';
const testFile3Path = RNFS.DocumentDirectoryPath + '/upload/dev_update.zip';

//单品
const testDir1PathSingle_tmp = RNFS.DocumentDirectoryPath + '/single/upload_tmp';
//tmp存储路径
const testFile3PathSingle_tmp = RNFS.DocumentDirectoryPath + '/single/upload_tmp/dev_update.zip';
//存储目录
const testDir1PathSingle = RNFS.DocumentDirectoryPath + '/single/upload';
const testFile3PathSingle = RNFS.DocumentDirectoryPath + '/single/upload/dev_update.zip';

//车载
const testDir1PathCar_tmp = RNFS.DocumentDirectoryPath + '/vehide/upload_tmp';
//tmp存储路径
const testFile3PathCar_tmp = RNFS.DocumentDirectoryPath + '/vehide/upload_tmp/dev_update.zip';
//存储目录
const testDir1PathCar = RNFS.DocumentDirectoryPath + '/vehide/upload';
const testFile3PathCar = RNFS.DocumentDirectoryPath + '/vehide/upload/dev_update.zip';

var xinStatus = 1;
var singleStatus = 3;
var carStatus = 2;

var versionInfo = '';
var versionString = '';
var version_msg = '';

var BlueToothUtil=require('./BlueToothUtil');
var alertMessage = '请链接网络,退出客户端重新进入   ';

var DeviceUpdate = React.createClass({

  getInitialState(){

  //忻风
  AsyncStorage.getItem('local_version_msg_xin',(err,result) => {

	//没网或者后台数据清空 
    if (result == null){
      this.setState({version_xin:'',localVer_xin:'',update_msg_xin:''});
    }else {
       this.setState({version_xin:result,localVer_xin:result,update_msg_xin:Language.xinfenglatestversion});
       console.log(this.state.version_xin+'```local_version_xin');
    }

    });

    AsyncStorage.getItem('local_version_info_xin',(err,result) => {

        if (result== null){
            this.setState({info_xin:result});
          console.log(this.state.info_xin+'断网安装'); 
          }
           this.setState({info_xin:result});
    });

    //单品
  AsyncStorage.getItem('local_version_msg_single',(err,result) => {

  //没网或者后台数据清空 
    if (result == null){
      this.setState({version_single:'',localVer_single:'',update_msg_single:''});
    }else {
       this.setState({version_single:result,localVer_single:result,update_msg_single:Language.singlelatestver});
       console.log(this.state.version_single+'local_version_single');
    }

    });

    AsyncStorage.getItem('local_version_info_single',(err,result) => {

        if (result== null){
            this.setState({info_single:result});
          console.log(this.state.info_single+'断网安装'); 
          }
           this.setState({info_single:result});
    });

  //车载 
  AsyncStorage.getItem('local_version_msg_car',(err,result) => {

  //没网或者后台数据清空 
    if (result == null){
      this.setState({version_car:'',localVer_car:'',update_msg_car:''});
    }else {
       this.setState({version_car:result,localVer_car:result,update_msg_car:Language.vehidelatestver});
       console.log(this.state.version_car+'local_version_car');
    }

    });

    AsyncStorage.getItem('local_version_info_car',(err,result) => {

        if (result== null){
            this.setState({info_car:result});
          console.log(this.state.info_car+' 断网安装'); 
          }
           this.setState({info_car:result});
    });

    return {
      dataSource: new ListView.DataSource({
          rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      version_xin:'',
      update_msg_xin:'',
      info_xin:'',
      localVer_xin:'',

      version_single:'',
      localVer_single:'',
      info_single:'',
      update_msg_single:'',

      version_car:'',
      localVer_car:'',
      info_car:'',
      update_msg_car:'',

      md5_xin:'',
      md5_single:'',
      md5_car:'',

      url_xin:'',
      url_single:'',
      url_car:'',

      code:1,
      msg:'',
      url:'',
    
      connect_list:[],
      connecting_list:[],
      finish:false,
      use_upload_list:[],
      unuse_upload_list:[],
      last_version_list:[],
      cover:60,
      no_wifi:true,
      click_devicetype:null,
      click_devicename:null,
    }
  },

  componentWillUnmount:function(){
    // sub_ble_state1.remove();
    sub_ble_state.remove();
    sub_currentDevice.remove();
    NetInfo.removeEventListener(
        'change',
        this._handleConnectionInfoChange
    );

    versionInfo = '';
    versionString = '';
    version_msg = '';
  },

  _handleConnectionInfoChange: function(connectionInfo) {

    console.log(connectionInfo+'是否联网');
    this.fetchData(xinStatus);
    this.fetchData(singleStatus);
    this.fetchData(carStatus);
    // this.fetchDataSingle(singleStatus);
    // this.fetchDataCar(carStatus);
  },
    componentDidMount:function(){

 	   RCTDeviceEventEmitter.emit('DeviceUpload','true');
     sub_currentDevice = RCTDeviceEventEmitter.addListener('msg_select_item_update',(val)=>{
      if(val == null){
      }else{
          this.setState({
            click_devicetype:val.type,
            click_devicename:val.name,
            select_device:val.id,
          });
          if(val.type == '0'){
            versionInfo = this.state.info_xin;
            versionString = this.state.version_xin;
            version_msg = this.state.update_msg_xin;

          }else if(val.type == '1'){
            versionInfo = this.state.info_single;
            versionString = this.state.version_single;
            version_msg = this.state.update_msg_single;
          }else{
            versionInfo = this.state.info_car;
            versionString = this.state.version_car;
            version_msg = this.state.update_msg_car;
          }
          console.log('==type:'+val.type+' name:'+val.name);
      }
      
    });
        //检测网络变化
    NetInfo.addEventListener(
        'change',
        this._handleConnectionInfoChange
    );
    //this.fetchData();

    sub_ble_state = NativeAppEventEmitter.addListener(
         'ble_state',
         (data) => {
          this.init_data(data);
         });


    // sub_ble_state1 = DeviceEventEmitter.addListener(
    //      'ble_state',
    //      (data) => {
    //       this.init_data(data);
          
    //      }
    //  );

     BlueToothUtil.sendMydevice();   

    this.fetchData(xinStatus);
    this.fetchData(singleStatus);
    this.fetchData(carStatus);

    // this.fetchDataSingle(singleStatus);
    // this.fetchDataCar(carStatus);


  },

  init_data:function(data){
          var tmp_data = [];
          var tmp_data2 = [];
          var tmp_connect_list = [];
          var tmp_connecting_list = [];
          var tmp_use_upload_list = [];
          var tmp_unuse_upload_list = [];
          var tmp_last_version_list = [];

          for (var i = 0; i < data.dev_list.length; i++) {


            if(data.dev_list[i].ismatchd && data.dev_list[i].isconnectd == 1){
              tmp_data2.push({id:data.dev_list[i].addr, name:data.dev_list[i].name,type:data.dev_list[i].type});
            }else{
              //tmp_data.push({id:data.dev_list[i].addr, name:data.dev_list[i].name});
            }

            //已连接
            if(data.dev_list[i].isconnectd == 1){

              if (data.dev_list[i].type == 0){

                      var version1 = data.dev_list[i].version;
                      var version2 = parseInt(this.state.version_xin);
                      var cover1 = data.dev_list[i].battery_power;

                      if ((version1 < version2) && (cover1 > 50)){
                        tmp_use_upload_list.push(data.dev_list[i].addr);
                      }
                      //不可升级
                      if ((version1 < version2) && (cover1 <= 50)){
                        tmp_unuse_upload_list.push(data.dev_list[i].addr);
                      }
                      //最新版本
                      if ((this.state.version_xin == '') || (version1 == version2) || (version1 > version2) ){
                        tmp_last_version_list.push(data.dev_list[i].addr);
                      }

              } else if (data.dev_list[i].type == 1){

                      var version1 = data.dev_list[i].version;
                      var version2 = parseInt(this.state.version_single);
                      var cover1 = data.dev_list[i].battery_power;

                      if ((version1 < version2) && (cover1 > 50)){
                        tmp_use_upload_list.push(data.dev_list[i].addr);
                      }
                      //不可升级
                      if ((version1 < version2) && (cover1 <= 50)){
                        tmp_unuse_upload_list.push(data.dev_list[i].addr);
                      }
                      //最新版本
                      if ((this.state.version_single == '') || (version1 == version2) || (version1 > version2) ){
                        tmp_last_version_list.push(data.dev_list[i].addr);
                      }
              } else if (data.dev_list[i].type == 2){

                      var version1 = data.dev_list[i].version;
                      var version2 = parseInt(this.state.version_car);
                      var cover1 = data.dev_list[i].battery_power;

                      if ((version1 < version2) && (cover1 > 50)){
                        tmp_use_upload_list.push(data.dev_list[i].addr);
                      }
                      //不可升级
                      if ((version1 < version2) && (cover1 <= 50)){
                        tmp_unuse_upload_list.push(data.dev_list[i].addr);
                      }
                      //最新版本
                      if ((this.state.version_car == '') || (version1 == version2) || (version1 > version2) ){
                        tmp_last_version_list.push(data.dev_list[i].addr);
                      }
              }
            } else if(data.dev_list[i].isconnectd == 2){
              tmp_connecting_list.push(data.dev_list[i].addr);
            }
          };

          this.setState({
            //dataSource: this.state.dataSource.cloneWithRows(tmp_data),
            dataSource: this.state.dataSource.cloneWithRows(tmp_data2),
            connect_list:tmp_connect_list,
            connecting_list:tmp_connecting_list,
            use_upload_list:tmp_use_upload_list,
            unuse_upload_list:tmp_unuse_upload_list,
            last_version_list:tmp_last_version_list,
          });
  },



  fetchData:function (status){

   NetInfo.isConnected.fetch().done(
        (isConnected) => { 
          if (isConnected) {
    console.log('有网路啦');
    var DEV_URL = config.server_base + config.dev_update_uri+'?item_id='+status+'&version='+config.version+'&platform='+config.platform+'&phone_modal='+config.phone_modal+'&language='+config.language;

    console.log('DEV_URL: ' + DEV_URL);     
    fetch (DEV_URL)
      .then ((response) => response.json())
      .then((responseData) => {

        if (status == 1){
          if(responseData.code == 0)
          {
            this.setState({
              version_xin:responseData.data.ver,
              url_xin:responseData.data.url,
              md5_xin:responseData.data.md5,
              info_xin: responseData.data.info,
            });

            if (this.state.localVer_xin == null || this.state.localVer_xin == ''){
               this.download_md5(xinStatus,this.state.url_xin,testDir1Path_tmp,testFile3Path_tmp,testDir1Path,testFile3Path,this.state.md5_xin);
            }
            if (this.state.version_xin > this.state.localVer_xin && this.state.localVer_xin > 0 ){
              this.download_md5(xinStatus,this.state.url_xin,testDir1Path_tmp,testFile3Path_tmp,testDir1Path,testFile3Path,this.state.md5_xin);
            }
          }
          else
          {
            this.setState({
              code:responseData.code,
              msg:responseData.msg,
            });
          }
        } 
        else if (status == 3) {
        if(responseData.code == 0)
          {
            this.setState({
              version_single:responseData.data.ver,
              url_single:responseData.data.url,
              md5_single:responseData.data.md5,
              info_single: responseData.data.info,
            });
            if (this.state.localVer_single == null || this.state.localVer_single == ''){
               this.download_md5(singleStatus,this.state.url_single,testDir1PathSingle_tmp,testFile3PathSingle_tmp,testDir1PathSingle,testFile3PathSingle,this.state.md5_single);
            }
            if (this.state.version_single > this.state.localVer_single && this.state.localVer_single > 0 ){
              this.download_md5(singleStatus,this.state.url_single,testDir1PathSingle_tmp,testFile3PathSingle_tmp,testDir1PathSingle,testFile3PathSingle,this.state.md5_single);
            }
          }
          else
          {
            this.setState({
              code:responseData.code,
              msg:responseData.msg,
            });
          }
        }
        else if (status == 2){

          if(responseData.code == 0)
            {
              this.setState({
                version_car:responseData.data.ver,
                url_car:responseData.data.url,
                md5_car:responseData.data.md5,
                info_car: responseData.data.info,
              });

              if (this.state.localVer_car == null || this.state.localVer_car == ''){
                 this.download_md5(carStatus,this.state.url_car,testDir1PathCar_tmp,testFile3PathCar_tmp,testDir1PathCar,testFile3PathCar,this.state.md5_car);
              }

              if (this.state.version_car > this.state.localVer_car && this.state.localVer_car > 0 ){
                this.download_md5(carStatus,this.state.url_car,testDir1PathCar_tmp,testFile3PathCar_tmp,testDir1PathCar,testFile3PathCar,this.state.md5_car);
              }
            }
            else
            {
              this.setState({
                code:responseData.code,
                msg:responseData.msg,
              });
            }
        }

      })
      .catch((error) => {
         //ToastAndroid.show(''+error,ToastAndroid.SHORT);
      });
          }
        }
    );
 },



 download_md5:function(status,url,testDir1Path_tmp,testFile3Path_tmp,testDir1Path,testFile3Path,md5_msg) {
            //临时下载目录

            DownLoad.mkdirTest(testDir1Path_tmp);

            DownLoad.downloadFileTest(url,testFile3Path_tmp,() => {

             BlueToothUtil.verifyInstallPackage(testFile3Path_tmp,(message) => {

              if (md5_msg === message ){

                  DownLoad.exists(testFile3Path, (result) =>{
                    console.log('检测文件是否存在' + result);
                    if (result) {
                      DownLoad.deleteDirTest(testDir1Path);
                    }
                    DownLoad.mkdirTest(testDir1Path);
                    DownLoad.moveFile(testFile3Path_tmp,testFile3Path);

                    if (status == 1){
                      this.setState({
                        localVer_xin:this.state.version_xin,
                        update_msg_xin:Language.xinfenglatestversion,
                        shot:'false'}); 

                      AsyncStorage.setItem('local_version_msg_xin','' + this.state.version_xin,(err) => {});  
                      AsyncStorage.setItem('local_version_info_xin','' + this.state.info_xin,(err) => {});
                      AsyncStorage.setItem('shot_click',''+this.state.shot,(err) => {}); 

                      console.log(this.state.localVer_xin+'```忻风本地版本1.0');
                      console.log(this.state.info_xin+'```忻风本地版本1.0');
                      console.log(this.state.shot+'```忻风本地版本1.0shot');
                    }else if (status == 3){

                      this.setState({
                        localVer_single:this.state.version_single,
                        update_msg_single:Language.singlelatestver,
                      }); 

                      AsyncStorage.setItem('local_version_msg_single','' + this.state.version_single,(err) => {});  
                      AsyncStorage.setItem('local_version_info_single','' + this.state.info_single,(err) => {});

                      console.log(this.state.localVer_single+'```单品本地版本1.0');
                      console.log(this.state.info_single+'```单品本地版本1.0');

                    }else if (status == 2){

                      this.setState({
                        localVer_car:this.state.version_car,
                        update_msg_car:Language.vehidelatestver,
                      }); 

                      AsyncStorage.setItem('local_version_msg_car','' + this.state.version_car,(err) => {});  
                      AsyncStorage.setItem('local_version_info_car','' + this.state.info_car,(err) => {});

                      console.log(this.state.localVer_car+'```车载本地版本1.0');
                      console.log(this.state.info_car+'```车载本地版本1.0');
                    }
                  });
              }
              else {
                console.log('文件下载失败');
                DownLoad.deleteDirTest(testDir1Path_tmp);
                if (status == 1){
                  this.setState({localVer_xin:'',shot:'true',info_xin:''});
                  AsyncStorage.setItem('local_version_msg_xin',''+this.state.localVer_xin,(err) => {});  
                  AsyncStorage.setItem('local_version_info_xin',''+this.state.info_xin,(err) => {});
                  AsyncStorage.setItem('shot_click',''+this.state.shot,(err) => {}); 
                } else if (status == 3){
                this.setState({localVer_single:'',info_single:''});
                 AsyncStorage.setItem('local_version_msg_single',''+this.state.localVer_single,(err) => {});  
                 AsyncStorage.setItem('local_version_info_single',''+this.state.info_single,(err) => {});
                }else if (status == 2){
                this.setState({localVer_car:'',info_car:''});
                 AsyncStorage.setItem('local_version_msg_car',''+this.state.localVer_car,(err) => {});  
                  AsyncStorage.setItem('local_version_info_car',''+this.state.info_car,(err) => {});
                }
              } 
       
            });
          });
  },


  render(){
  var navigator = this.props.navigator;

  if (!this.state.no_wifi){
    return (
      <View style = {{flex:1,backgroundColor:'white'}}>
                    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center',backgroundColor: 'white',width:deviceWidth,height:deviceHeight*0.8}}>
              <Text allowFontScaling={false} style={{fontSize: 20,textAlign: 'center',marginBottom: 2,color:'red'}}>
                  暂无可用更新
              </Text>
          </View>
       
      </View>
    );
  }
  return  this._render1();

    },

_render1:function() {
    var type_img,image_style,type_name;
    if(this.state.click_devicetype == '0'){
        type_img = require('image!ic_xinfeng');
        image_style = {height:15/320 * deviceHeight,width:15/320 * deviceHeight,resizeMode:'stretch'};
        type_name = Language.xinfeng+'';
    }else if(this.state.click_devicetype == '1'){
        type_img = require('image!ic_single');
        image_style = {height:15/320 * deviceHeight,width:15/320 * deviceHeight,resizeMode:'stretch'};
        type_name = Language.single+'';
    }else if(this.state.click_devicetype == '2'){
        type_img = require('image!ic_car');
        image_style = {height:15/320 * deviceHeight,width:15/320 * deviceHeight,resizeMode:'stretch'};
        type_name = Language.vehide+'';
    }else{
        type_img = require('image!kong');
        image_style = {height:22,width:22,resizeMode:'stretch'};
        type_name = '';
    }

    return (

      <View
            style = {style_deviceupdate.container}>

            <Title title = {Language.firmwareupdate} hasGoBack = {true}  navigator = {this.props.navigator}/>

                <View style = {[style_deviceupdate.line,{marginTop:15}]}/>

                <View style = {style_deviceupdate.listview}>
                  <ListView
                          dataSource={this.state.dataSource}
                          renderRow={this.renderDevice}
                          style={{marginLeft:20,marginRight:20,}}
                          removeClippedSubviews={false}/>
                </View>
                <View style = {{flexDirection: 'row',justifyContent:'center',alignItems:'center',width:deviceWidth,height:40,marginTop:20}}>
                  <Image style={image_style} source={type_img}/>
                  <Text style={{color:'#2AB9F1',}}>{" "}{type_name}</Text>
                </View>
                <View
                  style = {style_deviceupdate.view1}>
                    <Text allowFontScaling={false} style = {{fontSize:15,color:'#2AB9F1',}}>{version_msg}{versionString}</Text>
                </View>


                <View style = {style_deviceupdate.infoview}>
                <ScrollView style = {style_deviceupdate.infoscroll} scrollEnabled={true}>
            
                      <View style={style_deviceupdate.infoview1}>
                        <View>
                          <Text allowFontScaling={false} style={{fontSize:15,color:'#2AB9F1',}}>{versionInfo}</Text>

                        </View>
                      </View>
         
                </ScrollView>
                </View>


                <View style = {style_deviceupdate.button}>

                      <View style={style_deviceupdate.buttonview1}>
                        <TouchableOpacity
                            onPress = {() => {this.props.navigator.pop(); UMeng.onEvent('deviceUpdate_03');}}>
                           <View style = {style_deviceupdate.buttonview2}>
                            <Text allowFontScaling={false} style={{		textAlign:'center',fontSize:18,
                            		color:'#FFFFFF',}}>{Language.unupgrade}</Text>
                           </View>
                        </TouchableOpacity>
                      </View>
                </View>
              
      </View>
    );
},

          // else  if (this.state.connecting_list.indexOf(device.id) >= 0){
          //   return <Item3 device = {device} navigator = {this.props.navigator}/>
          // }else {
          //    return <Item device = {device} navigator = {this.props.navigator}/>
          // } 

    /*已配对设备*/
  renderDevice: function(device) {
    if (this.state.use_upload_list.indexOf(device.id) >=0){
          return <Item4 device = {device} navigator = {this.props.navigator}/>
    }else if (this.state.unuse_upload_list.indexOf(device.id)>= 0){
          return <Item5 device = {device} navigator = {this.props.navigator}/>
    }else if (this.state.last_version_list.indexOf(device.id) >= 0){
         return <Item6 device = {device} navigator = {this.props.navigator}/>
    }       
},


});

//当前为最新版本
var Item6 = React.createClass({

render(){
  console.log('==item6==');
var navigator = this.props.navigator;
    var type_img,image_style;
    if(this.props.device.type == 0){
        type_img = require('image!ic_xinfeng');
        image_style = {height:15/320 * deviceHeight,width:15/320 * deviceHeight,resizeMode:'stretch'};
    }else if(this.props.device.type == 1){
        type_img = require('image!ic_single');
        image_style = {height:15/320 * deviceHeight,width:15/320 * deviceHeight,resizeMode:'stretch'};
    }else{
        type_img = require('image!ic_car');
        image_style = {height:15/320 * deviceHeight,width:15/320 * deviceHeight,resizeMode:'stretch'};
    }
    device_selected.type = this.props.device.type+'';
    device_selected.name = this.props.device.name;
    console.log('==111type:'+device_selected.type+' 111name:'+device_selected.name);
    return (

      <TouchableOpacity onPress={()=>{this.renderMovie(this.props.device);}}>
          <View style={{flexDirection: 'row',marginLeft:10,marginRight:0,alignItems: 'center',height:40,}}>
          <View style = {{width:13,marginLeft:5,alignItems: 'center',justifyContent:'center'}}>
              <Image style={image_style} source={type_img}/>
          </View>
            <Text allowFontScaling={false} style={{fontSize:13,color:'#969696',marginLeft:10/180 * deviceWidth}}>{this.props.device.name}    </Text>
            <Text allowFontScaling={false} style={{position:'absolute',right:10,marginVertical:12,fontSize:13,color:'#969696',}}>{Language.latestdevversion}</Text>
        </View>

          <View style={style_deviceupdate.line1}/>
          </TouchableOpacity>
    );
},

    renderMovie: function(device) {
      console.log(device);
      RCTDeviceEventEmitter.emit('msg_select_item_update',device); 
    },

});

//电量过低无法升级
var Item5 = React.createClass({

render(){
  console.log('==item5==');
var navigator = this.props.navigator;
    var type_img,image_style;
    if(this.props.device.type == 0){
        type_img = require('image!ic_xinfeng');
        image_style = {height:15/320 * deviceHeight,width:15/320 * deviceHeight,resizeMode:'stretch'};
    }else if(this.props.device.type == 1){
        type_img = require('image!ic_single');
        image_style = {height:15/320 * deviceHeight,width:15/320 * deviceHeight,resizeMode:'stretch'};
    }else{
        type_img = require('image!ic_car');
        image_style = {height:15/320 * deviceHeight,width:15/320 * deviceHeight,resizeMode:'stretch'};
    }
    text_color = '#969696';
    device_selected.type = this.props.device.type+'';
    device_selected.name = this.props.device.name;
    console.log('==11type:'+device_selected.type+' 11name:'+device_selected.name);
    return (
       <TouchableOpacity onPress={()=>{this.renderMovie(this.props.device);}}>
 
          <View style={{flexDirection: 'row',marginLeft:10,marginRight:0,alignItems: 'center',height:40,width:deviceWidth-70,}}>
            <View style = {{flex:1,flexDirection: 'row',alignItems: 'center',}}>
                <View style = {{width:13,marginLeft:5,alignItems: 'center',justifyContent:'center'}}>
                    <Image style={image_style} source={type_img}/>
                </View>
                <Text allowFontScaling={false} style={{fontSize:13,color:text_color,marginLeft:10/180 * deviceWidth}}>{this.props.device.name}    </Text>
                </View>
            <View style = {{flex:1,alignItems: 'center',height:40,}}>
                <Text allowFontScaling={false} style={{position:'absolute',right:10,marginVertical:12,fontSize:13,color:'#969696',}}>{Language.powerlow}</Text>
            </View>
          </View>
          <View style={style_deviceupdate.line1}/>
      </TouchableOpacity>
    );
},

    renderMovie: function(device) {
      console.log(device);
      text_color = '#2AB9F1';
      RCTDeviceEventEmitter.emit('msg_select_item_update',device); 
    },

});

//可升级设备
var Item4 = React.createClass({

render(){
  console.log('==item4==');
var navigator = this.props.navigator;
    var type_img,image_style;
    if(this.props.device.type == 0){
        type_img = require('image!ic_xinfeng');
        image_style = {height:15/320 * deviceHeight,width:15/320 * deviceHeight,resizeMode:'stretch'};
    }else if(this.props.device.type == 1){
        type_img = require('image!ic_single');
        image_style = {height:15/320 * deviceHeight,width:15/320 * deviceHeight,resizeMode:'stretch'};
    }else{
        type_img = require('image!ic_car');
        image_style = {height:15/320 * deviceHeight,width:15/320 * deviceHeight,resizeMode:'stretch'};
    }

    device_selected.type = this.props.device.type+'';
    device_selected.name = this.props.device.name;
     console.log('==1type:'+device_selected.type+' 1name:'+device_selected.name);
    return (
      <View >
          <View style={{flexDirection: 'row',marginLeft:10,marginRight:0,alignItems: 'center',height:40,}}>
          <TouchableOpacity style = {{flex:1,flexDirection: 'row',alignItems: 'center',}}  onPress={()=>{this.renderMovie(this.props.device);}}>
              <View style = {{width:13,marginLeft:5,alignItems: 'center',justifyContent:'center'}}>
                  <Image style={image_style} source={type_img}/>
              </View>
            <Text allowFontScaling={false} style={{fontSize:13,color:'#969696',marginLeft:10/180 * deviceWidth}}>{this.props.device.name}</Text>
          </TouchableOpacity>
          <TouchableOpacity style = {{flex:1.5,alignItems: 'center',height:40,}} onPress={()=> {this.renderMovie2(this.props.device);UMeng.onEvent('deviceUpdate_06');}}>
              <Text allowFontScaling={false} style={{position:'absolute',right:10,marginVertical:10,fontSize:13,color:'#50BCA1',justifyContent:'flex-end'}}>{Language.avaliablefirmwareupdate}</Text>
              <Image style={{height:13,width:7.5,position:'absolute',marginVertical:13.5,right:0,resizeMode:'stretch',justifyContent:'center'}} source={require('image!ic_next')}/>
          </TouchableOpacity>
        </View>
          <View style={style_deviceupdate.line1}/>
          </View>
    );
},
    renderMovie: function(device) {
      console.log(device);
      RCTDeviceEventEmitter.emit('msg_select_item_update',device); 
    },
 renderMovie2: function(device) {

      this.props.navigator.push({
     id: 'DeviceUpload',
     params:{
         deviceid:device.id,
         devicename:device.name,
         devicetype:device.type,
        }
     });
  },

});


module.exports = DeviceUpdate;
