/** 
* Dispaly status of the filters which user has.
*/
'use strict';


import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Platform,
} from 'react-native';

 
import Picker from 'react-native-wheel-picker'
import styles_wheeltimerpicker from './styles/style_wheeltimerpicker'
import Language from './Language'
var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter');



var PickerItem = Picker.Item;

var mheight = Dimensions.get('window').height;


var WheelTimerPicker = React.createClass({

  getDefaultProps: function() {
    return {
      value : '10',
      showFlag : true,
    };

  },

  getInitialState: function() {
    return {
      selectedItem : 2,
      // itemList: ['刘备', '张飞', '关羽', '赵云', '黄忠', '马超', '魏延', '诸葛亮']
      itemList:['10','15', '20', '25', '30', '40', '50', '60', '90', '120']
    }
  },

  componentDidMount: function() {
  },

  componentWillMount: function() {
    if (this.state.itemList.indexOf(this.props.value) == -1) {
      this.setState({
        selectedItem: this.state.itemList.indexOf('20'),
      })
    } else {
      this.setState({
        selectedItem: this.state.itemList.indexOf(this.props.value),
      })
    }

  },

  componentWillUnmount: function() {

  },
 
  onPikcerSelect: function(index) {
    this.setState({
      selectedItem: index,
    })
  },
 
  onAddItem: function() {
    var name = '司马懿'
    if (this.state.itemList.indexOf(name) == -1) {
      this.state.itemList.push(name)
    }
    this.setState({
      selectedItem: this.state.itemList.indexOf(name),
    })
  },

  close : function() {
    RCTDeviceEventEmitter.emit('wheel_timer_picker', this.state.itemList[this.state.selectedItem]);
    RCTDeviceEventEmitter.emit('wheel_timer_picker_close');
  },

        //   <View style={{position:'absolute', width: 200, height: 180,justifyContent: 'center',alignItems: 'center',}}>
        //   <Text style={{color: 'white',fontSize:24}}>
        //     {content}
        //   </Text>
        // </View>
 
  render: function() {
    //if(this.props.type!=1){
     // this.close();
    //}
    return (
      <View style={[styles_wheeltimerpicker.container, this.props.showFlag?{}:{marginTop : -200, width : 0, height : 0}]}>

        <View style = {[styles_wheeltimerpicker.picker, {backgroundColor : 'white',}, this.props.showFlag?{}:{width : 0, height : 0}]}>
          <Picker style={[styles_wheeltimerpicker.picker, this.props.showFlag?{}:{width : 0, height : 0}]}
            selectedValue={this.state.selectedItem}
            itemStyle={Platform.OS === 'android'? {color:'#2AB9F1',} : [{color:'#2AB9F1',}, (this.props.showFlag?{height : mheight * 71.5 / 320}:{width : 0, height : 0})]}
            onValueChange={(index) => this.onPikcerSelect(index)}>
              {this.state.itemList.map((value, i) => (
                <PickerItem label={value} value={i} key={'money'+value}/>
              ))}
          </Picker>
        </View>

        <TouchableOpacity activeOpacity = {1} style = {[styles_wheeltimerpicker.touchableOpacity, this.props.showFlag?{}:{width : 0, height : 0, marginTop : 0}]} onPress={() => {this.close();}}>
          <Text style = {[styles_wheeltimerpicker.text, this.props.showFlag?{}:{width : 0, height : 0}]}>
             {Language.ok}
          </Text>
        </TouchableOpacity>

      </View>
    );
  },

});

        // <Text style={{margin: 20, color: 'black',}}>
        //   你最喜欢的是：{this.state.itemList[this.state.selectedItem]}
        // </Text>
 
        // <Text style={{margin: 20, color: 'black'}}
        //     onPress={this.onAddItem}>
        //   怎么没有司马懿？
        // </Text>



module.exports = WheelTimerPicker;