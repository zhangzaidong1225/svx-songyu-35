/**
* webview封装页面
*/

'use strict';

import tools from './tools'
import Title from './title'
import style_register from './styles/style_register'
import styles_web from './styles/style_web'

import  config from './config'
import Language from './Language'
var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter');
var ProgressBar = require('react-native-progress-bar');
var WEBVIEW_REF = 'webview';
var SDIP_CONDITION = 'vida';
var SDIP_BROWSER = 'brow';
var localType=1;//判断是首次加载或者刷新，还是内部跳转链接

import React, { Component } from 'react';
import {
  StyleSheet,
  Dimensions,
  View,
  Text,
  NetInfo,
  WebView,
  Platform,
  Image,
  // LinkingIOS,
  Linking,
  IntentAndroid,
  TouchableOpacity
  } from 'react-native';


var deviceWidth = Dimensions.get('window').width;
var deviceHeight = Dimensions.get('window').height;
// var DEFAULT_URL = 'about:blink';
var DEFAULT_URL = 'about:blink';
var rctDeviceEventEmitter = null;
var skipBrowserEventEmitter = null;
var health_uri = config.server_base+config.health_uri+'?version='+config.version+'&platform='+config.platform+'&phone_modal='+config.phone_modal+'&language='+config.language;  //'https://baidu.com';
var timeout = new Date().getTime();

var Utils = require('./utils')

var WebViewPage = React.createClass({

  getDefaultProps:function () {
      return {
        url:'about:blink',
        title:'',
        hasGoBack: false,
        webviewStyle: View.propTypes.style,
        backAddress: null,
        hasProgressBar: true,
      };
    },

	getInitialState:function (){
    return {
      // number:1,
      _url:'about:blink',
      _urlTitle: '',
      isError: false,
      errorMessage: Language.network_connect_failed+'',
      progress: 0.1,
      hideProgressBarFlag: true,
    };
  },
  	
  componentWillMount: function() {
    if (Platform.OS === 'android') {
      Utils.setInputModeForWebView();
    }
      DEFAULT_URL = this.props.url;
      // console.log('--------网页地址:' + DEFAULT_URL);
      this.fetchData();
      this._initProgressBar();

      // this._fetch();
    },  


	render: function() {
/*    this.progressFresh && clearTimeout(this.progressFresh);
    if (this.props.hasProgressBar) {

      if (this.state.progress < 0.9) {
        this.progressFresh = setTimeout((function() {
          if (this.state.progress < 0.8) {
            this.setState({ 
              progress: this.state.progress + 0.2,
              hideProgressBarFlag: false,
            });
          } else {
            this.setState({ progress: 0.95});
          }
          
        console.log('--------webviewpage progress:' + this.state.progress);
        }).bind(this), this.state.hideProgressBarFlag ? 200 : 500);
      } else {
        if (!this.state.hideProgressBarFlag) {
          this.progressFresh = setTimeout((function() {
            this._hideProgressBar();
            console.log('--------webviewpage progress:' + this.state.progress);
          }).bind(this), 4000);
        }
      }
      if (this.hideProgressBarFlag) {
        this.progressFresh && clearTimeout(this.progressFresh);
        this.progressEnd && clearTimeout(this.progressEnd);
      }
    }*/
		if (this.state.isError){
          return (
              <View style = {style_register.container}>
                <Title title = {this.props.title} hasGoBack = {this.props.hasGoBack} 
                    backAddress = {this.props.backAddress} navigator = {this.props.navigator}/>
                  <TouchableOpacity 
                    onPress = {() => {this._refresh()}}>
                    <View style={{
                        
                          flexDirection: 'column',
                          justifyContent: 'center',
                          alignItems: 'center',
                          backgroundColor:'white',
                          height:deviceHeight/1.5}}>
                    
                      <Image style={{width:150, height:150,resizeMode: 'stretch'}} source={require('image!no_net')}/>
                      <Text allowFontScaling={false} style={{fontSize:16,textAlign: 'center',marginTop:10,color:'#a2bcfa'}}>
                          {this.state.errorMessage}
                      </Text>
                    </View>

                  </TouchableOpacity>
              </View>



              
            );

    }else {
      if (Platform.OS === 'android') {
        Utils.setInputModeForWebView();
      }
          return (
                <View style = {style_register.container}>
                  <Title title = {this.props.title == '' ? this.state._urlTitle : this.props.title} hasGoBack = {this.props.hasGoBack} backAddress = {this.props.backAddress} navigator = {this.props.navigator}/>
                  <ProgressBar
                    fillStyle={this.state.hideProgressBarFlag ? styles_web.pbfillStyleHide:styles_web.pbfillStyleShow}
                    backgroundStyle={styles_web.pbBackgroundStyle}
                    style={this.state.hideProgressBarFlag ? {height: 0,} : {marginTop: 0, width: deviceWidth}}
                    progress={this.state.progress}/>
                  <WebView  
                    ref={WEBVIEW_REF}
                    source={{uri: this.state._url}}
                    automaticallyAdjustContentInsets = {true} 
                    javaScriptEnabledAndroid = {true}
                    scalesPageToFit = {true}
                    renderError = {(domain, code, description) => this._renderWebViewError(domain, code, description)}
                    onShouldStartLoadWithRequest  = {(nativeEvent) => this._onShouldStartLoadWithRequest(nativeEvent)}
                    onProgressChange  = {(nativeEvent) => this._onProgressChange(nativeEvent)}
                    onHandleTitle  = {(nativeEvent) => this._onHandleTitle(nativeEvent)}
                    style = {{width:deviceWidth}}>
                  </WebView>

                </View>
            );

    }

	},
	
  //初始化render之后只执行一次
  componentDidMount: function() {
    rctDeviceEventEmitter = RCTDeviceEventEmitter.addListener(
      'net_error',
      (netError) => {
        this.setState({
          isError: true,
          _url: 'about:blink',
          errorMessage:Language.network_connect_failed+'',
          title: '',
        });
      }
    );
     //开启第三方浏览器监听
    skipBrowserEventEmitter = RCTDeviceEventEmitter.addListener(
      'skip_browser',
      (url) => {
        if (url != null){
            this._skipBrowser(url);
        }
      }
    );
  },


  //状态的改变判断(state状态发生变化，执行shouldComponentUpdate)//当props发生变化时执行，初始化render时不执行
  componentWillReceiveProps: function() {
      DEFAULT_URL = this.props.url;
      this.fetchData();
   },


  //组件是否更新判断(返回true执行componentWillUpdate->render->componentDidUpdate)
  shouldComponentUpdate: function(nextProps, nextState) {
      if (nextProps == this.props && nextState == this.state) {
        return false;
      } else {
        return true;
      }
  },


  //生命周期执行完毕，当组件要被从界面上移除的时候调用
  componentWillUnmount: function() {
      if (Platform.OS === 'android') {
        Utils.SetInputModeForTabBar();
      }
      rctDeviceEventEmitter.remove();
      skipBrowserEventEmitter.remove();
      // this.progressFresh && clearTimeout(this.progressFresh);
      // this.progressEnd && clearTimeout(this.progressEnd);
  },

  //获取网络数据
  fetchData:function (){
        NetInfo.isConnected.fetch(DEFAULT_URL)
        .done((isConnected) => { 
          if (isConnected) {
            tools.alertDebugShow('重新刷新' + DEFAULT_URL); 
              this.setState({
                isError: false,
                _url: DEFAULT_URL,
              });
          }else{
            this.setState({
              isError: true,
              _url: 'about:blink',
              errorMessage: Language.network_connect_failed+'',
              title: '',
            });
            tools.alertShow(Language.connectnetwork);
          }
        });
    },

  //渲染webview出错
  _renderWebViewError: function(domain, code, description) {
    try {
      RCTDeviceEventEmitter.emit('net_error', code);
    } catch(error) {
      //TODO deal with error.
    }
    // console.log('渲染webview出错');
    tools.alertDebugShow('请连接网络2' + domain + code + description);
  },



  //跳转与加载页面时调用
  _onShouldStartLoadWithRequest: function (nativeEvent) {
    // console.log('调用成功！！！！！！' + this.state._url);
    if (Platform.OS === 'android') {
      nativeEvent = nativeEvent.nativeEvent;
    }

    this._initProgressBar();
    
    if (Platform.OS === 'android') {
      if (this._isSkipNewWeb(nativeEvent.canGoBack, nativeEvent.url)) {
              this._skipNewWeb(nativeEvent);
              // console.log('调用成功新webview==========！！！！！！' + nativeEvent.url);
              return false;
          } else if (nativeEvent.url!=='about:blink'&&this._isSkipBrowser(nativeEvent.canGoBack, nativeEvent.url)) {
              // console.log('调用第三方浏览器！！！！！！' + nativeEvent.url);
              RCTDeviceEventEmitter.emit('skip_browser', nativeEvent.url);
              return false;
          }else{
              this._skipAndroidSelf(nativeEvent.url);
          }
    }else {
      // console.log('调用成功ios内部==========！！！！！！' + nativeEvent.url);
      if (this._isSkipNewWeb(nativeEvent.canGoBack, nativeEvent.url)) {
          this._skipNewWeb(nativeEvent);
          // console.log('调用成功新webview==========！！！！！！' + nativeEvent.url);
          return false;
      } else if (nativeEvent.url!=='about:blink'&&this._isSkipBrowser(nativeEvent.canGoBack, nativeEvent.url)) {
          // console.log('调用第三方浏览器！！！！！！' + nativeEvent.url);
          RCTDeviceEventEmitter.emit('skip_browser', nativeEvent.url);
          return false;
      }else{
          return true;
      }
    }
       
  },


  //刷新当前页面
  _refresh:function(){    
    localType =1;
    if (Platform.OS === 'android') {
      Utils.setInputModeForWebView();
    }
      DEFAULT_URL = this.props.url;
      // console.log('--------网页地址:' + DEFAULT_URL);
      this.fetchData();
      this._initProgressBar();
  },


  //判断字符串中是否包含vida(包含启动新的webview页面)
  _isSkip: function(canGoBack, url) {
    if(/*canGoBack && */url.substring(0, 4) == SDIP_CONDITION || url.substring(0, 4) ==  SDIP_BROWSER) {
      return true;
    } else {
      return false; 
    }
  },

  //判断是否开启新页面
  _isSkipNewWeb: function(canGoBack, url) {
    if(/*canGoBack && */url.substring(0, 4) == SDIP_CONDITION ) {
      return true;
    } else {
      return false; 
    }
  },

  //判断是否跳转到第三方浏览器
  _isSkipBrowser: function(canGoBack, url) {
    if(/*canGoBack && */url.substring(0, 4) ==  SDIP_BROWSER) {
      return true;
    } else {
      return false; 
    }
    // if(url.substring(0, 4)!='http'||url.substring(0, 5)!='https') {
    //   console.log('非正常链接====:' + url);
    //   return true;
    // } else {
    //   console.log('正常链接====:' + url);
    //   return false; 
    // }
  },

  //开启新的webview页面
  _skipNewWeb: function(nativeEvent) {
      RCTDeviceEventEmitter.emit('web_page_skip',
        {
          title:nativeEvent.title,
          url: nativeEvent.url.replace(SDIP_CONDITION, 'http', '$1'),
          hasGoBack: true,
          webviewStyle:styles_web.webview_protocol,
        }
      );   
  },

  //针对Android系统，开启新的webview页面
  _skipAndroidSelf: function (url) {
    // console.log('------------------ _skipAndroidSelf' + url+ '--c');
     if (url === null || url === '') return;
     if (url != null) {
          this.refs[WEBVIEW_REF].load(url);
     }
    // this.setState({ progress: 0.1});
  },

  //开启第三方浏览器
  _skipBrowser: function (url) {
      // console.log('------------------ _skipBrowser' + url + '--b');
      if (url === null || url === '') return;

    if(url.substring(0, 4) == SDIP_CONDITION || url.substring(0, 4) ==  SDIP_BROWSER) {
      url=url.replace(SDIP_BROWSER, 'http', '$1');
    }
  
    if (Platform.OS === 'android') {
        if (url != null){
            Linking.openURL(url);
        }
    } else {
      // LinkingIOS.openURL(url);
      if (url != null){

      Linking.canOpenURL(url).then(supported => {
        if (!supported) {
          // console.log('Can\'t handle url: ' + url);
        } else {
          return Linking.openURL(url);
        }
      }).catch(err => console.error('An error occurred', err));
      }

    }
  },

  _onProgressChange: function (nativeEvent) {
    if (Platform.OS === 'ios') {
      this.setState({
        _urlTitle:nativeEvent.title,
      });
    };
    // console.log('------------------WebViewPage teyou' + nativeEvent.progress);
/*    if(nativeEvent.progress / 100 >= 1) {
      this.progressEnd && clearTimeout(this.progressEnd);
      this.progressEnd = setTimeout((function() {
        this._hideProgressBar();
      }).bind(this), 2000);
    } else if (this.state.progress <= nativeEvent.progress / 100) {
      this.setState({ progress: nativeEvent.progress / 100});
    } */
  },

  _onHandleTitle: function (nativeEvent) {
    if (Platform.OS === 'android') {
      this.setState({
        _urlTitle:nativeEvent.title,
      });
    };
    // console.log('-----------ANDROID_LAB WebViewPage onHandleTitle' + nativeEvent.title); 
  },

  //隐藏进度条
  _hideProgressBar: function () {
    // this.progressFresh && clearTimeout(this.progressFresh);
    // this.progressEnd && clearTimeout(this.progressEnd);
    this.setState({ 
      progress: 1.0,
      hideProgressBarFlag: true,
    });
  },

  //初始化进度条
  _initProgressBar: function() {
    // console.log('---------------------------------WebViewPage _initProgressBar');
    // this.progressFresh && clearTimeout(this.progressFresh);
    // this.progressEnd && clearTimeout(this.progressEnd);
    this.setState({
      progress: 0,
      hideProgressBarFlag: true,
    });
  },
});

module.exports = WebViewPage;