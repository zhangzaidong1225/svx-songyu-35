'use strict'

import  styles from './styles/style_setting';
var share = require('./share');
import  config from './config'
//var AsyncStorage = require('./AsyncStorage');
var BlueToothUtil=require('./BlueToothUtil');
var TimerMixin = require ('react-timer-mixin');
import Language from './Language'
var UMeng = require('./UMeng');

import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    ScrollView,
    Dimensions,
    Navigator,
    TouchableHighlight,
    TouchableOpacity,
    PixelRatio,
    ToastAndroid,
    NativeModules,
    NetInfo,
    Alert,
    AsyncStorage,
    ListView,
    Platform,
    DeviceEventEmitter,
    NativeAppEventEmitter,
} from 'react-native';

var mwidth=Dimensions.get('window').width;
var mheight=Dimensions.get('window').height; 
// console.log(mheight+'屏幕高度');
// console.log(mwidth+'屏幕宽度');
var sub_currentDevice,sub_currentDevice2;
var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter');
var mwind_time;
var mwind_time1 = 0;
var mwind_time11;

var TimeLine = React.createClass({

  getInitialState:function(){
    return {
      device:'',
    }
  },

  render:function(){
        //首次使用
        var key_left;
        if (this.props.device.left =='0min'){
            key_left ='初次'
        }else {
          key_left = this.props.device.left;
        }

        return(
          <View style = {{width:mwidth,height:100,}}>
            <ScrollView style={{flex:1,width:mwidth,height:100}}>
              <View  style = {{width:mwidth,height:100,backgroundColor:'white',opacity:0}}>

              </View>

              <View style = {{backgroundColor: 'transparent', position:'absolute',left:mwidth * 0.04,top:45,justifyContent:'center',alignItems:'center',}}>
                <View style = {{width:mwidth * 0.2,}}>
                  <Text style = {{color:'white',fontSize:14,marginRight:15,textAlign:'right'}}>{key_left}</Text>
                </View>
              </View>

              <View style = {{position:'absolute',left:mwidth * 0.24,top:0}}>
                <Image 
                  style = {{backgroundColor:'white',width:1,height:100,resizeMode:'stretch'}}
                  source = {require('image!ic_shu_lien')}/>
              </View>

              <View style = {{position:'absolute',left:mwidth * 0.228,top:50}}>
                  <Image 
                    style = {{width:10,height:10,resizeMode:'stretch'}}
                    source = {require('image!ic_small_point')}/>
              </View>

              <View 
                style ={{position:'absolute',left:mwidth* 0.31,top:30,
                        width:mwidth * 0.68,height:mheight * 0.079,
                        borderRadius:5,borderWidth:1,borderColor:'#ADB5CE',
                        backgroundColor:'rgba(255,255,255,0.3)',opacity:0.4,justifyContent:'center',}}>
                <View style = {{}}>
                  <Text style = {{color:'rgba(255,255,255,1)',fontSize:15,marginLeft:13.5,textAlign:'left'}}>{this.props.device.right}</Text>
                </View>
              </View>
                  
            </ScrollView>
          </View>
        );
  }

});



var sort_array = [];//给android传输数据的
var tmp_array = [];//存储从map中提取的数据
var use_time_min = 0;
var sub_ble_state = null;
// var sub_ble_state2=null;
var connectd_list = [];

var use_time_array =[];
var clean_air_array = [];
var filter_num_array = [];
var map = [];
var alertMessage = '请连接设备 ';


var DEFAULT_URL = config.server_base+config.share_uri;
var DEFAULT_SHARE_IOCN_URL = config.server_base+config.share_icon_uri;
var showAlert = 0;//弹出框提示
var canvasShow = 0;




var sharePage = React.createClass({

    mixins:[TimerMixin],
      timer_delay: null,

	getInitialState:function(){
       this.arrayAdd();
       showAlert = 0;
       canvasShow = 0;

		return {
      dataSource: new ListView.DataSource({
          rowHasChanged: (row1, row2) => row1 !== row2,
      }),

			use_time:0,//滤棉使用总时长
			wind_time:0,//本次风扇使用市场
			use_time_this:0,//滤棉本次使用时长
      mwind_time1:0,
      device_name:'',
      filter_number:0,
      device_addr:'',
		}
	},


	componentWillMount:function(){

    this.setState({device_name:this.props.deviceName});

	},

  arrayAdd:function(){
//三个数组，表示使用时长，净化空气量、滤棉使用量
use_time_array = new Array('0','60','180','360','720','1440','2880','4320','10080');//min
clean_air_array = new Array('1','5','20','50','350','500','3000','6000','20000');//m3
filter_num_array = new Array('1','2','5','8','10','12','24','48');//滤棉个数

map = new Array();
map[0] = {};
map[0].left = '0min';//初次
map[0].right ='开始忻风健康旅程' ;

map[1] = {};
map[1].left = '60min';
map[1].right = '相当于云南某森林氧吧呼吸60分钟';

map[2] = {};
map[2].left = '180min';
map[2].right ='相当于云南某森林氧吧漫步3小时';

map[3] = {};
map[3].left = '360min';
map[3].right = '相当于云南某森林氧吧自驾游半天';

map[4] = {};
map[4].left = '720min';
map[4].right = '相当于云南某森林氧吧露营1夜';


map[5] = {};
map[5].left = '1440min';
map[5].right = '相当于云南某森林氧吧放松1天';

map[6] = {};
map[6].left = '2880min';
map[6].right = '相当于云南某森林氧吧游玩2天';

map[7] = {};
map[7].left = '4320min';
map[7].right = '相当于云南某森林氧吧畅游3天';

map[8] = {};
map[8].left = '10080min';
map[8].right = '相当于云南某森林氧吧旅游7天';


map[9] = {};
map[9].left = '1m³';
map[9].right = '相当于少吸34支二手烟';


map[10] = {};
map[10].left = '5m³';
map[10].right = '相当于少吸1.8L汽车尾气10分钟';


map[11] = {};
map[11].left = '20m³';
map[11].right = '相当于少抽了680支二手烟';

map[12] = {};
map[12].left = '50m³';
map[12].right = '相当于少吸4.0L汽车尾气50分钟';

map[13] = {};
map[13].left = '350m³';
map[13].right = '相当于少吸10L卡车尾气60分钟';

map[14] = {};
map[14].left = '500m³';
map[14].right = '相当于少抽17000支二手烟';

map[15] = {};
map[15].left = '3000m³';
map[15].right = '相当于少吸4.0L汽车尾气50小时';

map[16] = {};
map[16].left = '6000m³';
map[16].right = '相当于少抽20万支二手烟';

map[17] = {};
map[17].left = '2000m³';
map[17].right = '相当于少吸10L卡车尾气60小时';

map[18] = {};
map[18].left = '1个';
map[18].right = '您已经使用了1个滤芯';

map[19] = {};
map[19].left = '2个';
map[19].right = '您已经使用了2个滤芯';

map[20] = {};
map[20].left = '5个';
map[20].right = '您已经使用了5个滤芯';

map[21] = {};
map[21].left = '8个';
map[21].right = '您已经使用了8个滤芯';

map[22] = {};
map[22].left = '10个';
map[22].right = '您已经使用了10个滤芯';

map[23] = {};
map[23].left = '12个';
map[23].right = '您已经使用了12个滤芯';

map[24] = {};
map[24].left = '24个';
map[24].right = '您已经使用了24个滤芯';

map[25] = {};
map[25].left = '48个';
map[25].right = '您已经使用超过48个滤芯';

  },

	componentDidMount:function(){

    // sub_ble_state2 = DeviceEventEmitter.addListener(
    //      'ble_state',
    //      (data) => {
    //         this.init_data(data);
    //      }
    //  );
     
    sub_ble_state = NativeAppEventEmitter.addListener(
         'ble_state',
         (data) => {
            this.init_data(data)
         }
     );  
    BlueToothUtil.sendMydevice();


	},

  init_data:function(data){
          connectd_list = [];
          var tmp_select_item = null;
          for (var i = 0; i < data.dev_list.length; i++) {
            
            if(data.dev_list[i].isconnectd == 1){
              connectd_list.push(data.dev_list[i]);
            }
          console.log('开始接收数据-----'+data.dev_list[i].use_time +'--'+data.dev_list[i].use_time_this+'--'+data.dev_list[i].filter_number);

          AsyncStorage.getItem('select_item_addr',(error,result) =>{
            console.log('select_item_addr----',result);
            tmp_select_item = null;


            for (var i = 0; i < connectd_list.length; i++) {
          
              if(result == connectd_list[i].addr){
                tmp_select_item = connectd_list[i];
              }
            }

            if(tmp_select_item == null){
              if(connectd_list.length > 0){
                tmp_select_item = connectd_list[0];
              }
            }
            if(tmp_select_item == null){

              //操作顺序
              use_time_min  = 0;
              
              this.setState({
                  device_name:this.props.deviceName,
                  use_time:0,
                  use_time_this:0,
                  filter_number:0,
                  mwind_time1:0,
                  
                });

              //弹出对话框
              if (showAlert == 0){
                showAlert = 1;

                Alert.alert(
                  Language.prompt+'',
                  alertMessage,
                  [
                    {text: Language.ok+'', onPress: () => {this.props.navigator.pop();}},
                  ]
                )
              }

            }else{
              console.log('---this--init-' + tmp_select_item.filter_use_time_this + '--' + tmp_select_item.filters_maxfan_usetime + '--' + tmp_select_item.filter_number);

                mwind_time=parseInt(tmp_select_item.filters_maxfan_usetime)*1000;
                    //计算滤棉的过滤量
                if(mwind_time>=1000000){
                 
                      mwind_time11=Math.floor(mwind_time/10000);

                      if(mwind_time11>=1000){
                         mwind_time1=Math.round(Math.floor(mwind_time11/10)/10);
                      }else{
                        mwind_time1=Math.round(mwind_time11/100);      
                    }
                } else {
                  mwind_time1 = 0;
                }
                //解决开始画两次图
                this.setState({
                  device_name:tmp_select_item.name,
                  use_time:tmp_select_item.filter_use_time_this,
                  mwind_time1:mwind_time1,
                  filter_number:tmp_select_item.filter_number,
                });


                console.log('---this--set-'+this.state.mwind_time1+'--'+this.state.use_time+'---'+this.state.filter_number);

                this.initData(this.state.use_time,this.state.mwind_time1,this.state.filter_number);   

            }   
          });
          }
  },

  shouldComponentUpdate:function(nextProps,nextState){


    //到底返回的true还是false，需要再考虑

    if ((nextState.mwind_time1 != this.state.mwind_time1) 
      || (nextState.use_time != this.state.use_time) 
      || (nextState.filter_number != this.state.filter_number)) {
      //console.log('shouldComponentUpdate---this--'+this.state.mwind_time1+'---'+this.state.use_time+'---'+this.state.filter_number);
      //console.log('shouldComponentUpdate---'+nextState.mwind_time1+'---'+nextState.use_time+'---'+nextState.filter_number);
      console.log('shouldComponentUpdate---进入状态');
      //sort_array.reverse()
      // for (var x in sort_array){
      //   console.log('shouldComponentUpdate--tmp_array---'+sort_array[x]);
      // }

      if (canvasShow == 0){
        console.log('shouldComponentUpdate---0--');

       this.timer_delay = this.setTimeout( () => {
        console.log('shouldComponentUpdate---0--0--');
          share.CanvasBitmap(nextState.device_name,nextState.mwind_time1,Math.floor(nextState.use_time /60),nextState.filter_number,sort_array); 
        },1000);  

        canvasShow = 1;
        console.log('shouldComponentUpdate---0--0--0--');


      }else {
         console.log('shouldComponentUpdate---1--');
          share.CanvasBitmap(nextState.device_name,nextState.mwind_time1,Math.floor(nextState.use_time /60),nextState.filter_number,sort_array); 

      }

      return false;
    }
    return true;

  },

  // componentWillUpdate:function (nextProps,nextState){
  //   // console.log('componentWillUpdate---'+nextState.use_time_this);
  //   // console.log('componentWillUpdate---'+nextState.use_time);
  //   // console.log('componentWillUpdate---'+ nextState.filter_number);

  //   // return true;
  // },





	componentWillUnmount:function(){
     // sub_currentDevice.remove();
      sub_ble_state.remove();
    	// sub_ble_state2.remove();

    this.timer_delay = this.setTimeout( () => {
      //清空数组释放内存
      use_time_array.length = 0;
      clean_air_array.length =0;
      filter_num_array.length = 0;
      map.length = 0;
      sort_array.length = 0;
      tmp_array.length = 0;

      },2000);

      this.timer_delay && clearTimeout(this.timer_delay);
	},

  initData:function(use_time,mwind_time1,filter_number){
    //必须清空 
      tmp_array = [];
      sort_array = [];


      use_time_min = Math.floor(use_time /60);

      //min
      for (var x in use_time_array){

          if (use_time_min >= use_time_array[x]){
            var min = use_time_array[x]+'min';
            sort_array.push(min);

            for (var i = 0;i< map.length;i++){
              if (min == map[i].left){
                  tmp_array.push(map[i]);
              }
            }
          }
      }
      
      //m3
      for (var x in clean_air_array){
      	
        if (clean_air_array[x] <= mwind_time1){
          //sort_array.push(clean_air_array[x]);
          var m = clean_air_array[x]+'m³';
    
          sort_array.splice(x+1,0,m);

          for (var i = 0;i< map.length;i++){
            if (m == map[i].left){
              tmp_array.splice(x+1,0,map[i]);
            }
          }
        }

      }

      for (var x in filter_num_array){

        if ((filter_number-1) >= filter_num_array[x]){
          var num = filter_num_array[x] + '个';
          sort_array.push(num);

          for (var i = 0;i< map.length;i++){
            if (num == map[i].left){
               //console.log('sharePage ---i---'+i);
              //tmp_array.push(map[i]);
              tmp_array.push(map[i]);
      
            }
          }
        }
      }

      // //console.log('sharePage--tmp_array-clean_air_array-'+tmp_array.length); 
      // //已经能够添加到数组里啦
      // for (var x in tmp_array){

      //   //console.log('sharePage--tmp_array---'+tmp_array[x].left+'------'+tmp_array[x].right);
      // }  
    sort_array.reverse();
      for (var x in sort_array){
        console.log('sharePage--tmp_array---'+sort_array[x]);
      }
    //sort_array.reverse();
    //tmp_array.reverse();
    this.setState({dataSource:this.state.dataSource.cloneWithRows(tmp_array.reverse())});

    //console.log(this.state.dataSource.getRowCount()+'------');
  },


	render:function(){
  		var navigator = this.props.navigator;
  		var string='m³';  
  		var mfilter = parseInt(this.state.use_time);
      var list_height;
      if (this.state.dataSource.getRowCount() == 0){
        list_height = mheight * 11/20;
      }else {
        list_height = this.state.dataSource.getRowCount() * 100+20;
      }

		return (
			<View>
			    <View style={{backgroundColor:'#5DA4C0',height:mheight/10,flexDirection: 'row',alignItems: 'center',}}>
		            <TouchableOpacity style = {styles.touch_view}
		              onPress={() => {this.props.navigator.pop();UMeng.onEvent('home_sharepageback');}}>
		              <Image style={{height:17.5,width:16.5,margin: 30,resizeMode: 'stretch'}} source={require('image!ic_back_home')}/>
		            </TouchableOpacity>
		            <Text allowFontScaling={false} style={{fontSize:17,color:'#FFFFFF',flex:1,textAlign: 'center',}}>统计分享</Text>
                <TouchableOpacity style={{height:mheight/10,alignItems: 'center',justifyContent: 'center',}}
                  onPress = {this.gotoShare}>
                      <Image style ={{height:17.5,width:17.5,margin: 30,resizeMode:'stretch'}} source = {require('image!img_selecte_share_gray')}/>
                </TouchableOpacity>
		        </View>

            <View style = {{backgroundColor:'#5CA5C1',flexDirection:'row',width:mwidth,height:mheight * 2/10,}}>

              <View style = {{flex:1.8,alignItems:'center',justifyContent:'center',}}>
                <Image 
                  style = {{width:90,height:90,resizeMode:'stretch'}}s
                  source = {require('image!ic_circle')}/>
              </View>
              <View style ={{flex:2.2,justifyContent:'center'}}>
                <Text style={{fontSize:20.5,color:'#FFFFFF',textAlign:'left'}}>{this.state.device_name}</Text>
              </View>

            </View>

            <View style = {{backgroundColor:'#587CA6',flexDirection:'row',width:mwidth,height:mheight * 3/20,}}>
                <View style = {{flex:0.9,flexDirection:'column',backgroundColor:'#FF000000',}}>
                  
                  <View style = {{flex:2,alignItems:'center',justifyContent:'center',backgroundColor:'#FF000000',}}>
                    <View style = {{flex:1,flexDirection:'row',justifyContent:'center'}}>
                      <View style = {{flex:2.3,justifyContent:'flex-end',backgroundColor:'#FF000000'}}>
                        <Text  style = {{color:'#FFFFFF',fontSize:45,textAlignVertical:'bottom',textAlign:'right'}}>{Math.round(this.state.mwind_time1)}</Text>
                      </View>
                      <View style = {{flex:0.7,justifyContent:'flex-end',backgroundColor:'#FF000000',marginBottom:10}}>
                        <Text  style = {{color:'#FFFFFF',fontSize:15,textAlignVertical:'bottom',textAlign:'left'}}>m³</Text>
                      </View>
                    </View>
                  </View>

                  <View style = {{flex:1,alignItems:'center',justifyContent:'center'}}>
                      <Text  style = {{color:'#FFFFFF',fontSize:17,textAlign:'center',}}>净化空气量</Text>
                  </View>

                </View>

                <Image 
                  style ={{backgroundColor:'white',width:1,height:mheight * 3/20 -20,resizeMode:'stretch',marginTop:10,marginBottom:10,}} 
                  source = {require('image!ic_shu_lien')}/>


                <View style = {{flex:1.4,flexDirection:'column',backgroundColor:'#FF000000',}}>

                  <View style = {{flex:2,alignItems:'center',justifyContent:'center',backgroundColor:'#FF000000',}}>
                    <View style = {{flex:1,flexDirection:'row',justifyContent:'center'}}>
                      <View style = {{flex:2.3,justifyContent:'flex-end',backgroundColor:'#FF000000'}}>
                        <Text  style = {{color:'#FFFFFF',fontSize:45,textAlignVertical:'bottom',textAlign:'right'}}>{use_time_min}</Text>
                      </View>
                      <View style = {{flex:0.7,justifyContent:'flex-end',backgroundColor:'#FF000000',marginBottom:10}}>
                        <Text  style = {{color:'#FFFFFF',fontSize:15,textAlignVertical:'bottom',textAlign:'left'}}>min</Text>
                      </View>
                    </View>
                  </View>

                  <View style = {{flex:1,alignItems:'center',justifyContent:'center'}}>
                      <Text  style = {{color:'#FFFFFF',fontSize:17,textAlign:'center',}}>{Language.usage}</Text>
                  </View>
                </View>
                  <Image 
                    style ={{backgroundColor:'white',width:1,height:mheight * 3/20 - 20,resizeMode:'stretch',marginTop:10,marginBottom:10,}} 
                    source = {require('image!ic_shu_lien')}/>


                <View style = {{flex:0.9,flexDirection:'column',backgroundColor:'#FF000000',}}>
                   <View style = {{flex:2,alignItems:'center',justifyContent:'center',backgroundColor:'#FF000000',}}>
                    <View style = {{flex:2,flexDirection:'row',justifyContent:'center'}}>
                      <View style = {{flex:1.8,justifyContent:'flex-end',backgroundColor:'#FF000000'}}>
                        <Text  style = {{color:'#FFFFFF',fontSize:45,textAlignVertical:'bottom',textAlign:'right'}}>{this.state.filter_number}</Text>
                      </View>
                      <View style = {{flex:1.2,justifyContent:'flex-end',backgroundColor:'#FF000000',marginBottom:10}}>
                        <Text  style = {{color:'#FFFFFF',fontSize:15,textAlignVertical:'bottom',textAlign:'left'}}>pcs</Text>
                      </View>
                    </View>
                  </View>

                  <View style = {{flex:1,alignItems:'center',justifyContent:'center'}}>
                      <Text  style = {{color:'#FFFFFF',fontSize:17,textAlign:'center',}}>滤芯使用量</Text>
                  </View>
                </View>

            </View>
           <ScrollView style={{width:mwidth,height:mheight * 11/20,backgroundColor: '#7980AC',}}>
              <View>
                <Image 
                  style={[{width:mwidth,height: list_height,resizeMode: 'stretch'}]} 
                  source={require('image!ic_share_page')}/>
              </View>
              <View style = {{width:mwidth,height:list_height,position:'absolute',top:0,}}>
              <ListView
                dataSource={this.state.dataSource}
                renderRow={this.renderDevice}
                style={{width:mwidth,}}/>
              </View>
            </ScrollView>
			</View>

		);
	},

  gotoShare:function(){
    // share.pullMenu('MOPS•VIDA 忻风', DEFAULT_URL, '雾霾来袭，您还在“等风来”？\n忻风，守护您的健康，伴您绿色出行',  DEFAULT_SHARE_IOCN_URL)
    //sort_array.reverse();
    UMeng.onEvent('home_sharepageimg');
      console.log('shouldComponentUpdate---222gotoShare--');
    share.pullMenu(this.state.device_name, DEFAULT_URL, Language.fearless,  DEFAULT_SHARE_IOCN_URL, 'count', Math.round(this.state.mwind_time1) == null ? 0 : Math.round(this.state.mwind_time1), use_time_min == null ? 0 : use_time_min, this.state.filter_number == null ? 0: this.state.filter_number, sort_array);

  },

  renderDevice:function (device){
    return <TimeLine device={device} />
  }
});



module.exports = sharePage;


